#include "b2sum.stripped.h"



undefined8 _DT_INIT(void)

{
  undefined8 in_RAX;
  
  FUN_00402eb0();
  FUN_00410470();
  return in_RAX;
}



void FUN_004018d0(void)

{
                    // WARNING: Treating indirect jump as call
  (*(code *)(undefined *)0x0)();
  return;
}



void thunk_FUN_00615028(void)

{
  FUN_00615028();
  return;
}



void FUN_00401bc0(void)

{
  do {
                    // WARNING: Do nothing block with infinite loop
  } while( true );
}



// WARNING: Removing unreachable block (ram,0x00402dbb)
// WARNING: Removing unreachable block (ram,0x00402dc7)

ulong FUN_00401dd0(ulong uParm1,undefined8 *puParm2)

{
  long *plVar1;
  char cVar2;
  byte bVar3;
  byte bVar4;
  uint uVar5;
  undefined8 *puVar6;
  char cVar7;
  int iVar8;
  uint uVar9;
  int iVar10;
  undefined8 uVar11;
  undefined8 uVar12;
  long lVar13;
  char *pcVar14;
  undefined8 uVar15;
  uint *puVar16;
  long lVar17;
  char *pcVar18;
  undefined8 *puVar19;
  ulong uVar20;
  ulong uVar21;
  undefined *puVar22;
  char *pcVar23;
  char *unaff_R13;
  char *pcVar24;
  bool bVar25;
  byte bVar26;
  undefined auStack360 [36];
  undefined4 local_144;
  undefined local_fe;
  undefined local_fd;
  
  bVar26 = 0;
  puVar19 = (undefined8 *)auStack360;
  FUN_004052a0(*puParm2);
  func_0x00401c40(6,0x412147);
  func_0x00401c20(FUN_00405140);
  func_0x004019a0(DAT_006144c0,0,1,0);
  local_fd = 0;
  local_144 = 0xffffffff;
  local_fe = 0;
  uVar15 = 0x412147;
LAB_00401e34:
  while( true ) {
    iVar10 = (int)(uParm1 & 0xffffffff);
    puVar19[-1] = 0x401e4b;
    uVar11 = FUN_004084f0(uParm1 & 0xffffffff,puParm2,"l:bctw",null_ARRAY_00411100,0);
    uVar12 = DAT_006144c0;
    puVar22 = PTR_DAT_00614450;
    iVar8 = (int)uVar11;
    if (iVar8 == -1) break;
    if (iVar8 == 0x74) {
      *(undefined4 *)((long)puVar19 + 0x24) = 0;
    }
    else {
      if (iVar8 < 0x75) {
        if (iVar8 == 0x62) {
          *(undefined4 *)((long)puVar19 + 0x24) = 1;
        }
        else {
          if (iVar8 < 99) {
            if (iVar8 != -0x83) {
              if (iVar8 != -0x82) goto LAB_00402059;
              puVar19[-1] = 0x401ec8;
              iVar8 = FUN_004031f0();
              goto LAB_00401ed0;
            }
            puVar19[-1] = uVar11;
            puVar19[-2] = 0;
            puVar19[-3] = 0x401fbc;
            FUN_00406930(uVar12,"b2sum","GNU coreutils",puVar22);
            puVar19[-3] = 0x401fc3;
            func_0x00401cc0();
            puVar19 = puVar19 + -2;
LAB_00401fc3:
            DAT_00614560 = 0;
            DAT_0061455f = 1;
            DAT_0061455d = 0;
          }
          else {
LAB_00401ed0:
            uVar12 = DAT_00614758;
            if (iVar8 == 99) {
              *(undefined *)((long)puVar19 + 0x6a) = 1;
            }
            else {
              if (iVar8 != 0x6c) goto LAB_00402059;
              puVar19[-1] = 0x401f02;
              DAT_00614550 = FUN_00406b30(uVar12,0,0xffffffffffffffff,0x412147);
              uVar15 = DAT_00614758;
              if ((DAT_00614550 & 7) != 0) {
                puVar19[-1] = 0x401f20;
                uVar12 = FUN_00406630(uVar15);
                puVar19[-1] = 0x401f33;
                FUN_00407510(0,0,"invalid length: %s",uVar12);
                puVar19[-1] = 0x401f46;
                iVar8 = FUN_00407510(1,0,"length is not a multiple of 8");
                goto LAB_00401f50;
              }
            }
          }
        }
      }
      else {
        if (iVar8 == 0x81) {
          DAT_00614560 = 1;
          DAT_0061455f = 0;
          DAT_0061455d = 0;
        }
        else {
          if (iVar8 < 0x82) {
LAB_00401f50:
            if (iVar8 == 0x77) goto LAB_00401fc3;
            if (iVar8 != 0x80) goto LAB_00402059;
            DAT_0061455e = 1;
          }
          else {
            if (iVar8 == 0x83) goto LAB_00402063;
            if (iVar8 < 0x83) {
              DAT_00614560 = 0;
              DAT_0061455f = 0;
              DAT_0061455d = 1;
            }
            else {
              if (iVar8 != 0x84) goto LAB_00402059;
              *(undefined *)((long)puVar19 + 0x6b) = 1;
              *(undefined4 *)((long)puVar19 + 0x24) = 1;
            }
          }
        }
      }
    }
  }
  DAT_00614570 = 3;
  if (0x200 < DAT_00614550) {
    puVar19[-1] = 0x402d10;
    uVar15 = FUN_00406630(uVar15);
    puVar19[-1] = 0x402d23;
    FUN_00407510(0,0,"invalid length: %s",uVar15);
    uVar15 = *(undefined8 *)(null_ARRAY_004112b0 + (ulong)DAT_00614558 * 8);
    puVar19[-1] = 0x402d36;
    uVar15 = FUN_00406630(uVar15);
    puVar19[-1] = 0x402d52;
    FUN_00407510(1,0,"maximum digest length for %s is %lu bits",uVar15,0x200);
    uVar20 = (ulong)puVar19 & 0xfffffffffffffff0;
    *(undefined8 *)(uVar20 - 8) = 0x402d68;
    FUN_00402d70(puVar19,_DYNAMIC);
    uVar15 = *puVar19;
    *(undefined8 *)(uVar20 - 0x10) = 0x402d93;
    func_0x00401d10(FUN_00401dd0,uVar15,puVar19 + 1,_DT_INIT,_DT_FINI,0);
    *(undefined8 *)(uVar20 - 0x10) = 0;
    return 7;
  }
  if ((*(char *)((long)puVar19 + 0x6a) == '\0') && (DAT_00614550 == 0)) {
    DAT_00614550 = 0x200;
  }
  DAT_00614568 = (char *)(DAT_00614550 >> 2);
  if ((*(int *)((long)puVar19 + 0x24) == 0) && (*(char *)((long)puVar19 + 0x6b) != '\0')) {
    pcVar24 = "--tag does not support --text mode";
  }
  else {
    if (((*(char *)((long)puVar19 + 0x6a) == '\0') ||
        (pcVar24 = "the --tag option is meaningless when verifying checksums",
        *(char *)((long)puVar19 + 0x6b) == '\0')) &&
       ((((pcVar24 = "the --binary and --text options are meaningless when verifying checksums",
          (*(byte *)((long)puVar19 + 0x6a) &
          ~(byte)((uint)*(undefined4 *)((long)puVar19 + 0x24) >> 0x18) >> 7) == 0 &&
          (pcVar24 = "the --ignore-missing option is meaningful only when verifying checksums",
          DAT_0061455e <= *(byte *)((long)puVar19 + 0x6a))) &&
         (pcVar24 = "the --status option is meaningful only when verifying checksums",
         DAT_00614560 <= *(byte *)((long)puVar19 + 0x6a))) &&
        (((pcVar24 = "the --warn option is meaningful only when verifying checksums",
          DAT_0061455f <= *(byte *)((long)puVar19 + 0x6a) &&
          (pcVar24 = "the --quiet option is meaningful only when verifying checksums",
          DAT_0061455d <= *(byte *)((long)puVar19 + 0x6a))) &&
         (pcVar24 = "the --strict option is meaningful only when verifying checksums",
         DAT_0061455c <= *(byte *)((long)puVar19 + 0x6a))))))) goto code_r0x0040211a;
  }
  puVar19[-1] = 0x402059;
  FUN_00407510(0,0,pcVar24);
LAB_00402059:
  puVar19[-1] = 0x402063;
  FUN_004031f0();
LAB_00402063:
  DAT_0061455c = 1;
  goto LAB_00401e34;
code_r0x0040211a:
  iVar8 = 0;
  if (*(int *)((long)puVar19 + 0x24) != -1) {
    iVar8 = *(int *)((long)puVar19 + 0x24);
  }
  *(int *)((long)puVar19 + 0x24) = iVar8;
  *(undefined8 **)(puVar19 + 0xb) = puParm2 + (long)iVar10;
  lVar13 = (long)DAT_006144bc;
  if (DAT_006144bc == iVar10) {
    puVar6 = (undefined8 *)puVar19[0xb];
    *puVar6 = 0x41212a;
    *(undefined8 **)(puVar19 + 0xb) = puVar6 + 1;
  }
  *(undefined *)((long)puVar19 + 0x6f) = 1;
  *(undefined8 **)(puVar19 + 9) = puParm2 + lVar13;
  do {
    uVar15 = DAT_006144c8;
    uVar9 = (uint)puParm2;
    if ((ulong)puVar19[0xb] < (ulong)puVar19[9] || puVar19[0xb] == puVar19[9]) {
      if (DAT_00614578 == '\0') {
LAB_004029e3:
        return (ulong)(*(byte *)((long)puVar19 + 0x6f) ^ 1);
      }
      puVar19[-1] = 0x402a61;
      iVar8 = FUN_004075f0(uVar15);
      if (iVar8 != -1) goto LAB_004029e3;
      puVar19[-1] = 0x402a6f;
      puVar16 = (uint *)func_0x00401cb0();
      uVar5 = *puVar16;
      puVar19[-1] = 0x402a82;
      FUN_00407510(1,(ulong)uVar5,"standard input");
LAB_00402a82:
      puVar19[-1] = 0x402a93;
      FUN_004065a0(0,3,puVar19[6]);
      puVar19[-1] = 0x402aa6;
      FUN_00407510(0,0,"%s: no file was verified");
LAB_004028f8:
      if ((*(char *)((long)puVar19 + 0x6e) != '\0') && ((puVar19[0xc] | puVar19[10]) == 0)) {
        uVar9 = (uint)(DAT_0061455c <= (puVar19[8] == 0));
      }
LAB_0040290f:
      puParm2 = (undefined8 *)(ulong)(uVar9 & 1);
LAB_00402912:
      *(byte *)((long)puVar19 + 0x6f) = *(byte *)((long)puVar19 + 0x6f) & (byte)puParm2;
    }
    else {
      uVar15 = *(undefined8 *)puVar19[9];
      puVar19[6] = uVar15;
      if (*(char *)((long)puVar19 + 0x6a) != '\0') {
        puVar19[-1] = 0x40218d;
        iVar8 = func_0x00401bf0(uVar15,&DAT_0041212a);
        *(bool *)((long)puVar19 + 0x6c) = iVar8 == 0;
        uVar15 = DAT_006144c8;
        if (iVar8 == 0) {
          DAT_00614578 = '\x01';
          puVar19[6] = 0x410564;
          puVar19[1] = uVar15;
LAB_004021b6:
          puVar19[0xf] = 0;
          puVar19[0x10] = 0;
          *(undefined *)((long)puVar19 + 0x6e) = 0;
          *(undefined *)((long)puVar19 + 0x6d) = 0;
          puVar19[0xc] = 0;
          puVar19[10] = 0;
          puVar19[8] = 0;
          puVar19[7] = 0;
          puVar19[3] = 1;
LAB_00402208:
          puVar19[-1] = 0x40221f;
          lVar13 = FUN_00407870(puVar19 + 0xf,puVar19 + 0x10,puVar19[1]);
          if (0 < lVar13) {
            pcVar24 = (char *)puVar19[0xf];
            cVar7 = *pcVar24;
            if (cVar7 == '#') goto LAB_0040249b;
            cVar2 = pcVar24[lVar13 + -1];
            puVar19[2] = lVar13;
            if (cVar2 == '\n') {
              pcVar24[lVar13 + -1] = '\0';
              pcVar24 = (char *)puVar19[0xf];
              puVar19[2] = lVar13 + -1;
              cVar7 = *pcVar24;
            }
            lVar13 = 0;
            if (cVar7 != '\t') goto LAB_00402266;
            do {
              do {
                lVar13 = lVar13 + 1;
                cVar7 = pcVar24[lVar13];
              } while (cVar7 == '\t');
LAB_00402266:
            } while (cVar7 == ' ');
            bVar25 = cVar7 == '\\';
            *(undefined *)((long)puVar19 + 0x69) = 0;
            if (bVar25) {
              lVar13 = lVar13 + 1;
              bVar25 = lVar13 == 0;
              *(undefined *)((long)puVar19 + 0x69) = 1;
            }
            pcVar18 = pcVar24 + lVar13;
            lVar17 = 6;
            pcVar23 = pcVar18;
            pcVar14 = "BLAKE2";
            do {
              if (lVar17 == 0) break;
              lVar17 = lVar17 + -1;
              bVar25 = *pcVar23 == *pcVar14;
              pcVar23 = pcVar23 + (ulong)bVar26 * -2 + 1;
              pcVar14 = pcVar14 + (ulong)bVar26 * -2 + 1;
            } while (bVar25);
            if (!bVar25) {
              if ((ulong)(puVar19[2] - lVar13) < (ulong)(*pcVar18 == '\\') + DAT_00614570)
              goto LAB_00402530;
              pcVar23 = (char *)0x0;
              do {
                unaff_R13 = pcVar23;
                bVar4 = pcVar18[(long)unaff_R13];
                puVar19[-1] = 0x402353;
                iVar8 = func_0x00401a60((ulong)bVar4);
                pcVar23 = unaff_R13 + 1;
              } while (iVar8 != 0);
              DAT_00614568 = unaff_R13;
              if ((char *)0x7e < unaff_R13 + -2) goto LAB_00402530;
              DAT_00614568 = unaff_R13;
              if (((ulong)unaff_R13 & 1) != 0) goto LAB_00402530;
              DAT_00614550 = (long)unaff_R13 * 4;
              pcVar23 = unaff_R13 + lVar13;
              cVar7 = pcVar24[(long)pcVar23];
              if ((cVar7 != '\t') && (DAT_00614568 = unaff_R13, cVar7 != ' ')) goto LAB_00402530;
              DAT_00614568 = unaff_R13;
              pcVar24[(long)pcVar23] = '\0';
              puVar19[-1] = 0x4023a9;
              cVar7 = FUN_00402f80(pcVar18);
              if (cVar7 == '\0') goto LAB_00402530;
              pcVar14 = pcVar23 + 1;
              if ((puVar19[2] - (long)pcVar14 == 1) ||
                 ((pcVar24[(long)(pcVar23 + 1)] != '*' && (pcVar24[(long)(pcVar23 + 1)] != ' ')))) {
                if (DAT_00614440 == 0) goto LAB_00402530;
                DAT_00614440 = 1;
              }
              else {
                if (DAT_00614440 != 1) {
                  pcVar14 = pcVar23 + 2;
                  DAT_00614440 = 0;
                }
              }
              pcVar23 = pcVar24 + (long)pcVar14;
              if (*(char *)((long)puVar19 + 0x69) == '\0') goto LAB_00402403;
              puVar19[-1] = 0x402cba;
              lVar13 = FUN_00402f00(pcVar23,puVar19[2] - (long)pcVar14);
              bVar25 = lVar13 != 0;
              goto LAB_0040265e;
            }
            unaff_R13 = (char *)(lVar13 + 6);
            pcVar23 = pcVar24 + (long)unaff_R13;
            cVar7 = *pcVar23;
            while (cVar7 != '\0') {
              if ((cVar7 == '\t') || (cVar7 == ' ')) {
                *pcVar23 = '\0';
                *(char **)(puVar19 + 5) = pcVar23;
                puVar19[-1] = 0x40256a;
                lVar13 = FUN_00405010(pcVar18,null_ARRAY_004112a0,0);
                if (lVar13 < 0) goto LAB_00402530;
                DAT_00614558 = (uint)lVar13;
                puVar22 = (undefined *)puVar19[5];
                if (cVar7 != '(') goto LAB_00402588;
LAB_004026f8:
                *puVar22 = 0x28;
                goto LAB_00402592;
              }
              if (cVar7 == '-') break;
              if (cVar7 == '(') {
                *pcVar23 = '\0';
                *(char **)(puVar19 + 5) = pcVar23;
                puVar19[-1] = 0x4026e4;
                lVar13 = FUN_00405010(pcVar18,null_ARRAY_004112a0,0);
                puVar22 = (undefined *)puVar19[5];
                if (lVar13 < 0) goto LAB_00402530;
                DAT_00614558 = (uint)lVar13;
                goto LAB_004026f8;
              }
              unaff_R13 = unaff_R13 + 1;
              cVar7 = pcVar24[(long)unaff_R13];
              pcVar23 = pcVar24 + (long)unaff_R13;
            }
            *pcVar23 = '\0';
            puVar19[-1] = 0x4022f3;
            lVar13 = FUN_00405010(pcVar18,null_ARRAY_004112a0,0);
            if (lVar13 < 0) goto LAB_00402530;
            DAT_00614558 = (uint)lVar13;
LAB_00402588:
            unaff_R13 = unaff_R13 + 1;
            if (cVar7 == '-') goto LAB_004024f3;
LAB_00402592:
            DAT_00614550 = 0x200;
            DAT_00614568 = (char *)0x80;
            do {
              unaff_R13 = unaff_R13 + (ulong)(pcVar24[(long)unaff_R13] == ' ');
              if (pcVar24[(long)unaff_R13] == '(') {
                lVar13 = puVar19[2] - (long)(unaff_R13 + 1);
                if (lVar13 != 0) {
                  pcVar23 = pcVar24 + (long)(unaff_R13 + 1);
                  lVar17 = lVar13 + -1;
                  if (lVar17 == 0) {
LAB_004025fe:
                    if (*pcVar23 != ')') goto LAB_00402530;
                    lVar17 = 0;
                    unaff_R13 = pcVar23;
                  }
                  else {
                    cVar7 = pcVar23[lVar13 + -1];
                    while (cVar7 != ')') {
                      lVar17 = lVar17 + -1;
                      if (lVar17 == 0) goto LAB_004025fe;
                      cVar7 = pcVar23[lVar17];
                    }
                    unaff_R13 = pcVar23 + lVar17;
                  }
                  if (*(char *)((long)puVar19 + 0x69) != '\0') {
                    puVar19[-1] = 0x402cfa;
                    lVar13 = FUN_00402f00(pcVar23,lVar17);
                    if (lVar13 == 0) goto LAB_00402530;
                  }
                  *unaff_R13 = '\0';
                  do {
                    do {
                      lVar13 = lVar17;
                      lVar17 = lVar13 + 1;
                      cVar7 = pcVar23[lVar17];
                    } while (cVar7 == '\t');
                  } while (cVar7 == ' ');
                  if (cVar7 == '=') {
                    pcVar14 = pcVar23 + lVar13 + 2;
                    do {
                      do {
                        pcVar18 = pcVar14;
                        pcVar14 = pcVar18 + 1;
                      } while (*pcVar18 == '\t');
                      pcVar14 = pcVar18 + 1;
                    } while (*pcVar18 == ' ');
                    puVar19[-1] = 0x40265e;
                    bVar25 = (bool)FUN_00402f80(pcVar18);
LAB_0040265e:
                    if (bVar25 != false) {
LAB_00402403:
                      if (*(char *)((long)puVar19 + 0x6c) != '\0') {
                        puVar19[-1] = 0x402417;
                        iVar8 = func_0x00401bf0(pcVar23,&DAT_0041212a);
                        if (iVar8 == 0) goto LAB_00402530;
                      }
                      uVar21 = 0;
                      if (DAT_00614560 == 0) {
                        puVar19[-1] = 0x40243b;
                        lVar13 = func_0x00401d40(pcVar23,10);
                        uVar21 = (ulong)(lVar13 != 0);
                      }
                      puVar19[-1] = 0x40245a;
                      cVar7 = FUN_00403070(pcVar23,puVar19 + 0x1c,puVar19 + 0x11);
                      iVar8 = (int)uVar21;
                      if (cVar7 == '\0') {
                        puVar19[10] = puVar19[10] + 1;
                        if (DAT_00614560 == 0) {
                          if (iVar8 != 0) {
                            puVar19[-1] = 0x402bee;
                            func_0x00401c50(0x5c);
                          }
                          puVar19[-1] = 0x402485;
                          FUN_00402fd0(pcVar23,uVar21);
                          puVar19[-1] = 0x402496;
                          func_0x004018f0(0x411917,"FAILED open or read");
                        }
                        goto LAB_00402496;
                      }
                      if ((DAT_0061455e != 0) && (*(char *)(puVar19 + 0x11) != '\0'))
                      goto LAB_00402496;
                      unaff_R13 = (char *)((ulong)DAT_00614568 >> 1);
                      if (unaff_R13 != (char *)0x0) {
                        pcVar24 = (char *)0x0;
                        goto LAB_00402b06;
                      }
                      bVar25 = DAT_00614560 != 0;
                      *(undefined *)((long)puVar19 + 0x6e) = 1;
                      if (bVar25) goto LAB_00402496;
                      pcVar24 = (char *)0x0;
                      goto LAB_00402b86;
                    }
                  }
                }
              }
LAB_00402530:
              do {
                puVar19[7] = puVar19[7] + 1;
                if (DAT_0061455f != 0) {
                  puVar19[-1] = 0x4026a7;
                  uVar15 = FUN_004065a0(0,3,puVar19[6]);
                  puVar19[-1] = 0x4026c5;
                  FUN_00407510(0,0,"%s: %lu: improperly formatted %s checksum line",uVar15,
                               puVar19[3],"BLAKE2");
                }
                puVar19[8] = puVar19[8] + 1;
LAB_0040249b:
                uVar15 = puVar19[1];
                puVar19[-1] = 0x4024a8;
                iVar8 = func_0x00401c70(uVar15);
                if (iVar8 != 0) goto LAB_0040281f;
                puVar19[-1] = 0x4024b8;
                iVar8 = func_0x00401ce0(uVar15);
                if (iVar8 != 0) goto LAB_0040281f;
                plVar1 = puVar19 + 3;
                *plVar1 = *plVar1 + 1;
                if (*plVar1 != 0) goto LAB_00402208;
                puVar19[-1] = 0x4024dd;
                uVar15 = FUN_004065a0(0,3,puVar19[6]);
                puVar19[-1] = 0x4024f3;
                FUN_00407510(1,0,"%s: too many checksum lines",uVar15);
LAB_004024f3:
                puVar19[-1] = 0x40250b;
                iVar8 = FUN_00406b60(pcVar24 + (long)unaff_R13,0,0,puVar19 + 0x11,0);
              } while (((iVar8 != 0) || (uVar21 = puVar19[0x11], 0x1ff < uVar21 - 1)) ||
                      ((uVar21 & 7) != 0));
              while ((int)pcVar24[(long)unaff_R13] - 0x30U < 10) {
                unaff_R13 = unaff_R13 + 1;
              }
              DAT_00614568 = (char *)(uVar21 >> 2);
              DAT_00614550 = uVar21;
            } while( true );
          }
LAB_0040281f:
          puVar19[-1] = 0x402829;
          func_0x00401db0(puVar19[0xf]);
          puVar19[-1] = 0x402833;
          uVar9 = func_0x00401ce0(puVar19[1]);
          if (uVar9 == 0) {
            if (*(char *)((long)puVar19 + 0x6c) == '\0') {
              puVar19[-1] = 0x402bfd;
              iVar8 = FUN_004075f0(puVar19[1]);
              if (iVar8 != 0) goto LAB_00402942;
            }
            if (*(char *)((long)puVar19 + 0x6d) != '\0') {
              if (DAT_00614560 == 0) {
                if (puVar19[7] != 0) {
                  pcVar24 = "WARNING: %lu lines are improperly formatted";
                  if (puVar19[7] == 1) {
                    pcVar24 = "WARNING: %lu line is improperly formatted";
                  }
                  puVar19[-1] = 0x40288d;
                  FUN_00407510(0,0,pcVar24);
                }
                if (puVar19[10] != 0) {
                  pcVar24 = "WARNING: %lu listed files could not be read";
                  if (puVar19[10] == 1) {
                    pcVar24 = "WARNING: %lu listed file could not be read";
                  }
                  puVar19[-1] = 0x4028ba;
                  FUN_00407510(0,0,pcVar24);
                }
                if (puVar19[0xc] != 0) {
                  pcVar24 = "WARNING: %lu computed checksums did NOT match";
                  if (puVar19[0xc] == 1) {
                    pcVar24 = "WARNING: %lu computed checksum did NOT match";
                  }
                  puVar19[-1] = 0x4028e7;
                  FUN_00407510(0,0,pcVar24);
                }
                if (*(byte *)((long)puVar19 + 0x6e) < DAT_0061455e) goto LAB_00402a82;
              }
              goto LAB_004028f8;
            }
            puVar19[-1] = 0x402c1b;
            uVar15 = FUN_004065a0(0,3,puVar19[6]);
            puVar19[-1] = 0x402c34;
            FUN_00407510(0,0,"%s: no properly formatted %s checksum lines found",uVar15,"BLAKE2");
            goto LAB_0040290f;
          }
          puParm2 = (undefined8 *)0x0;
          puVar19[-1] = 0x402c4c;
          FUN_004065a0(0,3,puVar19[6]);
          puVar19[-1] = 0x402c5f;
          FUN_00407510(0,0,"%s: read error");
        }
        else {
          puVar19[-1] = 0x402934;
          lVar13 = FUN_00405200(puVar19[6],0x41061c);
          puVar19[1] = lVar13;
          if (lVar13 != 0) goto LAB_004021b6;
LAB_00402942:
          puVar19[-1] = 0x402953;
          FUN_004065a0(0,3,puVar19[6]);
          puVar19[-1] = 0x40295b;
          puVar16 = (uint *)func_0x00401cb0();
          uVar9 = *puVar16;
          puParm2 = (undefined8 *)0x0;
          puVar19[-1] = 0x402970;
          FUN_00407510(0,(ulong)uVar9,0x410592);
        }
        goto LAB_00402912;
      }
      puVar19[-1] = 0x40271b;
      cVar7 = FUN_00403070(puVar19[6],puVar19 + 0x12,puVar19 + 0x1c);
      if (cVar7 == '\0') {
        *(undefined *)((long)puVar19 + 0x6f) = 0;
      }
      else {
        puVar19[-1] = 0x402732;
        lVar13 = func_0x00401d40(puVar19[6],0x5c);
        if (lVar13 == 0) {
          puVar19[-1] = 0x402cd4;
          lVar13 = func_0x00401d40(puVar19[6],10);
          if (lVar13 != 0) goto LAB_0040273b;
          puParm2 = (undefined8 *)0x0;
          if (*(char *)((long)puVar19 + 0x6b) == '\0') goto LAB_00402981;
LAB_00402755:
          uVar12 = DAT_006144c0;
          uVar15 = *(undefined8 *)(null_ARRAY_004112a0 + (ulong)DAT_00614558 * 8);
          puVar19[-1] = 0x40276f;
          func_0x00401ad0(uVar15,uVar12);
          if (DAT_00614550 < 0x200) {
            puVar19[-1] = 0x402a50;
            func_0x004018f0(&DAT_00410637);
          }
          uVar15 = DAT_006144c0;
          puVar19[-1] = 0x40279e;
          func_0x00401c90(&DAT_0041063c,1,2,uVar15);
          puVar19[-1] = 0x4027aa;
          FUN_00402fd0(puVar19[6],puParm2);
          puVar19[-1] = 0x4027c5;
          func_0x00401c90(&DAT_0041063f,1,4);
          if ((ulong)DAT_00614568 >> 1 != 0) {
LAB_004027d1:
            uVar21 = 0;
            do {
              bVar4 = *(byte *)((long)puVar19 + uVar21 + 0x90);
              uVar21 = uVar21 + 1;
              puVar19[-1] = 0x4027f0;
              func_0x004018f0(&DAT_00410644,(ulong)bVar4);
            } while (uVar21 < (ulong)DAT_00614568 >> 1);
          }
          if (*(char *)((long)puVar19 + 0x6b) == '\0') goto LAB_00402991;
        }
        else {
LAB_0040273b:
          if (*(char *)((long)puVar19 + 0x6b) != '\0') {
            puParm2 = (undefined8 *)0x1;
            puVar19[-1] = 0x402755;
            func_0x00401c50(0x5c);
            goto LAB_00402755;
          }
          puParm2 = (undefined8 *)0x1;
          puVar19[-1] = 0x402981;
          func_0x00401c50(0x5c);
LAB_00402981:
          if ((ulong)DAT_00614568 >> 1 != 0) goto LAB_004027d1;
LAB_00402991:
          puVar19[-1] = 0x40299b;
          func_0x00401c50(0x20);
          puVar19[-1] = 0x4029ad;
          func_0x00401c50((ulong)((-(uint)(*(int *)((long)puVar19 + 0x24) == 0) & 0xfffffff6) + 0x2a
                                 ));
          puVar19[-1] = 0x4029b9;
          FUN_00402fd0(puVar19[6],puParm2);
        }
        puVar19[-1] = 0x402814;
        func_0x00401c50(10);
      }
    }
    puVar19[9] = puVar19[9] + 8;
  } while( true );
  while( true ) {
    bVar3 = pcVar18[(long)pcVar24 * 2 + 1];
    puVar19[-1] = 0x402aee;
    iVar10 = func_0x004019d0((ulong)bVar3);
    if (iVar10 != (int)"0123456789abcdef"[(ulong)((uint)bVar4 & 0xf)]) goto LAB_00402b2c;
    pcVar24 = pcVar24 + 1;
    if (pcVar24 == unaff_R13) break;
LAB_00402b06:
    bVar4 = pcVar18[(long)pcVar24 * 2];
    puVar19[-1] = 0x402b10;
    iVar10 = func_0x004019d0((ulong)bVar4);
    bVar4 = *(byte *)((long)((long)puVar19 + 0xe0) + (long)pcVar24);
    if (iVar10 != (int)"0123456789abcdef"[(ulong)(bVar4 >> 4)]) {
LAB_00402b2c:
      if (unaff_R13 != pcVar24) {
        puVar19[0xc] = puVar19[0xc] + 1;
        if (DAT_00614560 != 0) goto LAB_00402496;
        if (iVar8 != 0) goto LAB_00402c64;
        puVar19[-1] = 0x402b5b;
        FUN_00402fd0(pcVar23,0);
        goto LAB_00402b5b;
      }
      break;
    }
  }
  bVar25 = DAT_00614560 == 0;
  *(undefined *)((long)puVar19 + 0x6e) = 1;
  pcVar24 = unaff_R13;
  if (bVar25) {
LAB_00402b86:
    bVar25 = DAT_0061455d == 0;
    *(undefined *)((long)puVar19 + 0x6e) = 1;
    if (bVar25) {
      if (iVar8 == 0) {
        puVar19[-1] = 0x402bab;
        FUN_00402fd0(pcVar23,0);
      }
      else {
LAB_00402c64:
        puVar19[-1] = 0x402c6e;
        func_0x00401c50(0x5c);
        puVar19[-1] = 0x402c79;
        FUN_00402fd0(pcVar23,uVar21);
        if (unaff_R13 != pcVar24) {
LAB_00402b5b:
          puVar19[-1] = 0x402b6c;
          func_0x004018f0(0x411917,"FAILED");
          goto LAB_00402496;
        }
      }
      if (DAT_0061455d == 0) {
        puVar19[-1] = 0x402bc9;
        func_0x004018f0(0x411917,&DAT_0041060c);
      }
    }
  }
LAB_00402496:
  *(undefined *)((long)puVar19 + 0x6d) = 1;
  goto LAB_0040249b;
}



// WARNING: Removing unreachable block (ram,0x00402dbb)
// WARNING: Removing unreachable block (ram,0x00402dc7)

void entry(void)

{
  undefined8 uVar1;
  
  uVar1 = 0x402d68;
  FUN_00402d70();
  func_0x00401d10(FUN_00401dd0,*(undefined8 *)register0x00000020,
                  (undefined8 *)register0x00000020 + 1,_DT_INIT,_DT_FINI,0,uVar1);
  return;
}



// WARNING: Removing unreachable block (ram,0x00402dbb)
// WARNING: Removing unreachable block (ram,0x00402dc7)

void FUN_00402d70(undefined8 *puParm1)

{
  func_0x00401d10(FUN_00401dd0,*puParm1,puParm1 + 1,_DT_INIT,_DT_FINI,0);
  return;
}



// WARNING: Removing unreachable block (ram,0x00402dbb)
// WARNING: Removing unreachable block (ram,0x00402dc7)

void FUN_00402da0(void)

{
  return;
}



// WARNING: Removing unreachable block (ram,0x00402e68)

void FUN_00402e30(void)

{
  if (DAT_00614500 == '\0') {
    FUN_00402da0();
    func_0x00401ae0(&fde_00412c10);
    DAT_00614500 = '\x01';
  }
  return;
}



// WARNING: Removing unreachable block (ram,0x00402e08)
// WARNING: Removing unreachable block (ram,0x00402e14)

void FUN_00402eb0(void)

{
  func_0x00401d70(&fde_00412c10,null_ARRAY_00614520);
  if (DAT_00614020 != 0) {
    FUN_00615240();
  }
  return;
}



long FUN_00402f00(long lParm1,ulong uParm2)

{
  char cVar1;
  undefined *puVar2;
  undefined *puVar3;
  ulong uVar4;
  
  uVar4 = 0;
  puVar2 = (undefined *)(lParm1 + 1);
  if (uParm2 != 0) {
    do {
      puVar3 = puVar2;
      cVar1 = *(char *)(lParm1 + uVar4);
      if (cVar1 == '\0') {
        return 0;
      }
      if (cVar1 == '\\') {
        if (uParm2 - 1 == uVar4) {
          return 0;
        }
        uVar4 = uVar4 + 1;
        if (*(char *)(lParm1 + uVar4) == '\\') {
          puVar3[-1] = 0x5c;
        }
        else {
          if (*(char *)(lParm1 + uVar4) != 'n') {
            return 0;
          }
          puVar3[-1] = 10;
        }
      }
      else {
        puVar3[-1] = cVar1;
      }
      uVar4 = uVar4 + 1;
      puVar2 = puVar3 + 1;
    } while (uVar4 < uParm2);
    if (puVar3 < (undefined *)(uParm2 + lParm1)) {
      *puVar3 = 0;
    }
  }
  return lParm1;
}



ulong FUN_00402f80(byte *pbParm1)

{
  ulong uVar1;
  int iVar2;
  ulong in_RAX;
  byte *pbVar3;
  
  uVar1 = DAT_00614568;
  pbVar3 = pbParm1;
  if (DAT_00614568 != 0) {
    do {
      iVar2 = func_0x00401a60((ulong)*pbVar3);
      if (iVar2 == 0) {
        return 0;
      }
      pbVar3 = pbVar3 + 1;
      in_RAX = (ulong)(uint)((int)pbVar3 - (int)pbParm1);
    } while (in_RAX < uVar1);
  }
  return in_RAX & 0xffffffffffffff00 | (ulong)(*pbVar3 == 0);
}



void FUN_00402fd0(char *pcParm1,char cParm2)

{
  char cVar1;
  undefined8 uVar2;
  
  uVar2 = DAT_006144c0;
  if (cParm2 == '\0') {
    FUN_006150f8(pcParm1,DAT_006144c0);
    return;
  }
  cVar1 = *pcParm1;
  do {
    while( true ) {
      if (cVar1 == '\0') {
        return;
      }
      if (cVar1 != '\n') break;
      func_0x00401c90(&DAT_004104c0,1,2,DAT_006144c0);
LAB_00403007:
      pcParm1 = pcParm1 + 1;
      cVar1 = *pcParm1;
    }
    if (cVar1 == '\\') {
      func_0x00401c90(&DAT_004104c3,1,2,uVar2);
      goto LAB_00403007;
    }
    pcParm1 = pcParm1 + 1;
    func_0x00401c50((ulong)(uint)(int)cVar1);
    cVar1 = *pcParm1;
  } while( true );
}



ulong FUN_00403070(byte *pbParm1,undefined8 uParm2,undefined *puParm3)

{
  int iVar1;
  undefined8 uVar2;
  uint *puVar3;
  int *piVar4;
  long lVar5;
  ulong uVar6;
  byte *pbVar7;
  byte *pbVar8;
  bool bVar9;
  bool bVar10;
  
  lVar5 = 2;
  bVar9 = &stack0xffffffffffffffe8 < &DAT_00000010;
  bVar10 = (undefined *)register0x00000020 == (undefined *)0x28;
  pbVar7 = pbParm1;
  pbVar8 = &DAT_0041212a;
  do {
    if (lVar5 == 0) break;
    lVar5 = lVar5 + -1;
    bVar9 = *pbVar7 < *pbVar8;
    bVar10 = *pbVar7 == *pbVar8;
    pbVar7 = pbVar7 + 1;
    pbVar8 = pbVar8 + 1;
  } while (bVar10);
  *puParm3 = 0;
  lVar5 = DAT_006144c8;
  if ((!bVar9 && !bVar10) == bVar9) {
    DAT_00614578 = 1;
    FUN_004051d0(DAT_006144c8,2);
    iVar1 = FUN_00404f20(lVar5,uParm2,DAT_00614550 >> 3);
  }
  else {
    lVar5 = FUN_00405200(pbParm1,0x41061c);
    if (lVar5 == 0) {
      uVar6 = (ulong)DAT_0061455e;
      if ((DAT_0061455e != 0) && (piVar4 = (int *)func_0x00401cb0(), *piVar4 == 2)) {
        *puParm3 = 1;
        return uVar6;
      }
      goto LAB_00403131;
    }
    FUN_004051d0(lVar5,2);
    iVar1 = FUN_00404f20(lVar5,uParm2,DAT_00614550 >> 3);
    if (iVar1 != 0) {
      uVar2 = FUN_004065a0(0,3,pbParm1);
      puVar3 = (uint *)func_0x00401cb0();
      FUN_00407510(0,(ulong)*puVar3,0x410592,uVar2);
      if (DAT_006144c8 == lVar5) {
        return 0;
      }
      FUN_004075f0(lVar5);
      return 0;
    }
    iVar1 = FUN_004075f0(lVar5);
  }
  if (iVar1 == 0) {
    return 1;
  }
LAB_00403131:
  uVar2 = FUN_004065a0(0,3,pbParm1);
  puVar3 = (uint *)func_0x00401cb0();
  FUN_00407510(0,(ulong)*puVar3,0x410592,uVar2);
  return 0;
}



void FUN_004031f0(uint uParm1)

{
  int iVar1;
  long lVar2;
  char **ppcVar3;
  undefined8 uVar4;
  char *pcVar5;
  char *pcVar6;
  bool bVar7;
  byte bVar8;
  char *local_88;
  char *local_80;
  undefined8 local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  char *local_48;
  char *local_40;
  char *local_38;
  char *local_30;
  undefined8 local_28;
  undefined8 local_20;
  
  bVar8 = 0;
  if (uParm1 == 0) goto LAB_0040321d;
  func_0x00401ac0(DAT_006144e0,"Try \'%s --help\' for more information.\n",DAT_00614590);
  do {
    func_0x00401cc0((ulong)uParm1);
LAB_0040321d:
    func_0x004018f0("Usage: %s [OPTION]... [FILE]...\nPrint or check %s (%d-bit) checksums.\n",
                    DAT_00614590,"BLAKE2",0x200);
    uVar4 = DAT_006144c0;
    func_0x00401c90("\nWith no FILE, or when FILE is -, read standard input.\n",1,0x37,DAT_006144c0)
    ;
    func_0x00401c90("\n  -b, --binary         read in binary mode\n",1,0x2c,uVar4);
    func_0x004018f0("  -c, --check          read %s sums from the FILEs and check them\n","BLAKE2");
    func_0x00401c90(
                    "  -l, --length         digest length in bits; must not exceed the maximum for\n                       the blake2 algorithm and must be a multiple of 8\n"
                    ,1,0x96,uVar4);
    func_0x00401c90("      --tag            create a BSD-style checksum\n",1,0x33,uVar4);
    func_0x00401c90("  -t, --text           read in text mode (default)\n",1,0x33,uVar4);
    func_0x00401c90(
                    "\nThe following five options are useful only when verifying checksums:\n      --ignore-missing  don\'t fail or report status for missing files\n      --quiet          don\'t print OK for each successfully verified file\n      --status         don\'t output anything, status code shows success\n      --strict         exit non-zero for improperly formatted checksum lines\n  -w, --warn           warn about improperly formatted checksum lines\n\n"
                    ,1,0x1b2,uVar4);
    func_0x00401c90("      --help     display this help and exit\n",1,0x2c,uVar4);
    func_0x00401c90("      --version  output version information and exit\n",1,0x35,uVar4);
    bVar7 = true;
    func_0x004018f0(
                    "\nThe sums are computed as described in %s.  When checking, the input\nshould be a former output of this program.  The default mode is to print a\nline with checksum, a space, a character indicating input mode (\'*\' for binary,\n\' \' for text or where binary is insignificant), and name for each FILE.\n"
                    ,"RFC 7693");
    local_88 = "[";
    local_80 = "test invocation";
    pcVar5 = "[";
    local_78 = 0x41053f;
    local_70 = "Multi-call invocation";
    local_68 = "sha224sum";
    local_60 = "sha2 utilities";
    local_58 = "sha256sum";
    local_50 = "sha2 utilities";
    local_48 = "sha384sum";
    local_40 = "sha2 utilities";
    local_38 = "sha512sum";
    local_30 = "sha2 utilities";
    local_28 = 0;
    local_20 = 0;
    ppcVar3 = &local_88;
    do {
      lVar2 = 6;
      pcVar6 = "b2sum";
      do {
        if (lVar2 == 0) break;
        lVar2 = lVar2 + -1;
        bVar7 = *pcVar6 == *pcVar5;
        pcVar6 = pcVar6 + (ulong)bVar8 * -2 + 1;
        pcVar5 = pcVar5 + (ulong)bVar8 * -2 + 1;
      } while (bVar7);
      if (bVar7) break;
      ppcVar3 = ppcVar3 + 2;
      pcVar5 = *ppcVar3;
      bVar7 = pcVar5 == (char *)0x0;
    } while (!bVar7);
    pcVar5 = ppcVar3[1];
    if (pcVar5 == (char *)0x0) {
      func_0x004018f0("\n%s online help: <%s>\n","GNU coreutils",
                      "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c40(5,0);
      if (lVar2 != 0) {
        iVar1 = func_0x00401b40(lVar2,&DAT_00410560,3);
        if (iVar1 != 0) {
          pcVar5 = "b2sum";
          goto LAB_00403475;
        }
      }
      func_0x004018f0("Full documentation at: <%s%s>\n","https://www.gnu.org/software/coreutils/",
                      "b2sum");
LAB_0040349e:
      pcVar5 = "b2sum";
      uVar4 = 0x4104f8;
    }
    else {
      func_0x004018f0("\n%s online help: <%s>\n","GNU coreutils",
                      "https://www.gnu.org/software/coreutils/");
      lVar2 = func_0x00401c40(5,0);
      if (lVar2 != 0) {
        iVar1 = func_0x00401b40(lVar2,&DAT_00410560,3);
        if (iVar1 != 0) {
LAB_00403475:
          func_0x004018f0("Report %s translation bugs to <https://translationproject.org/team/>\n",
                          "b2sum");
        }
      }
      func_0x004018f0("Full documentation at: <%s%s>\n","https://www.gnu.org/software/coreutils/",
                      "b2sum");
      uVar4 = 0x412147;
      if (pcVar5 == "b2sum") goto LAB_0040349e;
    }
    func_0x004018f0("or available locally via: info \'(coreutils) %s%s\'\n",pcVar5,uVar4);
  } while( true );
}



void FUN_004034b0(long lParm1,byte *pbParm2)

{
  ulong *puVar1;
  ulong uVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  long lVar6;
  ulong uVar7;
  ulong uVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  ulong uVar12;
  ulong uVar13;
  ulong uVar14;
  ulong uVar15;
  ulong uVar16;
  ulong uVar17;
  ulong uVar18;
  ulong uVar19;
  ulong uVar20;
  ulong local_138;
  long local_130;
  long local_128;
  long local_120;
  long local_118;
  long local_110;
  long local_108;
  long local_100;
  long local_f8;
  long local_f0;
  long local_e8;
  long local_e0;
  long local_d8;
  long local_d0;
  long local_c8;
  long local_c0;
  ulong local_b8;
  long local_b0;
  long local_a8;
  long local_a0;
  ulong local_98;
  ulong local_90;
  ulong local_88;
  ulong local_80;
  ulong local_78 [9];
  
  lVar6 = 0;
  do {
    (&local_138)[lVar6] =
         (ulong)pbParm2[2] << 0x10 | (ulong)pbParm2[1] << 8 | (ulong)*pbParm2 |
         (ulong)pbParm2[3] << 0x18 | (ulong)pbParm2[4] << 0x20 | (ulong)pbParm2[5] << 0x28 |
         (ulong)pbParm2[6] << 0x30 | (ulong)pbParm2[7] << 0x38;
    lVar6 = lVar6 + 1;
    pbParm2 = pbParm2 + 8;
  } while (lVar6 != 0x10);
  lVar6 = 0;
  do {
    (&local_b8)[lVar6] = *(ulong *)(lParm1 + lVar6 * 8);
    lVar6 = lVar6 + 1;
  } while (lVar6 != 8);
  uVar4 = local_90 + local_b0 + local_128;
  uVar5 = local_98 + local_b8 + local_138;
  uVar12 = local_88 + local_a8 + local_118;
  uVar2 = *(ulong *)(lParm1 + 0x48) ^ 0x9b05688c2b3e6c1f ^ uVar4;
  uVar3 = uVar2 << 0x20 | uVar2 >> 0x20;
  uVar9 = *(ulong *)(lParm1 + 0x40) ^ 0x510e527fade682d1 ^ uVar5;
  uVar2 = uVar3 + 0xbb67ae8584caa73b;
  uVar10 = uVar9 << 0x20 | uVar9 >> 0x20;
  uVar9 = *(ulong *)(lParm1 + 0x50) ^ 0x1f83d9abfb41bd6b ^ uVar12;
  uVar7 = local_90 ^ uVar2;
  uVar20 = uVar10 + 0x6a09e667f3bcc908;
  uVar8 = uVar7 >> 0x18 | uVar7 << 0x28;
  uVar14 = local_98 ^ uVar20;
  uVar7 = uVar4 + local_120 + uVar8;
  uVar15 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar3 = uVar3 ^ uVar7;
  uVar5 = uVar5 + local_130 + uVar15;
  uVar4 = uVar3 >> 0x10 | uVar3 << 0x30;
  uVar10 = uVar10 ^ uVar5;
  uVar2 = uVar4 + uVar2;
  uVar11 = uVar10 >> 0x10 | uVar10 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar8 = uVar8 ^ uVar2;
  uVar15 = uVar15 ^ uVar20;
  uVar14 = uVar8 << 1 | (ulong)((long)uVar8 < 0);
  uVar16 = uVar15 << 1 | (ulong)((long)uVar15 < 0);
  uVar9 = uVar9 << 0x20 | uVar9 >> 0x20;
  uVar15 = local_80 + local_a0 + local_108;
  uVar13 = uVar9 + 0x3c6ef372fe94f82b;
  uVar3 = local_88 ^ uVar13;
  uVar10 = uVar3 >> 0x18 | uVar3 << 0x28;
  uVar3 = *(ulong *)(lParm1 + 0x58) ^ 0x5be0cd19137e2179 ^ uVar15;
  uVar12 = uVar12 + local_110 + uVar10;
  uVar17 = uVar3 << 0x20 | uVar3 >> 0x20;
  uVar9 = uVar9 ^ uVar12;
  uVar8 = uVar5 + local_f8 + uVar14;
  uVar3 = uVar17 + 0xa54ff53a5f1d36f1;
  uVar5 = uVar9 >> 0x10 | uVar9 << 0x30;
  uVar9 = local_80 ^ uVar3;
  uVar13 = uVar13 + uVar5;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar10 = uVar10 ^ uVar13;
  uVar15 = uVar15 + local_100 + uVar9;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar17 = uVar17 ^ uVar15;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar3 = uVar3 + uVar17;
  uVar17 = uVar17 ^ uVar8;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar17;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar8 = uVar8 + local_f0 + uVar14;
  uVar7 = uVar7 + local_e8 + uVar10;
  uVar17 = uVar17 ^ uVar8;
  uVar11 = uVar11 ^ uVar7;
  uVar12 = uVar12 + local_d8 + uVar9;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar13 = uVar13 + uVar18;
  uVar4 = uVar4 ^ uVar12;
  uVar3 = uVar11 + uVar3;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar9 = uVar9 ^ uVar20;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar7 = uVar7 + local_e0 + uVar10;
  uVar12 = uVar12 + local_d0 + uVar9;
  uVar11 = uVar11 ^ uVar7;
  uVar4 = uVar4 ^ uVar12;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar3 = uVar11 + uVar3;
  uVar20 = uVar20 + uVar4;
  uVar9 = uVar9 ^ uVar20;
  uVar10 = uVar10 ^ uVar3;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar15 = uVar15 + local_c8 + uVar16;
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar5;
  uVar16 = uVar16 ^ uVar2;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar15 = uVar15 + local_c0 + uVar17;
  uVar7 = uVar7 + local_118 + uVar14;
  uVar5 = uVar5 ^ uVar15;
  uVar4 = uVar4 ^ uVar7;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar2 = uVar2 + uVar4;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar8 = uVar8 + local_c8 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar8 = uVar8 + local_e8 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar14 = uVar14 ^ uVar2;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar7 = uVar7 + local_f8 + uVar14;
  uVar12 = uVar12 + local_f0 + uVar10;
  uVar4 = uVar4 ^ uVar7;
  uVar15 = uVar15 + local_d0 + uVar9;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar5 = uVar5 ^ uVar12;
  uVar2 = uVar2 + uVar4;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar18 = uVar18 ^ uVar15;
  uVar13 = uVar13 + uVar5;
  uVar14 = uVar14 ^ uVar2;
  uVar10 = uVar10 ^ uVar13;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar12 = uVar12 + local_c0 + uVar10;
  uVar3 = uVar3 + uVar17;
  uVar5 = uVar5 ^ uVar12;
  uVar9 = uVar9 ^ uVar3;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar13 = uVar13 + uVar5;
  uVar15 = uVar15 + local_108 + uVar9;
  uVar10 = uVar10 ^ uVar13;
  uVar17 = uVar17 ^ uVar15;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar3 = uVar3 + uVar17;
  uVar9 = uVar9 ^ uVar3;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar8 = uVar8 + local_130 + uVar14;
  uVar7 = uVar7 + local_138 + uVar10;
  uVar17 = uVar17 ^ uVar8;
  uVar11 = uVar11 ^ uVar7;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar13 = uVar13 + uVar17;
  uVar3 = uVar3 + uVar11;
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar8 = uVar8 + local_d8 + uVar14;
  uVar7 = uVar7 + local_128 + uVar10;
  uVar17 = uVar17 ^ uVar8;
  uVar11 = uVar11 ^ uVar7;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar3 = uVar3 + uVar11;
  uVar13 = uVar13 + uVar18;
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar12 = uVar12 + local_e0 + uVar9;
  uVar4 = uVar4 ^ uVar12;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar9 = uVar9 ^ uVar20;
  uVar15 = uVar15 + local_110 + uVar16;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar5 = uVar5 ^ uVar15;
  uVar12 = uVar12 + local_100 + uVar9;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar4 = uVar4 ^ uVar12;
  uVar2 = uVar2 + uVar5;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar16 = uVar16 ^ uVar2;
  uVar20 = uVar20 + uVar4;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar9 = uVar9 ^ uVar20;
  uVar15 = uVar15 + local_120 + uVar17;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar8 = uVar8 + local_e0 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar7 = uVar7 + local_d8 + uVar14;
  uVar12 = uVar12 + local_110 + uVar10;
  uVar4 = uVar4 ^ uVar7;
  uVar5 = uVar5 ^ uVar12;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar4;
  uVar8 = uVar8 + local_f8 + uVar17;
  uVar13 = uVar13 + uVar5;
  uVar14 = uVar14 ^ uVar2;
  uVar11 = uVar11 ^ uVar8;
  uVar10 = uVar10 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar7 = uVar7 + local_138 + uVar14;
  uVar20 = uVar20 + uVar11;
  uVar12 = uVar12 + local_128 + uVar10;
  uVar4 = uVar4 ^ uVar7;
  uVar17 = uVar17 ^ uVar20;
  uVar5 = uVar5 ^ uVar12;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar4;
  uVar14 = uVar14 ^ uVar2;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar13 = uVar13 + uVar5;
  uVar10 = uVar10 ^ uVar13;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar15 = uVar15 + local_c0 + uVar9;
  uVar18 = uVar18 ^ uVar15;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar8 = uVar8 + local_e8 + uVar14;
  uVar7 = uVar7 + local_120 + uVar10;
  uVar3 = uVar3 + uVar17;
  uVar9 = uVar9 ^ uVar3;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar15 = uVar15 + local_d0 + uVar9;
  uVar17 = uVar17 ^ uVar15;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar3 = uVar3 + uVar17;
  uVar17 = uVar17 ^ uVar8;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar17;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar8 = uVar8 + local_c8 + uVar14;
  uVar17 = uVar17 ^ uVar8;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar11 = uVar11 ^ uVar7;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar3 = uVar3 + uVar11;
  uVar10 = uVar10 ^ uVar3;
  uVar12 = uVar12 + local_100 + uVar9;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar4 = uVar4 ^ uVar12;
  uVar7 = uVar7 + local_108 + uVar10;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar11 = uVar11 ^ uVar7;
  uVar15 = uVar15 + local_f0 + uVar16;
  uVar20 = uVar20 + uVar4;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar5 = uVar5 ^ uVar15;
  uVar9 = uVar9 ^ uVar20;
  uVar3 = uVar3 + uVar11;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar10 = uVar10 ^ uVar3;
  uVar12 = uVar12 + local_130 + uVar9;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar4 = uVar4 ^ uVar12;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar20 = uVar20 + uVar4;
  uVar2 = uVar2 + uVar5;
  uVar9 = uVar9 ^ uVar20;
  uVar16 = uVar16 ^ uVar2;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar15 = uVar15 + local_118 + uVar17;
  uVar5 = uVar5 ^ uVar15;
  uVar7 = uVar7 + local_120 + uVar14;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar4 = uVar4 ^ uVar7;
  uVar2 = uVar2 + uVar5;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar17 = uVar17 ^ uVar2;
  uVar2 = uVar2 + uVar4;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar14 = uVar14 ^ uVar2;
  uVar8 = uVar8 + local_100 + uVar17;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar11 = uVar11 ^ uVar8;
  uVar7 = uVar7 + local_130 + uVar14;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar4 = uVar4 ^ uVar7;
  uVar20 = uVar20 + uVar11;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar17 = uVar17 ^ uVar20;
  uVar2 = uVar2 + uVar4;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar8 = uVar8 + local_f0 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar14 = uVar14 ^ uVar2;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar12 = uVar12 + local_d0 + uVar10;
  uVar15 = uVar15 + local_e0 + uVar9;
  uVar5 = uVar5 ^ uVar12;
  uVar18 = uVar18 ^ uVar15;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar13 = uVar13 + uVar5;
  uVar8 = uVar8 + local_128 + uVar14;
  uVar3 = uVar3 + uVar17;
  uVar10 = uVar10 ^ uVar13;
  uVar9 = uVar9 ^ uVar3;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar12 = uVar12 + local_d8 + uVar10;
  uVar15 = uVar15 + local_c8 + uVar9;
  uVar5 = uVar5 ^ uVar12;
  uVar17 = uVar17 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar13 = uVar13 + uVar5;
  uVar3 = uVar3 + uVar17;
  uVar10 = uVar10 ^ uVar13;
  uVar17 = uVar17 ^ uVar8;
  uVar9 = uVar9 ^ uVar3;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar13 = uVar13 + uVar17;
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar7 = uVar7 + local_110 + uVar10;
  uVar11 = uVar11 ^ uVar7;
  uVar8 = uVar8 + local_108 + uVar14;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar17 = uVar17 ^ uVar8;
  uVar3 = uVar3 + uVar11;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar10 = uVar10 ^ uVar3;
  uVar13 = uVar13 + uVar18;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar14 = uVar14 ^ uVar13;
  uVar7 = uVar7 + local_e8 + uVar10;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar11 = uVar11 ^ uVar7;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar3 = uVar3 + uVar11;
  uVar10 = uVar10 ^ uVar3;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar12 = uVar12 + local_118 + uVar9;
  uVar4 = uVar4 ^ uVar12;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar9 = uVar9 ^ uVar20;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar12 = uVar12 + local_138 + uVar9;
  uVar4 = uVar4 ^ uVar12;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar20 = uVar20 + uVar4;
  uVar15 = uVar15 + local_c0 + uVar16;
  uVar9 = uVar9 ^ uVar20;
  uVar5 = uVar5 ^ uVar15;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar5;
  uVar16 = uVar16 ^ uVar2;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar15 = uVar15 + local_f8 + uVar17;
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar8 = uVar8 + local_f0 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar8 = uVar8 + local_138 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar7 = uVar7 + local_110 + uVar14;
  uVar12 = uVar12 + local_128 + uVar10;
  uVar4 = uVar4 ^ uVar7;
  uVar5 = uVar5 ^ uVar12;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar4;
  uVar13 = uVar13 + uVar5;
  uVar15 = uVar15 + local_e8 + uVar9;
  uVar14 = uVar14 ^ uVar2;
  uVar10 = uVar10 ^ uVar13;
  uVar18 = uVar18 ^ uVar15;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar7 = uVar7 + local_100 + uVar14;
  uVar12 = uVar12 + local_118 + uVar10;
  uVar4 = uVar4 ^ uVar7;
  uVar5 = uVar5 ^ uVar12;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar4;
  uVar13 = uVar13 + uVar5;
  uVar14 = uVar14 ^ uVar2;
  uVar10 = uVar10 ^ uVar13;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar3 = uVar3 + uVar17;
  uVar9 = uVar9 ^ uVar3;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar15 = uVar15 + local_c0 + uVar9;
  uVar8 = uVar8 + local_c8 + uVar14;
  uVar17 = uVar17 ^ uVar15;
  uVar7 = uVar7 + local_e0 + uVar10;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar11 = uVar11 ^ uVar7;
  uVar3 = uVar3 + uVar17;
  uVar17 = uVar17 ^ uVar8;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar17;
  uVar3 = uVar3 + uVar11;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar8 = uVar8 + local_130 + uVar14;
  uVar7 = uVar7 + local_d8 + uVar10;
  uVar17 = uVar17 ^ uVar8;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar11 = uVar11 ^ uVar7;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar3 = uVar3 + uVar11;
  uVar10 = uVar10 ^ uVar3;
  uVar12 = uVar12 + local_108 + uVar9;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar15 = uVar15 + local_120 + uVar16;
  uVar4 = uVar4 ^ uVar12;
  uVar5 = uVar5 ^ uVar15;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar2 = uVar2 + uVar5;
  uVar9 = uVar9 ^ uVar20;
  uVar16 = uVar16 ^ uVar2;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar12 = uVar12 + local_f8 + uVar9;
  uVar15 = uVar15 + local_d0 + uVar17;
  uVar4 = uVar4 ^ uVar12;
  uVar5 = uVar5 ^ uVar15;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar20 = uVar20 + uVar4;
  uVar2 = uVar2 + uVar5;
  uVar9 = uVar9 ^ uVar20;
  uVar17 = uVar17 ^ uVar2;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar8 = uVar8 + local_128 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar8 = uVar8 + local_d8 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar7 = uVar7 + local_108 + uVar14;
  uVar4 = uVar4 ^ uVar7;
  uVar12 = uVar12 + local_138 + uVar10;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar5 = uVar5 ^ uVar12;
  uVar2 = uVar2 + uVar4;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar14 = uVar14 ^ uVar2;
  uVar13 = uVar13 + uVar5;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar7 = uVar7 + local_e8 + uVar14;
  uVar4 = uVar4 ^ uVar7;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar2 = uVar2 + uVar4;
  uVar14 = uVar14 ^ uVar2;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 ^ uVar13;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar15 = uVar15 + local_f8 + uVar9;
  uVar18 = uVar18 ^ uVar15;
  uVar12 = uVar12 + local_e0 + uVar10;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar8 = uVar8 + local_118 + uVar14;
  uVar5 = uVar5 ^ uVar12;
  uVar3 = uVar3 + uVar17;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar5;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar10 = uVar10 ^ uVar13;
  uVar15 = uVar15 + local_120 + uVar9;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar17 = uVar17 ^ uVar15;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar3 = uVar3 + uVar17;
  uVar17 = uVar17 ^ uVar8;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar17;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar8 = uVar8 + local_d0 + uVar14;
  uVar17 = uVar17 ^ uVar8;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar7 = uVar7 + local_100 + uVar10;
  uVar12 = uVar12 + local_c0 + uVar9;
  uVar11 = uVar11 ^ uVar7;
  uVar4 = uVar4 ^ uVar12;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar3 = uVar3 + uVar11;
  uVar20 = uVar20 + uVar4;
  uVar10 = uVar10 ^ uVar3;
  uVar9 = uVar9 ^ uVar20;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar7 = uVar7 + local_110 + uVar10;
  uVar12 = uVar12 + local_c8 + uVar9;
  uVar11 = uVar11 ^ uVar7;
  uVar4 = uVar4 ^ uVar12;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar3 = uVar3 + uVar11;
  uVar20 = uVar20 + uVar4;
  uVar10 = uVar10 ^ uVar3;
  uVar9 = uVar9 ^ uVar20;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar15 = uVar15 + local_130 + uVar16;
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar5;
  uVar16 = uVar16 ^ uVar2;
  uVar7 = uVar7 + local_130 + uVar14;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar4 = uVar4 ^ uVar7;
  uVar15 = uVar15 + local_f0 + uVar17;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar2 = uVar2 + uVar4;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar14 = uVar14 ^ uVar2;
  uVar8 = uVar8 + local_d8 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar8 = uVar8 + local_110 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar7 = uVar7 + local_c0 + uVar14;
  uVar4 = uVar4 ^ uVar7;
  uVar12 = uVar12 + local_c8 + uVar10;
  uVar15 = uVar15 + local_118 + uVar9;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar5 = uVar5 ^ uVar12;
  uVar2 = uVar2 + uVar4;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar18 = uVar18 ^ uVar15;
  uVar14 = uVar14 ^ uVar2;
  uVar13 = uVar13 + uVar5;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar10 = uVar10 ^ uVar13;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar3 = uVar3 + uVar17;
  uVar12 = uVar12 + local_d0 + uVar10;
  uVar9 = uVar9 ^ uVar3;
  uVar5 = uVar5 ^ uVar12;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar15 = uVar15 + local_e8 + uVar9;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar13 = uVar13 + uVar5;
  uVar17 = uVar17 ^ uVar15;
  uVar10 = uVar10 ^ uVar13;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar3 = uVar3 + uVar17;
  uVar9 = uVar9 ^ uVar3;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar8 = uVar8 + local_138 + uVar14;
  uVar7 = uVar7 + local_108 + uVar10;
  uVar17 = uVar17 ^ uVar8;
  uVar11 = uVar11 ^ uVar7;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar13 = uVar13 + uVar17;
  uVar12 = uVar12 + local_f0 + uVar9;
  uVar3 = uVar3 + uVar11;
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar8 = uVar8 + local_100 + uVar14;
  uVar7 = uVar7 + local_120 + uVar10;
  uVar17 = uVar17 ^ uVar8;
  uVar11 = uVar11 ^ uVar7;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar3 = uVar3 + uVar11;
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar4 = uVar4 ^ uVar12;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar9 = uVar9 ^ uVar20;
  uVar15 = uVar15 + local_f8 + uVar16;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar5 = uVar5 ^ uVar15;
  uVar12 = uVar12 + local_128 + uVar9;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar4 = uVar4 ^ uVar12;
  uVar2 = uVar2 + uVar5;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar16 = uVar16 ^ uVar2;
  uVar20 = uVar20 + uVar4;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar9 = uVar9 ^ uVar20;
  uVar15 = uVar15 + local_e0 + uVar17;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar8 = uVar8 + local_d0 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar7 = uVar7 + local_100 + uVar14;
  uVar8 = uVar8 + local_e0 + uVar17;
  uVar12 = uVar12 + local_d8 + uVar10;
  uVar4 = uVar4 ^ uVar7;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar5 = uVar5 ^ uVar12;
  uVar2 = uVar2 + uVar4;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar11 = uVar11 ^ uVar8;
  uVar13 = uVar13 + uVar5;
  uVar14 = uVar14 ^ uVar2;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar10 = uVar10 ^ uVar13;
  uVar20 = uVar20 + uVar11;
  uVar7 = uVar7 + local_c8 + uVar14;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar17 = uVar17 ^ uVar20;
  uVar4 = uVar4 ^ uVar7;
  uVar12 = uVar12 + local_130 + uVar10;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar5 = uVar5 ^ uVar12;
  uVar2 = uVar2 + uVar4;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar14 = uVar14 ^ uVar2;
  uVar13 = uVar13 + uVar5;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar10 = uVar10 ^ uVar13;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar15 = uVar15 + local_120 + uVar9;
  uVar18 = uVar18 ^ uVar15;
  uVar8 = uVar8 + local_110 + uVar14;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar7 = uVar7 + local_c0 + uVar10;
  uVar3 = uVar3 + uVar17;
  uVar11 = uVar11 ^ uVar7;
  uVar9 = uVar9 ^ uVar3;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar15 = uVar15 + local_f0 + uVar9;
  uVar17 = uVar17 ^ uVar15;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar3 = uVar3 + uVar17;
  uVar17 = uVar17 ^ uVar8;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar17;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar8 = uVar8 + local_138 + uVar14;
  uVar17 = uVar17 ^ uVar8;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar3 = uVar3 + uVar11;
  uVar10 = uVar10 ^ uVar3;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar7 = uVar7 + local_118 + uVar10;
  uVar11 = uVar11 ^ uVar7;
  uVar12 = uVar12 + local_f8 + uVar9;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar4 = uVar4 ^ uVar12;
  uVar3 = uVar3 + uVar11;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar9 = uVar9 ^ uVar20;
  uVar10 = uVar10 ^ uVar3;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar15 = uVar15 + local_128 + uVar16;
  uVar12 = uVar12 + local_108 + uVar9;
  uVar5 = uVar5 ^ uVar15;
  uVar4 = uVar4 ^ uVar12;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar20 = uVar20 + uVar4;
  uVar16 = uVar16 ^ uVar2;
  uVar9 = uVar9 ^ uVar20;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar15 = uVar15 + local_e8 + uVar17;
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar7 = uVar7 + local_c8 + uVar14;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar4 = uVar4 ^ uVar7;
  uVar8 = uVar8 + local_108 + uVar17;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar11 = uVar11 ^ uVar8;
  uVar2 = uVar2 + uVar4;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar14 = uVar14 ^ uVar2;
  uVar20 = uVar20 + uVar11;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar17 = uVar17 ^ uVar20;
  uVar7 = uVar7 + local_f0 + uVar14;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar4 = uVar4 ^ uVar7;
  uVar8 = uVar8 + local_c0 + uVar17;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar11 = uVar11 ^ uVar8;
  uVar2 = uVar2 + uVar4;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar14 = uVar14 ^ uVar2;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar16 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar12 = uVar12 + local_e0 + uVar10;
  uVar15 = uVar15 + local_138 + uVar9;
  uVar5 = uVar5 ^ uVar12;
  uVar18 = uVar18 ^ uVar15;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar17 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar13 = uVar13 + uVar5;
  uVar8 = uVar8 + local_d8 + uVar14;
  uVar3 = uVar3 + uVar17;
  uVar10 = uVar10 ^ uVar13;
  uVar9 = uVar9 ^ uVar3;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar12 = uVar12 + local_120 + uVar10;
  uVar15 = uVar15 + local_f8 + uVar9;
  uVar5 = uVar5 ^ uVar12;
  uVar17 = uVar17 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar17 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar13 = uVar13 + uVar5;
  uVar3 = uVar3 + uVar17;
  uVar17 = uVar17 ^ uVar8;
  uVar10 = uVar10 ^ uVar13;
  uVar9 = uVar9 ^ uVar3;
  uVar17 = uVar17 << 0x20 | uVar17 >> 0x20;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar13 = uVar13 + uVar17;
  uVar14 = uVar14 ^ uVar13;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar7 = uVar7 + local_d0 + uVar10;
  uVar11 = uVar11 ^ uVar7;
  uVar12 = uVar12 + local_130 + uVar9;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar4 = uVar4 ^ uVar12;
  uVar8 = uVar8 + local_128 + uVar14;
  uVar3 = uVar3 + uVar11;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar10 = uVar10 ^ uVar3;
  uVar17 = uVar17 ^ uVar8;
  uVar20 = uVar20 + uVar4;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar18 = uVar17 >> 0x10 | uVar17 << 0x30;
  uVar9 = uVar9 ^ uVar20;
  uVar7 = uVar7 + local_100 + uVar10;
  uVar13 = uVar13 + uVar18;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar11 = uVar11 ^ uVar7;
  uVar14 = uVar14 ^ uVar13;
  uVar12 = uVar12 + local_118 + uVar9;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar3 = uVar3 + uVar11;
  uVar10 = uVar10 ^ uVar3;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar4 = uVar4 ^ uVar12;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar20 = uVar20 + uVar4;
  uVar9 = uVar9 ^ uVar20;
  uVar15 = uVar15 + local_e8 + uVar16;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar5;
  uVar16 = uVar16 ^ uVar2;
  uVar17 = uVar16 >> 0x18 | uVar16 << 0x28;
  uVar15 = uVar15 + local_110 + uVar17;
  uVar5 = uVar5 ^ uVar15;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar17 = uVar17 ^ uVar2;
  uVar17 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar8 = uVar8 + local_e8 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar17 = uVar17 >> 0x18 | uVar17 << 0x28;
  uVar8 = uVar8 + local_128 + uVar17;
  uVar11 = uVar11 ^ uVar8;
  uVar11 = uVar11 >> 0x10 | uVar11 << 0x30;
  uVar20 = uVar20 + uVar11;
  uVar17 = uVar17 ^ uVar20;
  uVar19 = uVar17 << 1 | (ulong)((long)uVar17 < 0);
  uVar7 = uVar7 + local_f8 + uVar14;
  uVar4 = uVar4 ^ uVar7;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar2 = uVar2 + uVar4;
  uVar14 = uVar14 ^ uVar2;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar7 = uVar7 + local_118 + uVar14;
  uVar4 = uVar4 ^ uVar7;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar2 = uVar2 + uVar4;
  uVar14 = uVar14 ^ uVar2;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar12 = uVar12 + local_100 + uVar10;
  uVar5 = uVar5 ^ uVar12;
  uVar15 = uVar15 + local_130 + uVar9;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar18 = uVar18 ^ uVar15;
  uVar13 = uVar13 + uVar5;
  uVar16 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar10 = uVar10 ^ uVar13;
  uVar3 = uVar3 + uVar16;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar17 = uVar12 + local_108 + uVar10;
  uVar5 = uVar5 ^ uVar17;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar13 = uVar13 + uVar5;
  uVar10 = uVar10 ^ uVar13;
  uVar10 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar9 = uVar9 ^ uVar3;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar12 = uVar15 + local_110 + uVar9;
  uVar16 = uVar16 ^ uVar12;
  uVar8 = uVar8 + local_c0 + uVar14;
  uVar7 = uVar7 + local_f0 + uVar10;
  uVar15 = uVar16 >> 0x10 | uVar16 << 0x30;
  uVar11 = uVar11 ^ uVar7;
  uVar3 = uVar3 + uVar15;
  uVar15 = uVar15 ^ uVar8;
  uVar16 = uVar15 << 0x20 | uVar15 >> 0x20;
  uVar15 = uVar11 << 0x20 | uVar11 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar16;
  uVar3 = uVar3 + uVar15;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar14 = uVar14 ^ uVar13;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar10 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar8 = uVar8 + local_e0 + uVar14;
  uVar7 = uVar7 + local_c8 + uVar10;
  uVar16 = uVar16 ^ uVar8;
  uVar15 = uVar15 ^ uVar7;
  uVar18 = uVar16 >> 0x10 | uVar16 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar14 = uVar14 ^ uVar13;
  uVar11 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar15 = uVar15 >> 0x10 | uVar15 << 0x30;
  uVar3 = uVar3 + uVar15;
  uVar10 = uVar10 ^ uVar3;
  uVar14 = uVar10 << 1 | (ulong)((long)uVar10 < 0);
  uVar10 = uVar12 + local_d0 + uVar19;
  uVar17 = uVar17 + local_120 + uVar9;
  uVar5 = uVar5 ^ uVar10;
  uVar4 = uVar4 ^ uVar17;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar2 = uVar2 + uVar5;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar20 = uVar20 + uVar4;
  uVar19 = uVar19 ^ uVar2;
  uVar9 = uVar9 ^ uVar20;
  uVar16 = uVar19 >> 0x18 | uVar19 << 0x28;
  uVar12 = uVar10 + local_138 + uVar16;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar17 = uVar17 + local_d8 + uVar9;
  uVar5 = uVar5 ^ uVar12;
  uVar4 = uVar4 ^ uVar17;
  uVar5 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar2 = uVar2 + uVar5;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar20 = uVar20 + uVar4;
  uVar16 = uVar16 ^ uVar2;
  uVar9 = uVar9 ^ uVar20;
  uVar10 = uVar16 << 1 | (ulong)((long)uVar16 < 0);
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar8 = uVar8 + local_138 + uVar10;
  uVar15 = uVar15 ^ uVar8;
  uVar15 = uVar15 << 0x20 | uVar15 >> 0x20;
  uVar20 = uVar20 + uVar15;
  uVar7 = uVar7 + local_128 + uVar11;
  uVar10 = uVar10 ^ uVar20;
  uVar4 = uVar4 ^ uVar7;
  uVar16 = uVar10 >> 0x18 | uVar10 << 0x28;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar10 = uVar8 + local_130 + uVar16;
  uVar17 = uVar17 + local_118 + uVar14;
  uVar2 = uVar2 + uVar4;
  uVar15 = uVar15 ^ uVar10;
  uVar5 = uVar5 ^ uVar17;
  uVar11 = uVar11 ^ uVar2;
  uVar15 = uVar15 >> 0x10 | uVar15 << 0x30;
  uVar5 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar11 = uVar11 >> 0x18 | uVar11 << 0x28;
  uVar20 = uVar20 + uVar15;
  uVar13 = uVar13 + uVar5;
  uVar8 = uVar7 + local_120 + uVar11;
  uVar16 = uVar16 ^ uVar20;
  uVar14 = uVar14 ^ uVar13;
  uVar4 = uVar4 ^ uVar8;
  uVar19 = uVar16 << 1 | (ulong)((long)uVar16 < 0);
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar2 = uVar2 + uVar4;
  uVar11 = uVar11 ^ uVar2;
  uVar11 = uVar11 << 1 | (ulong)((long)uVar11 < 0);
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar12 = uVar12 + local_108 + uVar9;
  uVar18 = uVar18 ^ uVar12;
  uVar17 = uVar17 + local_110 + uVar14;
  uVar16 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar10 = uVar10 + local_f8 + uVar11;
  uVar5 = uVar5 ^ uVar17;
  uVar3 = uVar3 + uVar16;
  uVar7 = uVar5 >> 0x10 | uVar5 << 0x30;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar7;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar14 = uVar14 ^ uVar13;
  uVar12 = uVar12 + local_100 + uVar9;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar16 = uVar16 ^ uVar12;
  uVar5 = uVar16 >> 0x10 | uVar16 << 0x30;
  uVar3 = uVar3 + uVar5;
  uVar5 = uVar5 ^ uVar10;
  uVar16 = uVar5 << 0x20 | uVar5 >> 0x20;
  uVar9 = uVar9 ^ uVar3;
  uVar13 = uVar13 + uVar16;
  uVar9 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar11 = uVar11 ^ uVar13;
  uVar5 = uVar11 >> 0x18 | uVar11 << 0x28;
  uVar10 = uVar10 + local_f0 + uVar5;
  uVar16 = uVar16 ^ uVar10;
  uVar18 = uVar16 >> 0x10 | uVar16 << 0x30;
  uVar13 = uVar13 + uVar18;
  uVar5 = uVar5 ^ uVar13;
  uVar11 = uVar5 << 1 | (ulong)((long)uVar5 < 0);
  uVar5 = uVar8 + local_e8 + uVar14;
  uVar17 = uVar17 + local_d8 + uVar9;
  uVar15 = uVar15 ^ uVar5;
  uVar4 = uVar4 ^ uVar17;
  uVar15 = uVar15 << 0x20 | uVar15 >> 0x20;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar3 = uVar3 + uVar15;
  uVar12 = uVar12 + local_c8 + uVar19;
  uVar20 = uVar20 + uVar4;
  uVar14 = uVar14 ^ uVar3;
  uVar9 = uVar9 ^ uVar20;
  uVar14 = uVar14 >> 0x18 | uVar14 << 0x28;
  uVar9 = uVar9 >> 0x18 | uVar9 << 0x28;
  uVar8 = uVar5 + local_e0 + uVar14;
  uVar17 = uVar17 + local_d0 + uVar9;
  uVar15 = uVar15 ^ uVar8;
  uVar4 = uVar4 ^ uVar17;
  uVar15 = uVar15 >> 0x10 | uVar15 << 0x30;
  uVar4 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar3 = uVar3 + uVar15;
  uVar20 = uVar20 + uVar4;
  uVar14 = uVar14 ^ uVar3;
  uVar9 = uVar9 ^ uVar20;
  uVar14 = uVar14 << 1 | (ulong)((long)uVar14 < 0);
  uVar5 = uVar9 << 1 | (ulong)((long)uVar9 < 0);
  uVar7 = uVar7 ^ uVar12;
  uVar9 = uVar7 << 0x20 | uVar7 >> 0x20;
  uVar2 = uVar2 + uVar9;
  uVar19 = uVar19 ^ uVar2;
  uVar7 = uVar19 >> 0x18 | uVar19 << 0x28;
  uVar12 = uVar12 + local_c0 + uVar7;
  uVar8 = uVar8 + local_118 + uVar11;
  uVar9 = uVar9 ^ uVar12;
  uVar4 = uVar4 ^ uVar8;
  uVar9 = uVar9 >> 0x10 | uVar9 << 0x30;
  uVar4 = uVar4 << 0x20 | uVar4 >> 0x20;
  uVar2 = uVar2 + uVar9;
  uVar7 = uVar7 ^ uVar2;
  uVar2 = uVar2 + uVar4;
  uVar16 = uVar7 << 1 | (ulong)((long)uVar7 < 0);
  uVar11 = uVar11 ^ uVar2;
  uVar7 = uVar10 + local_c8 + uVar16;
  uVar11 = uVar11 >> 0x18 | uVar11 << 0x28;
  uVar15 = uVar15 ^ uVar7;
  uVar15 = uVar15 << 0x20 | uVar15 >> 0x20;
  uVar20 = uVar20 + uVar15;
  uVar16 = uVar16 ^ uVar20;
  uVar10 = uVar7 + local_e8 + (uVar16 >> 0x18 | uVar16 << 0x28);
  uVar15 = uVar15 ^ uVar10;
  uVar17 = uVar17 + local_f0 + uVar14;
  uVar12 = uVar12 + local_d0 + uVar5;
  uVar9 = uVar9 ^ uVar17;
  uVar7 = uVar9 << 0x20 | uVar9 >> 0x20;
  uVar18 = uVar18 ^ uVar12;
  uVar13 = uVar13 + uVar7;
  uVar16 = uVar18 << 0x20 | uVar18 >> 0x20;
  uVar4 = uVar4 ^ uVar8 + local_f8 + uVar11;
  uVar3 = uVar3 + uVar16;
  uVar14 = uVar14 ^ uVar13;
  uVar9 = uVar4 >> 0x10 | uVar4 << 0x30;
  uVar5 = uVar5 ^ uVar3;
  uVar14 = uVar17 + local_c0 + (uVar14 >> 0x18 | uVar14 << 0x28);
  uVar4 = uVar5 >> 0x18 | uVar5 << 0x28;
  uVar11 = uVar11 ^ uVar2 + uVar9;
  uVar7 = uVar7 ^ uVar14;
  uVar8 = uVar11 << 1 | (ulong)((long)uVar11 < 0);
  uVar16 = uVar16 ^ uVar12 + local_108 + uVar4;
  uVar11 = uVar16 >> 0x10 | uVar16 << 0x30;
  uVar4 = uVar4 ^ uVar3 + uVar11;
  uVar5 = uVar4 << 1 | (ulong)((long)uVar4 < 0);
  uVar2 = uVar10 + local_130 + uVar8;
  uVar11 = uVar11 ^ uVar2;
  uVar8 = uVar8 ^ uVar13 + (uVar7 >> 0x10 | uVar7 << 0x30) + (uVar11 << 0x20 | uVar11 >> 0x20);
  uVar4 = uVar2 + local_d8 + (uVar8 >> 0x18 | uVar8 << 0x28);
  uVar2 = uVar14 + local_e0 + uVar5;
  uVar9 = uVar9 ^ uVar2;
  uVar9 = uVar9 << 0x20 | uVar9 >> 0x20;
  uVar3 = uVar9 + uVar20 + (uVar15 >> 0x10 | uVar15 << 0x30);
  uVar5 = uVar5 ^ uVar3;
  uVar9 = uVar9 ^ uVar2 + local_100 + (uVar5 >> 0x18 | uVar5 << 0x28);
  uVar3 = (uVar9 >> 0x10 | uVar9 << 0x30) + uVar3;
  lVar6 = 0;
  while( true ) {
    puVar1 = (ulong *)(lParm1 + lVar6 * 8);
    *puVar1 = *puVar1 ^ uVar4 ^ uVar3;
    lVar6 = lVar6 + 1;
    if (lVar6 == 8) break;
    uVar4 = (&local_b8)[lVar6];
    uVar3 = local_78[lVar6];
  }
  return;
}



void FUN_00404aa0(long lParm1,long lParm2,ulong uParm3)

{
  long lVar1;
  ulong uVar2;
  ulong uVar3;
  
  lVar1 = *(long *)(lParm1 + 0xe0);
  uVar3 = 0x80 - lVar1;
  if (uVar3 < uParm3) {
    *(undefined8 *)(lParm1 + 0xe0) = 0;
    func_0x00401990(lParm1 + 0x60 + lVar1,lParm2,uVar3);
    uVar2 = *(long *)(lParm1 + 0x40) + 0x80;
    *(ulong *)(lParm1 + 0x40) = uVar2;
    uParm3 = uParm3 - uVar3;
    lParm2 = lParm2 + uVar3;
    *(long *)(lParm1 + 0x48) = *(long *)(lParm1 + 0x48) + (ulong)(uVar2 < 0x80);
    FUN_004034b0(lParm1,lParm1 + 0x60);
    lVar1 = lParm2;
    uVar3 = uParm3;
    if (0x80 < uParm3) {
      do {
        uVar2 = *(long *)(lParm1 + 0x40) + 0x80;
        *(ulong *)(lParm1 + 0x40) = uVar2;
        uVar3 = uVar3 - 0x80;
        *(long *)(lParm1 + 0x48) = *(long *)(lParm1 + 0x48) + (ulong)(uVar2 < 0x80);
        FUN_004034b0(lParm1,lVar1);
        lVar1 = lVar1 + 0x80;
      } while (0x80 < uVar3);
      uVar3 = uParm3 - 0x81 >> 7;
      uParm3 = (uParm3 - 0x80) + uVar3 * -0x80;
      lParm2 = lParm2 + (uVar3 + 1) * 0x80;
    }
    lVar1 = *(long *)(lParm1 + 0xe0);
  }
  func_0x00401990(lParm1 + 0x60 + lVar1,lParm2,uParm3);
  *(long *)(lParm1 + 0xe0) = *(long *)(lParm1 + 0xe0) + uParm3;
  return;
}



undefined8 FUN_00404bb0(undefined8 *puParm1,byte *pbParm2)

{
  byte *pbVar1;
  ulong uVar2;
  undefined8 *puVar3;
  long lVar4;
  ulong uVar5;
  
  puParm1[0x1e] = 0;
  uVar5 = 0x6a09e667f3bcc908;
  uVar2 = (ulong)(((int)puParm1 - (int)(undefined8 *)((ulong)(puParm1 + 1) & 0xfffffffffffffff8)) +
                  0xf8U >> 3);
  puVar3 = (undefined8 *)((ulong)(puParm1 + 1) & 0xfffffffffffffff8);
  while (uVar2 != 0) {
    uVar2 = uVar2 - 1;
    *puVar3 = 0;
    puVar3 = puVar3 + 1;
  }
  lVar4 = 0;
  *puParm1 = 0x6a09e667f3bcc908;
  puParm1[1] = 0xbb67ae8584caa73b;
  puParm1[2] = 0x3c6ef372fe94f82b;
  puParm1[3] = 0xa54ff53a5f1d36f1;
  puParm1[4] = 0x510e527fade682d1;
  puParm1[5] = 0x9b05688c2b3e6c1f;
  puParm1[6] = 0x1f83d9abfb41bd6b;
  puParm1[7] = 0x5be0cd19137e2179;
  pbVar1 = pbParm2;
  while( true ) {
    puParm1[lVar4] =
         ((ulong)pbVar1[2] << 0x10 | (ulong)pbVar1[1] << 8 | (ulong)*pbVar1 |
          (ulong)pbVar1[3] << 0x18 | (ulong)pbVar1[4] << 0x20 | (ulong)pbVar1[5] << 0x28 |
          (ulong)pbVar1[6] << 0x30 | (ulong)pbVar1[7] << 0x38) ^ uVar5;
    lVar4 = lVar4 + 1;
    if (lVar4 == 8) break;
    uVar5 = puParm1[lVar4];
    pbVar1 = pbVar1 + 8;
  }
  puParm1[0x1d] = (ulong)*pbParm2;
  return 0;
}



void FUN_00404ce0(undefined8 uParm1,long lParm2)

{
  undefined local_48;
  undefined local_47;
  undefined local_46;
  undefined local_45;
  undefined local_44;
  undefined local_43;
  undefined local_42;
  undefined local_41;
  undefined local_40;
  undefined local_3f;
  undefined local_3e;
  undefined local_3d;
  undefined local_3c;
  undefined local_3b;
  undefined local_3a;
  undefined local_39;
  undefined local_38;
  undefined local_37;
  undefined8 local_36;
  undefined4 local_2e;
  undefined2 local_2a;
  undefined8 local_28;
  undefined8 local_20;
  undefined8 local_18;
  undefined8 local_10;
  
  if (lParm2 - 1U < 0x40) {
    local_48 = (undefined)lParm2;
    local_47 = 0;
    local_46 = 1;
    local_45 = 1;
    local_44 = 0;
    local_43 = 0;
    local_42 = 0;
    local_41 = 0;
    local_40 = 0;
    local_3f = 0;
    local_3e = 0;
    local_3d = 0;
    local_3c = 0;
    local_3b = 0;
    local_3a = 0;
    local_39 = 0;
    local_38 = 0;
    local_37 = 0;
    local_36 = 0;
    local_2e = 0;
    local_2a = 0;
    local_28 = 0;
    local_20 = 0;
    local_18 = 0;
    local_10 = 0;
    FUN_00404bb0(uParm1,&local_48);
  }
  return;
}



undefined8 FUN_00404da0(undefined8 uParm1,undefined8 uParm2,long lParm3)

{
  if (lParm3 != 0) {
    FUN_00404aa0();
    return 0;
  }
  return 0;
}



undefined8 FUN_00404dc0(undefined8 *puParm1,long lParm2,ulong uParm3)

{
  ulong uVar1;
  undefined8 *puVar2;
  undefined *puVar3;
  undefined8 uVar4;
  long lVar5;
  undefined8 *puVar6;
  undefined8 local_58 [8];
  
  lVar5 = 8;
  puVar2 = local_58;
  while (lVar5 != 0) {
    lVar5 = lVar5 + -1;
    *puVar2 = 0;
    puVar2 = puVar2 + 1;
  }
  if (((lParm2 == 0) || (uParm3 <= (ulong)puParm1[0x1d] && puParm1[0x1d] != uParm3)) ||
     (puParm1[10] != 0)) {
    uVar4 = 0xffffffff;
  }
  else {
    uVar1 = puParm1[0x1c];
    lVar5 = puParm1[8];
    puParm1[8] = uVar1 + lVar5;
    puParm1[9] = puParm1[9] + (ulong)(uVar1 + lVar5 < uVar1);
    if (*(char *)(puParm1 + 0x1e) != '\0') {
      puParm1[0xb] = 0xffffffffffffffff;
    }
    puParm1[10] = 0xffffffffffffffff;
    FUN_00401bc0((long)puParm1 + uVar1 + 0x60,0,0x80 - uVar1);
    FUN_004034b0(puParm1,puParm1 + 0xc);
    puVar2 = local_58;
    puVar6 = puParm1;
    do {
      uVar4 = *puVar6;
      puVar3 = (undefined *)puVar2 + 8;
      puVar6 = puVar6 + 1;
      *(char *)puVar2 = (char)uVar4;
      ((undefined *)puVar2)[1] = (char)((ulong)uVar4 >> 8);
      ((undefined *)puVar2)[2] = (char)((ulong)uVar4 >> 0x10);
      ((undefined *)puVar2)[3] = (char)((ulong)uVar4 >> 0x18);
      ((undefined *)puVar2)[4] = (char)((ulong)uVar4 >> 0x20);
      ((undefined *)puVar2)[5] = (char)((ulong)uVar4 >> 0x28);
      ((undefined *)puVar2)[7] = (char)((ulong)uVar4 >> 0x38);
      ((undefined *)puVar2)[6] = (char)((ulong)uVar4 >> 0x30);
      puVar2 = (undefined8 *)puVar3;
    } while (puVar3 != &stack0xffffffffffffffe8);
    func_0x00401990(lParm2,local_58,puParm1[0x1d]);
    (*(code *)PTR_FUN_00614448)(local_58,0,0x40);
    uVar4 = 0;
  }
  return uVar4;
}



undefined8 FUN_00404f20(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3)

{
  int iVar1;
  long lVar2;
  long lVar3;
  long lVar4;
  undefined8 uVar5;
  undefined auStack312 [264];
  
  lVar2 = FUN_00408510(0x8000);
  if (lVar2 == 0) {
    return 0xffffffff;
  }
  FUN_00404ce0(auStack312,uParm3);
  do {
    lVar4 = 0;
LAB_00404f79:
    lVar3 = func_0x00401b90(lVar2 + lVar4,1,0x8000 - lVar4,uParm1);
    lVar4 = lVar4 + lVar3;
    if (lVar4 != 0x8000) {
      if (lVar3 != 0) break;
      iVar1 = func_0x00401b20(uParm1);
      uVar5 = 0xffffffff;
      if (iVar1 != 0) goto LAB_00404fd3;
      goto LAB_00404fbe;
    }
    FUN_00404da0(auStack312,lVar2,0x8000);
  } while( true );
  iVar1 = func_0x004019e0();
  if (iVar1 != 0) {
LAB_00404fbe:
    if (lVar4 != 0) {
      FUN_00404da0(auStack312,lVar2,lVar4);
    }
    uVar5 = 0;
    FUN_00404dc0(auStack312,uParm2,uParm3);
LAB_00404fd3:
    func_0x00401db0(lVar2);
    return uVar5;
  }
  goto LAB_00404f79;
}



long FUN_00405010(undefined8 uParm1,long *plParm2,long lParm3,long lParm4)

{
  bool bVar1;
  long lVar2;
  int iVar3;
  long lVar4;
  long lVar5;
  long lVar6;
  long lVar7;
  long local_50;
  
  lVar4 = func_0x00401d20();
  lVar5 = *plParm2;
  if (lVar5 == 0) {
    local_50 = -1;
  }
  else {
    bVar1 = false;
    local_50 = 0xffffffffffffffff;
    lVar2 = 0;
    lVar7 = lParm3;
LAB_004050a8:
    do {
      lVar6 = lVar2;
      iVar3 = func_0x00401b40(lVar5,uParm1,lVar4);
      if (iVar3 == 0) {
        lVar5 = func_0x00401d20(lVar5);
        if (lVar5 == lVar4) {
          return lVar6;
        }
        if (local_50 == -1) {
          lVar7 = lVar7 + lParm4;
          lVar5 = plParm2[lVar6 + 1];
          lVar2 = lVar6 + 1;
          local_50 = lVar6;
          if (lVar5 == 0) break;
          goto LAB_004050a8;
        }
        if (lParm3 == 0) {
          bVar1 = true;
        }
        else {
          iVar3 = func_0x00401b70(local_50 * lParm4 + lParm3,lVar7,lParm4);
          if (iVar3 != 0) {
            bVar1 = true;
          }
        }
      }
      lVar7 = lVar7 + lParm4;
      lVar5 = plParm2[lVar6 + 1];
      lVar2 = lVar6 + 1;
    } while (lVar5 != 0);
    if (bVar1) {
      return 0xfffffffffffffffe;
    }
  }
  return local_50;
}



void FUN_00405140(void)

{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  uint *puVar4;
  
  iVar1 = FUN_004086b0(DAT_006144c0);
  if (iVar1 == 0) {
LAB_00405164:
    iVar1 = FUN_004086b0(DAT_006144e0);
    if (iVar1 == 0) {
      return;
    }
  }
  else {
    if (DAT_00614580 != '\0') {
      piVar2 = (int *)func_0x00401cb0();
      if (*piVar2 == 0x20) goto LAB_00405164;
    }
    if (DAT_00614588 == 0) goto LAB_004051b3;
    uVar3 = FUN_00406580();
    puVar4 = (uint *)func_0x00401cb0();
    FUN_00407510(0,(ulong)*puVar4,"%s: %s",uVar3,"write error");
  }
  do {
    func_0x00401d00((ulong)DAT_00614458);
LAB_004051b3:
    puVar4 = (uint *)func_0x00401cb0();
    FUN_00407510(0,(ulong)*puVar4,0x410592,"write error");
  } while( true );
}



void FUN_004051d0(long lParm1,ulong uParm2)

{
  uint uVar1;
  
  if (lParm1 != 0) {
    uVar1 = func_0x00401cf0();
    FUN_00615110((ulong)uVar1,0,0,uParm2 & 0xffffffff);
    return;
  }
  return;
}



long FUN_00405200(undefined8 uParm1,undefined8 uParm2)

{
  undefined4 uVar1;
  uint uVar2;
  int iVar3;
  long lVar4;
  long lVar5;
  undefined4 *puVar6;
  
  lVar4 = func_0x00401bb0();
  lVar5 = 0;
  if (lVar4 != 0) {
    uVar2 = func_0x00401cf0(lVar4);
    lVar5 = lVar4;
    if (uVar2 < 3) {
      uVar2 = FUN_00406650((ulong)uVar2);
      if ((int)uVar2 < 0) {
        puVar6 = (undefined4 *)func_0x00401cb0();
        uVar1 = *puVar6;
        lVar5 = 0;
        FUN_004075f0(lVar4);
        puVar6 = (undefined4 *)func_0x00401cb0();
        *puVar6 = uVar1;
      }
      else {
        iVar3 = FUN_004075f0(lVar4);
        if (iVar3 == 0) {
          lVar5 = func_0x00401b80((ulong)uVar2,uParm2);
          if (lVar5 != 0) {
            return lVar5;
          }
        }
        puVar6 = (undefined4 *)func_0x00401cb0();
        uVar1 = *puVar6;
        func_0x00401da0((ulong)uVar2);
        puVar6 = (undefined4 *)func_0x00401cb0();
        *puVar6 = uVar1;
        lVar5 = 0;
      }
    }
  }
  return lVar5;
}



void FUN_004052a0(byte *pbParm1)

{
  byte *pbVar1;
  long lVar2;
  byte *pbVar3;
  long lVar4;
  byte *pbVar5;
  bool bVar6;
  bool bVar7;
  byte bVar8;
  
  bVar8 = 0;
  if (pbParm1 == (byte *)0x0) {
    func_0x00401ca0("A NULL argv[0] was passed through an exec system call.\n",1,0x37,DAT_006144e0);
    func_0x00401a50();
    return;
  }
  lVar2 = func_0x00401aa0(pbParm1,0x2f);
  if (lVar2 != 0) {
    pbVar1 = (byte *)(lVar2 + 1);
    pbVar3 = pbVar1 + -(long)pbParm1;
    bVar6 = pbVar3 < (byte *)0x6;
    bVar7 = pbVar3 == (byte *)0x6;
    if (6 < (long)pbVar3) {
      lVar4 = 7;
      pbVar3 = (byte *)(lVar2 + -6);
      pbVar5 = (byte *)"/.libs/";
      do {
        if (lVar4 == 0) break;
        lVar4 = lVar4 + -1;
        bVar6 = *pbVar3 < *pbVar5;
        bVar7 = *pbVar3 == *pbVar5;
        pbVar3 = pbVar3 + (ulong)bVar8 * -2 + 1;
        pbVar5 = pbVar5 + (ulong)bVar8 * -2 + 1;
      } while (bVar7);
      if (bVar7) {
        lVar4 = 3;
        pbVar3 = pbVar1;
        pbVar5 = &DAT_00411318;
        do {
          if (lVar4 == 0) break;
          lVar4 = lVar4 + -1;
          bVar6 = *pbVar3 < *pbVar5;
          bVar7 = *pbVar3 == *pbVar5;
          pbVar3 = pbVar3 + (ulong)bVar8 * -2 + 1;
          pbVar5 = pbVar5 + (ulong)bVar8 * -2 + 1;
        } while (bVar7);
        pbParm1 = pbVar1;
        if ((!bVar6 && !bVar7) == bVar6) {
          pbParm1 = (byte *)(lVar2 + 4);
          DAT_006144d0 = pbParm1;
        }
      }
    }
  }
  DAT_006144e8 = pbParm1;
  DAT_00614590 = pbParm1;
  return;
}



undefined8 * FUN_00405340(undefined8 *puParm1,int iParm2)

{
  undefined8 uVar1;
  long lVar2;
  undefined8 *puVar3;
  undefined8 local_48;
  undefined8 local_40;
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  undefined8 local_20;
  undefined8 local_18;
  
  lVar2 = 7;
  puVar3 = &local_48;
  while (lVar2 != 0) {
    lVar2 = lVar2 + -1;
    *puVar3 = 0;
    puVar3 = puVar3 + 1;
  }
  if (iParm2 != 10) {
    *puParm1 = CONCAT44(local_48._4_4_,iParm2);
    puParm1[1] = local_40;
    puParm1[2] = local_38;
    puParm1[3] = local_30;
    puParm1[4] = local_28;
    puParm1[5] = local_20;
    puParm1[6] = local_18;
    return puParm1;
  }
  uVar1 = func_0x00401a50();
  return (undefined8 *)uVar1;
}



undefined * FUN_004053b0(char *pcParm1,int iParm2)

{
  byte *pbVar1;
  undefined *puVar2;
  
  pbVar1 = (byte *)FUN_004087e0();
  if ((*pbVar1 & 0xdf) == 0x55) {
    if (((((pbVar1[1] & 0xdf) == 0x54) && ((pbVar1[2] & 0xdf) == 0x46)) && (pbVar1[3] == 0x2d)) &&
       ((pbVar1[4] == 0x38 && (pbVar1[5] == 0)))) {
      if (*pcParm1 == '`') {
        return &DAT_00411329;
      }
      return &DAT_0041131c;
    }
  }
  else {
    if (((((*pbVar1 & 0xdf) == 0x47) && ((pbVar1[1] & 0xdf) == 0x42)) && (pbVar1[2] == 0x31)) &&
       (((pbVar1[3] == 0x38 && (pbVar1[4] == 0x30)) &&
        ((pbVar1[5] == 0x33 && ((pbVar1[6] == 0x30 && (pbVar1[7] == 0)))))))) {
      if (*pcParm1 == '`') {
        return &DAT_00411325;
      }
      return &DAT_00411320;
    }
  }
  puVar2 = &DAT_00411323;
  if (iParm2 != 9) {
    puVar2 = &DAT_00412117;
  }
  return puVar2;
}



ulong FUN_00405480(undefined *param_1,ulong param_2,long param_3,ulong param_4,uint param_5,
                  ulong param_6,long param_7,char *param_8,char *param_9)

{
  char *pcVar1;
  bool bVar2;
  bool bVar3;
  bool bVar4;
  int iVar5;
  long lVar6;
  long lVar7;
  ulong uVar8;
  char cVar9;
  byte bVar10;
  long lVar11;
  ulong uVar12;
  byte bVar13;
  byte bVar14;
  ulong uVar15;
  ulong uVar16;
  ulong uVar17;
  bool bVar18;
  bool bVar19;
  bool bVar20;
  undefined8 uVar21;
  byte local_c8;
  uint local_c0;
  byte local_ba;
  byte local_b9;
  byte *local_b8;
  ulong local_b0;
  char *local_a0;
  ulong local_78;
  uint local_4c;
  undefined8 local_48 [3];
  
  uVar21 = 0x4054b3;
  lVar6 = func_0x00401ab0();
  bVar4 = true;
  local_ba = (byte)((param_6 & 0xffffffff) >> 1) & 1;
  bVar3 = false;
  local_b9 = 0;
  local_c8 = 0;
  local_b0 = 0;
  local_a0 = (char *)0x0;
  local_78 = 0;
  local_c0 = param_5;
LAB_00405511:
  uVar17 = param_2;
  switch(local_c0) {
  case 0:
    local_ba = 0;
    uVar16 = 0;
    break;
  case 1:
    goto switchD_0040551e_caseD_1;
  case 2:
    if (local_ba != 0) {
      local_b0 = 1;
      local_a0 = "\'";
      uVar16 = 0;
      break;
    }
LAB_00405bc2:
    if (uVar17 != 0) {
      *param_1 = 0x27;
    }
    local_ba = 0;
    local_b0 = 1;
    uVar16 = 1;
    local_a0 = "\'";
    local_c0 = 2;
    break;
  case 3:
    local_c8 = 1;
    goto switchD_0040551e_caseD_1;
  case 4:
    if (local_ba == 0) {
      local_c8 = 1;
      goto LAB_00405bc2;
    }
switchD_0040551e_caseD_1:
    local_ba = 1;
    local_b0 = 1;
    uVar16 = 0;
    local_a0 = "\'";
    local_c0 = 2;
    break;
  case 5:
    if (local_ba == 0) {
      if (uVar17 == 0) {
        local_c8 = 1;
        local_b0 = 1;
        uVar16 = 1;
        local_a0 = "\"";
      }
      else {
        *param_1 = 0x22;
        local_c8 = 1;
        uVar16 = 1;
        local_b0 = 1;
        local_a0 = "\"";
      }
    }
    else {
      local_c8 = 1;
      local_b0 = 1;
      uVar16 = 0;
      local_a0 = "\"";
    }
    break;
  case 6:
    local_ba = 1;
    local_c8 = 1;
    uVar16 = 0;
    local_b0 = 1;
    local_a0 = "\"";
    local_c0 = 5;
    break;
  case 7:
    local_ba = 0;
    local_c8 = 1;
    uVar16 = 0;
    break;
  case 8:
  case 9:
  case 10:
    if (local_c0 != 10) {
      param_8 = (char *)FUN_004053b0(&DAT_0041132d);
      param_9 = (char *)FUN_004053b0(&DAT_00412117);
    }
    uVar16 = 0;
    if (local_ba == 0) {
      cVar9 = *param_8;
      while (cVar9 != '\0') {
        if (uVar16 < uVar17) {
          param_1[uVar16] = cVar9;
        }
        uVar16 = uVar16 + 1;
        cVar9 = param_8[uVar16];
      }
    }
    uVar21 = 0x405b35;
    local_b0 = func_0x00401d20(param_9);
    local_a0 = param_9;
    local_c8 = 1;
    break;
  default:
    uVar21 = func_0x00401a50();
    return uVar21;
  }
  uVar12 = 0;
LAB_0040555e:
  bVar18 = uVar12 != param_4;
  if (param_4 == 0xffffffffffffffff) goto LAB_0040572c;
LAB_00405570:
  if (bVar18 != false) {
LAB_00405579:
    if ((local_c0 == 2 || local_b0 == 0) || (local_c8 == 0)) {
LAB_00405cb0:
      bVar19 = false;
    }
    else {
      if ((param_4 == 0xffffffffffffffff) && (1 < local_b0)) {
        uVar21 = 0x4055cb;
        param_4 = func_0x00401d20(param_3);
      }
      if (param_4 < uVar12 + local_b0) goto LAB_00405cb0;
      uVar21 = 0x405619;
      iVar5 = func_0x00401b70(param_3 + uVar12,local_a0,local_b0);
      if (iVar5 == 0) {
        if (local_ba != 0) {
          uVar16 = (ulong)local_c0;
          local_c8 = 1;
          bVar18 = local_c0 == 2;
          goto LAB_00405c50;
        }
        bVar19 = true;
      }
      else {
        bVar19 = false;
      }
    }
    local_b8 = (byte *)(param_3 + uVar12);
    bVar13 = *local_b8;
    uVar15 = (ulong)bVar13;
    switch(bVar13) {
    case 0:
      if (local_c8 != 0) {
        if (local_ba == 0) {
          bVar20 = local_c0 == 2;
          if (local_b9 < bVar20) {
            if (uVar16 < uVar17) {
              param_1[uVar16] = 0x27;
            }
            if (uVar16 + 1 < uVar17) {
              param_1[uVar16 + 1] = 0x24;
            }
            if (uVar16 + 2 < uVar17) {
              param_1[uVar16 + 2] = 0x27;
            }
            uVar16 = uVar16 + 3;
            local_b9 = 1;
          }
          if (uVar16 < uVar17) {
            param_1[uVar16] = 0x5c;
          }
          uVar8 = uVar16 + 1;
          if (((local_c0 == 2) || (param_4 <= uVar12 + 1)) ||
             (9 < (byte)(*(char *)(param_3 + 1 + uVar12) - 0x30U))) {
            bVar2 = false;
            bVar10 = 1;
            uVar15 = 0x30;
            uVar16 = uVar8;
          }
          else {
            if (uVar8 < uVar17) {
              param_1[uVar8] = 0x30;
            }
            if (uVar16 + 2 < uVar17) {
              param_1[uVar16 + 2] = 0x30;
            }
            bVar2 = false;
            bVar10 = 1;
            uVar15 = 0x30;
            uVar16 = uVar16 + 3;
          }
          goto joined_r0x004057d6;
        }
        goto LAB_00405f48;
      }
      if ((param_6 & 1) == 0) goto LAB_00405898;
      uVar12 = uVar12 + 1;
      goto LAB_0040555e;
    default:
      if (lVar6 != 1) {
        local_48[0] = 0;
        if (param_4 == 0xffffffffffffffff) {
          param_4 = func_0x00401d20(param_3);
        }
        uVar15 = 0;
        bVar2 = true;
        do {
          uVar8 = uVar15 + uVar12;
          pcVar1 = (char *)(param_3 + uVar8);
          uVar21 = 0x405d3d;
          lVar7 = FUN_00408530(&local_4c,pcVar1,param_4 - uVar8);
          if (lVar7 == 0) break;
          if (lVar7 == -1) {
            bVar2 = false;
            break;
          }
          if (lVar7 == -2) {
            if ((param_4 <= uVar8) || (*pcVar1 == '\0')) goto LAB_004061e6;
            goto LAB_004061d6;
          }
          bVar18 = (bool)(local_c0 == 2 & local_ba);
          if ((bVar18 != false) && (lVar7 != 1)) {
            lVar11 = 1;
            do {
              if (((byte)(pcVar1[lVar11] + 0xa5U) < 0x22) &&
                 ((1 << (pcVar1[lVar11] + 0xa5U & 0x3f) & 0x20000002bU) != 0)) {
                uVar16 = 2;
                goto LAB_00405c50;
              }
              lVar11 = lVar11 + 1;
            } while (lVar11 != lVar7);
          }
          iVar5 = func_0x00401a90((ulong)local_4c);
          if (iVar5 == 0) {
            bVar2 = false;
          }
          uVar15 = uVar15 + lVar7;
          uVar21 = 0x405dd3;
          iVar5 = func_0x00401dc0(local_48);
        } while (iVar5 == 0);
        goto LAB_00405e00;
      }
      uVar15 = 1;
      bVar2 = (uint)bVar13 - 0x20 < 0x5f;
      goto LAB_00405a77;
    case 7:
      bVar14 = 0x61;
      break;
    case 8:
      bVar14 = 0x62;
      break;
    case 9:
      bVar14 = 0x74;
      goto LAB_00405805;
    case 10:
      bVar14 = 0x6e;
      goto LAB_00405805;
    case 0xb:
      bVar14 = 0x76;
      break;
    case 0xc:
      bVar14 = 0x66;
      break;
    case 0xd:
      bVar14 = 0x72;
LAB_00405805:
      bVar18 = (bool)(local_c0 == 2 & local_ba);
      if (bVar18 != false) {
LAB_00405c3b:
        uVar16 = 2;
        goto LAB_00405c50;
      }
      break;
    case 0x20:
      goto switchD_00405666_caseD_20;
    case 0x21:
    case 0x22:
    case 0x24:
    case 0x26:
    case 0x28:
    case 0x29:
    case 0x2a:
    case 0x3b:
    case 0x3c:
    case 0x3d:
    case 0x3e:
    case 0x5b:
    case 0x5e:
    case 0x60:
    case 0x7c:
      bVar2 = false;
      goto LAB_00405676;
    case 0x23:
    case 0x7e:
switchD_00405666_caseD_23:
      if (uVar12 != 0) goto LAB_004057c6;
switchD_00405666_caseD_20:
      bVar2 = true;
LAB_00405676:
      bVar20 = local_c0 == 2;
      bVar18 = (bool)(local_ba & bVar20);
      if (bVar18 == false) {
LAB_0040568d:
        bVar10 = 0;
        goto joined_r0x004057d6;
      }
      goto LAB_00405c3b;
    case 0x25:
    case 0x2b:
    case 0x2c:
    case 0x2d:
    case 0x2e:
    case 0x2f:
    case 0x30:
    case 0x31:
    case 0x32:
    case 0x33:
    case 0x34:
    case 0x35:
    case 0x36:
    case 0x37:
    case 0x38:
    case 0x39:
    case 0x3a:
    case 0x41:
    case 0x42:
    case 0x43:
    case 0x44:
    case 0x45:
    case 0x46:
    case 0x47:
    case 0x48:
    case 0x49:
    case 0x4a:
    case 0x4b:
    case 0x4c:
    case 0x4d:
    case 0x4e:
    case 0x4f:
    case 0x50:
    case 0x51:
    case 0x52:
    case 0x53:
    case 0x54:
    case 0x55:
    case 0x56:
    case 0x57:
    case 0x58:
    case 0x59:
    case 0x5a:
    case 0x5d:
    case 0x5f:
    case 0x61:
    case 0x62:
    case 99:
    case 100:
    case 0x65:
    case 0x66:
    case 0x67:
    case 0x68:
    case 0x69:
    case 0x6a:
    case 0x6b:
    case 0x6c:
    case 0x6d:
    case 0x6e:
    case 0x6f:
    case 0x70:
    case 0x71:
    case 0x72:
    case 0x73:
    case 0x74:
    case 0x75:
    case 0x76:
    case 0x77:
    case 0x78:
    case 0x79:
    case 0x7a:
      bVar20 = local_c0 == 2;
      bVar2 = true;
      bVar10 = 0;
      goto joined_r0x004057d6;
    case 0x27:
      if (local_c0 != 2) {
        bVar20 = false;
        bVar2 = true;
        bVar10 = 0;
        bVar3 = true;
        goto joined_r0x004057d6;
      }
      if (local_ba == 0) {
        if ((local_78 != 0) || (uVar8 = 0, uVar15 = uVar17, uVar17 == 0)) {
          if (uVar16 < uVar17) {
            param_1[uVar16] = 0x27;
          }
          if (uVar16 + 1 < uVar17) {
            param_1[uVar16 + 1] = 0x5c;
          }
          uVar8 = uVar17;
          uVar15 = local_78;
          if (uVar16 + 2 < uVar17) {
            param_1[uVar16 + 2] = 0x27;
            uVar8 = uVar17;
            uVar15 = local_78;
          }
        }
        uVar16 = uVar16 + 3;
        bVar2 = true;
        bVar3 = true;
        local_b9 = 0;
        bVar10 = 0;
        bVar14 = bVar13;
        local_78 = uVar15;
        goto LAB_004056c4;
      }
      goto LAB_00405c3b;
    case 0x3f:
      if (local_c0 == 2) {
        if (local_ba != 0) goto LAB_00405c3b;
        bVar2 = false;
        bVar10 = 0;
        uVar8 = uVar17;
        bVar14 = bVar13;
        goto LAB_004056c4;
      }
      if (local_c0 != 5) {
        bVar20 = local_c0 == 2;
        bVar2 = false;
        bVar10 = 0;
        goto joined_r0x004057d6;
      }
      if ((((param_6 & 4) != 0) && (uVar8 = uVar12 + 2, uVar8 < param_4)) &&
         (*(char *)(param_3 + 1 + uVar12) == '?')) {
        bVar13 = *(byte *)(param_3 + uVar8);
        bVar14 = bVar13 - 0x21;
        if ((bVar14 < 0x1e) && ((1 << (bVar14 & 0x3f) & 0x380051c1U) != 0)) {
          if (local_ba == 0) {
            if (uVar16 < uVar17) {
              param_1[uVar16] = 0x3f;
            }
            if (uVar16 + 1 < uVar17) {
              param_1[uVar16 + 1] = 0x22;
            }
            if (uVar16 + 2 < uVar17) {
              param_1[uVar16 + 2] = 0x22;
            }
            if (uVar16 + 3 < uVar17) {
              param_1[uVar16 + 3] = 0x3f;
            }
            bVar20 = false;
            bVar2 = false;
            bVar10 = 0;
            uVar12 = uVar8;
            uVar15 = (ulong)bVar13;
            uVar16 = uVar16 + 4;
            goto joined_r0x004057d6;
          }
          uVar16 = 5;
          goto LAB_00405c62;
        }
      }
      bVar20 = false;
      bVar2 = false;
      goto LAB_0040568d;
    case 0x5c:
      if (local_c0 == 2) {
        if (local_ba != 0) goto LAB_00405c3b;
      }
      else {
        bVar14 = bVar13;
        if (((local_ba & local_c8) == 0) || (bVar14 = bVar13, local_b0 == 0)) break;
      }
      uVar12 = uVar12 + 1;
      bVar2 = false;
      bVar10 = 0;
      bVar14 = bVar13;
      goto LAB_004056d1;
    case 0x7b:
    case 0x7d:
      bVar18 = param_4 != 1;
      if (param_4 == 0xffffffffffffffff) {
        bVar18 = *(char *)(param_3 + 1) != '\0';
      }
      if (!bVar18) goto switchD_00405666_caseD_23;
LAB_004057c6:
      bVar20 = local_c0 == 2;
      bVar2 = false;
      bVar10 = 0;
      goto joined_r0x004057d6;
    }
    if (local_c8 == 0) {
LAB_00405898:
      bVar2 = false;
      bVar10 = 0;
      goto joined_r0x004057e5;
    }
    bVar2 = false;
    goto LAB_00405824;
  }
LAB_00405742:
  bVar19 = local_c0 == 2;
  bVar18 = uVar16 == 0 && bVar19;
  if (uVar16 == 0 && bVar19) {
    if (local_ba != 0) {
      uVar16 = 2;
LAB_00405c50:
      if ((bVar18 != false) && (local_c8 != 0)) {
        uVar16 = 4;
      }
LAB_00405c62:
      uVar21 = FUN_00405480(param_1,uVar17,param_3,param_4,uVar16,
                            (ulong)((uint)param_6 & 0xfffffffd),0,param_8,param_9,uVar21);
      return uVar21;
    }
  }
  else {
    if (bVar19 <= local_ba) goto LAB_0040608d;
  }
  if (bVar3) {
    if (bVar4) {
      uVar21 = FUN_00405480(param_1,local_78,param_3,param_4,5,param_6 & 0xffffffff,param_7,param_8,
                            param_9);
      return uVar21;
    }
    local_ba = 0;
    if ((local_78 != 0) && (param_2 = local_78, uVar17 == 0)) goto LAB_00405511;
  }
  local_ba = 0;
LAB_0040608d:
  uVar12 = uVar16;
  if ((local_ba < (local_a0 != (char *)0x0)) && (cVar9 = *local_a0, uVar12 = uVar16, cVar9 != '\0'))
  {
    uVar12 = uVar16;
    do {
      if (uVar12 < uVar17) {
        param_1[uVar12] = cVar9;
      }
      uVar12 = uVar12 + 1;
      cVar9 = local_a0[uVar12 - uVar16];
    } while (cVar9 != '\0');
  }
  if (uVar12 < uVar17) {
    param_1[uVar12] = 0;
  }
  return uVar12;
  while (local_b8[uVar15] != 0) {
LAB_004061d6:
    uVar15 = uVar15 + 1;
    if (param_4 <= uVar12 + uVar15) break;
  }
LAB_004061e6:
  bVar2 = false;
LAB_00405e00:
  if (1 < uVar15) {
LAB_00405e0a:
    bVar10 = 0;
    uVar15 = uVar15 + uVar12;
    bVar14 = bVar13;
    do {
      uVar12 = uVar12 + 1;
      if (bVar2 < local_c8) {
        if (local_ba != 0) goto LAB_00405f48;
        if (local_b9 < (local_c0 == 2)) {
          if (uVar16 < uVar17) {
            param_1[uVar16] = 0x27;
          }
          if (uVar16 + 1 < uVar17) {
            param_1[uVar16 + 1] = 0x24;
          }
          if (uVar16 + 2 < uVar17) {
            param_1[uVar16 + 2] = 0x27;
          }
          uVar16 = uVar16 + 3;
          local_b9 = 1;
        }
        if (uVar16 < uVar17) {
          param_1[uVar16] = 0x5c;
        }
        if (uVar16 + 1 < uVar17) {
          param_1[uVar16 + 1] = (bVar14 >> 6) + 0x30;
        }
        if (uVar16 + 2 < uVar17) {
          param_1[uVar16 + 2] = (bVar14 >> 3 & 7) + 0x30;
        }
        uVar16 = uVar16 + 3;
        bVar14 = (bVar14 & 7) + 0x30;
        if (uVar15 <= uVar12) goto LAB_004056f9;
        bVar10 = 1;
      }
      else {
        if (bVar19) {
          if (uVar16 < uVar17) {
            param_1[uVar16] = 0x5c;
          }
          uVar16 = uVar16 + 1;
        }
        if (uVar15 <= uVar12) goto LAB_004056d1;
        bVar19 = false;
        if (bVar10 < local_b9) {
          if (uVar16 < uVar17) {
            param_1[uVar16] = 0x27;
          }
          if (uVar16 + 1 < uVar17) {
            param_1[uVar16 + 1] = 0x27;
          }
          uVar16 = uVar16 + 2;
          bVar19 = false;
          local_b9 = 0;
        }
      }
      if (uVar16 < uVar17) {
        param_1[uVar16] = bVar14;
      }
      bVar14 = *(byte *)(param_3 + uVar12);
      uVar16 = uVar16 + 1;
    } while( true );
  }
LAB_00405a77:
  if (bVar2 < local_c8) goto LAB_00405e0a;
  bVar20 = local_c0 == 2;
  bVar10 = 0;
  uVar15 = (ulong)bVar13;
joined_r0x004057d6:
  if (bVar20 < local_c8) {
LAB_00405698:
    bVar14 = (byte)uVar15;
    uVar8 = uVar17;
    if ((param_7 == 0) ||
       (uVar8 = uVar17, (*(uint *)(param_7 + (uVar15 >> 5) * 4) >> ((uint)uVar15 & 0x1f) & 1) == 0))
    goto LAB_004056c4;
  }
  else {
    bVar13 = (byte)uVar15;
joined_r0x004057e5:
    uVar8 = uVar17;
    bVar14 = bVar13;
    if (local_ba != 0) goto LAB_00405698;
LAB_004056c4:
    uVar17 = uVar8;
    if (!bVar19) {
      uVar12 = uVar12 + 1;
      uVar17 = uVar8;
LAB_004056d1:
      if (bVar10 < local_b9) {
        if (uVar16 < uVar17) {
          param_1[uVar16] = 0x27;
        }
        if (uVar16 + 1 < uVar17) {
          param_1[uVar16 + 1] = 0x27;
        }
        uVar16 = uVar16 + 2;
        local_b9 = 0;
      }
      goto LAB_004056f9;
    }
  }
LAB_00405824:
  if (local_ba != 0) {
LAB_00405f48:
    uVar16 = (ulong)local_c0;
    bVar18 = local_c0 == 2;
    goto LAB_00405c50;
  }
  if (local_b9 < (local_c0 == 2)) {
    if (uVar16 < uVar17) {
      param_1[uVar16] = 0x27;
    }
    if (uVar16 + 1 < uVar17) {
      param_1[uVar16 + 1] = 0x24;
    }
    if (uVar16 + 2 < uVar17) {
      param_1[uVar16 + 2] = 0x27;
    }
    uVar16 = uVar16 + 3;
    local_b9 = 1;
  }
  if (uVar16 < uVar17) {
    param_1[uVar16] = 0x5c;
  }
  uVar16 = uVar16 + 1;
  uVar12 = uVar12 + 1;
LAB_004056f9:
  if (uVar16 < uVar17) {
    param_1[uVar16] = bVar14;
  }
  uVar16 = uVar16 + 1;
  if (!bVar2) {
    bVar4 = false;
  }
  bVar18 = uVar12 != param_4;
  if (param_4 != 0xffffffffffffffff) goto LAB_00405570;
LAB_0040572c:
  bVar18 = *(char *)(param_3 + uVar12) != '\0';
  if (!bVar18) goto LAB_00405742;
  goto LAB_00405579;
}



undefined1 * FUN_00406330(uint uParm1,undefined8 uParm2,undefined8 uParm3,uint *puParm4)

{
  ulong *puVar1;
  undefined4 uVar2;
  uint uVar3;
  ulong uVar4;
  undefined4 *puVar5;
  ulong uVar6;
  undefined1 *puVar7;
  int iVar8;
  undefined8 *puVar9;
  undefined8 uVar10;
  
  uVar10 = 0x406351;
  puVar5 = (undefined4 *)func_0x00401cb0();
  uVar2 = *puVar5;
  if ((int)uParm1 < 0) {
    uVar10 = func_0x00401a50();
    return (undefined1 *)uVar10;
  }
  puVar9 = (undefined8 *)PTR_null_ARRAY_00614498;
  if (DAT_006144b0 <= (int)uParm1) {
    if (uParm1 == 0x7fffffff) {
                    // WARNING: Subroutine does not return
      FUN_00406a20();
    }
    iVar8 = uParm1 + 1;
    if (PTR_null_ARRAY_00614498 == null_ARRAY_006144a0) {
      puVar9 = (undefined8 *)FUN_004069e0(0,(long)iVar8 << 4);
      uVar10 = null_ARRAY_006144a0._0_8_;
      PTR_null_ARRAY_00614498 = (undefined *)puVar9;
      puVar9[1] = null_ARRAY_006144a0._8_8_;
      *puVar9 = uVar10;
    }
    else {
      puVar9 = (undefined8 *)FUN_004069e0(PTR_null_ARRAY_00614498,(long)iVar8 << 4);
      PTR_null_ARRAY_00614498 = (undefined *)puVar9;
    }
    uVar10 = 0x4063ca;
    FUN_00401bc0(puVar9 + (long)DAT_006144b0 * 2,0,(long)(iVar8 - DAT_006144b0) << 4,
                 (long)DAT_006144b0 * 0x10);
    DAT_006144b0 = iVar8;
  }
  uVar3 = puParm4[1];
  puVar1 = puVar9 + (ulong)uParm1 * 2;
  uVar4 = *puVar1;
  puVar7 = (undefined1 *)puVar1[1];
  uVar6 = FUN_00405480(puVar7,uVar4,uParm2,uParm3,(ulong)*puParm4,(ulong)(uVar3 | 1),puParm4 + 2,
                       *(undefined8 *)(puParm4 + 10),*(undefined8 *)(puParm4 + 0xc),uVar10);
  if (uVar4 <= uVar6) {
    uVar6 = uVar6 + 1;
    *puVar1 = uVar6;
    if (puVar7 != null_ARRAY_006145c0) {
      func_0x00401db0(puVar7);
    }
    uVar10 = 0x40644c;
    puVar7 = (undefined1 *)FUN_004069c0(uVar6);
    *(undefined1 **)(puVar1 + 1) = puVar7;
    FUN_00405480(puVar7,uVar6,uParm2,uParm3,(ulong)*puParm4,(ulong)(uVar3 | 1),puParm4 + 2,
                 *(undefined8 *)(puParm4 + 10),*(undefined8 *)(puParm4 + 0xc),uVar10);
  }
  puVar5 = (undefined4 *)func_0x00401cb0();
  *puVar5 = uVar2;
  return puVar7;
}



void FUN_004064e0(undefined8 uParm1,undefined8 uParm2,byte bParm3)

{
  uint uVar1;
  undefined8 local_48;
  undefined8 local_40;
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  undefined8 local_20;
  undefined8 local_18;
  
  local_48 = null_ARRAY_006146c0._0_8_;
  local_40 = null_ARRAY_006146c0._8_8_;
  local_38 = null_ARRAY_006146c0._16_8_;
  local_30 = null_ARRAY_006146c0._24_8_;
  local_28 = null_ARRAY_006146c0._32_8_;
  uVar1 = *(uint *)((long)&local_40 + (ulong)(bParm3 >> 5) * 4);
  local_20 = null_ARRAY_006146c0._40_8_;
  local_18 = null_ARRAY_006146c0._48_8_;
  *(uint *)((long)&local_40 + (ulong)(bParm3 >> 5) * 4) =
       ((uVar1 >> (bParm3 & 0x1f) ^ 1) & 1) << (bParm3 & 0x1f) ^ uVar1;
  FUN_00406330(0,uParm1,uParm2,&local_48);
  return;
}



void FUN_00406580(undefined8 uParm1)

{
  FUN_004064e0(uParm1,0xffffffffffffffff,0x3a);
  return;
}



void FUN_004065a0(uint uParm1,undefined8 uParm2,undefined8 uParm3)

{
  uint uVar1;
  undefined8 local_98;
  ulong local_90;
  undefined8 local_88;
  undefined8 local_80;
  undefined8 local_78;
  undefined8 local_70;
  undefined8 local_68;
  undefined8 local_58;
  ulong local_50;
  undefined8 local_48;
  undefined8 local_40;
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  
  FUN_00405340(&local_98);
  local_48 = local_88;
  local_58 = local_98;
  local_40 = local_80;
  uVar1 = (uint)(local_90 >> 0x20);
  local_38 = local_78;
  local_30 = local_70;
  local_50 = local_90 & 0xffffffff | (ulong)(uVar1 ^ ~uVar1 & 0x4000000) << 0x20;
  local_28 = local_68;
  FUN_00406330((ulong)uParm1,uParm3,0xffffffffffffffff,&local_58);
  return;
}



void FUN_00406630(undefined8 uParm1)

{
  FUN_00406330(0,uParm1,0xffffffffffffffff,null_ARRAY_00614460);
  return;
}



void FUN_00406650(undefined8 uParm1)

{
  FUN_00407680(uParm1,0,3);
  return;
}



// WARNING: Possible PIC construction at 0x00406686: Changing call to branch
// WARNING: Possible PIC construction at 0x004068a4: Changing call to branch
// WARNING: Possible PIC construction at 0x00406734: Changing call to branch
// WARNING: Possible PIC construction at 0x0040684e: Changing call to branch
// WARNING: Possible PIC construction at 0x004067e8: Changing call to branch
// WARNING: Removing unreachable block (ram,0x00406853)
// WARNING: Removing unreachable block (ram,0x00406739)
// WARNING: Removing unreachable block (ram,0x004068a9)
// WARNING: Removing unreachable block (ram,0x004067ed)

long FUN_00406660(undefined8 uParm1,long lParm2,undefined8 uParm3,undefined8 uParm4,long *plParm5,
                 undefined8 uParm6)

{
  long lVar1;
  undefined8 uVar2;
  char *pcVar3;
  
  if (lParm2 == 0) {
    func_0x00401ac0(uParm1,"%s %s\n");
    func_0x00401ac0(uParm1,"Copyright %s %d Free Software Foundation, Inc.",&DAT_004118ab,0x7e1);
    func_0x00401c90(
                    "\nLicense GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n\n"
                    ,1,0xcb,uParm1);
    switch(uParm6) {
    case 0:
      func_0x00401a50();
    case 1:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s.\n";
      break;
    case 2:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s and %s.\n";
      break;
    case 3:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s, %s, and %s.\n";
      break;
    case 4:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s, %s, %s,\nand %s.\n";
      break;
    case 5:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s, %s, %s,\n%s, and %s.\n";
      break;
    case 6:
      lVar1 = plParm5[4];
      func_0x00401ac0(uParm1,"Written by %s, %s, %s,\n%s, %s, and %s.\n",*plParm5,plParm5[1],
                      plParm5[2],plParm5[3]);
      return lVar1;
    case 7:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n";
      break;
    case 8:
      uVar2 = func_0x00401ac0(uParm1,"Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n",*plParm5,
                              plParm5[1],plParm5[2],plParm5[3]);
      return uVar2;
    case 9:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n";
      break;
    default:
      lParm2 = *plParm5;
      pcVar3 = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    }
  }
  else {
    pcVar3 = "%s (%s) %s\n";
  }
  uVar2 = FUN_006150f0(uParm1,pcVar3,lParm2);
  return uVar2;
}



void FUN_004068c0(void)

{
  uint uVar1;
  long *plVar2;
  long lVar3;
  uint *in_R8;
  long lVar4;
  long local_58 [11];
  
  lVar4 = 0;
  do {
    uVar1 = *in_R8;
    if (uVar1 < 0x30) {
      *in_R8 = uVar1 + 8;
      lVar3 = *(long *)((ulong)uVar1 + *(long *)(in_R8 + 4));
      local_58[lVar4] = lVar3;
    }
    else {
      plVar2 = *(long **)(in_R8 + 2);
      *(long **)(in_R8 + 2) = plVar2 + 1;
      lVar3 = *plVar2;
      local_58[lVar4] = lVar3;
    }
  } while ((lVar3 != 0) && (lVar4 = lVar4 + 1, lVar4 != 10));
  FUN_00406660();
  return;
}



void FUN_00406930(void)

{
  FUN_004068c0();
  return;
}



void FUN_004069c0(long lParm1)

{
  long lVar1;
  
  lVar1 = FUN_00408510();
  if ((lVar1 == 0) && (lParm1 != 0)) {
                    // WARNING: Subroutine does not return
    FUN_00406a20();
  }
  return;
}



long FUN_004069e0(long lParm1,long lParm2)

{
  long lVar1;
  
  if ((lParm2 == 0) && (lParm1 != 0)) {
    func_0x00401db0();
    return 0;
  }
  lVar1 = FUN_004085a0(lParm1,lParm2);
  if ((lVar1 == 0) && (lParm2 != 0)) {
                    // WARNING: Subroutine does not return
    FUN_00406a20();
  }
  return lVar1;
}



void FUN_00406a20(void)

{
  FUN_00407510((ulong)DAT_00614458,0,0x410592,"memory exhausted");
  func_0x00401a50();
  return;
}



ulong FUN_00406a50(undefined8 param_1,ulong param_2,ulong param_3,ulong param_4,undefined8 param_5,
                  undefined8 param_6,uint param_7)

{
  int iVar1;
  undefined8 uVar2;
  int *piVar3;
  uint *puVar4;
  undefined4 *puVar5;
  ulong uVar6;
  ulong uVar7;
  ulong local_30 [2];
  
  iVar1 = FUN_00406fc0(param_1,0,param_2 & 0xffffffff,local_30);
  if (iVar1 == 0) {
    if ((param_3 <= local_30[0]) && (local_30[0] <= param_4)) {
      return local_30[0];
    }
    if (local_30[0] < 0x40000000) {
      puVar5 = (undefined4 *)func_0x00401cb0();
      *puVar5 = 0x22;
      goto LAB_00406aa2;
    }
  }
  else {
    if (iVar1 != 1) {
      if (iVar1 == 3) {
        puVar5 = (undefined4 *)func_0x00401cb0();
        *puVar5 = 0;
      }
      goto LAB_00406aa2;
    }
  }
  puVar5 = (undefined4 *)func_0x00401cb0();
  *puVar5 = 0x4b;
LAB_00406aa2:
  uVar2 = FUN_00406630(param_1);
  piVar3 = (int *)func_0x00401cb0();
  uVar6 = 0;
  if (*piVar3 != 0x16) {
    puVar4 = (uint *)func_0x00401cb0();
    uVar6 = (ulong)*puVar4;
  }
  uVar7 = 1;
  if (param_7 != 0) {
    uVar7 = (ulong)param_7;
  }
  FUN_00407510(uVar7,uVar6,"%s: %s",param_6,uVar2);
  return local_30[0];
}



void FUN_00406b30(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3,undefined8 uParm4,
                 undefined8 uParm5,undefined8 uParm6)

{
  FUN_00406a50(uParm1,10,uParm2,uParm3,uParm4,uParm5,uParm6);
  return;
}



ulong FUN_00406b60(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5)

{
  byte bVar1;
  byte bVar2;
  uint uVar3;
  int iVar4;
  undefined4 *puVar5;
  ulong uVar6;
  int *piVar7;
  long lVar8;
  ulong uVar9;
  uint uVar10;
  byte *pbVar11;
  uint uVar12;
  int iVar13;
  int iVar14;
  bool bVar15;
  byte *local_40 [2];
  
  if (0x24 < uParm3) {
    func_0x00401be0("0 <= strtol_base && strtol_base <= 36","lib/xstrtol.c",0x60);
  }
  if (ppbParm2 == (byte **)0x0) {
    ppbParm2 = local_40;
  }
  puVar5 = (undefined4 *)func_0x00401cb0();
  *puVar5 = 0;
  bVar1 = *pbParm1;
  pbVar11 = pbParm1;
  while (((uint)bVar1 - 9 < 5 || (bVar1 == 0x20))) {
    pbVar11 = pbVar11 + 1;
    bVar1 = *pbVar11;
  }
  if (bVar1 == 0x2d) {
    return 4;
  }
  uVar6 = func_0x00401a20(pbParm1,ppbParm2,(ulong)uParm3);
  pbVar11 = *ppbParm2;
  if (pbVar11 == pbParm1) {
    if ((lParm5 == 0) || (bVar1 = *pbParm1, bVar1 == 0)) {
      return 4;
    }
    uVar12 = 0;
    uVar6 = 1;
    lVar8 = func_0x00401d40(lParm5,(ulong)(uint)(int)(char)bVar1);
    if (lVar8 == 0) {
      return 4;
    }
  }
  else {
    piVar7 = (int *)func_0x00401cb0();
    uVar9 = 0;
    if (*piVar7 != 0) {
      piVar7 = (int *)func_0x00401cb0();
      if (*piVar7 != 0x22) {
        return 4;
      }
      uVar9 = 1;
    }
    uVar12 = (uint)uVar9;
    if (lParm5 == 0) goto LAB_00406c29;
    pbVar11 = *ppbParm2;
    bVar1 = *pbVar11;
    if (bVar1 == 0) goto LAB_00406c29;
    lVar8 = func_0x00401d40(lParm5,(ulong)(uint)(int)(char)bVar1);
    if (lVar8 == 0) goto switchD_00406d39_caseD_43;
  }
  if ((((byte)((uint)bVar1 - 0x45) < 0x30) &&
      ((0x814400308945U >> ((ulong)((uint)bVar1 - 0x45) & 0x1f) & 1) != 0)) &&
     (lVar8 = func_0x00401d40(lParm5), lVar8 != 0)) {
    bVar2 = pbVar11[1];
    if (bVar2 != 0x44) {
      if (bVar2 == 0x69) {
        iVar4 = 0x400;
        iVar13 = (uint)(pbVar11[2] == 0x42) + 1 + (uint)(pbVar11[2] == 0x42);
        goto LAB_00406d2a;
      }
      if (bVar2 != 0x42) goto LAB_00406d20;
    }
    iVar13 = 2;
    iVar4 = 1000;
  }
  else {
LAB_00406d20:
    iVar13 = 1;
    iVar4 = 0x400;
  }
LAB_00406d2a:
  switch(bVar1) {
  case 0x42:
    if (uVar6 < 0x40000000000000) {
      uVar6 = uVar6 << 10;
      uVar10 = 0;
    }
    else {
LAB_00406f6e:
      uVar10 = 1;
      uVar6 = 0xffffffffffffffff;
    }
    break;
  default:
switchD_00406d39_caseD_43:
    *puParm4 = uVar6;
    return (ulong)(uVar12 | 2);
  case 0x45:
    iVar14 = 6;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x47:
  case 0x67:
    iVar14 = 3;
    uVar10 = 0;
    do {
      bVar15 = SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                      ZEXT816((ulong)(long)iVar4),0) < uVar6;
      if (bVar15) {
        uVar6 = 0xffffffffffffffff;
      }
      else {
        uVar6 = uVar6 * (long)iVar4;
      }
      uVar10 = uVar10 | (uint)bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x4b:
  case 0x6b:
    if (SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) / ZEXT816((ulong)(long)iVar4),0) <
        uVar6) {
      uVar6 = 0xffffffffffffffff;
      uVar10 = 1;
    }
    else {
LAB_00406ea9:
      uVar10 = 0;
      uVar6 = uVar6 * (long)iVar4;
    }
    break;
  case 0x4d:
  case 0x6d:
    uVar9 = SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) / ZEXT816((ulong)(long)iVar4),
                   0);
    if ((uVar6 <= uVar9) && (uVar6 = uVar6 * (long)iVar4, uVar6 <= uVar9)) goto LAB_00406ea9;
    uVar10 = 1;
    uVar6 = 0xffffffffffffffff;
    break;
  case 0x50:
    iVar14 = 5;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x54:
  case 0x74:
    iVar14 = 4;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x59:
    iVar14 = 8;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x5a:
    iVar14 = 7;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x62:
    if (0x7fffffffffffff < uVar6) goto LAB_00406f6e;
    uVar6 = uVar6 << 9;
    uVar10 = 0;
    break;
  case 99:
    uVar10 = 0;
    break;
  case 0x77:
    if ((long)uVar6 < 0) goto LAB_00406f6e;
    uVar10 = 0;
    uVar6 = uVar6 * 2;
  }
  *ppbParm2 = pbVar11 + (long)iVar13;
  uVar3 = uVar12 | uVar10;
  if (pbVar11[(long)iVar13] != 0) {
    uVar3 = uVar12 | uVar10 | 2;
  }
  uVar9 = (ulong)uVar3;
LAB_00406c29:
  *puParm4 = uVar6;
  return uVar9;
}



ulong FUN_00406fc0(byte *pbParm1,byte **ppbParm2,uint uParm3,ulong *puParm4,long lParm5)

{
  byte bVar1;
  byte bVar2;
  uint uVar3;
  int iVar4;
  undefined4 *puVar5;
  ulong uVar6;
  int *piVar7;
  long lVar8;
  ulong uVar9;
  uint uVar10;
  byte *pbVar11;
  uint uVar12;
  int iVar13;
  int iVar14;
  bool bVar15;
  byte *local_40 [2];
  
  if (0x24 < uParm3) {
    func_0x00401be0("0 <= strtol_base && strtol_base <= 36","lib/xstrtol.c",0x60);
  }
  if (ppbParm2 == (byte **)0x0) {
    ppbParm2 = local_40;
  }
  puVar5 = (undefined4 *)func_0x00401cb0();
  *puVar5 = 0;
  bVar1 = *pbParm1;
  pbVar11 = pbParm1;
  while (((uint)bVar1 - 9 < 5 || (bVar1 == 0x20))) {
    pbVar11 = pbVar11 + 1;
    bVar1 = *pbVar11;
  }
  if (bVar1 == 0x2d) {
    return 4;
  }
  uVar6 = func_0x00401a10(pbParm1,ppbParm2,(ulong)uParm3);
  pbVar11 = *ppbParm2;
  if (pbVar11 == pbParm1) {
    if ((lParm5 == 0) || (bVar1 = *pbParm1, bVar1 == 0)) {
      return 4;
    }
    uVar12 = 0;
    uVar6 = 1;
    lVar8 = func_0x00401d40(lParm5,(ulong)(uint)(int)(char)bVar1);
    if (lVar8 == 0) {
      return 4;
    }
  }
  else {
    piVar7 = (int *)func_0x00401cb0();
    uVar9 = 0;
    if (*piVar7 != 0) {
      piVar7 = (int *)func_0x00401cb0();
      if (*piVar7 != 0x22) {
        return 4;
      }
      uVar9 = 1;
    }
    uVar12 = (uint)uVar9;
    if (lParm5 == 0) goto LAB_00407089;
    pbVar11 = *ppbParm2;
    bVar1 = *pbVar11;
    if (bVar1 == 0) goto LAB_00407089;
    lVar8 = func_0x00401d40(lParm5,(ulong)(uint)(int)(char)bVar1);
    if (lVar8 == 0) goto switchD_00407199_caseD_43;
  }
  if ((((byte)((uint)bVar1 - 0x45) < 0x30) &&
      ((0x814400308945U >> ((ulong)((uint)bVar1 - 0x45) & 0x1f) & 1) != 0)) &&
     (lVar8 = func_0x00401d40(lParm5), lVar8 != 0)) {
    bVar2 = pbVar11[1];
    if (bVar2 != 0x44) {
      if (bVar2 == 0x69) {
        iVar4 = 0x400;
        iVar13 = (uint)(pbVar11[2] == 0x42) + 1 + (uint)(pbVar11[2] == 0x42);
        goto LAB_0040718a;
      }
      if (bVar2 != 0x42) goto LAB_00407180;
    }
    iVar13 = 2;
    iVar4 = 1000;
  }
  else {
LAB_00407180:
    iVar13 = 1;
    iVar4 = 0x400;
  }
LAB_0040718a:
  switch(bVar1) {
  case 0x42:
    if (uVar6 < 0x40000000000000) {
      uVar6 = uVar6 << 10;
      uVar10 = 0;
    }
    else {
LAB_004073ce:
      uVar10 = 1;
      uVar6 = 0xffffffffffffffff;
    }
    break;
  default:
switchD_00407199_caseD_43:
    *puParm4 = uVar6;
    return (ulong)(uVar12 | 2);
  case 0x45:
    iVar14 = 6;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x47:
  case 0x67:
    iVar14 = 3;
    uVar10 = 0;
    do {
      bVar15 = SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                      ZEXT816((ulong)(long)iVar4),0) < uVar6;
      if (bVar15) {
        uVar6 = 0xffffffffffffffff;
      }
      else {
        uVar6 = uVar6 * (long)iVar4;
      }
      uVar10 = uVar10 | (uint)bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x4b:
  case 0x6b:
    if (SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) / ZEXT816((ulong)(long)iVar4),0) <
        uVar6) {
      uVar6 = 0xffffffffffffffff;
      uVar10 = 1;
    }
    else {
LAB_00407309:
      uVar10 = 0;
      uVar6 = uVar6 * (long)iVar4;
    }
    break;
  case 0x4d:
  case 0x6d:
    uVar9 = SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) / ZEXT816((ulong)(long)iVar4),
                   0);
    if ((uVar6 <= uVar9) && (uVar6 = uVar6 * (long)iVar4, uVar6 <= uVar9)) goto LAB_00407309;
    uVar10 = 1;
    uVar6 = 0xffffffffffffffff;
    break;
  case 0x50:
    iVar14 = 5;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x54:
  case 0x74:
    iVar14 = 4;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x59:
    iVar14 = 8;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x5a:
    iVar14 = 7;
    uVar10 = 0;
    do {
      bVar15 = uVar6 <= SUB168((ZEXT816(0) << 0x40 | ZEXT816(0xffffffffffffffff)) /
                               ZEXT816((ulong)(long)iVar4),0);
      if (bVar15) {
        uVar6 = uVar6 * (long)iVar4;
      }
      else {
        uVar6 = 0xffffffffffffffff;
      }
      uVar10 = uVar10 | (uint)!bVar15;
      iVar14 = iVar14 + -1;
    } while (iVar14 != 0);
    break;
  case 0x62:
    if (0x7fffffffffffff < uVar6) goto LAB_004073ce;
    uVar6 = uVar6 << 9;
    uVar10 = 0;
    break;
  case 99:
    uVar10 = 0;
    break;
  case 0x77:
    if ((long)uVar6 < 0) goto LAB_004073ce;
    uVar10 = 0;
    uVar6 = uVar6 * 2;
  }
  *ppbParm2 = pbVar11 + (long)iVar13;
  uVar3 = uVar12 | uVar10;
  if (pbVar11[(long)iVar13] != 0) {
    uVar3 = uVar12 | uVar10 | 2;
  }
  uVar9 = (ulong)uVar3;
LAB_00407089:
  *puParm4 = uVar6;
  return uVar9;
}



void FUN_00407420(undefined8 uParm1)

{
  int iVar1;
  char *pcVar2;
  char acStack1032 [1024];
  
  iVar1 = func_0x00401c10(uParm1,acStack1032,0x400);
  pcVar2 = "Unknown system error";
  if (iVar1 == 0) {
    pcVar2 = acStack1032;
  }
  func_0x00401ac0(DAT_006144e0,0x410590,pcVar2);
  return;
}



// WARNING: Possible PIC construction at 0x00407499: Changing call to branch
// WARNING: Removing unreachable block (ram,0x0040749e)
// WARNING: Removing unreachable block (ram,0x004074b9)
// WARNING: Removing unreachable block (ram,0x00407500)
// WARNING: Removing unreachable block (ram,0x004074e9)
// WARNING: Removing unreachable block (ram,0x004074a3)

void FUN_00407460(undefined8 uParm1,uint uParm2,undefined8 uParm3,undefined8 uParm4)

{
  undefined8 uVar1;
  
  uVar1 = DAT_006144e0;
  FUN_004085e0(DAT_006144e0,uParm3,uParm4);
  DAT_00614740 = DAT_00614740 + 1;
  if (uParm2 != 0) {
    FUN_00407420((ulong)uParm2);
  }
  func_0x00401900(10,uVar1);
  FUN_006151d8(uVar1);
  return;
}



void FUN_004074d0(void)

{
  int iVar1;
  
  iVar1 = func_0x00401d50(1,3);
  if (-1 < iVar1) {
    FUN_006151d8(DAT_006144c0);
    return;
  }
  return;
}



void FUN_00407510(undefined4 uParm1,undefined4 uParm2,undefined4 uParm3,undefined4 uParm4,
                 undefined4 uParm5,undefined4 uParm6,undefined4 uParm7,undefined4 uParm8,
                 ulong param_1,ulong param_2,undefined8 param_3,undefined8 param_4,
                 undefined8 param_5,undefined8 param_6)

{
  char in_AL;
  undefined8 uVar1;
  undefined4 local_e0;
  undefined4 local_dc;
  undefined *local_d8;
  undefined *local_d0;
  undefined local_c8 [24];
  undefined8 local_b0;
  undefined8 local_a8;
  undefined8 local_a0;
  undefined4 local_98;
  undefined4 local_88;
  undefined4 local_78;
  undefined4 local_68;
  undefined4 local_58;
  undefined4 local_48;
  undefined4 local_38;
  undefined4 local_28;
  
  if (in_AL != '\0') {
    local_98 = uParm1;
    local_88 = uParm2;
    local_78 = uParm3;
    local_68 = uParm4;
    local_58 = uParm5;
    local_48 = uParm6;
    local_38 = uParm7;
    local_28 = uParm8;
  }
  local_b0 = param_4;
  local_a8 = param_5;
  local_a0 = param_6;
  FUN_004074d0();
  if (DAT_00614748 == (code *)0x0) {
    uVar1 = FUN_00408720();
    func_0x00401ac0(DAT_006144e0,&DAT_00412008,uVar1);
  }
  else {
    (*DAT_00614748)();
  }
  local_d8 = &stack0x00000008;
  local_e0 = 0x18;
  local_d0 = local_c8;
  local_dc = 0x30;
  FUN_00407460(param_1 & 0xffffffff,param_2 & 0xffffffff,param_3,&local_e0);
  return;
}



// WARNING: Possible PIC construction at 0x00407662: Changing call to branch
// WARNING: Removing unreachable block (ram,0x00407667)
// WARNING: Removing unreachable block (ram,0x0040766b)
// WARNING: Removing unreachable block (ram,0x00407677)

void FUN_004075f0(undefined8 uParm1)

{
  long lVar1;
  int iVar2;
  uint uVar3;
  
  iVar2 = func_0x00401cf0();
  if (-1 < iVar2) {
    iVar2 = func_0x00401a70(uParm1);
    if (iVar2 == 0) {
      iVar2 = FUN_004077c0(uParm1);
    }
    else {
      uVar3 = func_0x00401cf0(uParm1);
      lVar1 = func_0x00401a40((ulong)uVar3,0,1);
      if (lVar1 == -1) goto SUB_00401bd0;
      iVar2 = FUN_004077c0(uParm1);
    }
    if (iVar2 != 0) {
      func_0x00401cb0();
    }
  }
SUB_00401bd0:
  FUN_00615180(uParm1);
  return;
}



// WARNING: Restarted to delay deadcode elimination for space: stack

ulong FUN_00407680(ulong param_1,undefined8 param_2,ulong param_3)

{
  undefined4 uVar1;
  uint uVar2;
  int iVar3;
  undefined4 *puVar4;
  int *piVar5;
  ulong uVar6;
  ulong uVar7;
  
  if ((int)param_2 == 0x406) {
    if (DAT_006146f8 < 0) {
      uVar2 = FUN_00407680(param_1,0,param_3 & 0xffffffff);
      uVar6 = (ulong)uVar2;
      if ((int)uVar2 < 0) {
        return uVar6;
      }
      if (DAT_006146f8 != -1) {
        return uVar6;
      }
    }
    else {
      uVar2 = func_0x00401d50();
      if (-1 < (int)uVar2) {
        DAT_006146f8 = 1;
        return (ulong)uVar2;
      }
      piVar5 = (int *)func_0x00401cb0();
      if (*piVar5 != 0x16) {
        DAT_006146f8 = 1;
        return (ulong)uVar2;
      }
      uVar2 = FUN_00407680(param_1 & 0xffffffff,0,param_3 & 0xffffffff);
      uVar6 = (ulong)uVar2;
      if ((int)uVar2 < 0) {
        return uVar6;
      }
      DAT_006146f8 = -1;
    }
    uVar2 = func_0x00401d50(uVar6,1);
    if ((-1 < (int)uVar2) && (iVar3 = func_0x00401d50(uVar6,2,(ulong)(uVar2 | 1)), iVar3 != -1)) {
      return uVar6;
    }
    puVar4 = (undefined4 *)func_0x00401cb0();
    uVar1 = *puVar4;
    uVar7 = 0xffffffff;
    func_0x00401da0(uVar6);
    puVar4 = (undefined4 *)func_0x00401cb0();
    *puVar4 = uVar1;
  }
  else {
    uVar2 = func_0x00401d50(param_1,param_2,param_3);
    uVar7 = (ulong)uVar2;
  }
  return uVar7;
}



ulong FUN_004077c0(long lParm1)

{
  int iVar1;
  uint uVar2;
  undefined8 uVar3;
  long lVar4;
  ulong uVar5;
  undefined4 *puVar6;
  
  if (lParm1 != 0) {
    iVar1 = func_0x00401a70();
    if (iVar1 != 0) {
      lVar4 = func_0x00401b30(lParm1);
      if (lVar4 == -1) {
        puVar6 = (undefined4 *)func_0x00401cb0();
        *puVar6 = 9;
        uVar5 = 0xffffffff;
      }
      else {
        FUN_00407860(lParm1,0,1);
        uVar5 = FUN_00407850(lParm1);
        if ((int)uVar5 == 0) {
          uVar2 = func_0x00401cf0(lParm1);
          lVar4 = func_0x00401a40((ulong)uVar2,lVar4,0);
          uVar5 = (ulong)-(uint)(lVar4 == -1);
        }
      }
      return uVar5;
    }
  }
  uVar3 = FUN_006150a8(lParm1);
  return uVar3;
}



undefined8 FUN_00407850(void)

{
  func_0x004019c0();
  return 0;
}



void FUN_00407860(void)

{
  FUN_00615108();
  return;
}



void FUN_00407870(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3)

{
  FUN_00408b90(uParm1,uParm2,10,uParm3);
  return;
}



void FUN_00407880(long lParm1,int *piParm2)

{
  long lVar1;
  long lVar2;
  int iVar3;
  int iVar4;
  int iVar5;
  undefined8 uVar6;
  long lVar7;
  long lVar8;
  int iVar9;
  int iVar10;
  int iVar11;
  int iVar12;
  
  iVar3 = piParm2[0xc];
  iVar4 = piParm2[0xb];
  iVar5 = *piParm2;
  lVar1 = lParm1 + (long)iVar3 * 8;
  iVar10 = iVar4;
  iVar9 = iVar5;
  while (iVar10 < iVar3) {
    while( true ) {
      if (iVar9 <= iVar3) goto LAB_004078f1;
      iVar11 = iVar9 - iVar3;
      iVar12 = iVar3 - iVar10;
      if (iVar12 < iVar11) break;
      if (0 < iVar11) {
        lVar2 = lParm1 + (long)iVar10 * 8;
        lVar7 = 0;
        do {
          uVar6 = *(undefined8 *)(lVar2 + lVar7 * 8);
          *(undefined8 *)(lVar2 + lVar7 * 8) = *(undefined8 *)(lVar1 + lVar7 * 8);
          *(undefined8 *)(lVar1 + lVar7 * 8) = uVar6;
          lVar7 = lVar7 + 1;
        } while ((int)lVar7 < iVar11);
      }
      iVar10 = iVar10 + iVar11;
      if (iVar3 <= iVar10) goto LAB_004078f1;
    }
    if (0 < iVar12) {
      lVar2 = lParm1 + (long)iVar10 * 8;
      lVar7 = lParm1 + (long)((iVar10 - iVar3) + iVar9) * 8;
      lVar8 = 0;
      do {
        uVar6 = *(undefined8 *)(lVar2 + lVar8 * 8);
        *(undefined8 *)(lVar2 + lVar8 * 8) = *(undefined8 *)(lVar7 + lVar8 * 8);
        *(undefined8 *)(lVar7 + lVar8 * 8) = uVar6;
        lVar8 = lVar8 + 1;
      } while ((int)lVar8 < iVar12);
    }
    iVar9 = iVar9 - iVar12;
  }
LAB_004078f1:
  piParm2[0xc] = iVar5;
  piParm2[0xb] = iVar4 + (iVar5 - iVar3);
  return;
}



ulong FUN_00407960(int param_1,undefined8 *param_2,char *param_3,long *param_4,int *param_5,
                  int param_6,int *param_7,int param_8,undefined8 param_9)

{
  undefined8 uVar1;
  bool bVar2;
  bool bVar3;
  int iVar4;
  char *pcVar5;
  long lVar6;
  long *plVar7;
  int iVar8;
  char *pcVar9;
  char *pcVar10;
  long lVar11;
  long lVar12;
  long *plVar13;
  char *local_78;
  int local_48;
  
  pcVar9 = *(char **)(param_7 + 8);
  if ((*pcVar9 == '=') || (local_78 = pcVar9, *pcVar9 == '\0')) {
    pcVar10 = (char *)0x0;
    local_78 = pcVar9;
  }
  else {
    do {
      local_78 = local_78 + 1;
      if (*local_78 == '=') break;
    } while (*local_78 != '\0');
    pcVar10 = local_78 + -(long)pcVar9;
  }
  lVar11 = *param_4;
  if (lVar11 != 0) {
    iVar8 = 0;
    plVar7 = param_4;
    lVar6 = lVar11;
    do {
      iVar4 = func_0x00401b40(lVar6,pcVar9,pcVar10);
      if ((iVar4 == 0) &&
         (pcVar5 = (char *)func_0x00401d20(lVar6), local_48 = iVar8, pcVar5 == pcVar10))
      goto LAB_00407ab8;
      plVar7 = plVar7 + 4;
      lVar6 = *plVar7;
      iVar8 = iVar8 + 1;
    } while (lVar6 != 0);
    lVar12 = 0;
    local_48 = -1;
    bVar3 = false;
    bVar2 = false;
    lVar6 = 0;
    plVar7 = (long *)0x0;
    plVar13 = param_4;
    do {
      iVar4 = func_0x00401b40(lVar11,pcVar9,pcVar10);
      if (iVar4 == 0) {
        if (plVar7 == (long *)0x0) {
          local_48 = (int)lVar12;
          plVar7 = plVar13;
          goto LAB_00407a85;
        }
        if (((((param_6 == 0) && (*(int *)(plVar7 + 1) == *(int *)(plVar13 + 1))) &&
             (plVar7[2] == plVar13[2])) && (*(int *)(plVar7 + 3) == *(int *)(plVar13 + 3))) ||
           (bVar3)) goto LAB_00407a85;
        if (param_8 == 0) {
          bVar3 = true;
          if (lVar6 == 0) goto LAB_00407a85;
        }
        else {
          if (lVar6 == 0) {
            lVar6 = FUN_00408510((long)iVar8);
            if (lVar6 == 0) {
              bVar3 = true;
              goto LAB_00407a85;
            }
            FUN_00401bc0(lVar6,0,(long)iVar8);
            bVar2 = true;
            *(undefined *)(lVar6 + (long)local_48) = 1;
          }
        }
        *(undefined *)(lVar6 + lVar12) = 1;
        lVar11 = plVar13[4];
        uVar1 = DAT_006144e0;
      }
      else {
LAB_00407a85:
        lVar11 = plVar13[4];
        uVar1 = DAT_006144e0;
      }
      if (lVar11 == 0) goto LAB_00407bc3;
      plVar13 = plVar13 + 4;
      lVar12 = lVar12 + 1;
      pcVar9 = *(char **)(param_7 + 8);
      DAT_006144e0 = uVar1;
    } while( true );
  }
LAB_00407cb1:
  if (((param_6 != 0) && (*(char *)(param_2[(long)*param_7] + 1) != '-')) &&
     (lVar11 = func_0x00401d40(param_3,(ulong)(uint)(int)**(char **)(param_7 + 8)), lVar11 != 0)) {
    return 0xffffffff;
  }
  if (param_8 != 0) {
    func_0x00401ac0(DAT_006144e0,"%s: unrecognized option \'%s%s\'\n",*param_2,param_9,
                    *(undefined8 *)(param_7 + 8));
  }
  *(undefined8 *)(param_7 + 8) = 0;
  *param_7 = *param_7 + 1;
  param_7[2] = 0;
  return 0x3f;
LAB_00407bc3:
  if ((bVar3) || (lVar6 != 0)) {
    DAT_006144e0 = uVar1;
    if (param_8 != 0) {
      if (bVar3) {
        DAT_006144e0 = uVar1;
        func_0x00401ac0(uVar1,"%s: option \'%s%s\' is ambiguous\n",*param_2,param_9,
                        *(undefined8 *)(param_7 + 8));
      }
      else {
        lVar11 = 0;
        DAT_006144e0 = uVar1;
        func_0x00401c30(uVar1);
        func_0x00401ac0(uVar1,"%s: option \'%s%s\' is ambiguous; possibilities:",*param_2,param_9,
                        *(undefined8 *)(param_7 + 8));
        do {
          if (*(char *)(lVar6 + lVar11) != '\0') {
            func_0x00401ac0(uVar1,&DAT_00412111,param_9,*param_4);
          }
          lVar11 = lVar11 + 1;
          param_4 = param_4 + 4;
        } while ((int)lVar11 < iVar8);
        func_0x00401c60(10,uVar1);
        func_0x00401b50(uVar1);
      }
    }
    if (bVar2) {
      func_0x00401db0(lVar6);
    }
    lVar11 = *(long *)(param_7 + 8);
    lVar6 = func_0x00401d20(lVar11);
    *(long *)(param_7 + 8) = lVar6 + lVar11;
    *param_7 = *param_7 + 1;
    param_7[2] = 0;
    return 0x3f;
  }
  DAT_006144e0 = uVar1;
  if (plVar7 != (long *)0x0) {
LAB_00407ab8:
    iVar4 = *param_7;
    *(undefined8 *)(param_7 + 8) = 0;
    iVar8 = iVar4 + 1;
    *param_7 = iVar8;
    if (*local_78 == '\0') {
      if (*(int *)(plVar7 + 1) == 1) {
        if (param_1 <= iVar8) {
          if (param_8 != 0) {
            func_0x00401ac0(DAT_006144e0,"%s: option \'%s%s\' requires an argument\n",*param_2,
                            param_9,*plVar7);
          }
          param_7[2] = *(int *)(plVar7 + 3);
          return (ulong)(*param_3 != ':') * 5 + 0x3a;
        }
        *param_7 = iVar4 + 2;
        *(undefined8 *)(param_7 + 4) = param_2[(long)iVar8];
      }
    }
    else {
      if (*(int *)(plVar7 + 1) == 0) {
        if (param_8 != 0) {
          func_0x00401ac0(DAT_006144e0,"%s: option \'%s%s\' doesn\'t allow an argument\n",*param_2,
                          param_9,*plVar7);
        }
        param_7[2] = *(int *)(plVar7 + 3);
        return 0x3f;
      }
      *(char **)(param_7 + 4) = local_78 + 1;
    }
    if (param_5 != (int *)0x0) {
      *param_5 = local_48;
    }
    if ((undefined4 *)plVar7[2] != (undefined4 *)0x0) {
      *(undefined4 *)plVar7[2] = *(undefined4 *)(plVar7 + 3);
      return 0;
    }
    return (ulong)*(uint *)(plVar7 + 3);
  }
  goto LAB_00407cb1;
}



ulong FUN_00407f20(uint param_1,undefined8 *param_2,char *param_3,long param_4,undefined8 param_5,
                  ulong param_6,uint *param_7,int param_8)

{
  undefined8 uVar1;
  char cVar2;
  uint uVar3;
  long lVar4;
  char *pcVar5;
  char **ppcVar6;
  char *pcVar7;
  char *pcVar8;
  uint uVar9;
  int iVar10;
  ulong uVar11;
  bool bVar12;
  byte bVar13;
  undefined1 *puVar14;
  
  bVar13 = 0;
  uVar11 = (ulong)param_7[1];
  if ((int)param_1 < 1) {
    return 0xffffffff;
  }
  uVar9 = *param_7;
  *(undefined8 *)(param_7 + 4) = 0;
  if (uVar9 == 0) {
    *param_7 = 1;
    uVar9 = 1;
LAB_00407f6e:
    param_7[0xc] = uVar9;
    param_7[0xb] = uVar9;
    *(undefined8 *)(param_7 + 8) = 0;
    if (*param_3 == '-') {
      param_7[10] = 2;
      param_3 = param_3 + 1;
      pcVar5 = (char *)0x0;
    }
    else {
      if (*param_3 == '+') {
        param_7[10] = 0;
        param_3 = param_3 + 1;
        pcVar5 = (char *)0x0;
      }
      else {
        pcVar5 = (char *)0x0;
        if (param_8 == 0) {
          lVar4 = func_0x00401950("POSIXLY_CORRECT");
          param_6 = param_6 & 0xffffffff;
          if (lVar4 == 0) {
            param_7[10] = 1;
            pcVar5 = *(char **)(param_7 + 8);
            goto LAB_00407fa6;
          }
          pcVar5 = *(char **)(param_7 + 8);
        }
        param_7[10] = 0;
      }
    }
LAB_00407fa6:
    param_7[6] = 1;
    cVar2 = *param_3;
LAB_00407fb2:
    if (cVar2 == ':') {
      uVar11 = 0;
    }
    if (pcVar5 == (char *)0x0) goto LAB_004080b0;
LAB_00407fc6:
    if (*pcVar5 == '\0') goto LAB_004080b0;
  }
  else {
    if (param_7[6] == 0) goto LAB_00407f6e;
    cVar2 = *param_3;
    if ((cVar2 - 0x2bU & 0xfd) != 0) {
      pcVar5 = *(char **)(param_7 + 8);
      goto LAB_00407fb2;
    }
    pcVar7 = param_3 + 1;
    pcVar5 = *(char **)(param_7 + 8);
    param_3 = param_3 + 1;
    if (*pcVar7 == ':') {
      uVar11 = 0;
    }
    if (pcVar5 != (char *)0x0) goto LAB_00407fc6;
LAB_004080b0:
    uVar9 = *param_7;
    if (param_7[0xc] != uVar9 && (int)uVar9 <= (int)param_7[0xc]) {
      param_7[0xc] = uVar9;
    }
    if ((int)uVar9 < (int)param_7[0xb]) {
      param_7[0xb] = uVar9;
    }
    if (param_7[10] == 1) {
      uVar3 = param_7[0xc];
      if (param_7[0xb] == uVar3) {
        if (uVar9 != uVar3) {
          param_7[0xb] = uVar9;
          uVar3 = uVar9;
        }
      }
      else {
        if (uVar9 != uVar3) {
          FUN_00407880(param_2,param_7);
          param_6 = param_6 & 0xffffffff;
          uVar3 = *param_7;
        }
      }
      uVar9 = uVar3;
      if ((int)uVar3 < (int)param_1) {
        ppcVar6 = (char **)(param_2 + (long)(int)uVar3);
        do {
          if ((**ppcVar6 == '-') && ((*ppcVar6)[1] != '\0')) {
            uVar9 = *param_7;
            break;
          }
          uVar3 = uVar3 + 1;
          ppcVar6 = ppcVar6 + 1;
          *param_7 = uVar3;
          uVar9 = uVar3;
        } while (uVar3 != param_1);
      }
      param_7[0xc] = uVar3;
    }
    bVar12 = param_1 == uVar9;
    if (bVar12) {
      param_1 = param_7[0xc];
      uVar9 = param_7[0xb];
LAB_00408124:
      if (param_1 != uVar9) {
        *param_7 = uVar9;
      }
      return 0xffffffff;
    }
    lVar4 = 3;
    pcVar5 = (char *)param_2[(long)(int)uVar9];
    pcVar7 = pcVar5;
    pcVar8 = "--";
    do {
      if (lVar4 == 0) break;
      lVar4 = lVar4 + -1;
      bVar12 = *pcVar7 == *pcVar8;
      pcVar7 = pcVar7 + (ulong)bVar13 * -2 + 1;
      pcVar8 = pcVar8 + (ulong)bVar13 * -2 + 1;
    } while (bVar12);
    if (bVar12) {
      uVar9 = uVar9 + 1;
      *param_7 = uVar9;
      if (param_7[0xb] == param_7[0xc]) {
        param_7[0xb] = uVar9;
      }
      else {
        bVar12 = uVar9 != param_7[0xc];
        uVar9 = param_7[0xb];
        if (bVar12) {
          FUN_00407880(param_2,param_7);
          uVar9 = param_7[0xb];
        }
      }
      param_7[0xc] = param_1;
      *param_7 = param_1;
      goto LAB_00408124;
    }
    if ((*pcVar5 != '-') || (cVar2 = pcVar5[1], cVar2 == '\0')) {
      if (param_7[10] == 0) {
        return 0xffffffff;
      }
      *(char **)(param_7 + 4) = pcVar5;
      *param_7 = uVar9 + 1;
      return 1;
    }
    if (param_4 != 0) {
      if (cVar2 == '-') {
        *(char **)(param_7 + 8) = pcVar5 + 2;
        puVar14 = &DAT_00412129;
        goto LAB_0040834a;
      }
      if ((int)param_6 != 0) {
        if (pcVar5[2] == '\0') {
          lVar4 = func_0x00401d40(param_3,(ulong)(uint)(int)cVar2);
          param_6 = param_6 & 0xffffffff;
          if (lVar4 != 0) goto LAB_0040821f;
        }
        *(char **)(param_7 + 8) = pcVar5 + 1;
        uVar9 = FUN_00407960((ulong)param_1,param_2,param_3,param_4,param_5,param_6,param_7,uVar11,
                             &DAT_0041212a);
        if (uVar9 != 0xffffffff) {
          return (ulong)uVar9;
        }
        pcVar5 = (char *)param_2[(long)(int)*param_7];
      }
    }
LAB_0040821f:
    pcVar5 = pcVar5 + 1;
  }
  pcVar7 = pcVar5 + 1;
  *(char **)(param_7 + 8) = pcVar7;
  cVar2 = *pcVar5;
  uVar9 = SEXT14(cVar2);
  _uVar9 = (ulong)uVar9;
  pcVar8 = (char *)func_0x00401d40(param_3,_uVar9);
  if (pcVar5[1] == '\0') {
    *param_7 = *param_7 + 1;
  }
  iVar10 = (int)uVar11;
  if (((byte)(cVar2 - 0x3aU) < 2) || (pcVar8 == (char *)0x0)) {
    if (iVar10 != 0) {
      func_0x00401ac0(DAT_006144e0,"%s: invalid option -- \'%c\'\n",*param_2,(ulong)uVar9);
    }
    param_7[2] = uVar9;
    return 0x3f;
  }
  if (((*pcVar8 == 'W') && (pcVar8[1] == ';')) && (param_4 != 0)) {
    if (pcVar5[1] == '\0') {
      if (*param_7 == param_1) {
        if (iVar10 != 0) {
          func_0x00401ac0(DAT_006144e0,"%s: option requires an argument -- \'%c\'\n",*param_2,
                          (ulong)uVar9);
        }
        param_7[2] = uVar9;
        return (ulong)(uint)(int)(char)((*param_3 != ':') * '\x05' + ':');
      }
      pcVar7 = (char *)param_2[(long)(int)*param_7];
    }
    *(char **)(param_7 + 8) = pcVar7;
    *(undefined8 *)(param_7 + 4) = 0;
    puVar14 = &DAT_00412148;
    param_6 = 0;
LAB_0040834a:
    uVar9 = FUN_00407960((ulong)param_1,param_2,param_3,param_4,param_5,param_6,param_7,uVar11,
                         puVar14);
    return (ulong)uVar9;
  }
  if (pcVar8[1] != ':') {
    return _uVar9;
  }
  if (pcVar8[2] == ':') {
    if (pcVar5[1] == '\0') {
      *(undefined8 *)(param_7 + 4) = 0;
      goto LAB_00408065;
    }
  }
  else {
    if (pcVar5[1] == '\0') {
      uVar3 = *param_7;
      if (uVar3 == param_1) {
        if (iVar10 != 0) {
          func_0x00401ac0(DAT_006144e0,"%s: option requires an argument -- \'%c\'\n",*param_2,
                          (ulong)uVar9);
        }
        param_7[2] = uVar9;
        _uVar9 = (ulong)(*param_3 != ':') * 5 + 0x3a;
      }
      else {
        uVar1 = param_2[(long)(int)uVar3];
        *param_7 = uVar3 + 1;
        *(undefined8 *)(param_7 + 4) = uVar1;
      }
      goto LAB_00408065;
    }
  }
  *(char **)(param_7 + 4) = pcVar7;
  *param_7 = *param_7 + 1;
LAB_00408065:
  *(undefined8 *)(param_7 + 8) = 0;
  return _uVar9;
}



void FUN_00408490(void)

{
  null_ARRAY_00614700._0_4_ = DAT_006144bc;
  null_ARRAY_00614700._4_4_ = DAT_006144b8;
  FUN_00407f20();
  DAT_006144b4 = null_ARRAY_00614700._8_4_;
  DAT_006144bc = null_ARRAY_00614700._0_4_;
  DAT_00614758 = null_ARRAY_00614700._16_8_;
  return;
}



void FUN_004084f0(void)

{
  FUN_00408490();
  return;
}



void FUN_00408510(long lParm1)

{
  if (lParm1 == 0) {
    lParm1 = 1;
  }
  FUN_00615088(lParm1);
  return;
}



ulong FUN_00408530(uint *puParm1,byte *pbParm2,long lParm3)

{
  char cVar1;
  ulong uVar2;
  uint local_2c [3];
  
  if (puParm1 == (uint *)0x0) {
    puParm1 = local_2c;
  }
  uVar2 = func_0x004019b0(puParm1);
  if (((0xfffffffffffffffd < uVar2) && (lParm3 != 0)) && (cVar1 = FUN_00408730(0), cVar1 == '\0')) {
    *puParm1 = (uint)*pbParm2;
    return 1;
  }
  return uVar2;
}



void FUN_004085a0(long lParm1,long lParm2)

{
  if (lParm2 == 0) {
    func_0x00401db0();
    lParm2 = 1;
  }
  else {
    if (lParm1 != 0) {
      FUN_00615148();
      return;
    }
  }
  FUN_00615088(lParm2,lParm2);
  return;
}



ulong FUN_004085e0(undefined8 uParm1,undefined8 uParm2,undefined8 uParm3)

{
  undefined4 uVar1;
  undefined *puVar2;
  ulong uVar3;
  undefined4 *puVar4;
  ulong uVar5;
  ulong local_800;
  undefined local_7f8 [2008];
  
  local_800 = 2000;
  puVar2 = (undefined *)FUN_0040a130(local_7f8,&local_800,uParm2,uParm3);
  uVar5 = local_800;
  if (puVar2 != (undefined *)0x0) {
    uVar3 = func_0x00401ca0(puVar2,1,local_800,uParm1);
    if (uVar3 < uVar5) {
      uVar5 = 0xffffffff;
      if (puVar2 != local_7f8) {
        puVar4 = (undefined4 *)func_0x00401cb0();
        uVar1 = *puVar4;
        func_0x00401db0(puVar2);
        puVar4 = (undefined4 *)func_0x00401cb0();
        *puVar4 = uVar1;
        return 0xffffffff;
      }
      goto LAB_0040864c;
    }
    if (puVar2 != local_7f8) {
      func_0x00401db0(puVar2);
    }
    if (uVar5 < 0x80000000) goto LAB_0040864c;
    puVar4 = (undefined4 *)func_0x00401cb0();
    *puVar4 = 0x4b;
  }
  uVar5 = 0xffffffff;
  func_0x00401970(uParm1);
LAB_0040864c:
  return uVar5 & 0xffffffff;
}



ulong FUN_004086b0(undefined8 uParm1)

{
  uint uVar1;
  int iVar2;
  long lVar3;
  undefined4 *puVar4;
  int *piVar5;
  ulong uVar6;
  
  lVar3 = func_0x00401920();
  uVar1 = func_0x00401ce0(uParm1);
  uVar6 = (ulong)uVar1;
  iVar2 = FUN_004075f0(uParm1);
  if (uVar1 == 0) {
    if ((iVar2 != 0) && ((lVar3 != 0 || (piVar5 = (int *)func_0x00401cb0(), *piVar5 != 9)))) {
      uVar6 = 0xffffffff;
    }
  }
  else {
    uVar6 = 0xffffffff;
    if (iVar2 == 0) {
      puVar4 = (undefined4 *)func_0x00401cb0();
      *puVar4 = 0;
      return 0xffffffff;
    }
  }
  return uVar6;
}



undefined8 FUN_00408720(void)

{
  return DAT_006144d0;
}



ulong FUN_00408730(ulong uParm1)

{
  int iVar1;
  long lVar2;
  long lVar3;
  ulong uVar4;
  ulong unaff_R12;
  
  uVar4 = uParm1 & 0xffffffff;
  lVar2 = func_0x00401c40(uParm1,0);
  if (lVar2 == 0) {
    return 1;
  }
  lVar2 = func_0x00401ba0(lVar2);
  if (lVar2 == 0) {
    return 1;
  }
  lVar3 = func_0x00401c40(uVar4,&DAT_0041214c);
  if (lVar3 != 0) {
    unaff_R12 = 0;
    iVar1 = func_0x00401bf0(lVar3,lVar2);
    if (iVar1 == 0) goto LAB_00408775;
  }
  lVar3 = func_0x00401c40(uVar4,"POSIX");
  if (lVar3 == 0) {
    unaff_R12 = 1;
  }
  else {
    iVar1 = func_0x00401bf0(lVar3,lVar2);
    unaff_R12 = unaff_R12 & 0xffffffffffffff00 | (ulong)(iVar1 != 0);
  }
LAB_00408775:
  func_0x00401c40(uVar4,lVar2);
  func_0x00401db0(lVar2);
  return unaff_R12 & 0xffffffff;
}



char * FUN_004087e0(void)

{
  undefined8 *puVar1;
  char cVar2;
  byte bVar3;
  int iVar4;
  uint uVar5;
  long lVar6;
  long lVar7;
  ulong uVar8;
  long lVar9;
  uint *puVar10;
  uint *puVar11;
  long lVar12;
  char *pcVar13;
  char *pcVar14;
  long lVar15;
  long lVar16;
  char *pcVar17;
  bool bVar18;
  uint local_b8 [16];
  uint local_78 [18];
  
  pcVar13 = (char *)func_0x00401b10(0xe);
  pcVar17 = DAT_00614738;
  if (pcVar13 == (char *)0x0) {
    pcVar13 = "";
  }
  if (DAT_00614738 != (char *)0x0) goto LAB_00408816;
  pcVar14 = (char *)func_0x00401950("CHARSETALIASDIR");
  if ((pcVar14 == (char *)0x0) || (*pcVar14 == '\0')) {
    lVar7 = 0x3d;
    lVar6 = 0x3e;
    pcVar14 = "/home/andrea/paper/orchestra/root/x86_64-gentoo-linux-musl/lib";
LAB_004088bc:
    lVar16 = lVar6 + 0xe;
    lVar12 = lVar6;
    if (pcVar14[lVar7] == '/') goto LAB_00408911;
    lVar7 = FUN_00408510(lVar6 + 0xf);
    if (lVar7 != 0) {
      func_0x00401990(lVar7,pcVar14,lVar6);
      lVar12 = lVar6 + 1;
      *(undefined *)(lVar7 + lVar6) = 0x2f;
      goto LAB_00408933;
    }
  }
  else {
    lVar6 = func_0x00401d20(pcVar14);
    if (lVar6 != 0) {
      lVar7 = lVar6 + -1;
      goto LAB_004088bc;
    }
    lVar16 = 0xe;
    lVar12 = lVar6;
LAB_00408911:
    lVar7 = FUN_00408510(lVar16);
    if (lVar7 != 0) {
      func_0x00401990(lVar7,pcVar14,lVar12);
LAB_00408933:
      puVar1 = (undefined8 *)(lVar7 + lVar12);
      *puVar1 = 0x2e74657372616863;
      *(undefined4 *)(puVar1 + 1) = 0x61696c61;
      *(undefined2 *)((long)puVar1 + 0xc) = 0x73;
      uVar5 = func_0x00401d30(lVar7,0);
      if (-1 < (int)uVar5) {
        lVar16 = 0;
        lVar6 = func_0x00401b80((ulong)uVar5,0x41061c);
        if (lVar6 != 0) {
LAB_00408990:
          while (uVar8 = func_0x00401940(lVar6), pcVar14 = pcVar17, (int)uVar8 != -1) {
            while ((iVar4 = (int)uVar8, 1 < iVar4 - 9U && (iVar4 != 0x20))) {
              if (iVar4 == 0x23) goto LAB_00408ae5;
              func_0x00401910(uVar8 & 0xffffffff,lVar6);
              iVar4 = func_0x00401980(lVar6,"%50s %50s",local_b8);
              pcVar14 = pcVar17;
              if (iVar4 < 2) goto LAB_00408afb;
              puVar11 = local_b8;
              do {
                puVar10 = puVar11;
                uVar5 = *puVar10 + 0xfefefeff & ~*puVar10;
                _bVar3 = uVar5 & 0x80808080;
                bVar3 = (byte)_bVar3;
                puVar11 = puVar10 + 1;
              } while (_bVar3 == 0);
              bVar18 = (uVar5 & 0x8080) == 0;
              if (bVar18) {
                bVar3 = (byte)(_bVar3 >> 0x10);
              }
              puVar11 = puVar10 + 1;
              if (bVar18) {
                puVar11 = (uint *)((long)puVar10 + 6);
              }
              lVar12 = (long)puVar11 + ((-3 - (ulong)CARRY1(bVar3,bVar3)) - (long)local_b8);
              puVar11 = local_78;
              do {
                puVar10 = puVar11;
                uVar5 = *puVar10 + 0xfefefeff & ~*puVar10;
                _bVar3 = uVar5 & 0x80808080;
                bVar3 = (byte)_bVar3;
                puVar11 = puVar10 + 1;
              } while (_bVar3 == 0);
              bVar18 = (uVar5 & 0x8080) == 0;
              if (bVar18) {
                bVar3 = (byte)(_bVar3 >> 0x10);
              }
              puVar11 = puVar10 + 1;
              if (bVar18) {
                puVar11 = (uint *)((long)puVar10 + 6);
              }
              lVar15 = (long)puVar11 + ((-3 - (ulong)CARRY1(bVar3,bVar3)) - (long)local_78);
              lVar9 = lVar15 + lVar12;
              if (lVar16 == 0) {
                pcVar14 = (char *)FUN_00408510(lVar9 + 3);
              }
              else {
                lVar9 = lVar9 + lVar16;
                pcVar14 = (char *)FUN_004085a0(pcVar17,lVar9 + 3);
              }
              lVar16 = lVar9 + 2;
              if (pcVar14 == (char *)0x0) {
                pcVar14 = "";
                func_0x00401db0(pcVar17);
                FUN_004075f0(lVar6);
                goto LAB_00408b0d;
              }
              lVar15 = lVar16 - lVar15;
              func_0x004018e0(pcVar14 + (-2 - lVar12) + lVar15,local_b8);
              func_0x004018e0(pcVar14 + lVar15 + -1,local_78);
              uVar8 = func_0x00401940();
              pcVar17 = pcVar14;
              if ((int)uVar8 == -1) goto LAB_00408afb;
            }
          }
          goto LAB_00408afb;
        }
        func_0x00401da0((ulong)uVar5);
      }
      goto LAB_00408b31;
    }
  }
  DAT_00614738 = "";
LAB_00408816:
  cVar2 = *DAT_00614738;
  pcVar17 = DAT_00614738;
  do {
    if (cVar2 == '\0') {
LAB_00408874:
      if (*pcVar13 == '\0') {
        pcVar13 = "ASCII";
      }
      return pcVar13;
    }
    iVar4 = func_0x00401bf0(pcVar13,pcVar17);
    if ((iVar4 == 0) || ((cVar2 == '*' && (pcVar17[1] == '\0')))) {
      lVar7 = func_0x00401d20(pcVar17);
      pcVar13 = pcVar17 + lVar7 + 1;
      goto LAB_00408874;
    }
    lVar7 = func_0x00401d20(pcVar17);
    lVar6 = func_0x00401d20(pcVar17 + lVar7 + 1);
    pcVar17 = pcVar17 + lVar7 + 1 + lVar6 + 1;
    cVar2 = *pcVar17;
  } while( true );
  while (iVar4 != -1) {
LAB_00408ae5:
    iVar4 = func_0x00401940(lVar6);
    if (iVar4 == 10) break;
  }
  pcVar14 = pcVar17;
  if (iVar4 == -1) goto LAB_00408afb;
  goto LAB_00408990;
LAB_00408afb:
  FUN_004075f0(lVar6);
  if (lVar16 == 0) {
LAB_00408b31:
    pcVar14 = "";
  }
  else {
    pcVar14[lVar16] = '\0';
  }
LAB_00408b0d:
  func_0x00401db0(lVar7);
  DAT_00614738 = pcVar14;
  goto LAB_00408816;
}



ulong FUN_00408b90(long *plParm1,ulong *puParm2,int iParm3,long lParm4)

{
  ulong uVar1;
  int iVar2;
  long lVar3;
  undefined4 *puVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  
  if ((puParm2 == (ulong *)0x0 || lParm4 == 0) || (plParm1 == (long *)0x0)) {
    puVar4 = (undefined4 *)func_0x00401cb0();
    *puVar4 = 0x16;
  }
  else {
    lVar3 = *plParm1;
    if ((lVar3 == 0) || (*puParm2 == 0)) {
      *puParm2 = 0x78;
      lVar3 = FUN_004085a0(lVar3,0x78);
      if (lVar3 == 0) {
        return 0xffffffffffffffff;
      }
      *plParm1 = lVar3;
    }
    uVar6 = 0;
    do {
      while( true ) {
        iVar2 = func_0x00401940(lParm4);
        uVar7 = uVar6;
        if (iVar2 == -1) goto LAB_00408c5e;
        uVar7 = uVar6 + 1;
        if (uVar7 < *puParm2) break;
        uVar1 = *puParm2 * 2 + 1;
        uVar5 = 0x8000000000000000;
        if (uVar1 < 0x8000000000000001) {
          uVar5 = uVar1;
        }
        if (uVar5 <= uVar7) {
          puVar4 = (undefined4 *)func_0x00401cb0();
          *puVar4 = 0x4b;
          return 0xffffffffffffffff;
        }
        lVar3 = FUN_004085a0(*plParm1,uVar5);
        if (lVar3 == 0) {
          return 0xffffffffffffffff;
        }
        *plParm1 = lVar3;
        *puParm2 = uVar5;
        *(undefined *)(lVar3 + uVar6) = (char)iVar2;
        uVar6 = uVar7;
        if (iVar2 == iParm3) goto LAB_00408c5e;
      }
      *(undefined *)(*plParm1 + uVar6) = (char)iVar2;
      uVar6 = uVar7;
    } while (iVar2 != iParm3);
LAB_00408c5e:
    *(undefined *)(*plParm1 + uVar7) = 0;
    if (uVar7 != 0) {
      return uVar7;
    }
  }
  return 0xffffffffffffffff;
}



undefined4 * FUN_00408cf0(ulong uParm1,uint *puParm2,ulong uParm3,uint *puParm4,long *plParm5)

{
  uint uVar1;
  int iVar2;
  ulong uVar3;
  long lVar4;
  long lVar5;
  undefined4 *puVar6;
  undefined4 *puVar7;
  ulong uVar8;
  ulong uVar9;
  uint *puVar10;
  ulong uVar11;
  
  puVar10 = puParm2;
  uVar3 = uParm3;
  if (uParm1 <= uParm3) {
    puVar10 = puParm4;
    puParm4 = puParm2;
    uVar3 = uParm1;
    uParm1 = uParm3;
  }
  if (uVar3 == 0) {
    *plParm5 = 0;
    lVar5 = FUN_00408510(1);
    plParm5[1] = lVar5;
  }
  else {
    lVar5 = uParm1 + uVar3;
    puVar6 = (undefined4 *)FUN_00408510(lVar5 * 4);
    uVar9 = uParm1;
    if (puVar6 != (undefined4 *)0x0) {
      while (uVar9 != 0) {
        puVar6[uVar9 - 1] = 0;
        uVar9 = uVar9 - 1;
      }
      puVar7 = puVar6;
      do {
        while (uVar1 = *puParm4, uParm1 != 0) {
          uVar9 = 0;
          uVar11 = 0;
          do {
            uVar8 = uVar9 + (ulong)(uint)puVar7[uVar11] + (ulong)puVar10[uVar11] * (ulong)uVar1;
            puVar7[uVar11] = (int)uVar8;
            uVar11 = uVar11 + 1;
            uVar9 = uVar8 >> 0x20;
          } while (uVar11 != uParm1);
          puVar7[uParm1] = (int)(uVar8 >> 0x20);
          puVar7 = puVar7 + 1;
          puParm4 = puParm4 + 1;
          if (puVar7 == puVar6 + uVar3) goto LAB_00408de7;
        }
        puParm4 = puParm4 + 1;
        *puVar7 = 0;
        puVar7 = puVar7 + 1;
      } while (puVar7 != puVar6 + uVar3);
LAB_00408de7:
      if (lVar5 != 0) {
        iVar2 = puVar6[lVar5 + -1];
        lVar4 = lVar5;
        while ((lVar5 = lVar4, iVar2 == 0 && (lVar5 = lVar4 + -1, lVar5 != 0))) {
          iVar2 = puVar6[lVar4 + -2];
          lVar4 = lVar5;
        }
      }
      *plParm5 = lVar5;
      *(undefined4 **)(plParm5 + 1) = puVar6;
      return puVar6;
    }
    lVar5 = 0;
  }
  return (undefined4 *)lVar5;
}



ulong FUN_00408e50(void)

{
  bool bVar1;
  float10 fVar2;
  ulong uVar3;
  undefined8 uVar4;
  float10 in_ST0;
  double dVar5;
  double dVar6;
  int local_c [3];
  
  func_0x00401cd0(local_c);
  fVar2 = (float10)0;
  bVar1 = fVar2 <= in_ST0 && in_ST0 < (float10)1;
  if (fVar2 > in_ST0 || in_ST0 >= (float10)1) {
LAB_004090ac:
    uVar4 = func_0x00401a50();
    return uVar4;
  }
  if (in_ST0 == fVar2) {
    uVar3 = 0x80000000;
  }
  else {
    if (in_ST0 < (float10)0.50000000) {
      if (in_ST0 < (float10)0.00000000) {
        do {
          local_c[0] = local_c[0] + -0x20;
          in_ST0 = in_ST0 * (float10)4294967296.00000000;
        } while (in_ST0 < (float10)0.00000000);
      }
      if (in_ST0 < (float10)0.00001526) {
        in_ST0 = in_ST0 * (float10)65536.00000000;
        local_c[0] = local_c[0] + -0x10;
      }
      if (in_ST0 < (float10)0.00390625) {
        in_ST0 = in_ST0 * (float10)256.00000000;
        local_c[0] = local_c[0] + -8;
      }
      if (in_ST0 < (float10)0.06250000) {
        in_ST0 = in_ST0 * (float10)16.00000000;
        local_c[0] = local_c[0] + -4;
      }
      if (in_ST0 < (float10)0.25000000) {
        in_ST0 = in_ST0 * (float10)4.00000000;
        local_c[0] = local_c[0] + -2;
      }
      if (in_ST0 < (float10)0.50000000) {
        in_ST0 = in_ST0 + in_ST0;
        local_c[0] = local_c[0] + -1;
      }
      bVar1 = in_ST0 < (float10)1;
    }
    if ((in_ST0 < (float10)0.50000000) || (!bVar1)) goto LAB_004090ac;
    dVar5 = (double)in_ST0;
    dVar6 = (double)local_c[0];
    if (dVar5 < 0.70710678) {
      dVar5 = dVar5 * 1.41421356;
      dVar6 = dVar6 - 0.50000000;
    }
    if (dVar5 < 0.84089642) {
      dVar5 = dVar5 * 1.18920712;
      dVar6 = dVar6 - 0.25000000;
    }
    if (dVar5 < 0.91700404) {
      dVar5 = dVar5 * 1.09050773;
      dVar6 = dVar6 - 0.12500000;
    }
    if (dVar5 < 0.95760328) {
      dVar5 = dVar5 * 1.04427378;
      dVar6 = dVar6 - 0.06250000;
    }
    dVar5 = 1.00000000 - dVar5;
    dVar5 = (dVar6 - (dVar5 * ((dVar5 * 0.25000000 + 0.33333333) * dVar5 + 0.50000000) + 1.00000000)
                     * dVar5 * 1.44269504) * 0.30103000;
    uVar3 = (ulong)((int)dVar5 - (uint)(dVar5 < 0.00000000));
  }
  return uVar3;
}



uint * FUN_004090c0(ulong uParm1,long lParm2,ulong uParm3,uint *puParm4,ulong *puParm5)

{
  int iVar1;
  uint uVar2;
  uint uVar3;
  undefined auVar4 [16];
  long lVar5;
  uint uVar6;
  uint *puVar7;
  ulong uVar8;
  ulong uVar9;
  ulong uVar10;
  ulong uVar11;
  undefined8 uVar12;
  byte bVar13;
  uint uVar14;
  uint uVar15;
  ulong uVar16;
  uint *puVar17;
  uint *puVar18;
  ulong uVar19;
  uint uVar20;
  long lVar21;
  uint *puVar22;
  long lVar23;
  ulong uVar24;
  bool bVar25;
  uint *local_68;
  
  puVar7 = (uint *)FUN_00408510();
  if (puVar7 == (uint *)0x0) {
    return (uint *)0;
  }
  if (uParm1 != 0) {
    iVar1 = *(int *)(uParm1 * 4 + -4 + lParm2);
    while ((iVar1 == 0 && (uParm1 = uParm1 - 1, uParm1 != 0))) {
      iVar1 = *(int *)(lParm2 + -4 + uParm1 * 4);
    }
  }
  if (uParm3 == 0) {
LAB_00409663:
    uVar12 = func_0x00401a50();
    return (uint *)uVar12;
  }
  lVar21 = uParm3 * 4;
  uVar6 = puParm4[uParm3 - 1];
  lVar23 = lVar21 + -4;
  if (uVar6 == 0) {
    lVar5 = lVar21 + -8;
    do {
      lVar23 = lVar5;
      uParm3 = uParm3 - 1;
      if (uParm3 == 0) goto LAB_00409663;
      uVar6 = *(uint *)((long)puParm4 + lVar23);
      lVar21 = lVar23 + 4;
      lVar5 = lVar23 + -4;
    } while (uVar6 == 0);
  }
  if (uParm1 < uParm3) {
    uVar24 = 0;
    puVar17 = puVar7 + uParm1;
    func_0x00401990(puVar7,lParm2,uParm1 * 4);
    local_68 = (uint *)0x0;
    uVar8 = uParm3;
    puVar18 = puParm4;
    goto LAB_00409379;
  }
  if (uParm3 != 1) {
    iVar1 = 0x1f;
    if (uVar6 != 0) {
      while (uVar6 >> iVar1 == 0) {
        iVar1 = iVar1 + -1;
      }
    }
    if (iVar1 == 0x1f) {
      func_0x00401990(puVar7,lParm2,uParm1 * 4);
      local_68 = (uint *)0x0;
      puVar7[uParm1] = 0;
      puVar18 = puParm4;
    }
    else {
      bVar13 = (byte)iVar1 ^ 0x1f;
      puVar18 = (uint *)FUN_00408510();
      if (puVar18 == (uint *)0x0) {
        func_0x00401db0(puVar7);
        return (uint *)0;
      }
      uVar24 = 0;
      uVar8 = 0;
      do {
        uVar8 = uVar8 + ((ulong)puParm4[uVar24] << (bVar13 & 0x3f));
        puVar18[uVar24] = (uint)uVar8;
        uVar24 = uVar24 + 1;
        uVar8 = uVar8 >> 0x20;
      } while (uVar24 != uParm3);
      if (uVar8 != 0) goto LAB_00409663;
      uVar24 = 0;
      uVar8 = 0;
      uVar6 = 0;
      puVar17 = puVar7;
      if (uParm1 != 0) {
        do {
          uVar9 = uVar8 + ((ulong)*(uint *)(lParm2 + uVar24 * 4) << (bVar13 & 0x3f));
          puVar7[uVar24] = (uint)uVar9;
          uVar24 = uVar24 + 1;
          uVar8 = uVar9 >> 0x20;
          uVar6 = (uint)(uVar9 >> 0x20);
        } while (uVar24 != uParm1);
        puVar17 = puVar7 + uParm1;
      }
      *puVar17 = uVar6;
      local_68 = puVar18;
    }
    puVar17 = (uint *)((long)puVar7 + lVar21);
    puVar22 = puVar7 + uParm1;
    uVar8 = uParm1 - uParm3;
    uVar6 = *(uint *)((long)puVar18 + lVar23);
    lVar23 = uVar8 * 4;
    uVar15 = *(uint *)((long)puVar18 + lVar21 + -8);
    uVar24 = (ulong)uVar15;
    uVar9 = (uVar24 << 0x20) - uVar24;
    do {
      uVar2 = *puVar22;
      if (uVar2 < uVar6) {
        uVar10 = CONCAT44(uVar2,puVar22[-1]) / (ulong)uVar6;
        uVar20 = (uint)uVar10;
        uVar10 = (uVar10 & 0xffffffff) * uVar24;
        uVar16 = CONCAT44(uVar2,puVar22[-1]) % (ulong)uVar6 << 0x20 | (ulong)puVar22[-2];
        if (uVar16 < uVar10) {
LAB_00409267:
          uVar2 = uVar20 - 1;
          uVar20 = uVar20 - 2;
          if (uVar10 - uVar16 <= CONCAT44(uVar6,uVar15)) {
            uVar20 = uVar2;
          }
        }
        if (uVar20 != 0) {
LAB_00409284:
          uVar10 = (ulong)uVar20;
          goto LAB_00409287;
        }
      }
      else {
        if ((uVar2 <= uVar6) && (!CARRY4(uVar6,puVar22[-1]))) {
          uVar20 = 0xffffffff;
          uVar16 = CONCAT44(uVar6 + puVar22[-1],puVar22[-2]);
          uVar10 = uVar9;
          if (uVar16 < uVar9) goto LAB_00409267;
          goto LAB_00409284;
        }
        uVar10 = 0xffffffff;
        uVar20 = 0xffffffff;
LAB_00409287:
        lVar21 = (long)puVar7 + lVar23;
        uVar19 = 0;
        uVar16 = 0;
        do {
          uVar11 = uVar16 + (ulong)~*(uint *)(lVar21 + uVar19 * 4) + (ulong)puVar18[uVar19] * uVar10
          ;
          uVar16 = uVar11 >> 0x20;
          *(uint *)(lVar21 + uVar19 * 4) = ~(uint)uVar11;
          uVar19 = uVar19 + 1;
        } while (uVar19 != uParm3);
        if (*puVar22 < (uint)(uVar11 >> 0x20)) {
          uVar20 = uVar20 - 1;
          uVar16 = 0;
          uVar10 = 0;
          do {
            while( true ) {
              uVar2 = *(uint *)(lVar21 + uVar16 * 4);
              uVar3 = puVar18[uVar16];
              uVar14 = ~uVar2;
              *(int *)(lVar21 + uVar16 * 4) = uVar3 + uVar2 + (int)uVar10;
              if ((int)uVar10 != 0) break;
              uVar10 = (ulong)(uVar14 < uVar3);
              uVar16 = uVar16 + 1;
              if (uVar16 == uParm3) goto LAB_00409310;
            }
            uVar10 = (ulong)(uVar14 <= uVar3);
            uVar16 = uVar16 + 1;
          } while (uVar16 != uParm3);
        }
      }
LAB_00409310:
      *(uint *)((long)puVar17 + lVar23) = uVar20;
      lVar23 = lVar23 + -4;
      puVar22 = puVar22 + -1;
    } while (lVar23 != -4);
    uParm1 = uParm3;
    uVar24 = uVar8 + 1;
    if (puVar17[uVar8] == 0) {
      uParm1 = uParm3;
      uVar24 = uVar8;
    }
    do {
      if (puVar7[uParm1 - 1] != 0) {
        uVar8 = uParm3;
        if (uParm3 < uParm1) goto LAB_004093b0;
        break;
      }
      uParm1 = uParm1 - 1;
      uVar8 = uParm3;
    } while (uParm1 != 0);
    goto LAB_00409379;
  }
  puVar17 = puVar7 + 1;
  uVar6 = *puParm4;
  if (uParm1 == 0) {
LAB_0040961b:
    if (puVar7[uParm1] != 0) {
      local_68 = (uint *)0x0;
      uVar8 = uParm3;
      uVar24 = uParm1;
      uParm1 = 0;
      puVar18 = puParm4;
      goto LAB_00409379;
    }
    uVar9 = 0;
  }
  else {
    lVar23 = 0;
    uVar8 = 0;
    do {
      auVar4 = ZEXT816((ulong)*(uint *)(lParm2 + uParm1 * 4 + -4 + lVar23 * 4) | uVar8 << 0x20);
      puVar17[uParm1 + lVar23 + -1] = SUB164(auVar4 / ZEXT416(uVar6),0);
      lVar23 = lVar23 + -1;
      uVar15 = SUB164(auVar4 % ZEXT416(uVar6),0);
      uVar8 = (ulong)uVar15;
    } while (lVar23 != -uParm1);
    if (uVar15 == 0) goto LAB_0040961b;
    *puVar7 = uVar15;
    uVar9 = 1;
    if (puVar7[uParm1] != 0) {
      local_68 = (uint *)0x0;
      uVar8 = uParm3;
      uVar24 = uParm1;
      uParm1 = 1;
      puVar18 = puParm4;
      goto LAB_00409379;
    }
  }
  local_68 = (uint *)0x0;
  uVar8 = uParm3;
  uVar24 = uParm1 - 1;
  uParm1 = uVar9;
  puVar18 = puParm4;
LAB_00409379:
  do {
    if ((uVar8 == 0) || (uParm1 < uVar8)) {
      uVar6 = 0;
    }
    else {
      uVar6 = puVar7[uVar8 - 1] >> 0x1f;
    }
    uVar15 = 0;
    if (uVar8 < uParm1) {
      uVar15 = puVar7[uVar8] * 2;
    }
    uVar6 = uVar6 | uVar15;
    if (uVar8 < uParm3) {
      bVar25 = uVar6 < puVar18[uVar8];
      if (!bVar25 && uVar6 != puVar18[uVar8]) break;
      if (bVar25) goto LAB_004093ff;
    }
    else {
      if (uVar6 != 0) break;
    }
    if (uVar8 == 0) goto LAB_00409557;
    uVar8 = uVar8 - 1;
  } while( true );
LAB_004093b0:
  if (uVar24 == 0) {
    lVar23 = 0;
    uVar24 = 1;
LAB_004093f6:
    *(undefined4 *)((long)puVar17 + lVar23) = 1;
    goto LAB_004093ff;
  }
  uVar6 = *puVar17;
  goto LAB_004093bd;
LAB_00409557:
  if ((uVar24 == 0) || (uVar6 = *puVar17, (uVar6 & 1) == 0)) goto LAB_004093ff;
LAB_004093bd:
  *puVar17 = uVar6 + 1;
  if (uVar6 + 1 == 0) {
    uVar8 = 0;
    do {
      uVar8 = uVar8 + 1;
      if (uVar24 <= uVar8) {
        lVar23 = uVar24 * 4;
        uVar24 = uVar24 + 1;
        goto LAB_004093f6;
      }
      uVar6 = puVar17[uVar8];
      puVar17[uVar8] = uVar6 + 1;
    } while (uVar6 + 1 == 0);
  }
LAB_004093ff:
  if (local_68 != (uint *)0x0) {
    func_0x00401db0(local_68);
  }
  *(uint **)(puParm5 + 1) = puVar17;
  *puParm5 = uVar24;
  return puVar7;
}



char * FUN_00409680(int iParm1,long lParm2,long lParm3,long lParm4,uint uParm5)

{
  uint uVar1;
  ulong uVar2;
  ulong uVar3;
  ulong uVar4;
  char *pcVar5;
  undefined4 *puVar6;
  byte bVar7;
  uint uVar8;
  uint uVar9;
  ulong uVar10;
  char *pcVar11;
  char *pcVar12;
  char *pcVar13;
  uint *puVar14;
  uint uVar15;
  uint *puVar16;
  undefined4 *puVar17;
  uint *puVar18;
  long lVar19;
  uint uVar20;
  long lVar21;
  uint uVar22;
  float fVar23;
  ulong local_58;
  long local_50;
  undefined8 local_48;
  undefined8 local_40;
  
  if (lParm4 == 0) {
    return (char *)0;
  }
  uVar9 = iParm1 + uParm5;
  if (((int)uVar9 < 1) || ((int)uParm5 < 1)) {
    uVar10 = 0;
  }
  else {
    uVar8 = uParm5;
    if ((int)uVar9 <= (int)uParm5) {
      uVar8 = uVar9;
    }
    uVar10 = SEXT48((int)uVar8);
    uVar9 = uVar9 - uVar8;
    uParm5 = uParm5 - uVar8;
  }
  uVar20 = ((int)uParm5 >> 0x1f ^ uParm5) - ((int)uParm5 >> 0x1f);
  uVar8 = ((int)uVar9 >> 0x1f ^ uVar9) - ((int)uVar9 >> 0x1f);
  uVar22 = uVar8 >> 5;
  uVar4 = (ulong)uVar22;
  puVar14 = (uint *)FUN_00408510();
  if (puVar14 == (uint *)0x0) goto LAB_00409b2d;
  *puVar14 = 1;
  lVar21 = 1;
  if (uVar20 != 0) {
    uVar15 = 0;
    do {
      uVar15 = uVar15 + 0xd;
      if (uVar20 < uVar15) {
        uVar2 = (ulong)((uVar20 + 0xd) - uVar15);
      }
      else {
        uVar2 = 0xd;
      }
      uVar1 = *(uint *)(null_ARRAY_00412460 + uVar2 * 4);
      uVar2 = 0;
      puVar16 = puVar14;
      do {
        puVar18 = puVar16 + 1;
        uVar3 = uVar2 + (ulong)*puVar16 * (ulong)uVar1;
        *puVar16 = (uint)uVar3;
        uVar2 = uVar3 >> 0x20;
        puVar16 = puVar18;
      } while (puVar18 != puVar14 + lVar21);
      if (uVar2 != 0) {
        lVar21 = lVar21 + 1;
        *puVar18 = (uint)(uVar3 >> 0x20);
      }
    } while (uVar15 <= uVar20);
  }
  bVar7 = (byte)uVar8 & 0x1f;
  if ((int)uParm5 < 0) {
    if ((int)uVar9 < 1) goto LAB_004097ca;
    puVar6 = (undefined4 *)FUN_00408510((uVar4 + 1 + lParm2) * 4);
    if (puVar6 == (undefined4 *)0x0) goto LAB_00409b25;
    uVar2 = 0;
    puVar17 = puVar6;
    if (uVar22 != 0) {
      do {
        puVar6[uVar2] = 0;
        uVar2 = uVar2 + 1;
      } while (uVar4 != uVar2);
      puVar17 = puVar6 + uVar4;
    }
    if ((uVar8 & 0x1f) == 0) {
      lVar19 = 0;
      if (lParm2 != 0) {
        do {
          puVar17[lVar19] = *(undefined4 *)(lParm3 + lVar19 * 4);
          lVar19 = lVar19 + 1;
        } while (lVar19 != lParm2);
        puVar17 = puVar17 + lVar19;
      }
    }
    else {
      if (lParm2 != 0) {
        lVar19 = 0;
        uVar4 = 0;
        do {
          uVar2 = uVar4 + ((ulong)*(uint *)(lParm3 + lVar19 * 4) << bVar7);
          puVar17[lVar19] = (int)uVar2;
          lVar19 = lVar19 + 1;
          uVar4 = uVar2 >> 0x20;
        } while (lVar19 != lParm2);
        puVar17 = puVar17 + lVar19;
        if (uVar4 != 0) {
          *puVar17 = (int)(uVar2 >> 0x20);
          puVar17 = puVar17 + 1;
        }
      }
    }
    lVar21 = FUN_004090c0((long)((long)puVar17 - (long)puVar6) >> 2,puVar6,lVar21,puVar14,&local_58)
    ;
    func_0x00401db0(puVar6);
  }
  else {
    if ((int)uVar9 < 0) {
      lVar19 = FUN_00408cf0(lParm2,lParm3,lVar21,puVar14,&local_48);
      if (lVar19 == 0) {
LAB_00409b25:
        func_0x00401db0(puVar14);
LAB_00409b2d:
        func_0x00401db0(lParm4);
        return (char *)0;
      }
      uVar2 = 0;
      puVar16 = puVar14 + lVar21;
      if (uVar22 != 0) {
        do {
          puVar16[uVar2] = 0;
          uVar2 = uVar2 + 1;
        } while (uVar2 != uVar4);
        uVar2 = uVar2 * 4;
      }
      *(int *)((long)puVar16 + uVar2) = 1 << bVar7;
      lVar21 = FUN_004090c0(local_48,local_40,(ulong)(uVar22 + 1),puVar16,&local_58);
      func_0x00401db0(lVar19);
    }
    else {
LAB_004097ca:
      if ((uVar8 & 0x1f) != 0) {
        uVar2 = 0;
        puVar16 = puVar14;
        lVar19 = lVar21;
        do {
          uVar3 = uVar2 + ((ulong)*puVar16 << bVar7);
          *puVar16 = (uint)uVar3;
          uVar2 = uVar3 >> 0x20;
          lVar19 = lVar19 + -1;
          puVar16 = puVar16 + 1;
        } while (lVar19 != 0);
        if (uVar2 != 0) {
          puVar14[lVar21] = (uint)(uVar3 >> 0x20);
          lVar21 = lVar21 + 1;
        }
      }
      lVar19 = lVar21;
      if (uVar22 != 0) {
        while (uVar2 = uVar4, lVar19 != 0) {
          lVar19 = lVar19 + -1;
          puVar14[uVar4 + lVar19] = puVar14[lVar19];
        }
        do {
          uVar2 = uVar2 - 1;
          puVar14[uVar2] = 0;
        } while (uVar2 != 0);
        lVar21 = lVar21 + uVar4;
      }
      if ((int)uParm5 < 0) {
        lVar21 = FUN_004090c0(lParm2,lParm3,lVar21,puVar14,&local_58);
      }
      else {
        lVar21 = FUN_00408cf0();
      }
    }
  }
  func_0x00401db0(puVar14);
  func_0x00401db0(lParm4);
  if (lVar21 == 0) {
    return (char *)0;
  }
  if ((long)local_58 < 0) {
    fVar23 = (float)(local_58 >> 1 | (ulong)((uint)local_58 & 1));
    fVar23 = fVar23 + fVar23;
  }
  else {
    fVar23 = (float)local_58;
  }
  fVar23 = fVar23 * 1.07040000;
  if (9223372036854775808.00000000 <= fVar23) {
    uVar4 = (long)(fVar23 - 9223372036854775808.00000000) ^ 0x8000000000000000;
  }
  else {
    uVar4 = (ulong)fVar23;
  }
  uVar4 = uVar4 * 9 + 9;
  lVar19 = -1;
  if (!CARRY8(uVar4,uVar10)) {
    lVar19 = uVar4 + uVar10;
  }
  pcVar5 = (char *)FUN_00408510(lVar19);
  if (pcVar5 == (char *)0x0) goto LAB_004099e2;
  if (uVar10 == 0) {
    pcVar11 = pcVar5;
    if (local_58 != 0) goto LAB_00409930;
LAB_004099d7:
    pcVar13 = pcVar5 + 1;
    *pcVar5 = '0';
  }
  else {
    pcVar11 = pcVar5 + uVar10;
    pcVar12 = pcVar5;
    do {
      pcVar13 = pcVar12 + 1;
      *pcVar12 = '0';
      pcVar12 = pcVar13;
    } while (pcVar13 != pcVar11);
    while (local_58 != 0) {
LAB_00409930:
      uVar10 = 0;
      puVar14 = (uint *)(local_50 + local_58 * 4);
      uVar4 = local_58;
      do {
        puVar14 = puVar14 + -1;
        uVar9 = *puVar14;
        uVar8 = (uint)(((uVar10 << 0x20 | (ulong)uVar9) >> 9) / 0x1dcd65);
        *puVar14 = uVar8;
        uVar10 = (ulong)(uVar9 + uVar8 * -1000000000);
        uVar4 = uVar4 - 1;
      } while (uVar4 != 0);
      pcVar13 = pcVar11 + 9;
      pcVar12 = pcVar11;
      do {
        pcVar11 = pcVar12 + 1;
        uVar9 = (uint)(uVar10 * 0xcccccccd >> 0x23);
        *pcVar12 = (char)uVar10 + (char)uVar9 * -10 + '0';
        uVar10 = (ulong)uVar9;
        pcVar12 = pcVar11;
      } while (pcVar11 != pcVar13);
      local_58 = local_58 - (ulong)(*(int *)(local_50 + -4 + local_58 * 4) == 0);
    }
    if ((pcVar5 < pcVar13) && (pcVar11 = pcVar13, pcVar13[-1] == '0')) {
      do {
        pcVar13 = pcVar11 + -1;
        if (pcVar13 == pcVar5) goto LAB_004099d7;
        pcVar12 = pcVar11 + -2;
        pcVar11 = pcVar13;
      } while (*pcVar12 == '0');
    }
    else {
      if (pcVar13 == pcVar5) goto LAB_004099d7;
    }
  }
  *pcVar13 = '\0';
LAB_004099e2:
  func_0x00401db0(lVar21);
  return pcVar5;
}



void FUN_00409c20(uint param_1)

{
  long lVar1;
  uint uVar2;
  uint uVar3;
  float10 fVar4;
  int *piVar5;
  long lVar6;
  ulong unaff_R12;
  ulong unaff_R13;
  int *unaff_R14;
  float10 in_ST0;
  int local_2c;
  
  piVar5 = (int *)FUN_00408510(8);
  if (piVar5 != (int *)0x0) {
    func_0x00401cd0(&local_2c);
    if ((in_ST0 < (float10)0) || (fVar4 = (float10)1, fVar4 <= in_ST0)) {
LAB_00409d94:
      func_0x00401a50();
      return;
    }
    lVar6 = 2;
    do {
      in_ST0 = in_ST0 * (float10)65536.00000000;
      uVar2 = (uint)ROUND(in_ST0);
      in_ST0 = in_ST0 - (float10)(ulong)uVar2;
      if ((in_ST0 < (float10)0) || (fVar4 <= in_ST0)) goto LAB_00409d94;
      in_ST0 = in_ST0 * (float10)65536.00000000;
      uVar3 = (uint)ROUND(in_ST0);
      in_ST0 = in_ST0 - (float10)(ulong)uVar3;
      if ((in_ST0 < (float10)0) || (fVar4 <= in_ST0)) goto LAB_00409d94;
      lVar1 = lVar6 + -1;
      lVar6 = 1;
      piVar5[lVar1] = uVar2 << 0x10 | uVar3;
    } while (lVar1 != 0);
    if (piVar5[1] == 0) {
      unaff_R13 = (ulong)(*piVar5 != 0);
    }
    else {
      unaff_R13 = 2;
    }
    unaff_R12 = (ulong)(local_2c - 0x40);
    unaff_R14 = piVar5;
  }
  FUN_00409680(unaff_R12 & 0xffffffff,unaff_R13,unaff_R14,piVar5,(ulong)param_1);
  return;
}



ulong FUN_00409da0(void)

{
  bool bVar1;
  ulong uVar2;
  undefined8 uVar3;
  double dVar4;
  double dVar5;
  int local_c [3];
  
  dVar4 = (double)func_0x00401d90(local_c);
  bVar1 = 0.00000000 <= dVar4 && dVar4 < 1.00000000;
  if (0.00000000 <= dVar4 && dVar4 < 1.00000000) {
    if (dVar4 == 0.00000000) {
      uVar2 = 0x80000000;
    }
    else {
      if (dVar4 < 0.50000000) {
        while (dVar4 < 0.00000000) {
          local_c[0] = local_c[0] + -0x20;
          dVar4 = dVar4 * 4294967296.00000000;
        }
        if (dVar4 < 0.00001526) {
          dVar4 = dVar4 * 65536.00000000;
          local_c[0] = local_c[0] + -0x10;
        }
        if (dVar4 < 0.00390625) {
          dVar4 = dVar4 * 256.00000000;
          local_c[0] = local_c[0] + -8;
        }
        if (dVar4 < 0.06250000) {
          dVar4 = dVar4 * 16.00000000;
          local_c[0] = local_c[0] + -4;
        }
        if (dVar4 < 0.25000000) {
          dVar4 = dVar4 * 4.00000000;
          local_c[0] = local_c[0] + -2;
        }
        if (dVar4 < 0.50000000) {
          dVar4 = dVar4 + dVar4;
          local_c[0] = local_c[0] + -1;
        }
        bVar1 = dVar4 < 1.00000000;
      }
      if ((dVar4 < 0.50000000) || (!bVar1)) goto LAB_00409fb0;
      dVar5 = (double)local_c[0];
      if (dVar4 < 0.70710678) {
        dVar4 = dVar4 * 1.41421356;
        dVar5 = dVar5 - 0.50000000;
      }
      if (dVar4 < 0.84089642) {
        dVar4 = dVar4 * 1.18920712;
        dVar5 = dVar5 - 0.25000000;
      }
      if (dVar4 < 0.91700404) {
        dVar4 = dVar4 * 1.09050773;
        dVar5 = dVar5 - 0.12500000;
      }
      if (dVar4 < 0.95760328) {
        dVar4 = dVar4 * 1.04427378;
        dVar5 = dVar5 - 0.06250000;
      }
      dVar4 = 1.00000000 - dVar4;
      dVar4 = (dVar5 - (dVar4 * ((dVar4 * 0.25000000 + 0.33333333) * dVar4 + 0.50000000) +
                       1.00000000) * dVar4 * 1.44269504) * 0.30103000;
      uVar2 = (ulong)((int)dVar4 - (uint)(dVar4 < 0.00000000));
    }
    return uVar2;
  }
LAB_00409fb0:
  uVar3 = func_0x00401a50();
  return uVar3;
}



void FUN_00409fc0(undefined8 uParm1,uint uParm2)

{
  uint *puVar1;
  uint uVar2;
  uint uVar3;
  uint uVar4;
  ulong unaff_R12;
  ulong unaff_R13;
  uint *unaff_R14;
  double dVar5;
  int local_2c;
  
  puVar1 = (uint *)FUN_00408510(8);
  if (puVar1 == (uint *)0x0) {
LAB_0040a0f2:
    FUN_00409680(unaff_R12 & 0xffffffff,unaff_R13,unaff_R14,puVar1,(ulong)uParm2);
    return;
  }
  dVar5 = (double)func_0x00401d90(uParm1,&local_2c);
  if ((0.00000000 <= dVar5) && (dVar5 < 1.00000000)) {
    uVar4 = (uint)(dVar5 * 32.00000000);
    dVar5 = dVar5 * 32.00000000 - (double)(ulong)uVar4;
    if ((0.00000000 <= dVar5) && (dVar5 < 1.00000000)) {
      uVar2 = (uint)(dVar5 * 65536.00000000);
      dVar5 = dVar5 * 65536.00000000 - (double)(ulong)uVar2;
      if ((0.00000000 <= dVar5) && (dVar5 < 1.00000000)) {
        uVar2 = uVar4 << 0x10 | uVar2;
        puVar1[1] = uVar2;
        uVar4 = (uint)(dVar5 * 65536.00000000);
        dVar5 = dVar5 * 65536.00000000 - (double)(ulong)uVar4;
        if ((0.00000000 <= dVar5) && (dVar5 < 1.00000000)) {
          uVar3 = (uint)(dVar5 * 65536.00000000);
          dVar5 = dVar5 * 65536.00000000 - (double)(ulong)uVar3;
          if ((0.00000000 <= dVar5) && (dVar5 < 1.00000000)) {
            uVar3 = uVar4 << 0x10 | uVar3;
            *puVar1 = uVar3;
            if (dVar5 == 0.00000000) {
              if (uVar2 == 0) {
                unaff_R13 = (ulong)(uVar3 != 0);
              }
              else {
                unaff_R13 = 2;
              }
              unaff_R12 = (ulong)(local_2c - 0x35);
              unaff_R14 = puVar1;
              goto LAB_0040a0f2;
            }
          }
        }
      }
    }
  }
  func_0x00401a50();
  return;
}



// WARNING: Removing unreachable block (ram,0x0040ac47)
// WARNING: Removing unreachable block (ram,0x0040b928)
// WARNING: Removing unreachable block (ram,0x0040b92a)
// WARNING: Removing unreachable block (ram,0x0040d140)
// WARNING: Removing unreachable block (ram,0x0040b935)
// WARNING: Removing unreachable block (ram,0x0040d360)
// WARNING: Removing unreachable block (ram,0x0040d73a)
// WARNING: Removing unreachable block (ram,0x0040d377)
// WARNING: Type propagation algorithm not settling
// WARNING: Could not reconcile some variable overlaps

undefined8 *
FUN_0040a130(undefined8 *puParm1,undefined8 *puParm2,undefined8 *puParm3,undefined8 uParm4)

{
  unkbyte10 Var1;
  char cVar2;
  int iVar3;
  uint uVar4;
  byte **ppbVar5;
  byte bVar6;
  ushort uVar7;
  int iVar8;
  uint uVar9;
  uint uVar10;
  ulong uVar11;
  ulong uVar12;
  uint *puVar13;
  int *piVar14;
  undefined8 uVar15;
  undefined8 *puVar16;
  undefined8 *puVar17;
  undefined8 *puVar18;
  undefined4 *puVar19;
  byte *pbVar20;
  undefined8 *puVar21;
  byte *pbVar22;
  undefined8 *puVar23;
  byte *pbVar24;
  char cVar25;
  undefined8 extraout_RDX;
  long lVar26;
  undefined *puVar27;
  undefined8 *puVar28;
  undefined8 *puVar29;
  undefined *puVar30;
  char *pcVar31;
  byte *pbVar32;
  long lVar33;
  char *pcVar34;
  undefined *puVar35;
  char *pcVar36;
  char *pcVar37;
  undefined8 *puVar38;
  undefined8 *puVar39;
  long lVar40;
  bool bVar41;
  byte bVar42;
  float10 in_ST0;
  float10 fVar43;
  float10 in_ST1;
  float10 fVar44;
  float10 in_ST2;
  float10 fVar45;
  float10 in_ST3;
  float10 in_ST4;
  float10 fVar46;
  ushort in_FPUControlWord;
  double dVar47;
  double dVar48;
  undefined auVar49 [16];
  undefined auStack2776 [8];
  byte *local_ad0;
  undefined8 *local_ac8;
  undefined8 *local_ac0;
  undefined8 *local_ab8;
  undefined8 *local_ab0;
  undefined8 *local_aa8;
  undefined8 *local_aa0;
  undefined8 *local_a98;
  undefined8 *local_a90;
  undefined8 *local_a88;
  undefined8 *local_a80;
  undefined8 *local_a78;
  undefined8 *local_a70;
  undefined8 *local_a68;
  undefined8 local_a60;
  undefined8 *local_a58;
  undefined8 *local_a50;
  unkbyte10 local_a48;
  undefined8 *local_a38;
  undefined8 local_a30;
  byte **local_a28;
  undefined8 *local_a20;
  unkbyte10 local_a18;
  undefined8 local_a08;
  byte **local_a00;
  uint local_9f8;
  uint uStack2548;
  undefined2 uStack2544;
  undefined6 uStack2542;
  undefined8 *local_9e8;
  long local_9e0;
  undefined8 *local_9d8;
  undefined8 *local_9d0;
  undefined8 *local_9c8;
  int local_9c0;
  ushort local_9bc;
  ushort local_9ba;
  int local_9ac;
  undefined8 local_9a8 [2];
  undefined8 local_998 [2];
  uint local_988 [4];
  uint local_978 [16];
  undefined local_938 [8];
  undefined *local_930;
  undefined auStack2344 [224];
  long local_848;
  undefined8 *local_840;
  long local_838;
  ulong local_830;
  undefined8 auStack2088 [78];
  undefined8 local_5b8;
  undefined2 local_2f8;
  
  bVar42 = 0;
  puVar30 = auStack2776;
  puVar35 = auStack2776;
  local_a20 = puParm2;
  local_9d8 = puParm1;
  local_9c8 = puParm3;
  iVar8 = FUN_0040f7b0(puParm3,&local_848,local_938);
  if (iVar8 < 0) {
    return (undefined8 *)0x0;
  }
  iVar8 = FUN_0040f590(uParm4);
  if (iVar8 < 0) {
    if (local_840 != auStack2088) {
      func_0x00401db0();
    }
    if (local_930 != auStack2344) {
      func_0x00401db0();
    }
    puVar19 = (undefined4 *)func_0x00401cb0();
    *puVar19 = 0x16;
    return (undefined8 *)0x0;
  }
  uVar11 = local_838 + 7;
  if (uVar11 < 7) {
    uVar11 = 0xffffffffffffffff;
  }
  uVar12 = uVar11 + local_830;
  puVar27 = auStack2776;
  if ((!CARRY8(uVar11,local_830)) && (uVar11 = uVar12 + 6, puVar27 = auStack2776, uVar12 <= uVar11))
  {
    if (uVar11 < 4000) {
      local_a28 = (byte **)0x0;
      lVar33 = -(uVar12 + 0x24 & 0xfffffffffffffff0);
      puVar35 = auStack2776 + lVar33;
      puVar30 = auStack2776 + lVar33;
      local_a00 = (byte **)((long)&local_ad0 + lVar33);
      local_a48._0_2_ = in_FPUControlWord;
      ppbVar5 = local_a28;
      puVar17 = local_9d8;
      puVar29 = local_9c8;
      puVar16 = local_840;
    }
    else {
      puVar27 = auStack2776;
      if ((uVar11 == 0xffffffffffffffff) ||
         (local_a00 = (byte **)FUN_00408510(), puVar27 = auStack2776,
         local_a48._0_2_ = in_FPUControlWord, ppbVar5 = local_a00, puVar17 = local_9d8,
         puVar29 = local_9c8, puVar16 = local_840, local_a00 == (byte **)0x0)) goto LAB_0040a490;
    }
    local_a28 = ppbVar5;
    if (puVar17 == (undefined8 *)0x0) {
      puVar39 = (undefined8 *)0x0;
    }
    else {
      puVar39 = (undefined8 *)*local_a20;
      puVar30 = puVar35;
    }
    puVar38 = (undefined8 *)0x0;
    local_9e0 = 0;
    puVar28 = (undefined8 *)*puVar16;
    local_9d8 = puVar17;
    local_9c8 = puVar38;
    local_840 = puVar16;
    if (puVar28 == puVar29) goto LAB_0040a324;
LAB_0040a24c:
    puVar28 = (undefined8 *)((long)puVar28 - (long)puVar29);
    local_9c8 = (undefined8 *)0xffffffffffffffff;
    if (!CARRY8((ulong)puVar28,(ulong)puVar38)) {
      local_9c8 = (undefined8 *)((long)puVar28 + (long)puVar38);
    }
    if (local_9c8 <= puVar39) {
LAB_0040a30e:
      local_9d0 = puVar17;
      *(undefined8 *)(puVar30 + -8) = 0x40a324;
      func_0x00401990((long)local_9d0 + (long)puVar38,puVar29,puVar28);
      puVar17 = local_9d0;
LAB_0040a324:
      local_9d0 = puVar17;
      uVar9 = uStack2548;
      puVar29 = local_a08;
      if (local_848 == local_9e0) {
        puVar16 = (undefined8 *)0xffffffffffffffff;
        puVar29 = (undefined8 *)((long)local_9c8 + 1);
        puVar17 = (undefined8 *)0xffffffffffffffff;
        if (local_9c8 <= puVar29) {
          puVar17 = puVar29;
        }
        puVar38 = local_9d0;
        if (puVar17 <= puVar39) goto LAB_0040c5c1;
        if (puVar39 == (undefined8 *)0x0) {
          puVar17 = local_9d0;
          if (puVar29 < local_9c8) goto LAB_0040a455;
          puVar39 = (undefined8 *)0xc;
          puVar16 = puVar29;
          if ((undefined8 *)0xc < puVar29) goto LAB_0040c55e;
        }
        else {
          if ((long)puVar39 < 0) {
            puVar17 = local_9d0;
            if (local_9c8 <= puVar29) goto LAB_0040a455;
LAB_0040c542:
            puVar17 = (undefined8 *)0xffffffffffffffff;
          }
          else {
            puVar16 = (undefined8 *)((long)puVar39 * 2);
            puVar17 = puVar29;
            if (puVar29 < local_9c8) goto LAB_0040c542;
          }
          if ((puVar16 < puVar17) && (puVar16 = puVar29, puVar17 = local_9d0, puVar29 < local_9c8))
          goto LAB_0040a455;
LAB_0040c55e:
          puVar39 = puVar16;
          puVar17 = local_9d0;
          if (puVar16 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
        }
        bVar41 = local_9d8 == local_9d0;
        if ((local_9d0 == (undefined8 *)0x0) || (bVar41)) {
          *(undefined8 *)(puVar30 + -8) = 0x40da87;
          puVar38 = (undefined8 *)FUN_00408510(puVar39);
        }
        else {
          *(undefined8 *)(puVar30 + -8) = 0x40c59b;
          puVar38 = (undefined8 *)FUN_004085a0(local_9d0,puVar39);
        }
        puVar17 = local_9d0;
        if (puVar38 != (undefined8 *)0x0) {
          if ((local_9c8 != (undefined8 *)0x0) && (bVar41)) {
            *(undefined8 *)(puVar30 + -8) = 0x40dcbf;
            puVar38 = (undefined8 *)func_0x00401990(puVar38,local_9d0,local_9c8);
          }
LAB_0040c5c1:
          local_9d0 = puVar38;
          puVar16 = local_9d0;
          *(undefined *)((long)local_9d0 + (long)local_9c8) = 0;
          if ((local_9d0 != local_9d8) && (puVar29 < puVar39)) {
            *(undefined8 *)(puVar30 + -8) = 0x40c5ef;
            local_9d0 = (undefined8 *)FUN_004085a0(local_9d0,puVar29);
            if (local_9d0 == (undefined8 *)0x0) {
              local_9d0 = puVar16;
            }
          }
          if (local_a28 != (byte **)0x0) {
            *(undefined8 *)(puVar30 + -8) = 0x40c611;
            func_0x00401db0(local_a28);
          }
          if (local_840 != auStack2088) {
            *(undefined8 *)(puVar30 + -8) = 0x40c62d;
            func_0x00401db0();
          }
          if (local_930 != auStack2344) {
            *(undefined8 *)(puVar30 + -8) = 0x40c649;
            func_0x00401db0();
          }
          *(undefined8 **)local_a20 = local_9c8;
          return local_9d0;
        }
        goto LAB_0040a455;
      }
      bVar6 = *(byte *)(puVar16 + 9);
      if (bVar6 == 0x25) {
        uVar4 = local_9f8;
        if (puVar16[10] != -1) goto switchD_0040ab16_caseD_0;
        puVar38 = (undefined8 *)((long)local_9c8 + 1);
        if (puVar38 < local_9c8) {
          puVar38 = (undefined8 *)0xffffffffffffffff;
        }
        puVar29 = local_9d0;
        if (puVar39 < puVar38) {
          if (puVar39 == (undefined8 *)0x0) {
            puVar39 = (undefined8 *)0xc;
          }
          else {
            puVar17 = local_9d0;
            if ((long)puVar39 < 0) goto LAB_0040a455;
            puVar39 = (undefined8 *)((long)puVar39 * 2);
          }
          if (puVar39 < puVar38) {
            puVar39 = puVar38;
          }
          puVar17 = local_9d0;
          if (puVar39 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
          bVar41 = local_9d0 == local_9d8;
          if ((local_9d0 == (undefined8 *)0x0) || (bVar41)) {
            *(undefined8 *)(puVar30 + -8) = 0x40b618;
            puVar29 = (undefined8 *)FUN_00408510();
          }
          else {
            *(undefined8 *)(puVar30 + -8) = 0x40a5ad;
            puVar29 = (undefined8 *)FUN_004085a0();
          }
          puVar17 = local_9d0;
          if (puVar29 == (undefined8 *)0x0) goto LAB_0040a455;
          if ((local_9c8 != (undefined8 *)0x0) && (bVar41)) {
            *(undefined8 *)(puVar30 + -8) = 0x40c356;
            puVar29 = (undefined8 *)func_0x00401990(puVar29,local_9d0,local_9c8);
          }
        }
        local_9d0 = puVar29;
        *(undefined *)((long)local_9d0 + (long)local_9c8) = 0x25;
        puVar17 = local_9d0;
        goto LAB_0040a5e7;
      }
      lVar33 = puVar16[10];
      uVar4 = local_9f8;
      if (lVar33 == -1) goto switchD_0040ab16_caseD_0;
      if (bVar6 == 0x6e) {
        iVar8 = *(int *)(local_930 + lVar33 * 0x20);
        uVar4 = local_9f8;
        if (4 < iVar8 - 0x12U) goto switchD_0040ab16_caseD_0;
        puVar29 = *(undefined8 **)(local_930 + lVar33 * 0x20 + 4);
        switch(iVar8) {
        case 0x12:
          *(char *)puVar29 = (char)local_9c8;
          puVar17 = local_9d0;
          puVar38 = local_9c8;
          break;
        case 0x13:
          *(short *)puVar29 = (short)local_9c8;
          puVar17 = local_9d0;
          puVar38 = local_9c8;
          break;
        case 0x14:
          *(int *)puVar29 = (int)local_9c8;
          puVar17 = local_9d0;
          puVar38 = local_9c8;
          break;
        default:
          *(undefined8 **)puVar29 = local_9c8;
          puVar17 = local_9d0;
          puVar38 = local_9c8;
        }
        goto LAB_0040a5e7;
      }
      if (bVar6 != 0x73) {
        if ((bVar6 & 0xdf) == 0x41) {
          piVar14 = (int *)(local_930 + lVar33 * 0x20);
          iVar8 = *piVar14;
          local_9e8 = (undefined8 *)
                      ((ulong)local_9e8 & 0xffffffff00000000 | (ulong)*(uint *)(puVar16 + 2));
          if ((char *)puVar16[3] == (char *)puVar16[4]) {
            puVar29 = (undefined8 *)0x0;
          }
          else {
            if (puVar16[5] == -1) {
              puVar29 = (undefined8 *)0x0;
              pcVar36 = (char *)puVar16[3];
              do {
                pcVar31 = pcVar36 + 1;
                uVar11 = 0xffffffffffffffff;
                if (puVar29 < (undefined8 *)0x199999999999999a) {
                  uVar11 = (long)puVar29 * 10;
                }
                puVar29 = (undefined8 *)(uVar11 + (long)((int)*pcVar36 + -0x30));
                if (CARRY8(uVar11,(long)((int)*pcVar36 + -0x30))) {
                  puVar29 = (undefined8 *)0xffffffffffffffff;
                }
                pcVar36 = pcVar31;
              } while (pcVar31 != (char *)puVar16[4]);
            }
            else {
              uVar4 = local_9f8;
              if (*(int *)(local_930 + puVar16[5] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
              iVar3 = *(int *)(local_930 + puVar16[5] * 0x20 + 4);
              puVar29 = (undefined8 *)(long)iVar3;
              if (iVar3 < 0) {
                local_9e8 = (undefined8 *)((ulong)local_9e8 | 2);
                puVar29 = (undefined8 *)-(long)puVar29;
              }
            }
          }
          pcVar36 = (char *)puVar16[7];
          if ((char *)puVar16[6] == pcVar36) {
LAB_0040b0c8:
            local_a08 = (undefined8 *)((ulong)local_a08._4_4_ << 0x20);
LAB_0040b0d2:
            puVar38 = (undefined8 *)0x0;
            puVar28 = (undefined8 *)((ulong)(iVar8 == 0xc) * 2 + 0x1a);
LAB_0040b0e3:
            if (puVar28 < puVar29) {
              local_9f8 = (uint)(undefined *)((long)puVar29 + 1);
              uStack2548 = (uint)((ulong)(undefined *)((long)puVar29 + 1) >> 0x20);
              puVar28 = puVar29;
            }
            else {
              local_9f8 = (uint)(undefined *)((long)puVar28 + 1);
              uStack2548 = (uint)((ulong)(undefined *)((long)puVar28 + 1) >> 0x20);
            }
          }
          else {
            if (puVar16[8] == -1) {
              pcVar31 = (char *)puVar16[6] + 1;
              if (pcVar36 == pcVar31) {
                local_a08 = (undefined8 *)CONCAT44(local_a08._4_4_,1);
                goto LAB_0040b0d2;
              }
              puVar38 = (undefined8 *)0x0;
              do {
                pcVar34 = pcVar31 + 1;
                uVar11 = 0xffffffffffffffff;
                if (puVar38 < (undefined8 *)0x199999999999999a) {
                  uVar11 = (long)puVar38 * 10;
                }
                puVar38 = (undefined8 *)(uVar11 + (long)((int)*pcVar31 + -0x30));
                if (CARRY8(uVar11,(long)((int)*pcVar31 + -0x30))) {
                  puVar38 = (undefined8 *)0xffffffffffffffff;
                }
                pcVar31 = pcVar34;
              } while (pcVar34 != pcVar36);
            }
            else {
              uVar4 = local_9f8;
              if (*(int *)(local_930 + puVar16[8] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
              iVar3 = *(int *)(local_930 + puVar16[8] * 0x20 + 4);
              puVar38 = (undefined8 *)(long)iVar3;
              if (iVar3 < 0) goto LAB_0040b0c8;
            }
            if (iVar8 == 0xc) {
              puVar17 = (undefined8 *)&DAT_00000010;
              if ((undefined8 *)0xf < puVar38) {
                puVar17 = puVar38;
              }
              puVar28 = (undefined8 *)((long)puVar17 + 0xc);
              if (puVar17 <= puVar28) goto LAB_0040b69a;
            }
            else {
              puVar17 = (undefined8 *)0xe;
              if ((undefined8 *)0xd < puVar38) {
                puVar17 = puVar38;
              }
              puVar28 = (undefined8 *)((long)puVar17 + 0xc);
              if (puVar17 <= puVar28) {
LAB_0040b69a:
                local_a08 = (undefined8 *)CONCAT44(local_a08._4_4_,1);
                goto LAB_0040b0e3;
              }
            }
            local_9f8 = 0;
            uStack2548 = 0;
            local_a08 = (undefined8 *)CONCAT44(local_a08._4_4_,1);
            puVar28 = (undefined8 *)0xffffffffffffffff;
          }
          puVar17 = local_9d0;
          if ((undefined8 *)CONCAT44(uStack2548,local_9f8) <= puVar28 &&
              puVar28 != (undefined8 *)CONCAT44(uStack2548,local_9f8)) goto LAB_0040a455;
          if (CONCAT44(uStack2548,local_9f8) < 0x2bd) {
            puVar28 = &local_5b8;
            local_a30._0_2_ = (ushort)local_a48;
          }
          else {
            puVar17 = local_9d0;
            if (CONCAT44(uStack2548,local_9f8) == -1) goto LAB_0040a455;
            *(undefined8 *)(puVar30 + -8) = 0x40f25f;
            puVar28 = (undefined8 *)FUN_00408510();
            puVar17 = local_9d0;
            if (puVar28 == (undefined8 *)0x0) goto LAB_0040a455;
            piVar14 = (int *)(local_930 + puVar16[10] * 0x20);
            local_a30._0_2_ = (ushort)local_a48;
          }
          if (iVar8 == 0xc) {
            local_a18 = *(float10 *)(piVar14 + 4);
            local_a30 = (undefined8 *)
                        ((ulong)local_a30 & 0xffffffffffff0000 | (ulong)(ushort)local_a30);
            uVar7 = (ushort)local_a30 & 0xff | (ushort)(byte)((ulong)(ushort)local_a30 >> 8) << 8 |
                    0x300;
            *(float10 *)(puVar30 + -0x10) = local_a18;
            *(undefined8 *)(puVar30 + -0x18) = 0x40b73a;
            local_a38 = puVar28;
            local_2f8 = uVar7;
            iVar8 = FUN_00410440();
            if (iVar8 == 0) {
              if (((ulong)local_9e8 & 4) == 0) {
                local_a48._0_8_ = local_a38;
                fVar44 = local_a18;
                if (((ulong)local_9e8 & 8) != 0) {
                  local_a48._0_8_ = (undefined8 *)((long)local_a38 + 1);
                  *(undefined *)local_a38 = 0x20;
                  fVar44 = local_a18;
                }
              }
              else {
                *(undefined *)local_a38 = 0x2b;
                local_a48._0_8_ = (undefined8 *)((long)local_a38 + 1);
                fVar44 = local_a18;
              }
            }
            else {
              *(undefined *)local_a38 = 0x2d;
              local_a48._0_8_ = (undefined8 *)((long)local_a38 + 1);
              fVar44 = -local_a18;
            }
            fVar43 = (float10)0;
            if (fVar44 <= fVar43) {
              if (fVar43 < fVar44) goto LAB_0040c990;
              local_978[0] = 0;
              local_a18 = (float10)((unkuint10)local_a18 & 0xffffffff00000000);
              uVar15 = extraout_RDX;
              fVar44 = fVar43;
              fVar45 = fVar43;
LAB_0040ca03:
              if ((puVar38 < (undefined8 *)0x10) && ((uint)local_a08 != 0)) {
                if (puVar38 == (undefined8 *)0x0) {
                  local_9bc = uVar7 & 0xff | (ushort)(byte)((ulong)uVar7 >> 8) << 8;
                }
                else {
                  puVar17 = puVar38;
                  do {
                    puVar17 = (undefined8 *)((long)puVar17 + -1);
                    iVar8 = (int)ROUND(fVar44 * (float10)16.00000000);
                    local_a18 = (float10)CONCAT64(local_a18._4_6_,iVar8);
                    fVar44 = fVar44 * (float10)16.00000000 - (float10)iVar8;
                    in_ST3 = in_ST2;
                    local_9bc = uVar7;
                  } while (puVar17 != (undefined8 *)0x0);
                }
                local_9bc = local_9bc | 0xc00;
                if (((unkuint10)local_a18 & 1) == 0) {
                  bVar41 = (float10)0.50000000 < fVar44;
                }
                else {
                  bVar41 = (float10)0.50000000 <= fVar44;
                }
                if (bVar41) {
                  fVar44 = (float10)1.00000000 - fVar44;
                }
                else {
                  fVar44 = -fVar44;
                }
                if ((fVar44 != fVar43) && (puVar38 != (undefined8 *)0x0)) {
                  puVar17 = puVar38;
                  do {
                    puVar17 = (undefined8 *)((long)puVar17 + -1);
                    fVar44 = fVar44 * (float10)0.06250000;
                  } while (puVar17 != (undefined8 *)0x0);
                }
                local_9c0 = (int)ROUND(fVar44 + fVar45);
                cVar25 = (char)local_9c0;
                fVar44 = (fVar44 + fVar45) - (float10)local_9c0;
                in_ST4 = in_ST3;
                local_9ba = uVar7;
              }
              else {
                cVar25 = SUB101(local_a18,0);
              }
              local_a08 = (undefined8 *)((long)(undefined8 *)local_a48 + 2);
              *(undefined *)(undefined8 *)local_a48 = 0x30;
              cVar2 = *(char *)(puVar16 + 9);
              *(char *)((long)(undefined8 *)local_a48 + 2) = cVar25 + '0';
              *(char *)((long)(undefined8 *)local_a48 + 1) = cVar2 + '\x17';
              if (((ulong)local_9e8 & 0x10) == 0) {
                if (fVar43 < fVar44) {
                  local_a50 = (undefined8 *)
                              ((ulong)local_a50 & 0xffffffff00000000 | (ulong)(uint)(float)fVar43);
                  local_a18 = (float10)CONCAT28(local_a18._8_2_,(undefined8 *)local_a48);
                  *(undefined8 *)(puVar30 + -8) = 0x40cb6e;
                  local_a48 = fVar44;
                  func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_2f8,"%#.0f",uVar15);
                  fVar44 = (float10)local_a50._0_4_;
                  if (local_2f8._1_1_ == '\0') goto LAB_0040ccf4;
                  *(char *)((long)(undefined8 *)local_a18 + 3) = local_2f8._1_1_;
                  goto LAB_0040cba0;
                }
                if (puVar38 != (undefined8 *)0x0) goto LAB_0040cc90;
                pcVar36 = (char *)((long)(undefined8 *)local_a48 + 3);
              }
              else {
LAB_0040cc90:
                local_a50 = (undefined8 *)
                            ((ulong)local_a50 & 0xffffffff00000000 | (ulong)(uint)(float)fVar43);
                local_a18 = (float10)CONCAT28(local_a18._8_2_,(undefined8 *)local_a48);
                *(undefined8 *)(puVar30 + -8) = 0x40cccf;
                local_a48 = fVar44;
                func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_2f8,"%#.0f",uVar15);
                fVar44 = (float10)local_a50._0_4_;
                if (local_2f8._1_1_ == '\0') {
LAB_0040ccf4:
                  local_2f8._1_1_ = '.';
                }
                pcVar36 = (char *)((long)(undefined8 *)local_a48 + 4);
                *(char *)((long)(undefined8 *)local_a18 + 3) = local_2f8._1_1_;
                if (fVar44 < local_a48) {
LAB_0040cba0:
                  local_9bc = uVar7 & 0xff | (ushort)(byte)((ulong)uVar7 >> 8) << 8 | 0xc00;
                  pcVar31 = (char *)((long)(undefined8 *)local_a48 + 4);
                  fVar43 = local_a48;
                  do {
                    Var1 = local_a18;
                    pcVar36 = pcVar31 + 1;
                    cVar25 = '0';
                    local_9c0 = (int)ROUND(fVar43 * (float10)16.00000000);
                    local_a18 = (float10)CONCAT64(local_a18._4_6_,local_9c0);
                    local_a18._8_2_ = SUB102(Var1,8);
                    fVar43 = fVar43 * (float10)16.00000000 - (float10)local_9c0;
                    if (9 < local_9c0) {
                      cVar25 = *(char *)(puVar16 + 9) + -10;
                    }
                    puVar38 = (undefined8 *)
                              ((long)puVar38 + ((ulong)(puVar38 == (undefined8 *)0x0) - 1));
                    *pcVar31 = (char)local_9c0 + cVar25;
                    pcVar31 = pcVar36;
                    in_ST4 = in_ST3;
                    local_9ba = uVar7;
                  } while (fVar44 < fVar43);
                }
                if (puVar38 != (undefined8 *)0x0) {
                  pcVar34 = (char *)((long)puVar38 + (long)pcVar36);
                  pcVar31 = pcVar36;
                  do {
                    pcVar37 = pcVar31 + 1;
                    *pcVar31 = '0';
                    pcVar31 = pcVar37;
                    pcVar36 = pcVar34;
                  } while (pcVar37 != pcVar34);
                }
              }
              puVar17 = (undefined8 *)(pcVar36 + 1);
              local_a18 = (float10)CONCAT28(local_a18._8_2_,local_a38);
              *pcVar36 = *(char *)(puVar16 + 9) + '\x0f';
              *(undefined8 *)(puVar30 + -8) = 0x40cc6a;
              func_0x00401c00(puVar17,&DAT_004121be,(ulong)local_978[0]);
              cVar25 = pcVar36[1];
              while (cVar25 != '\0') {
                puVar17 = (undefined8 *)((long)puVar17 + 1);
                cVar25 = *(char *)puVar17;
              }
            }
            else {
              if (fVar44 + fVar44 != fVar44) {
LAB_0040c990:
                local_a50 = (undefined8 *)
                            ((ulong)local_a50 & 0xffffffff00000000 | (ulong)(uint)(float)fVar43);
                *(float10 *)(puVar30 + -0x10) = fVar44;
                *(undefined8 *)(puVar30 + -0x18) = 0x40c9ba;
                fVar46 = in_ST4;
                FUN_0040f510(local_978);
                local_9bc = uVar7 & 0xff | (ushort)(byte)((ulong)uVar7 >> 8) << 8 | 0xc00;
                local_a18 = (float10)CONCAT64(local_a18._4_6_,(int)ROUND(in_ST0));
                uVar15 = *(undefined8 *)(puVar30 + -8);
                fVar43 = (float10)local_a50._0_4_;
                fVar44 = in_ST0 - (float10)(int)ROUND(in_ST0);
                fVar45 = in_ST0;
                in_ST0 = in_ST1;
                in_ST1 = in_ST2;
                in_ST2 = in_ST3;
                in_ST3 = in_ST4;
                in_ST4 = fVar46;
                local_9ba = uVar7;
                goto LAB_0040ca03;
              }
              if (*(char *)(puVar16 + 9) == 'A') {
                *(undefined *)(undefined8 *)local_a48 = 0x49;
                *(undefined *)((long)(undefined8 *)local_a48 + 1) = 0x4e;
                puVar17 = (undefined8 *)((long)(undefined8 *)local_a48 + 3);
                *(undefined *)((long)(undefined8 *)local_a48 + 2) = 0x46;
                local_a08 = (undefined8 *)0x0;
                local_a18._0_8_ = local_a38;
              }
              else {
                puVar17 = (undefined8 *)((long)(undefined8 *)local_a48 + 3);
                *(undefined *)(undefined8 *)local_a48 = 0x69;
                *(undefined *)((long)(undefined8 *)local_a48 + 1) = 0x6e;
                *(undefined *)((long)(undefined8 *)local_a48 + 2) = 0x66;
                local_a08 = (undefined8 *)0x0;
                local_a18._0_8_ = local_a38;
              }
            }
            local_2f8 = (ushort)local_a30;
          }
          else {
            uVar11 = *(ulong *)(piVar14 + 4);
            local_a18 = (float10)CONCAT28(local_a18._8_2_,uVar11);
            *(undefined8 *)(puVar30 + -8) = 0x40aef9;
            local_a30 = puVar28;
            iVar8 = FUN_00410420(ZEXT816(uVar11));
            auVar49 = ZEXT816((undefined8 *)local_a18);
            if (iVar8 == 0) {
              if (((ulong)local_9e8 & 4) == 0) {
                puVar28 = local_a30;
                if (((ulong)local_9e8 & 8) != 0) {
                  puVar28 = (undefined8 *)((long)local_a30 + 1);
                  *(undefined *)local_a30 = 0x20;
                }
              }
              else {
                *(undefined *)local_a30 = 0x2b;
                puVar28 = (undefined8 *)((long)local_a30 + 1);
              }
            }
            else {
              auVar49 = ZEXT816((undefined8 *)local_a18) ^ (undefined  [16])0x8000000000000000;
              puVar28 = (undefined8 *)((long)local_a30 + 1);
              *(undefined *)local_a30 = 0x2d;
            }
            puVar17 = (undefined8 *)0x0;
            dVar47 = SUB168(auVar49,0);
            if (dVar47 <= 0.00000000) {
              if (0.00000000 < dVar47) goto LAB_0040b9f0;
              local_978[0] = 0;
              puVar21 = (undefined8 *)0x0;
              uVar9 = 0;
              dVar47 = 0.00000000;
              local_a18._0_8_ = local_a30;
            }
            else {
              if (dVar47 + dVar47 == dVar47) {
                if (*(char *)(puVar16 + 9) == 'A') {
                  *(undefined *)puVar28 = 0x49;
                  *(undefined *)((long)puVar28 + 1) = 0x4e;
                  puVar17 = (undefined8 *)((long)puVar28 + 3);
                  *(undefined *)((long)puVar28 + 2) = 0x46;
                  local_a08 = (undefined8 *)0x0;
                  local_a18._0_8_ = local_a30;
                }
                else {
                  puVar17 = (undefined8 *)((long)puVar28 + 3);
                  *(undefined *)puVar28 = 0x69;
                  *(undefined *)((long)puVar28 + 1) = 0x6e;
                  *(undefined *)((long)puVar28 + 2) = 0x66;
                  local_a08 = (undefined8 *)0x0;
                  local_a18._0_8_ = local_a30;
                }
                goto LAB_0040af70;
              }
LAB_0040b9f0:
              local_a38 = (undefined8 *)0x0;
              local_a18._0_8_ = local_a30;
              *(undefined8 *)(puVar30 + -8) = 0x40ba19;
              local_a30 = puVar28;
              dVar47 = (double)FUN_0040f4d0(auVar49,local_978);
              uVar9 = (uint)dVar47;
              puVar21 = (undefined8 *)(dVar47 - (double)uVar9);
              puVar28 = local_a30;
              puVar17 = local_a38;
            }
            cVar25 = (char)uVar9;
            if ((puVar38 < (undefined8 *)0xe) && (puVar18 = puVar38, (uint)local_a08 != 0)) {
              while (puVar18 != (undefined8 *)0x0) {
                uVar9 = (uint)((double)puVar21 * 16.00000000);
                puVar21 = (undefined8 *)((double)puVar21 * 16.00000000 - (double)uVar9);
                puVar18 = (undefined8 *)((long)puVar18 + -1);
              }
              if ((uVar9 & 1) == 0) {
                bVar41 = 0.50000000 < (double)puVar21;
              }
              else {
                bVar41 = 0.50000000 <= (double)puVar21;
              }
              if (bVar41) {
                dVar48 = 1.00000000 - (double)puVar21;
              }
              else {
                dVar48 = (double)((ulong)puVar21 ^ 0x8000000000000000);
              }
              puVar21 = puVar38;
              if (dVar48 != (double)puVar17) {
                while (puVar21 != (undefined8 *)0x0) {
                  dVar48 = dVar48 * 0.06250000;
                  puVar21 = (undefined8 *)((long)puVar21 + -1);
                }
              }
              iVar8 = (int)(dVar48 + dVar47);
              cVar25 = (char)iVar8;
              puVar21 = (undefined8 *)((dVar48 + dVar47) - (double)iVar8);
            }
            local_a08 = (undefined8 *)((long)puVar28 + 2);
            *(undefined *)puVar28 = 0x30;
            cVar2 = *(char *)(puVar16 + 9);
            *(char *)((long)puVar28 + 2) = cVar25 + '0';
            *(char *)((long)puVar28 + 1) = cVar2 + '\x17';
            if (((ulong)local_9e8 & 0x10) == 0) {
              if ((double)puVar17 < (double)puVar21) {
                local_a48 = (float10)CONCAT28(local_a48._8_2_,puVar17);
                *(undefined8 *)(puVar30 + -8) = 0x40bb70;
                local_a38 = puVar21;
                local_a30 = (undefined8 *)local_a18;
                local_a18._0_8_ = puVar28;
                func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_2f8,"%#.0f");
                if (local_2f8._1_1_ == '\0') goto LAB_0040bc0c;
                *(char *)((long)(undefined8 *)local_a18 + 3) = local_2f8._1_1_;
                goto LAB_0040bc1a;
              }
              if (puVar38 != (undefined8 *)0x0) goto LAB_0040bba0;
              pcVar36 = (char *)((long)puVar28 + 3);
            }
            else {
LAB_0040bba0:
              local_a48 = (float10)CONCAT28(local_a48._8_2_,puVar17);
              *(undefined8 *)(puVar30 + -8) = 0x40bbe3;
              local_a38 = puVar21;
              local_a30 = (undefined8 *)local_a18;
              local_a18._0_8_ = puVar28;
              func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_2f8,"%#.0f");
              if (local_2f8._1_1_ == '\0') {
LAB_0040bc0c:
                local_2f8._1_1_ = '.';
              }
              pcVar36 = (char *)((long)puVar28 + 4);
              *(char *)((long)(undefined8 *)local_a18 + 3) = local_2f8._1_1_;
              if ((double)(undefined8 *)local_a48 < (double)local_a38) {
LAB_0040bc1a:
                pcVar31 = (char *)((long)puVar28 + 4);
                puVar17 = local_a38;
                do {
                  pcVar36 = pcVar31 + 1;
                  cVar25 = '0';
                  iVar8 = (int)((double)puVar17 * 16.00000000);
                  puVar17 = (undefined8 *)((double)puVar17 * 16.00000000 - (double)iVar8);
                  if (9 < iVar8) {
                    cVar25 = *(char *)(puVar16 + 9) + -10;
                  }
                  puVar38 = (undefined8 *)
                            ((long)puVar38 + ((ulong)(puVar38 == (undefined8 *)0x0) - 1));
                  *pcVar31 = (char)iVar8 + cVar25;
                  pcVar31 = pcVar36;
                } while ((double)(undefined8 *)local_a48 < (double)puVar17);
              }
              local_a18._0_8_ = local_a30;
              if (puVar38 != (undefined8 *)0x0) {
                pcVar34 = (char *)((long)puVar38 + (long)pcVar36);
                pcVar31 = pcVar36;
                do {
                  pcVar37 = pcVar31 + 1;
                  *pcVar31 = '0';
                  local_a18._0_8_ = local_a30;
                  pcVar31 = pcVar37;
                  pcVar36 = pcVar34;
                } while (pcVar37 != pcVar34);
              }
            }
            puVar17 = (undefined8 *)(pcVar36 + 1);
            *pcVar36 = *(char *)(puVar16 + 9) + '\x0f';
            *(undefined8 *)(puVar30 + -8) = 0x40bcb2;
            func_0x00401c00(puVar17,&DAT_004121be,(ulong)local_978[0]);
            cVar25 = pcVar36[1];
            while (cVar25 != '\0') {
              puVar17 = (undefined8 *)((long)puVar17 + 1);
              cVar25 = *(char *)puVar17;
            }
          }
LAB_0040af70:
          puVar38 = local_9c8;
          puVar28 = (undefined8 *)((long)puVar17 - (long)(undefined8 *)local_a18);
          if (puVar28 < puVar29) {
            puVar29 = (undefined8 *)((long)puVar29 - (long)puVar28);
            puVar28 = (undefined8 *)((long)puVar17 + (long)puVar29);
            if (((ulong)local_9e8 & 2) == 0) {
              if ((local_a08 == (undefined8 *)0x0) || (((ulong)local_9e8 & 0x20) == 0)) {
                puVar21 = puVar28;
                puVar18 = puVar17;
                if ((undefined8 *)local_a18 < puVar17) {
                  do {
                    puVar17 = (undefined8 *)((long)puVar17 + -1);
                    *(undefined *)(undefined8 *)((long)puVar21 + -1) = *(undefined *)puVar17;
                    puVar21 = (undefined8 *)((long)puVar21 + -1);
                    puVar18 = (undefined8 *)local_a18;
                  } while (puVar17 != (undefined8 *)local_a18);
                }
                puVar17 = (undefined8 *)((long)puVar18 + (long)puVar29);
                if (puVar29 != (undefined8 *)0x0) {
                  do {
                    puVar29 = (undefined8 *)((long)puVar18 + 1);
                    *(undefined *)puVar18 = 0x20;
                    puVar18 = puVar29;
                  } while (puVar29 != puVar17);
                }
              }
              else {
                puVar21 = puVar28;
                if (local_a08 < puVar17) {
                  do {
                    puVar17 = (undefined8 *)((long)puVar17 + -1);
                    *(undefined *)(undefined8 *)((long)puVar21 + -1) = *(undefined *)puVar17;
                    puVar21 = (undefined8 *)((long)puVar21 + -1);
                  } while (puVar17 != local_a08);
                }
                puVar21 = puVar17;
                if (puVar29 != (undefined8 *)0x0) {
                  do {
                    puVar18 = (undefined8 *)((long)puVar21 + 1);
                    *(undefined *)puVar21 = 0x30;
                    puVar21 = puVar18;
                  } while (puVar18 != (undefined8 *)((long)puVar17 + (long)puVar29));
                }
              }
            }
            else {
              if (puVar29 != (undefined8 *)0x0) {
                do {
                  puVar29 = (undefined8 *)((long)puVar17 + 1);
                  *(undefined *)puVar17 = 0x20;
                  puVar17 = puVar29;
                } while (puVar28 != puVar29);
              }
            }
            puVar28 = (undefined8 *)((long)puVar28 - (long)(undefined8 *)local_a18);
          }
          uVar4 = local_9f8;
          uVar9 = uStack2548;
          if ((undefined8 *)CONCAT44(uStack2548,local_9f8) <= puVar28)
          goto switchD_0040ab16_caseD_0;
          puVar29 = local_9d0;
          if ((undefined8 *)((long)puVar39 - (long)local_9c8) <= puVar28) {
            puVar21 = (undefined8 *)0xffffffffffffffff;
            if (!CARRY8((ulong)local_9c8,(ulong)puVar28)) {
              puVar21 = (undefined8 *)((long)local_9c8 + (long)puVar28);
            }
            puVar29 = local_9d0;
            if (puVar39 < puVar21) {
              if (puVar39 == (undefined8 *)0x0) {
                puVar39 = (undefined8 *)0xc;
              }
              else {
                puVar17 = local_9d0;
                if ((long)puVar39 < 0) goto LAB_0040a455;
                puVar39 = (undefined8 *)((long)puVar39 * 2);
              }
              if (puVar39 < puVar21) {
                puVar39 = puVar21;
              }
              puVar17 = local_9d0;
              if (puVar39 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
              local_9f8 = (uint)(undefined8 *)local_a18;
              uStack2548 = (uint)((ulong)(undefined8 *)local_a18 >> 0x20);
              bVar41 = local_9d0 == local_9d8;
              if ((local_9d0 == (undefined8 *)0x0) || (bVar41)) {
                *(undefined8 *)(puVar30 + -8) = 0x40bde0;
                puVar29 = (undefined8 *)FUN_00408510(puVar39);
                local_a18._0_8_ = (undefined8 *)CONCAT44(uStack2548,local_9f8);
              }
              else {
                *(undefined8 *)(puVar30 + -8) = 0x40b04e;
                puVar29 = (undefined8 *)FUN_004085a0(local_9d0,puVar39);
                local_a18._0_8_ = (undefined8 *)CONCAT44(uStack2548,local_9f8);
              }
              puVar17 = local_9d0;
              if (puVar29 == (undefined8 *)0x0) goto LAB_0040a455;
              if ((local_9c8 != (undefined8 *)0x0) && (bVar41)) {
                local_9f8 = (uint)(undefined8 *)local_a18;
                uStack2548 = (uint)((ulong)(undefined8 *)local_a18 >> 0x20);
                *(undefined8 *)(puVar30 + -8) = 0x40c4b5;
                puVar29 = (undefined8 *)func_0x00401990(puVar29,local_9d0,local_9c8);
                local_a18._0_8_ = (undefined8 *)CONCAT44(uStack2548,local_9f8);
              }
            }
          }
          local_9d0 = puVar29;
          puVar38 = (undefined8 *)((long)puVar38 + (long)puVar28);
          lVar33 = (long)local_9c8 + (long)local_9d0;
          *(undefined8 *)(puVar30 + -8) = 0x40b09a;
          local_9c8 = (undefined8 *)local_a18;
          func_0x00401990(lVar33,(undefined8 *)local_a18,puVar28);
          puVar17 = local_9d0;
          local_a48._0_2_ = (ushort)local_a30;
          if (local_9c8 != &local_5b8) {
            *(undefined8 *)(puVar30 + -8) = 0x40b0b5;
            func_0x00401db0();
            puVar17 = local_9d0;
            local_a48._0_2_ = (ushort)local_a30;
          }
          goto LAB_0040a5e7;
        }
        puVar13 = (uint *)(local_930 + lVar33 * 0x20);
        bVar41 = (bVar6 & 0xdf) != 0x46;
        local_9f8 = *puVar13;
        if (((bVar41) && ((bVar6 & 0xdd) != 0x45)) || (1 < local_9f8 - 0xb)) goto LAB_0040a677;
        local_a30 = (undefined8 *)
                    ((ulong)local_a30 & 0xffffffff00000000 | (ulong)*(uint *)(puVar16 + 2));
        if ((char *)puVar16[3] == (char *)puVar16[4]) {
          puVar29 = (undefined8 *)0x0;
        }
        else {
          if (puVar16[5] == -1) {
            puVar29 = (undefined8 *)0x0;
            pcVar36 = (char *)puVar16[3];
            do {
              pcVar31 = pcVar36 + 1;
              uVar11 = 0xffffffffffffffff;
              if (puVar29 < (undefined8 *)0x199999999999999a) {
                uVar11 = (long)puVar29 * 10;
              }
              puVar29 = (undefined8 *)(uVar11 + (long)((int)*pcVar36 + -0x30));
              if (CARRY8(uVar11,(long)((int)*pcVar36 + -0x30))) {
                puVar29 = (undefined8 *)0xffffffffffffffff;
              }
              pcVar36 = pcVar31;
            } while (pcVar31 != (char *)puVar16[4]);
          }
          else {
            uVar4 = local_9f8;
            if (*(int *)(local_930 + puVar16[5] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
            iVar8 = *(int *)(local_930 + puVar16[5] * 0x20 + 4);
            puVar29 = (undefined8 *)(long)iVar8;
            if (iVar8 < 0) {
              local_a30 = (undefined8 *)((ulong)local_a30 | 2);
              puVar29 = (undefined8 *)-(long)puVar29;
            }
          }
        }
        pcVar36 = (char *)puVar16[6];
        if (pcVar36 == (char *)puVar16[7]) {
          puVar38 = (undefined8 *)0x6;
        }
        else {
          if (puVar16[8] == -1) {
            puVar38 = (undefined8 *)0x0;
            while (pcVar36 = pcVar36 + 1, (char *)puVar16[7] != pcVar36) {
              uVar11 = 0xffffffffffffffff;
              if (puVar38 < (undefined8 *)0x199999999999999a) {
                uVar11 = (long)puVar38 * 10;
              }
              puVar38 = (undefined8 *)(uVar11 + (long)((int)*pcVar36 + -0x30));
              if (CARRY8(uVar11,(long)((int)*pcVar36 + -0x30))) {
                puVar38 = (undefined8 *)0xffffffffffffffff;
              }
            }
          }
          else {
            uVar4 = local_9f8;
            if (*(int *)(local_930 + puVar16[8] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
            iVar8 = *(int *)(local_930 + puVar16[8] * 0x20 + 4);
            puVar38 = (undefined8 *)0x6;
            if (-1 < iVar8) {
              puVar38 = (undefined8 *)(long)iVar8;
            }
          }
        }
        if (local_9f8 == 0xc) {
          puVar17 = (undefined8 *)0x13;
          if ((undefined8 *)0x12 < puVar38) {
            puVar17 = puVar38;
          }
          if ((!bVar41) && (fVar44 = *(float10 *)(puVar13 + 4), fVar44 + fVar44 != fVar44)) {
            if (fVar44 < (float10)0) {
              fVar44 = -fVar44;
            }
            *(float10 *)(puVar30 + -0x10) = fVar44;
            *(undefined8 *)(puVar30 + -0x18) = 0x40cd5d;
            iVar8 = FUN_00408e50();
joined_r0x0040cd63:
            if ((-1 < iVar8) && (puVar17 < (undefined8 *)((long)iVar8 + (long)puVar38))) {
              puVar17 = (undefined8 *)((long)iVar8 + (long)puVar38);
            }
          }
        }
        else {
          puVar17 = (undefined8 *)&DAT_00000010;
          if ((undefined8 *)0xf < puVar38) {
            puVar17 = puVar38;
          }
          if (!bVar41) {
            dVar47 = *(double *)(puVar13 + 4);
            auVar49 = ZEXT816((ulong)dVar47);
            if (dVar47 + dVar47 != dVar47) {
              if (dVar47 < 0.00000000) {
                auVar49 = ZEXT816((ulong)dVar47) ^ (undefined  [16])0x8000000000000000;
              }
              *(undefined8 *)(puVar30 + -8) = 0x40ac35;
              iVar8 = FUN_00409da0(auVar49);
              goto joined_r0x0040cd63;
            }
          }
        }
        puVar28 = (undefined8 *)((long)puVar17 + 0xc);
        if (puVar28 < puVar17) {
          local_a18 = (float10)((unkuint10)local_a18._8_2_ << 0x40);
          puVar28 = (undefined8 *)0xffffffffffffffff;
        }
        else {
          if (puVar28 < puVar29) {
            local_a18 = (float10)CONCAT28(local_a18._8_2_,(undefined *)((long)puVar29 + 1));
            puVar28 = puVar29;
          }
          else {
            local_a18 = (float10)CONCAT28(local_a18._8_2_,(long)puVar17 + 0xd);
          }
        }
        puVar17 = local_9d0;
        if ((undefined8 *)local_a18 < puVar28) goto LAB_0040a455;
        if ((undefined8 *)local_a18 < (undefined8 *)0x2bd) {
          local_9e8 = &local_2f8;
        }
        else {
          puVar17 = local_9d0;
          if ((undefined8 *)local_a18 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
          *(undefined8 *)(puVar30 + -8) = 0x40f45c;
          local_9e8 = (undefined8 *)FUN_00408510();
          puVar17 = local_9d0;
          if (local_9e8 == (undefined8 *)0x0) goto LAB_0040a455;
        }
        if (local_9f8 == 0xc) {
          Var1 = *(unkbyte10 *)(local_930 + puVar16[10] * 0x20 + 0x10);
          local_9f8 = (uint)Var1;
          uStack2548 = (uint)((unkuint10)Var1 >> 0x20);
          uStack2544 = (undefined2)((unkuint10)Var1 >> 0x40);
          local_5b8 = local_5b8 & 0xffffffffffff0000 |
                      (ulong)(ushort)((ushort)local_a48 & 0xff |
                                     (ushort)(byte)((ulong)(ushort)local_a48 >> 8) << 8) | 0x300;
          *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
          *(long *)(puVar30 + -0x10) = (long)Var1;
          *(undefined8 *)(puVar30 + -0x18) = 0x40cdd0;
          iVar8 = FUN_00410440();
          if (iVar8 == 0) {
            if (((ulong)local_a30 & 4) == 0) {
              local_a08 = local_9e8;
              if (((ulong)local_a30 & 8) != 0) {
                local_a08 = (undefined8 *)((long)local_9e8 + 1);
                *(undefined *)local_9e8 = 0x20;
              }
            }
            else {
              local_a08 = (undefined8 *)((long)local_9e8 + 1);
              *(undefined *)local_9e8 = 0x2b;
            }
          }
          else {
            local_a08 = (undefined8 *)((long)local_9e8 + 1);
            *(undefined *)local_9e8 = 0x2d;
            fVar44 = -(float10)CONCAT28(uStack2544,CONCAT44(uStack2548,local_9f8));
            local_9f8 = SUB104(fVar44,0);
            uStack2548 = (uint)((unkuint10)fVar44 >> 0x20);
            uStack2544 = (undefined2)((unkuint10)fVar44 >> 0x40);
          }
          fVar44 = (float10)0;
          fVar43 = (float10)CONCAT28(uStack2544,CONCAT44(uStack2548,local_9f8));
          if ((fVar43 <= fVar44) || (fVar43 + fVar43 != fVar43)) {
            bVar6 = *(byte *)(puVar16 + 9) & 0xdf;
            if (bVar6 == 0x46) {
              *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
              *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
              *(undefined8 *)(puVar30 + -0x18) = 0x40ddb5;
              auVar49 = FUN_00409c20((ulong)puVar38 & 0xffffffff);
              lVar33 = SUB168(auVar49,0);
              uVar15 = *(undefined8 *)(puVar30 + -8);
              if (lVar33 == 0) {
LAB_0040e69e:
                local_5b8 = local_5b8 & 0xffffffffffff0000 | (ulong)(ushort)local_a48;
                puVar17 = local_9d0;
                goto LAB_0040a455;
              }
              *(undefined8 *)(puVar30 + -8) = 0x40ddcb;
              puVar17 = (undefined8 *)
                        func_0x00401d20(lVar33,uVar15,SUB168(auVar49 >> 0x40,0),
                                        *(undefined8 *)(puVar30 + -0x10));
              if (puVar38 < puVar17) {
                lVar26 = lVar33 + (long)puVar17;
                puVar28 = local_a08;
                do {
                  puVar35 = (undefined *)(lVar26 + -1);
                  lVar26 = lVar26 + -1;
                  *(undefined *)puVar28 = *puVar35;
                  puVar28 = (undefined8 *)((long)puVar28 + 1);
                } while (lVar26 != lVar33 + (long)puVar38);
                puVar28 = (undefined8 *)((long)local_a08 + (long)((long)puVar17 - (long)puVar38));
                puVar17 = puVar38;
              }
              else {
                puVar28 = (undefined8 *)((long)local_a08 + 1);
                *(undefined *)local_a08 = 0x30;
              }
              if ((puVar38 != (undefined8 *)0x0) || (((ulong)local_a30 & 0x10) != 0)) {
                local_9f8 = (uint)((long)puVar28 + 1);
                uStack2548 = (uint)((ulong)((long)puVar28 + 1) >> 0x20);
                *(undefined8 *)(puVar30 + -8) = 0x40e3f1;
                local_a50 = puVar17;
                local_a38 = puVar28;
                func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                puVar28 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                if (local_5b8._1_1_ == '\0') {
                  local_5b8._1_1_ = '.';
                }
                *(char *)local_a38 = local_5b8._1_1_;
                if (local_a50 < puVar38) {
                  puVar17 = puVar28;
                  do {
                    puVar21 = (undefined8 *)((long)puVar17 + 1);
                    *(undefined *)puVar17 = 0x30;
                    puVar17 = puVar21;
                  } while (puVar21 !=
                           (undefined8 *)((long)local_a38 + (1 - (long)local_a50) + (long)puVar38));
                  puVar28 = (undefined8 *)((long)((long)puVar38 - (long)local_a50) + (long)puVar28);
                }
                puVar17 = local_a50;
                puVar38 = puVar28;
                if (local_a50 != (undefined8 *)0x0) {
                  do {
                    puVar17 = (undefined8 *)((long)puVar17 + -1);
                    *(undefined *)puVar38 = *(undefined *)(lVar33 + (long)puVar17);
                    puVar38 = (undefined8 *)((long)puVar38 + 1);
                  } while (puVar17 != (undefined8 *)0x0);
                  puVar28 = (undefined8 *)((long)puVar28 + (long)local_a50);
                }
              }
              local_9f8 = (uint)puVar28;
              uStack2548 = (uint)((ulong)puVar28 >> 0x20);
              *(undefined8 *)(puVar30 + -8) = 0x40de3a;
              func_0x00401db0(lVar33);
              puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
            }
            else {
              if (bVar6 == 0x45) {
                if ((float10)CONCAT28(uStack2544,CONCAT44(uStack2548,local_9f8)) == fVar44) {
                  *(undefined *)local_a08 = 0x30;
                  if ((puVar38 != (undefined8 *)0x0) || (((ulong)local_a30 & 0x10) != 0)) {
                    local_9f8 = local_9f8 & 0xffffff00 | (uint)(puVar38 != (undefined8 *)0x0);
                    puVar17 = (undefined8 *)((long)local_a08 + 2);
                    *(undefined8 *)(puVar30 + -8) = 0x40e4c4;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                    if ((char)local_9f8 == '\0') {
                      uVar11 = 0;
                    }
                    else {
                      puVar28 = puVar17;
                      do {
                        puVar21 = (undefined8 *)((long)puVar28 + 1);
                        *(undefined *)puVar28 = 0x30;
                        puVar28 = puVar21;
                      } while (puVar21 != (undefined8 *)((long)puVar17 + (long)puVar38));
                      uVar11 = 0;
                      puVar17 = (undefined8 *)((long)puVar17 + (long)puVar38);
                    }
                  }
                  else {
                    uVar11 = 0;
                    puVar17 = (undefined8 *)((long)local_a08 + 1);
                  }
                }
                else {
                  *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
                  *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
                  *(undefined8 *)(puVar30 + -0x18) = 0x40e70c;
                  uVar9 = FUN_00408e50();
                  uVar11 = (ulong)uVar9;
                  local_a58 = (undefined8 *)((long)puVar38 + 1);
                  local_a50 = (undefined8 *)
                              ((ulong)local_a50 & 0xffffffff00000000 | (ulong)puVar38 & 0xffffffff);
                  local_a60 = (undefined8 *)((long)puVar38 + 2);
                  bVar41 = false;
                  local_a78 = puVar29;
                  local_a70 = puVar39;
                  local_a68 = puVar16;
                  while( true ) {
                    *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
                    *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
                    iVar8 = (int)uVar11;
                    *(undefined8 *)(puVar30 + -0x18) = 0x40e7c7;
                    puVar17 = (undefined8 *)
                              FUN_00409c20((ulong)(uint)((int)local_a50._0_4_ - iVar8));
                    if (puVar17 == (undefined8 *)0x0) goto LAB_0040e69e;
                    *(undefined8 *)(puVar30 + -8) = 0x40e758;
                    puVar28 = (undefined8 *)func_0x00401d20(puVar17);
                    puVar16 = local_a68;
                    puVar39 = local_a70;
                    puVar29 = local_a78;
                    if (local_a58 == puVar28) break;
                    uVar4 = local_9f8;
                    uVar9 = uStack2548;
                    if (((puVar28 < puVar38) ||
                        (local_a38 = puVar28, uVar4 = local_9f8, uVar9 = uStack2548,
                        local_a60 < puVar28)) ||
                       (local_a38 = puVar28, uVar4 = local_9f8, uVar9 = uStack2548, bVar41))
                    goto switchD_0040ab16_caseD_0;
                    bVar41 = true;
                    *(undefined8 *)(puVar30 + -8) = 0x40e796;
                    local_a38 = puVar28;
                    func_0x00401db0();
                    uVar11 = (ulong)(iVar8 + 1);
                    if (local_a38 == puVar38) {
                      uVar11 = (ulong)(iVar8 - 1);
                    }
                  }
                  if (puVar38 == (undefined8 *)0x0) {
                    puVar21 = puVar17;
                    if (*(char *)puVar17 == '1') {
LAB_0040ee43:
                      if (*(char *)((long)puVar21 + 1) == '\0') {
                        *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
                        *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
                        *(undefined8 *)(puVar30 + -0x18) = 0x40ee6f;
                        local_a50 = puVar17;
                        local_a38 = puVar28;
                        pcVar36 = (char *)FUN_00409c20((ulong)(((int)local_a50._0_4_ - iVar8) + 1));
                        puVar17 = local_a38;
                        puVar28 = local_a38;
                        if (pcVar36 == (char *)0x0) {
LAB_0040f4b9:
                          *(undefined8 *)(puVar30 + -8) = 0x40f4c1;
                          func_0x00401db0(local_a50,pcVar36,puVar28);
                          goto LAB_0040e69e;
                        }
                        uVar12 = 0xffffffffffffffff;
                        pcVar31 = pcVar36;
                        do {
                          if (uVar12 == 0) break;
                          uVar12 = uVar12 - 1;
                          cVar25 = *pcVar31;
                          pcVar31 = pcVar31 + (ulong)bVar42 * -2 + 1;
                        } while (cVar25 != '\0');
                        if ((undefined8 *)(~uVar12 - 1) == local_a38) {
                          local_9f8 = (uint)pcVar36;
                          uStack2548 = (uint)((ulong)pcVar36 >> 0x20);
                          *(undefined8 *)(puVar30 + -8) = 0x40f4a0;
                          func_0x00401db0(local_a50);
                          puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                          uVar11 = (ulong)(iVar8 - 1);
                          puVar28 = local_a38;
                        }
                        else {
                          local_a38 = local_a50;
                          local_9f8 = (uint)puVar17;
                          uStack2548 = (uint)((ulong)puVar17 >> 0x20);
                          *(undefined8 *)(puVar30 + -8) = 0x40eebe;
                          func_0x00401db0(pcVar36);
                          puVar28 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                          puVar17 = local_a38;
                        }
                      }
                    }
                    puVar21 = (undefined8 *)((long)puVar28 + -1);
                    puVar35 = (undefined *)((long)local_a08 + 1);
                    *(undefined *)local_a08 = *(undefined *)((long)puVar17 + -1 + (long)puVar28);
                    if ((puVar38 != (undefined8 *)0x0) || (((ulong)local_a30 & 0x10) != 0))
                    goto LAB_0040eb00;
                  }
                  else {
                    if (*(char *)puVar17 == '0') {
                      puVar21 = puVar17;
                      do {
                        puVar21 = (undefined8 *)((long)puVar21 + 1);
                        if (puVar21 == (undefined8 *)((long)puVar17 + (long)puVar38)) {
                          if (*(char *)puVar21 == '1') goto LAB_0040ee43;
                          puVar21 = (undefined8 *)((long)puVar28 + -1);
                          *(undefined *)local_a08 =
                               *(undefined *)((long)puVar17 + -1 + (long)puVar28);
                          goto LAB_0040eb00;
                        }
                      } while (*(char *)puVar21 == '0');
                    }
                    puVar21 = (undefined8 *)((long)puVar28 + -1);
                    *(undefined *)local_a08 = *(undefined *)((long)puVar17 + -1 + (long)puVar28);
LAB_0040eb00:
                    puVar38 = local_a08;
                    local_9f8 = (uint)((long)local_a08 + 2);
                    uStack2548 = (uint)((ulong)((long)local_a08 + 2) >> 0x20);
                    *(undefined8 *)(puVar30 + -8) = 0x40eb3f;
                    local_a50 = puVar17;
                    local_a38 = puVar21;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    puVar35 = (undefined *)CONCAT44(uStack2548,local_9f8);
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)puVar38 + 1) = local_5b8._1_1_;
                    puVar38 = local_a38;
                    puVar27 = puVar35;
                    puVar17 = local_a50;
                    if (local_a38 != (undefined8 *)0x0) {
                      do {
                        puVar38 = (undefined8 *)((long)puVar38 + -1);
                        *puVar27 = *(undefined *)((long)local_a50 + (long)puVar38);
                        puVar27 = puVar27 + 1;
                      } while (puVar38 != (undefined8 *)0x0);
                      puVar35 = puVar35 + (long)local_a38;
                      puVar17 = local_a50;
                    }
                  }
                  local_9f8 = (uint)puVar35;
                  uStack2548 = (uint)((ulong)puVar35 >> 0x20);
                  *(undefined8 *)(puVar30 + -8) = 0x40eb9c;
                  func_0x00401db0(puVar17);
                  puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                }
                lVar33 = (long)puVar17 + 1;
                local_9f8 = (uint)lVar33;
                uStack2548 = (uint)((ulong)lVar33 >> 0x20);
                *(undefined *)puVar17 = *(undefined *)(puVar16 + 9);
                *(undefined8 *)(puVar30 + -8) = 0x40d533;
                local_a38 = puVar17;
                func_0x00401c00(lVar33,"%+.2d",uVar11);
                puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                cVar25 = *(char *)((long)local_a38 + 1);
                while (cVar25 != '\0') {
                  puVar17 = (undefined8 *)((long)puVar17 + 1);
                  cVar25 = *(char *)puVar17;
                }
              }
              else {
                uVar4 = local_9f8;
                uVar9 = uStack2548;
                if (bVar6 != 0x47) goto switchD_0040ab16_caseD_0;
                if (puVar38 == (undefined8 *)0x0) {
                  puVar38 = (undefined8 *)0x1;
                }
                if ((float10)CONCAT28(uStack2544,CONCAT44(uStack2548,local_9f8)) == fVar44) {
                  if (((ulong)local_a30 & 0x10) == 0) {
                    *(undefined *)local_a08 = 0x30;
                    puVar17 = (undefined8 *)((long)local_a08 + 1);
                  }
                  else {
                    *(undefined *)local_a08 = 0x30;
                    puVar28 = (undefined8 *)((long)local_a08 + 2);
                    *(undefined8 *)(puVar30 + -8) = 0x40e57f;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                    puVar21 = (undefined8 *)((long)puVar28 + (long)puVar38 + -1);
                    puVar17 = puVar28;
                    if ((long)puVar38 + -1 != 0) {
                      do {
                        puVar38 = (undefined8 *)((long)puVar28 + 1);
                        *(undefined *)puVar28 = 0x30;
                        puVar17 = puVar21;
                        puVar28 = puVar38;
                      } while (puVar38 != puVar21);
                    }
                  }
                  goto LAB_0040ce60;
                }
                *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
                *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
                *(undefined8 *)(puVar30 + -0x18) = 0x40e5d5;
                uVar10 = FUN_00408e50();
                local_a58 = (undefined8 *)((long)puVar38 + -1);
                local_a60 = (undefined8 *)((long)puVar38 + 1);
                local_a50 = (undefined8 *)
                            ((ulong)local_a50 & 0xffffffff00000000 | (ulong)puVar38 & 0xffffffff);
                bVar41 = false;
                local_a78 = puVar29;
                local_a70 = puVar39;
                local_a68 = puVar16;
                while( true ) {
                  *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
                  *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
                  *(undefined8 *)(puVar30 + -0x18) = 0x40e690;
                  puVar17 = (undefined8 *)FUN_00409c20((ulong)(((int)local_a50._0_4_ - uVar10) - 1))
                  ;
                  if (puVar17 == (undefined8 *)0x0) goto LAB_0040e69e;
                  *(undefined8 *)(puVar30 + -8) = 0x40e620;
                  puVar28 = (undefined8 *)func_0x00401d20(puVar17);
                  puVar16 = local_a68;
                  puVar39 = local_a70;
                  puVar29 = local_a78;
                  if (puVar28 == puVar38) break;
                  uVar4 = local_9f8;
                  uVar9 = uStack2548;
                  if (((puVar28 < local_a58) ||
                      (local_a38 = puVar28, uVar4 = local_9f8, uVar9 = uStack2548,
                      local_a60 < puVar28)) ||
                     (local_a38 = puVar28, uVar4 = local_9f8, uVar9 = uStack2548, bVar41))
                  goto switchD_0040ab16_caseD_0;
                  bVar41 = true;
                  *(undefined8 *)(puVar30 + -8) = 0x40e65e;
                  local_a38 = puVar28;
                  func_0x00401db0();
                  uVar9 = uVar10 - 1;
                  uVar10 = uVar10 + 1;
                  if (local_a38 < puVar38) {
                    uVar10 = uVar9;
                  }
                }
                puVar21 = (undefined8 *)((long)puVar38 + -1);
                puVar28 = puVar17;
                if (puVar21 == (undefined8 *)0x0) {
LAB_0040e9d0:
                  if ((*(char *)puVar28 == '1') && (*(char *)((long)puVar28 + 1) == '\0')) {
                    *(ulong *)(puVar30 + -8) = CONCAT62(uStack2542,uStack2544);
                    *(ulong *)(puVar30 + -0x10) = CONCAT44(uStack2548,local_9f8);
                    *(undefined8 *)(puVar30 + -0x18) = 0x40f025;
                    local_a50 = puVar17;
                    local_a38 = puVar21;
                    pcVar36 = (char *)FUN_00409c20((ulong)((int)local_a50._0_4_ - uVar10));
                    puVar17 = local_a38;
                    puVar28 = *(undefined8 **)(puVar30 + -8);
                    if (pcVar36 == (char *)0x0) goto LAB_0040f4b9;
                    uVar11 = 0xffffffffffffffff;
                    pcVar31 = pcVar36;
                    do {
                      if (uVar11 == 0) break;
                      uVar11 = uVar11 - 1;
                      cVar25 = *pcVar31;
                      pcVar31 = pcVar31 + (ulong)bVar42 * -2 + 1;
                    } while (cVar25 != '\0');
                    if ((undefined8 *)(~uVar11 - 1) == puVar38) {
                      local_9f8 = (uint)pcVar36;
                      uStack2548 = (uint)((ulong)pcVar36 >> 0x20);
                      *(undefined8 *)(puVar30 + -8) = 0x40f34b;
                      func_0x00401db0(local_a50);
                      puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                      uVar10 = uVar10 - 1;
                      puVar21 = local_a38;
                    }
                    else {
                      local_a38 = local_a50;
                      local_9f8 = (uint)puVar17;
                      uStack2548 = (uint)((ulong)puVar17 >> 0x20);
                      *(undefined8 *)(puVar30 + -8) = 0x40f075;
                      func_0x00401db0(pcVar36);
                      puVar17 = local_a38;
                      puVar21 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                    }
                  }
                }
                else {
                  if (*(char *)puVar17 == '0') {
                    puVar28 = puVar17;
                    do {
                      puVar28 = (undefined8 *)((long)puVar28 + 1);
                      if (puVar28 == (undefined8 *)((long)puVar17 + (long)puVar21))
                      goto LAB_0040e9d0;
                    } while (*(char *)puVar28 == '0');
                  }
                }
                if (((ulong)local_a30 & 0x10) == 0) {
                  puVar28 = (undefined8 *)0x0;
                  cVar25 = *(char *)puVar17;
                  while ((cVar25 == '0' &&
                         (puVar28 = (undefined8 *)((long)puVar28 + 1), puVar28 != puVar38))) {
                    cVar25 = *(char *)((long)puVar17 + (long)puVar28);
                  }
                  if (((int)uVar10 < -4) || ((long)puVar38 <= (long)(int)uVar10)) {
                    bVar41 = puVar28 < puVar21;
                    *(undefined *)local_a08 = *(undefined *)((long)puVar17 + (long)puVar21);
                    if (bVar41) goto LAB_0040ec86;
                    pcVar36 = (char *)((long)local_a08 + 1);
                    goto LAB_0040ebe8;
                  }
LAB_0040ea1d:
                  if ((int)uVar10 < 0) {
                    *(undefined *)local_a08 = 0x30;
                    local_9f8 = (uint)((long)local_a08 + 2);
                    uStack2548 = (uint)((ulong)((long)local_a08 + 2) >> 0x20);
                    *(undefined8 *)(puVar30 + -8) = 0x40f2d5;
                    local_a50 = puVar17;
                    local_a38 = puVar28;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                    lVar33 = 0;
                    while ((long)(int)~uVar10 != lVar33) {
                      *(undefined *)(CONCAT44(uStack2548,local_9f8) + lVar33) = 0x30;
                      lVar33 = lVar33 + 1;
                    }
                    puVar21 = (undefined8 *)(CONCAT44(uStack2548,local_9f8) + (long)(int)~uVar10);
                    while (puVar17 = local_a50, local_a38 < puVar38) {
                      puVar38 = (undefined8 *)((long)puVar38 + -1);
                      *(undefined *)puVar21 = *(undefined *)((long)local_a50 + (long)puVar38);
                      puVar21 = (undefined8 *)((long)puVar21 + 1);
                    }
                  }
                  else {
                    lVar33 = (long)puVar17 + (long)puVar38;
                    puVar21 = (undefined8 *)((long)local_a08 + (long)(int)(uVar10 + 1));
                    puVar18 = local_a08;
                    do {
                      puVar35 = (undefined *)(lVar33 + -1);
                      puVar23 = (undefined8 *)((long)puVar18 + 1);
                      lVar33 = lVar33 + -1;
                      *(undefined *)puVar18 = *puVar35;
                      puVar18 = puVar23;
                    } while (puVar23 != puVar21);
                    puVar38 = (undefined8 *)((long)puVar38 - (long)(int)(uVar10 + 1));
                    local_9f8 = local_9f8 & 0xffffff00 | (uint)(puVar28 < puVar38);
                    if ((puVar28 < puVar38) || (((ulong)local_a30 & 0x10) != 0)) {
                      local_a38 = (undefined8 *)((long)puVar21 + 1);
                      *(undefined8 *)(puVar30 + -8) = 0x40f1ab;
                      local_a58 = puVar17;
                      local_a50 = puVar28;
                      func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                      if (local_5b8._1_1_ == '\0') {
                        local_5b8._1_1_ = '.';
                      }
                      *(char *)puVar21 = local_5b8._1_1_;
                      puVar28 = local_a38;
                      puVar21 = local_a38;
                      puVar17 = local_a58;
                      if ((char)local_9f8 != '\0') {
                        do {
                          puVar38 = (undefined8 *)((long)puVar38 + -1);
                          puVar21 = (undefined8 *)((long)puVar28 + 1);
                          *(undefined *)puVar28 = *(undefined *)((long)local_a58 + (long)puVar38);
                          puVar28 = puVar21;
                          puVar17 = local_a58;
                        } while (local_a50 < puVar38);
                      }
                    }
                  }
                }
                else {
                  if ((int)uVar10 < -4) {
                    bVar41 = puVar21 != (undefined8 *)0x0;
                    puVar28 = (undefined8 *)0x0;
                    *(undefined *)local_a08 = *(undefined *)((long)puVar17 + (long)puVar21);
                  }
                  else {
                    if ((long)(int)uVar10 < (long)puVar38) {
                      puVar28 = (undefined8 *)0x0;
                      goto LAB_0040ea1d;
                    }
                    bVar41 = puVar21 != (undefined8 *)0x0;
                    puVar28 = (undefined8 *)0x0;
                    *(undefined *)local_a08 = *(undefined *)((long)puVar17 + (long)puVar21);
                  }
LAB_0040ec86:
                  local_a50 = (undefined8 *)((ulong)local_a50 & 0xffffffffffffff00 | (ulong)bVar41);
                  pcVar36 = (char *)((long)local_a08 + 2);
                  local_9f8 = (uint)puVar28;
                  uStack2548 = (uint)((ulong)puVar28 >> 0x20);
                  *(undefined8 *)(puVar30 + -8) = 0x40ecca;
                  local_a58 = puVar17;
                  local_a38 = puVar21;
                  func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                  if (local_5b8._1_1_ == '\0') {
                    local_5b8._1_1_ = '.';
                  }
                  *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                  puVar17 = local_a58;
                  if ((char)local_a50 != '\0') {
                    pcVar31 = pcVar36;
                    do {
                      local_a38 = (undefined8 *)((long)local_a38 + -1);
                      pcVar36 = pcVar31 + 1;
                      *pcVar31 = *(char *)((long)local_a58 + (long)local_a38);
                      puVar17 = local_a58;
                      pcVar31 = pcVar36;
                    } while ((undefined8 *)CONCAT44(uStack2548,local_9f8) < local_a38);
                  }
LAB_0040ebe8:
                  pcVar31 = pcVar36 + 1;
                  local_9f8 = (uint)pcVar31;
                  uStack2548 = (uint)((ulong)pcVar31 >> 0x20);
                  *pcVar36 = *(char *)(puVar16 + 9) + -2;
                  *(undefined8 *)(puVar30 + -8) = 0x40ec18;
                  local_a38 = puVar17;
                  func_0x00401c00(pcVar31,"%+.2d",(ulong)uVar10);
                  cVar25 = pcVar36[1];
                  puVar21 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                  while (puVar17 = local_a38, cVar25 != '\0') {
                    puVar21 = (undefined8 *)((long)puVar21 + 1);
                    cVar25 = *(char *)puVar21;
                  }
                }
                local_9f8 = (uint)puVar21;
                uStack2548 = (uint)((ulong)puVar21 >> 0x20);
                *(undefined8 *)(puVar30 + -8) = 0x40ea7b;
                func_0x00401db0(puVar17);
                puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
              }
            }
          }
          else {
            if ((byte)(*(char *)(puVar16 + 9) + 0xbfU) < 0x1a) {
              puVar17 = (undefined8 *)((long)local_a08 + 3);
              *(undefined *)local_a08 = 0x49;
              *(undefined *)((long)local_a08 + 1) = 0x4e;
              *(undefined *)((long)local_a08 + 2) = 0x46;
              local_a08 = (undefined8 *)0x0;
            }
            else {
              *(undefined *)local_a08 = 0x69;
              *(undefined *)((long)local_a08 + 1) = 0x6e;
              puVar17 = (undefined8 *)((long)local_a08 + 3);
              *(undefined *)((long)local_a08 + 2) = 0x66;
              local_a08 = (undefined8 *)0x0;
            }
          }
LAB_0040ce60:
          local_5b8 = local_5b8 & 0xffffffffffff0000 | (ulong)(ushort)local_a48;
        }
        else {
          uVar11 = *(ulong *)(local_930 + puVar16[10] * 0x20 + 0x10);
          local_9f8 = (uint)uVar11;
          uStack2548 = (uint)(uVar11 >> 0x20);
          *(undefined8 *)(puVar30 + -8) = 0x40ace0;
          iVar8 = FUN_00410420(ZEXT816(uVar11));
          if (iVar8 == 0) {
            if (((ulong)local_a30 & 4) == 0) {
              local_a08 = local_9e8;
              if (((ulong)local_a30 & 8) != 0) {
                local_a08 = (undefined8 *)((long)local_9e8 + 1);
                *(undefined *)local_9e8 = 0x20;
              }
            }
            else {
              local_a08 = (undefined8 *)((long)local_9e8 + 1);
              *(undefined *)local_9e8 = 0x2b;
            }
          }
          else {
            uStack2548 = uStack2548 ^ 0x80000000;
            local_a08 = (undefined8 *)((long)local_9e8 + 1);
            *(undefined *)local_9e8 = 0x2d;
          }
          dVar47 = (double)CONCAT44(uStack2548,local_9f8);
          if ((dVar47 <= 0.00000000) || (dVar47 + dVar47 != dVar47)) {
            bVar6 = *(byte *)(puVar16 + 9) & 0xdf;
            if (bVar6 == 0x46) {
              *(undefined8 *)(puVar30 + -8) = 0x40d769;
              lVar33 = FUN_00409fc0(ZEXT816(CONCAT44(uStack2548,local_9f8)),
                                    (ulong)puVar38 & 0xffffffff);
              puVar17 = local_9d0;
              if (lVar33 == 0) goto LAB_0040a455;
              *(undefined8 *)(puVar30 + -8) = 0x40d77d;
              puVar17 = (undefined8 *)func_0x00401d20(lVar33);
              if (puVar38 < puVar17) {
                lVar26 = lVar33 + (long)puVar17;
                puVar28 = local_a08;
                do {
                  puVar35 = (undefined *)(lVar26 + -1);
                  lVar26 = lVar26 + -1;
                  *(undefined *)puVar28 = *puVar35;
                  puVar28 = (undefined8 *)((long)puVar28 + 1);
                } while (lVar26 != lVar33 + (long)puVar38);
                puVar28 = (undefined8 *)((long)local_a08 + (long)((long)puVar17 - (long)puVar38));
                puVar17 = puVar38;
              }
              else {
                puVar28 = (undefined8 *)((long)local_a08 + 1);
                *(undefined *)local_a08 = 0x30;
              }
              if ((puVar38 != (undefined8 *)0x0) || (((ulong)local_a30 & 0x10) != 0)) {
                local_a48 = (float10)CONCAT28(local_a48._8_2_,puVar17);
                local_9f8 = (uint)((long)puVar28 + 1);
                uStack2548 = (uint)((ulong)((long)puVar28 + 1) >> 0x20);
                *(undefined8 *)(puVar30 + -8) = 0x40db28;
                local_a38 = puVar28;
                func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                puVar28 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                if (local_5b8._1_1_ == '\0') {
                  local_5b8._1_1_ = '.';
                }
                *(char *)local_a38 = local_5b8._1_1_;
                if ((undefined8 *)local_a48 < puVar38) {
                  puVar17 = puVar28;
                  do {
                    puVar21 = (undefined8 *)((long)puVar17 + 1);
                    *(undefined *)puVar17 = 0x30;
                    puVar17 = puVar21;
                  } while (puVar21 !=
                           (undefined8 *)
                           ((long)local_a38 + (1 - (long)(undefined8 *)local_a48) + (long)puVar38));
                  puVar28 = (undefined8 *)
                            ((long)((long)puVar38 - (long)(undefined8 *)local_a48) + (long)puVar28);
                }
                puVar17 = (undefined8 *)local_a48;
                puVar38 = puVar28;
                if ((undefined8 *)local_a48 != (undefined8 *)0x0) {
                  do {
                    puVar17 = (undefined8 *)((long)puVar17 + -1);
                    *(undefined *)puVar38 = *(undefined *)(lVar33 + (long)puVar17);
                    puVar38 = (undefined8 *)((long)puVar38 + 1);
                  } while (puVar17 != (undefined8 *)0x0);
                  puVar28 = (undefined8 *)((long)puVar28 + (long)(undefined8 *)local_a48);
                }
              }
              local_9f8 = (uint)puVar28;
              uStack2548 = (uint)((ulong)puVar28 >> 0x20);
              *(undefined8 *)(puVar30 + -8) = 0x40d7e9;
              func_0x00401db0(lVar33);
              puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
            }
            else {
              if (bVar6 == 0x45) {
                if ((double)CONCAT44(uStack2548,local_9f8) == 0.00000000) {
                  *(undefined *)local_a08 = 0x30;
                  if ((puVar38 != (undefined8 *)0x0) || (((ulong)local_a30 & 0x10) != 0)) {
                    local_9f8 = local_9f8 & 0xffffff00 | (uint)(puVar38 != (undefined8 *)0x0);
                    puVar17 = (undefined8 *)((long)local_a08 + 2);
                    *(undefined8 *)(puVar30 + -8) = 0x40dc03;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                    if ((char)local_9f8 == '\0') {
                      uVar11 = 0;
                    }
                    else {
                      puVar28 = puVar17;
                      do {
                        puVar21 = (undefined8 *)((long)puVar28 + 1);
                        *(undefined *)puVar28 = 0x30;
                        puVar28 = puVar21;
                      } while (puVar21 != (undefined8 *)((long)puVar17 + (long)puVar38));
                      uVar11 = 0;
                      puVar17 = (undefined8 *)((long)puVar17 + (long)puVar38);
                    }
                  }
                  else {
                    uVar11 = 0;
                    puVar17 = (undefined8 *)((long)local_a08 + 1);
                  }
                }
                else {
                  *(undefined8 *)(puVar30 + -8) = 0x40e035;
                  uVar9 = FUN_00409da0(ZEXT816(CONCAT44(uStack2548,local_9f8)));
                  uVar11 = (ulong)uVar9;
                  local_a50 = (undefined8 *)((long)puVar38 + 1);
                  local_a38 = (undefined8 *)
                              ((ulong)local_a38 & 0xffffffff00000000 | (ulong)puVar38 & 0xffffffff);
                  bVar41 = false;
                  local_a58 = (undefined8 *)((long)puVar38 + 2);
                  local_a70 = puVar29;
                  local_a68 = puVar39;
                  local_a60 = puVar16;
                  while( true ) {
                    iVar8 = (int)uVar11;
                    local_a48 = (float10)CONCAT64(local_a48._4_6_,(uint)local_a38 - iVar8);
                    *(undefined8 *)(puVar30 + -8) = 0x40e0cb;
                    puVar28 = (undefined8 *)
                              FUN_00409fc0(ZEXT816(CONCAT44(uStack2548,local_9f8)),
                                           (ulong)((uint)local_a38 - iVar8));
                    puVar17 = local_9d0;
                    if (puVar28 == (undefined8 *)0x0) goto LAB_0040a455;
                    *(undefined8 *)(puVar30 + -8) = 0x40e0df;
                    puVar17 = (undefined8 *)func_0x00401d20(puVar28);
                    puVar16 = local_a60;
                    puVar39 = local_a68;
                    puVar29 = local_a70;
                    if (local_a50 == puVar17) break;
                    uVar4 = local_9f8;
                    uVar9 = uStack2548;
                    if (((puVar17 < puVar38) ||
                        (uVar4 = local_9f8, uVar9 = uStack2548, local_a58 < puVar17)) ||
                       (uVar4 = local_9f8, uVar9 = uStack2548, bVar41))
                    goto switchD_0040ab16_caseD_0;
                    bVar41 = true;
                    *(undefined8 *)(puVar30 + -8) = 0x40e0a2;
                    func_0x00401db0();
                    uVar11 = (ulong)(iVar8 + 1);
                    if (puVar17 == puVar38) {
                      uVar11 = (ulong)(iVar8 - 1);
                    }
                  }
                  if (puVar38 == (undefined8 *)0x0) {
                    puVar21 = puVar28;
                    if (*(char *)puVar28 == '1') {
LAB_0040eedc:
                      local_a58 = puVar28;
                      if (*(char *)((long)puVar21 + 1) == '\0') {
                        *(undefined8 *)(puVar30 + -8) = 0x40ef11;
                        local_a58 = puVar28;
                        local_a50 = puVar17;
                        local_a38 = puVar28;
                        lVar33 = FUN_00409fc0(ZEXT816(CONCAT44(uStack2548,local_9f8)),
                                              (ulong)((uint)local_a48 + 1));
                        puVar17 = local_a50;
                        if (lVar33 == 0) {
                          *(undefined8 *)(puVar30 + -8) = 0x40f36c;
                          func_0x00401db0(local_a58);
                          puVar17 = local_9d0;
                          goto LAB_0040a455;
                        }
                        local_a50 = local_a58;
                        local_a48 = (float10)CONCAT28(local_a48._8_2_,puVar17);
                        local_9f8 = (uint)lVar33;
                        uStack2548 = (uint)((ulong)lVar33 >> 0x20);
                        *(undefined8 *)(puVar30 + -8) = 0x40ef53;
                        puVar28 = (undefined8 *)func_0x00401d20(lVar33);
                        puVar17 = local_a38;
                        uVar15 = CONCAT44(uStack2548,local_9f8);
                        local_a38 = (undefined8 *)local_a48;
                        if (puVar28 == (undefined8 *)local_a48) {
                          uVar11 = (ulong)(iVar8 - 1);
                          *(undefined8 *)(puVar30 + -8) = 0x40efcc;
                          func_0x00401db0(local_a50,(undefined8 *)local_a48,uVar15);
                          puVar28 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                          puVar17 = local_a38;
                        }
                        else {
                          local_9f8 = (uint)puVar17;
                          uStack2548 = (uint)((ulong)puVar17 >> 0x20);
                          *(undefined8 *)(puVar30 + -8) = 0x40ef8a;
                          func_0x00401db0(uVar15,(undefined8 *)local_a48,uVar15);
                          puVar28 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                          puVar17 = local_a38;
                        }
                      }
                    }
                    lVar33 = (long)puVar17 + -1;
                    puVar35 = (undefined *)((long)local_a08 + 1);
                    *(undefined *)local_a08 = *(undefined *)((long)puVar28 + -1 + (long)puVar17);
                    if ((puVar38 != (undefined8 *)0x0) || (((ulong)local_a30 & 0x10) != 0))
                    goto LAB_0040e156;
                  }
                  else {
                    if (*(char *)puVar28 == '0') {
                      puVar21 = puVar28;
                      do {
                        puVar21 = (undefined8 *)((long)puVar21 + 1);
                        if (puVar21 == (undefined8 *)((long)puVar28 + (long)puVar38)) {
                          if (*(char *)puVar21 == '1') goto LAB_0040eedc;
                          break;
                        }
                      } while (*(char *)puVar21 == '0');
                    }
                    lVar33 = (long)puVar17 + -1;
                    *(undefined *)local_a08 = *(undefined *)((long)puVar28 + -1 + (long)puVar17);
LAB_0040e156:
                    puVar17 = local_a08;
                    local_a48 = (float10)CONCAT28(local_a48._8_2_,lVar33);
                    local_9f8 = (uint)((long)local_a08 + 2);
                    uStack2548 = (uint)((ulong)((long)local_a08 + 2) >> 0x20);
                    *(undefined8 *)(puVar30 + -8) = 0x40e195;
                    local_a38 = puVar28;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8);
                    puVar35 = (undefined *)CONCAT44(uStack2548,local_9f8);
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)puVar17 + 1) = local_5b8._1_1_;
                    lVar33 = (long)(undefined8 *)local_a48;
                    puVar27 = puVar35;
                    puVar28 = local_a38;
                    if ((undefined8 *)local_a48 != (undefined8 *)0x0) {
                      do {
                        lVar33 = lVar33 + -1;
                        *puVar27 = *(undefined *)((long)local_a38 + lVar33);
                        puVar27 = puVar27 + 1;
                      } while (lVar33 != 0);
                      puVar35 = puVar35 + (long)(undefined8 *)local_a48;
                      puVar28 = local_a38;
                    }
                  }
                  local_9f8 = (uint)puVar35;
                  uStack2548 = (uint)((ulong)puVar35 >> 0x20);
                  *(undefined8 *)(puVar30 + -8) = 0x40e1f3;
                  func_0x00401db0(puVar28);
                  puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                }
                lVar33 = (long)puVar17 + 1;
                local_9f8 = (uint)lVar33;
                uStack2548 = (uint)((ulong)lVar33 >> 0x20);
                *(undefined *)puVar17 = *(undefined *)(puVar16 + 9);
                *(undefined8 *)(puVar30 + -8) = 0x40c964;
                local_a38 = puVar17;
                func_0x00401c00(lVar33,"%+.2d",uVar11);
                puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                cVar25 = *(char *)((long)local_a38 + 1);
                while (cVar25 != '\0') {
                  puVar17 = (undefined8 *)((long)puVar17 + 1);
                  cVar25 = *(char *)puVar17;
                }
              }
              else {
                uVar4 = local_9f8;
                uVar9 = uStack2548;
                if (bVar6 != 0x47) goto switchD_0040ab16_caseD_0;
                if (puVar38 == (undefined8 *)0x0) {
                  puVar38 = (undefined8 *)0x1;
                }
                if ((double)CONCAT44(uStack2548,local_9f8) == 0.00000000) {
                  if (((ulong)local_a30 & 0x10) == 0) {
                    *(undefined *)local_a08 = 0x30;
                    puVar17 = (undefined8 *)((long)local_a08 + 1);
                  }
                  else {
                    *(undefined *)local_a08 = 0x30;
                    puVar17 = (undefined8 *)((long)local_a08 + 2);
                    *(undefined8 *)(puVar30 + -8) = 0x40dcfc;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    puVar28 = (undefined8 *)((long)puVar17 + (long)puVar38 + -1);
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                    puVar21 = puVar17;
                    if ((long)puVar38 + -1 != 0) {
                      do {
                        puVar38 = (undefined8 *)((long)puVar21 + 1);
                        *(undefined *)puVar21 = 0x30;
                        puVar17 = puVar28;
                        puVar21 = puVar38;
                      } while (puVar38 != puVar28);
                    }
                  }
                  goto LAB_0040ad70;
                }
                *(undefined8 *)(puVar30 + -8) = 0x40de53;
                uVar10 = FUN_00409da0(ZEXT816(CONCAT44(uStack2548,local_9f8)));
                local_a50 = (undefined8 *)((long)puVar38 + -1);
                local_a48 = (float10)CONCAT64(local_a48._4_6_,(int)puVar38);
                bVar41 = false;
                local_a58 = (undefined8 *)((long)puVar38 + 1);
                local_a70 = puVar29;
                local_a68 = puVar39;
                local_a60 = puVar16;
                local_a38 = puVar38;
                while( true ) {
                  *(undefined8 *)(puVar30 + -8) = 0x40def9;
                  local_a48._0_8_ =
                       (undefined8 *)
                       FUN_00409fc0(ZEXT816(CONCAT44(uStack2548,local_9f8)),
                                    (ulong)(((uint)local_a48 - uVar10) - 1));
                  puVar17 = local_9d0;
                  if ((undefined8 *)local_a48 == (undefined8 *)0x0) goto LAB_0040a455;
                  *(undefined8 *)(puVar30 + -8) = 0x40df0d;
                  puVar38 = (undefined8 *)func_0x00401d20();
                  puVar17 = local_a38;
                  puVar16 = local_a60;
                  puVar39 = local_a68;
                  puVar29 = local_a70;
                  if (puVar38 == local_a38) break;
                  uVar4 = local_9f8;
                  uVar9 = uStack2548;
                  if (((puVar38 < local_a50) ||
                      (uVar4 = local_9f8, uVar9 = uStack2548, local_a58 < puVar38)) ||
                     (uVar4 = local_9f8, uVar9 = uStack2548, bVar41)) goto switchD_0040ab16_caseD_0;
                  bVar41 = true;
                  *(undefined8 *)(puVar30 + -8) = 0x40dece;
                  func_0x00401db0();
                  uVar9 = uVar10 - 1;
                  uVar10 = uVar10 + 1;
                  if (puVar38 < local_a38) {
                    uVar10 = uVar9;
                  }
                }
                puVar38 = (undefined8 *)((long)local_a38 + -1);
                puVar28 = (undefined8 *)local_a48;
                if (puVar38 == (undefined8 *)0x0) {
LAB_0040df72:
                  if ((*(char *)puVar28 == '1') &&
                     (local_a50 = (undefined8 *)local_a48, *(char *)((long)puVar28 + 1) == '\0')) {
                    local_a48 = (float10)CONCAT28(local_a48._8_2_,puVar38);
                    *(undefined8 *)(puVar30 + -8) = 0x40f0ce;
                    local_a50 = (undefined8 *)local_a48;
                    local_a38 = (undefined8 *)local_a48;
                    lVar33 = FUN_00409fc0(ZEXT816(CONCAT44(uStack2548,local_9f8)),
                                          (ulong)((uint)local_a48 - uVar10));
                    if (lVar33 == 0) {
                      *(undefined8 *)(puVar30 + -8) = 0x40f231;
                      func_0x00401db0(local_a50);
                      puVar17 = local_9d0;
                      goto LAB_0040a455;
                    }
                    local_9f8 = (uint)lVar33;
                    uStack2548 = (uint)((ulong)lVar33 >> 0x20);
                    *(undefined8 *)(puVar30 + -8) = 0x40f110;
                    puVar28 = (undefined8 *)func_0x00401d20(lVar33);
                    puVar38 = local_a38;
                    local_a38 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                    if (puVar28 == puVar17) {
                      local_9f8 = SUB104(local_a48,0);
                      uStack2548 = (uint)((unkuint10)local_a48 >> 0x20);
                      *(undefined8 *)(puVar30 + -8) = 0x40f210;
                      func_0x00401db0();
                      uVar10 = uVar10 - 1;
                      puVar38 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                      local_a48._0_8_ = local_a38;
                    }
                    else {
                      local_a38 = (undefined8 *)local_a48;
                      local_9f8 = (uint)puVar38;
                      uStack2548 = (uint)((ulong)puVar38 >> 0x20);
                      *(undefined8 *)(puVar30 + -8) = 0x40f14b;
                      func_0x00401db0();
                      local_a48._0_8_ = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                      puVar38 = local_a38;
                    }
                  }
                }
                else {
                  if (*(char *)(undefined8 *)local_a48 == '0') {
                    puVar28 = (undefined8 *)local_a48;
                    do {
                      puVar28 = (undefined8 *)((long)puVar28 + 1);
                      if (puVar28 == (undefined8 *)((long)(undefined8 *)local_a48 + (long)puVar38))
                      goto LAB_0040df72;
                    } while (*(char *)puVar28 == '0');
                  }
                }
                if (((ulong)local_a30 & 0x10) == 0) {
                  puVar28 = (undefined8 *)0x0;
                  cVar25 = *(char *)(undefined8 *)local_a48;
                  while ((cVar25 == '0' &&
                         (puVar28 = (undefined8 *)((long)puVar28 + 1), puVar28 != puVar17))) {
                    cVar25 = *(char *)((long)(undefined8 *)local_a48 + (long)puVar28);
                  }
                  if (((int)uVar10 < -4) || ((long)puVar17 <= (long)(int)uVar10)) {
                    bVar41 = puVar28 < puVar38;
                    *(undefined *)local_a08 =
                         *(undefined *)((long)(undefined8 *)local_a48 + (long)puVar38);
                    if (bVar41) goto LAB_0040e8ab;
                    pcVar36 = (char *)((long)local_a08 + 1);
                    puVar17 = (undefined8 *)local_a48;
                    goto LAB_0040e7f9;
                  }
LAB_0040dfbd:
                  if ((int)uVar10 < 0) {
                    uVar10 = ~uVar10;
                    uStack2548 = (int)uVar10 >> 0x1f;
                    local_a38 = (undefined8 *)((long)local_a08 + 2);
                    *(undefined *)local_a08 = 0x30;
                    *(undefined8 *)(puVar30 + -8) = 0x40f3be;
                    local_a50 = puVar28;
                    local_9f8 = uVar10;
                    func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                    if (local_5b8._1_1_ == '\0') {
                      local_5b8._1_1_ = '.';
                    }
                    *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                    puVar38 = local_a38;
                    if (uVar10 != 0) {
                      puVar28 = local_a38;
                      do {
                        puVar38 = (undefined8 *)((long)puVar28 + 1);
                        *(undefined *)puVar28 = 0x30;
                        puVar28 = puVar38;
                      } while (puVar38 !=
                               (undefined8 *)(CONCAT44(uStack2548,local_9f8) + (long)local_a38));
                    }
                    if (local_a50 < puVar17) {
                      lVar33 = (long)(undefined8 *)local_a48 + (long)puVar17;
                      puVar28 = puVar38;
                      do {
                        puVar35 = (undefined *)(lVar33 + -1);
                        lVar33 = lVar33 + -1;
                        *(undefined *)puVar28 = *puVar35;
                        puVar28 = (undefined8 *)((long)puVar28 + 1);
                      } while (lVar33 != (long)(undefined8 *)local_a48 + (long)local_a50);
                      puVar38 = (undefined8 *)
                                ((long)puVar38 + (long)((long)puVar17 - (long)local_a50));
                    }
                  }
                  else {
                    lVar33 = (long)(undefined8 *)local_a48 + (long)puVar17;
                    puVar38 = (undefined8 *)((long)local_a08 + (long)(int)(uVar10 + 1));
                    puVar21 = local_a08;
                    do {
                      puVar35 = (undefined *)(lVar33 + -1);
                      puVar18 = (undefined8 *)((long)puVar21 + 1);
                      lVar33 = lVar33 + -1;
                      *(undefined *)puVar21 = *puVar35;
                      puVar21 = puVar18;
                    } while (puVar18 != puVar38);
                    puVar17 = (undefined8 *)((long)puVar17 - (long)(int)(uVar10 + 1));
                    local_9f8 = local_9f8 & 0xffffff00 | (uint)(puVar28 < puVar17);
                    if ((puVar28 < puVar17) || (((ulong)local_a30 & 0x10) != 0)) {
                      local_a38 = (undefined8 *)((long)puVar38 + 1);
                      *(undefined8 *)(puVar30 + -8) = 0x40edf1;
                      local_a50 = puVar28;
                      func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                      if (local_5b8._1_1_ == '\0') {
                        local_5b8._1_1_ = '.';
                      }
                      *(char *)puVar38 = local_5b8._1_1_;
                      puVar28 = local_a38;
                      puVar38 = local_a38;
                      if ((char)local_9f8 != '\0') {
                        do {
                          puVar17 = (undefined8 *)((long)puVar17 + -1);
                          puVar38 = (undefined8 *)((long)puVar28 + 1);
                          *(undefined *)puVar28 =
                               *(undefined *)((long)(undefined8 *)local_a48 + (long)puVar17);
                          puVar28 = puVar38;
                        } while (local_a50 < puVar17);
                      }
                    }
                  }
                }
                else {
                  if ((-5 < (int)uVar10) && ((long)(int)uVar10 < (long)puVar17)) {
                    puVar28 = (undefined8 *)0x0;
                    goto LAB_0040dfbd;
                  }
                  bVar41 = puVar38 != (undefined8 *)0x0;
                  *(undefined *)local_a08 =
                       *(undefined *)((long)(undefined8 *)local_a48 + (long)puVar38);
                  puVar28 = (undefined8 *)0x0;
LAB_0040e8ab:
                  local_a50 = (undefined8 *)((ulong)local_a50 & 0xffffffffffffff00 | (ulong)bVar41);
                  local_9f8 = (uint)(undefined8 *)local_a48;
                  uStack2548 = (uint)((ulong)(undefined8 *)local_a48 >> 0x20);
                  local_a48 = (float10)CONCAT28(local_a48._8_2_,puVar38);
                  pcVar36 = (char *)((long)local_a08 + 2);
                  *(undefined8 *)(puVar30 + -8) = 0x40e8ef;
                  local_a38 = puVar28;
                  func_0x00401c00(ZEXT816(0x3ff0000000000000),&local_5b8,"%#.0f");
                  puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                  if (local_5b8._1_1_ == '\0') {
                    local_5b8._1_1_ = '.';
                  }
                  *(char *)((long)local_a08 + 1) = local_5b8._1_1_;
                  if ((char)local_a50 != '\0') {
                    pcVar31 = pcVar36;
                    do {
                      local_a48._0_8_ = (undefined8 *)((long)(undefined8 *)local_a48 + -1);
                      pcVar36 = pcVar31 + 1;
                      *pcVar31 = *(char *)((long)puVar17 + (long)(undefined8 *)local_a48);
                      pcVar31 = pcVar36;
                    } while (local_a38 < (undefined8 *)local_a48);
                  }
LAB_0040e7f9:
                  pcVar31 = pcVar36 + 1;
                  local_9f8 = (uint)pcVar31;
                  uStack2548 = (uint)((ulong)pcVar31 >> 0x20);
                  *pcVar36 = *(char *)(puVar16 + 9) + -2;
                  *(undefined8 *)(puVar30 + -8) = 0x40e829;
                  local_a38 = puVar17;
                  func_0x00401c00(pcVar31,"%+.2d",(ulong)uVar10);
                  cVar25 = pcVar36[1];
                  puVar38 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                  while (local_a48._0_8_ = local_a38, cVar25 != '\0') {
                    puVar38 = (undefined8 *)((long)puVar38 + 1);
                    cVar25 = *(char *)puVar38;
                  }
                }
                local_9f8 = (uint)puVar38;
                uStack2548 = (uint)((ulong)puVar38 >> 0x20);
                *(undefined8 *)(puVar30 + -8) = 0x40e01c;
                func_0x00401db0((undefined8 *)local_a48);
                puVar17 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
              }
            }
          }
          else {
            if ((byte)(*(char *)(puVar16 + 9) + 0xbfU) < 0x1a) {
              puVar17 = (undefined8 *)((long)local_a08 + 3);
              *(undefined *)local_a08 = 0x49;
              *(undefined *)((long)local_a08 + 1) = 0x4e;
              *(undefined *)((long)local_a08 + 2) = 0x46;
              local_a08 = (undefined8 *)0x0;
            }
            else {
              *(undefined *)local_a08 = 0x69;
              *(undefined *)((long)local_a08 + 1) = 0x6e;
              puVar17 = (undefined8 *)((long)local_a08 + 3);
              *(undefined *)((long)local_a08 + 2) = 0x66;
              local_a08 = (undefined8 *)0x0;
            }
          }
        }
LAB_0040ad70:
        puVar38 = local_9c8;
        puVar28 = (undefined8 *)((long)puVar17 - (long)local_9e8);
        if (puVar28 < puVar29) {
          puVar29 = (undefined8 *)((long)puVar29 - (long)puVar28);
          puVar28 = (undefined8 *)((long)puVar17 + (long)puVar29);
          if (((ulong)local_a30 & 2) == 0) {
            if ((local_a08 == (undefined8 *)0x0) || (((ulong)local_a30 & 0x20) == 0)) {
              puVar21 = puVar17;
              puVar18 = puVar28;
              if (local_9e8 < puVar17) {
                do {
                  puVar17 = (undefined8 *)((long)puVar17 + -1);
                  puVar18 = (undefined8 *)((long)puVar18 + -1);
                  *(undefined *)puVar18 = *(undefined *)puVar17;
                  puVar21 = local_9e8;
                } while (puVar17 != local_9e8);
              }
              puVar17 = (undefined8 *)((long)puVar21 + (long)puVar29);
              if (puVar29 != (undefined8 *)0x0) {
                do {
                  puVar29 = (undefined8 *)((long)puVar21 + 1);
                  *(undefined *)puVar21 = 0x20;
                  puVar21 = puVar29;
                } while (puVar29 != puVar17);
              }
            }
            else {
              puVar21 = puVar28;
              if (local_a08 < puVar17) {
                do {
                  puVar17 = (undefined8 *)((long)puVar17 + -1);
                  *(undefined *)(undefined8 *)((long)puVar21 + -1) = *(undefined *)puVar17;
                  puVar21 = (undefined8 *)((long)puVar21 + -1);
                } while (puVar17 != local_a08);
              }
              puVar21 = puVar17;
              if (puVar29 != (undefined8 *)0x0) {
                do {
                  puVar18 = (undefined8 *)((long)puVar21 + 1);
                  *(undefined *)puVar21 = 0x30;
                  puVar21 = puVar18;
                } while (puVar18 != (undefined8 *)((long)puVar17 + (long)puVar29));
              }
            }
          }
          else {
            if (puVar29 != (undefined8 *)0x0) {
              do {
                puVar29 = (undefined8 *)((long)puVar17 + 1);
                *(undefined *)puVar17 = 0x20;
                puVar17 = puVar29;
              } while (puVar28 != puVar29);
            }
          }
          puVar28 = (undefined8 *)((long)puVar28 - (long)local_9e8);
        }
        uVar4 = local_9f8;
        uVar9 = uStack2548;
        if ((undefined8 *)local_a18 <= puVar28) goto switchD_0040ab16_caseD_0;
        puVar29 = local_9d0;
        if ((undefined8 *)((long)puVar39 - (long)local_9c8) <= puVar28) {
          puVar21 = (undefined8 *)0xffffffffffffffff;
          if (!CARRY8((ulong)local_9c8,(ulong)puVar28)) {
            puVar21 = (undefined8 *)((long)local_9c8 + (long)puVar28);
          }
          puVar29 = local_9d0;
          if (puVar39 < puVar21) {
            if (puVar39 == (undefined8 *)0x0) {
              puVar39 = (undefined8 *)0xc;
            }
            else {
              puVar17 = local_9d0;
              if ((long)puVar39 < 0) goto LAB_0040a455;
              puVar39 = (undefined8 *)((long)puVar39 * 2);
            }
            if (puVar39 < puVar21) {
              puVar39 = puVar21;
            }
            puVar17 = local_9d0;
            if (puVar39 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
            bVar41 = local_9d0 == local_9d8;
            if ((local_9d0 == (undefined8 *)0x0) || (bVar41)) {
              *(undefined8 *)(puVar30 + -8) = 0x40d494;
              puVar29 = (undefined8 *)FUN_00408510(puVar39);
            }
            else {
              *(undefined8 *)(puVar30 + -8) = 0x40ae47;
              puVar29 = (undefined8 *)FUN_004085a0(local_9d0,puVar39);
            }
            puVar17 = local_9d0;
            if (puVar29 == (undefined8 *)0x0) goto LAB_0040a455;
            if ((local_9c8 != (undefined8 *)0x0) && (bVar41)) {
              *(undefined8 *)(puVar30 + -8) = 0x40da42;
              puVar29 = (undefined8 *)func_0x00401990(puVar29,local_9d0,local_9c8);
            }
          }
        }
        local_9d0 = puVar29;
        puVar29 = local_9e8;
        puVar38 = (undefined8 *)((long)puVar38 + (long)puVar28);
        *(undefined8 *)(puVar30 + -8) = 0x40ae8d;
        func_0x00401990((long)local_9c8 + (long)local_9d0,local_9e8,puVar28);
        puVar17 = local_9d0;
        if (puVar29 != &local_2f8) {
          *(undefined8 *)(puVar30 + -8) = 0x40aeab;
          func_0x00401db0();
          puVar17 = local_9d0;
        }
        goto LAB_0040a5e7;
      }
      puVar13 = (uint *)(local_930 + lVar33 * 0x20);
      uVar4 = *puVar13;
      local_9f8 = uVar4;
      if (uVar4 != 0x10) {
LAB_0040a677:
        local_9e8 = (undefined8 *)
                    ((ulong)local_9e8 & 0xffffffff00000000 | (ulong)*(uint *)(puVar16 + 2));
        if ((char *)puVar16[3] == (char *)puVar16[4]) {
          local_a30 = (undefined8 *)0x0;
          local_a48 = (float10)((unkuint10)local_a48._4_6_ << 0x20);
        }
        else {
          if (puVar16[5] == -1) {
            local_a30 = (undefined8 *)0x0;
            pcVar36 = (char *)puVar16[3];
            do {
              pcVar31 = pcVar36 + 1;
              uVar11 = 0xffffffffffffffff;
              if (local_a30 < (undefined8 *)0x199999999999999a) {
                uVar11 = (long)local_a30 * 10;
              }
              local_a30 = (undefined8 *)(uVar11 + (long)((int)*pcVar36 + -0x30));
              if (CARRY8(uVar11,(long)((int)*pcVar36 + -0x30))) {
                local_a30 = (undefined8 *)0xffffffffffffffff;
              }
              pcVar36 = pcVar31;
            } while (pcVar31 != (char *)puVar16[4]);
            local_a48 = (float10)CONCAT64(local_a48._4_6_,1);
          }
          else {
            uVar4 = local_9f8;
            if (*(int *)(local_930 + puVar16[5] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
            iVar8 = *(int *)(local_930 + puVar16[5] * 0x20 + 4);
            local_a30 = (undefined8 *)(long)iVar8;
            local_a48 = (float10)CONCAT64(local_a48._4_6_,1);
            if (iVar8 < 0) {
              local_9e8 = (undefined8 *)((ulong)local_9e8 | 2);
              local_a30 = (undefined8 *)-(long)local_a30;
            }
          }
        }
        pcVar36 = (char *)puVar16[6];
        if (pcVar36 == (char *)puVar16[7]) {
LAB_0040b900:
          puVar38 = (undefined8 *)0x6;
          local_a60 = (undefined8 *)((ulong)local_a60._4_4_ << 0x20);
        }
        else {
          if (puVar16[8] == -1) {
            puVar38 = (undefined8 *)0x0;
            while (pcVar36 = pcVar36 + 1, (char *)puVar16[7] != pcVar36) {
              uVar11 = 0xffffffffffffffff;
              if (puVar38 < (undefined8 *)0x199999999999999a) {
                uVar11 = (long)puVar38 * 10;
              }
              puVar38 = (undefined8 *)(uVar11 + (long)((int)*pcVar36 + -0x30));
              if (CARRY8(uVar11,(long)((int)*pcVar36 + -0x30))) {
                puVar38 = (undefined8 *)0xffffffffffffffff;
              }
            }
            local_a60 = (undefined8 *)CONCAT44(local_a60._4_4_,1);
          }
          else {
            uVar4 = local_9f8;
            if (*(int *)(local_930 + puVar16[8] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
            iVar8 = *(int *)(local_930 + puVar16[8] * 0x20 + 4);
            if (iVar8 < 0) goto LAB_0040b900;
            puVar38 = (undefined8 *)(long)iVar8;
            local_a60 = (undefined8 *)CONCAT44(local_a60._4_4_,1);
          }
        }
        local_a08 = (undefined8 *)((ulong)local_a08._4_4_ << 0x20);
        if ((byte)(bVar6 + 0xbf) < 0x27) {
          local_a08 = (undefined8 *)
                      ((ulong)puVar29 & 0xffffffff00000000 |
                      (ulong)((1 << (bVar6 + 0xbf & 0x3f) & 0x7100000071U) != 0));
        }
        *(undefined *)local_a00 = 0x25;
        puVar29 = (undefined8 *)((long)local_a00 + 1);
        if (((ulong)local_9e8 & 1) != 0) {
          puVar29 = (undefined8 *)((long)local_a00 + 2);
          *(undefined *)((long)local_a00 + 1) = 0x27;
        }
        local_a50 = (undefined8 *)((ulong)local_a50 & 0xffffffff00000000 | (ulong)local_9e8 & 2);
        if (((ulong)local_9e8 & 2) != 0) {
          *(undefined *)puVar29 = 0x2d;
          puVar29 = (undefined8 *)((long)puVar29 + 1);
        }
        if (((ulong)local_9e8 & 4) != 0) {
          *(undefined *)puVar29 = 0x2b;
          puVar29 = (undefined8 *)((long)puVar29 + 1);
        }
        if (((ulong)local_9e8 & 8) != 0) {
          *(undefined *)puVar29 = 0x20;
          puVar29 = (undefined8 *)((long)puVar29 + 1);
        }
        if (((ulong)local_9e8 & 0x10) != 0) {
          *(undefined *)puVar29 = 0x23;
          puVar29 = (undefined8 *)((long)puVar29 + 1);
        }
        if ((uint)local_a08 == 0) {
          if (((ulong)local_9e8 & 0x20) != 0) {
            *(undefined *)puVar29 = 0x30;
            puVar29 = (undefined8 *)((long)puVar29 + 1);
          }
          if (puVar16[3] != puVar16[4]) {
            puVar29 = (undefined8 *)((long)puVar29 + (puVar16[4] - puVar16[3]));
            *(undefined8 *)(puVar30 + -8) = 0x40a7ef;
            func_0x00401990();
          }
        }
        if (puVar16[6] != puVar16[7]) {
          puVar29 = (undefined8 *)((long)puVar29 + (puVar16[7] - puVar16[6]));
          *(undefined8 *)(puVar30 + -8) = 0x40a80d;
          func_0x00401990();
        }
        uVar10 = local_9f8 - 7;
        if (uVar10 < 10) {
          uVar11 = 1 << ((byte)uVar10 & 0x3f);
          if ((uVar11 & 0x283) == 0) {
            if ((uVar11 & 0x20) == 0) {
              if ((uVar11 & 0xc) != 0) {
                *(undefined *)puVar29 = 0x6c;
                puVar29 = (undefined8 *)((long)puVar29 + 1);
                goto LAB_0040b167;
              }
            }
            else {
              *(undefined *)puVar29 = 0x4c;
              puVar29 = (undefined8 *)((long)puVar29 + 1);
            }
          }
          else {
LAB_0040b167:
            *(undefined *)puVar29 = 0x6c;
            puVar29 = (undefined8 *)((long)puVar29 + 1);
          }
        }
        cVar25 = *(char *)(puVar16 + 9);
        *(undefined *)((long)puVar29 + 1) = 0x25;
        *(undefined *)((long)puVar29 + 2) = 0x6e;
        *(undefined *)((long)puVar29 + 3) = 0;
        if (cVar25 == 'F') {
          cVar25 = 'f';
        }
        *(char *)puVar29 = cVar25;
        if (((uint)local_a08 == 0) && (puVar16[5] != -1)) {
          uVar4 = local_9f8;
          uVar9 = uStack2548;
          if (*(int *)(local_930 + puVar16[5] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
          local_988[0] = *(uint *)(local_930 + puVar16[5] * 0x20 + 4);
          lVar26 = 1;
          iVar8 = 1;
          lVar33 = puVar16[8];
        }
        else {
          lVar33 = puVar16[8];
          lVar26 = 0;
          iVar8 = 0;
        }
        if (lVar33 != -1) {
          uVar4 = local_9f8;
          uVar9 = uStack2548;
          if (*(int *)(local_930 + lVar33 * 0x20) != 5) goto switchD_0040ab16_caseD_0;
          iVar8 = (int)lVar26 + 1;
          local_988[lVar26] = *(uint *)(local_930 + lVar33 * 0x20 + 4);
        }
        local_a18 = (float10)CONCAT64(local_a18._4_6_,iVar8);
        puVar28 = (undefined8 *)((long)local_9c8 + 2);
        puVar17 = (undefined8 *)0xffffffffffffffff;
        if (local_9c8 <= puVar28) {
          puVar17 = puVar28;
        }
        puVar21 = local_9d0;
        if (puVar39 < puVar17) {
          if (puVar39 == (undefined8 *)0x0) {
            puVar17 = local_9d0;
            if (puVar28 < local_9c8) goto LAB_0040a455;
            puVar39 = (undefined8 *)0xc;
          }
          else {
            puVar17 = local_9d0;
            if (((long)puVar39 < 0) ||
               (puVar39 = (undefined8 *)((long)puVar39 * 2), puVar17 = local_9d0,
               puVar28 < local_9c8)) goto LAB_0040a455;
          }
          if ((puVar39 < puVar28) &&
             (puVar39 = puVar28, puVar17 = local_9d0, puVar28 == (undefined8 *)0xffffffffffffffff))
          goto LAB_0040a455;
          local_a38 = (undefined8 *)
                      ((ulong)local_a38 & 0xffffffffffffff00 | (ulong)(local_9d0 == local_9d8));
          if ((local_9d0 == (undefined8 *)0x0) || (local_9d0 == local_9d8)) {
            *(undefined8 *)(puVar30 + -8) = 0x40b7c8;
            puVar21 = (undefined8 *)FUN_00408510();
          }
          else {
            *(undefined8 *)(puVar30 + -8) = 0x40a97a;
            puVar21 = (undefined8 *)FUN_004085a0(local_9d0,puVar39);
          }
          puVar17 = local_9d0;
          if (puVar21 == (undefined8 *)0x0) goto LAB_0040a455;
          if ((local_9c8 != (undefined8 *)0x0) && ((char)local_a38 != '\0')) {
            *(undefined8 *)(puVar30 + -8) = 0x40a9b1;
            puVar21 = (undefined8 *)func_0x00401990(puVar21,local_9d0,local_9c8);
          }
        }
        *(undefined *)((long)puVar21 + (long)local_9c8) = 0;
        *(undefined8 *)(puVar30 + -8) = 0x40a9cc;
        local_9d0 = puVar21;
        puVar13 = (uint *)func_0x00401cb0();
        local_a58 = (undefined8 *)((ulong)local_a58 & 0xffffffff00000000 | (ulong)*puVar13);
        local_aa8 = (undefined8 *)((ulong)(-(uint)(uVar10 < 4) & 10) + 10);
        if (local_aa8 <= puVar38) {
          local_aa8 = puVar38;
        }
        local_a68 = (undefined8 *)((long)local_aa8 * 2);
        local_a70 = (undefined8 *)((long)local_a68 + 1);
        local_aa0 = (undefined8 *)((ulong)(-(uint)(uVar10 < 4) & 0xb) + 0xb);
        if (local_aa0 <= puVar38) {
          local_aa0 = puVar38;
        }
        local_a78 = (undefined8 *)((long)local_aa0 + 1);
        local_ab0 = (undefined8 *)((ulong)(-(uint)(uVar10 < 4) & 8) + 9);
        if (local_ab0 <= puVar38) {
          local_ab0 = puVar38;
        }
        local_a80 = (undefined8 *)((long)local_ab0 + 2);
        local_ab8 = (undefined8 *)0x273;
        if (local_9f8 == 0xc) {
          local_ab8 = (undefined8 *)0x2693;
        }
        local_a88 = (undefined8 *)((long)local_ab8 + (long)puVar38);
        local_a98 = (undefined8 *)((ulong)(local_9f8 == 0xc) * 2 + 0xd);
        if (local_a98 <= puVar38) {
          local_a98 = puVar38;
        }
        local_a90 = (undefined8 *)((long)local_a98 + 0xc);
        local_ac0 = (undefined8 *)((long)local_a30 + 1);
        local_ac8 = (undefined8 *)((long)puVar38 + 0xc);
        puVar28 = local_9d0;
        local_a38 = puVar29;
        local_9d0 = puVar38;
LAB_0040aad7:
        puVar21 = puVar28;
        puVar17 = (undefined8 *)((long)puVar39 - (long)local_9c8);
        local_9ac = -1;
        puVar29 = (undefined8 *)0x7fffffff;
        if (puVar17 < (undefined8 *)0x80000000) {
          puVar29 = puVar17;
        }
        *(undefined8 *)(puVar30 + -8) = 0x40ab01;
        puVar19 = (undefined4 *)func_0x00401cb0();
        *puVar19 = 0;
        uVar4 = local_9f8;
        uVar9 = uStack2548;
        switch(local_9f8) {
        default:
          goto switchD_0040ab16_caseD_0;
        case 1:
          break;
        case 2:
          break;
        case 3:
          break;
        case 4:
          break;
        case 5:
        case 6:
        case 0xd:
        case 0xe:
          break;
        case 7:
        case 8:
        case 9:
        case 10:
        case 0xf:
        case 0x10:
        case 0x11:
          if ((int)local_a18 == 1) {
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            *(undefined8 *)(puVar30 + -8) = 0x40b996;
            iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00);
          }
          else {
            if ((int)local_a18 == 2) {
              *(int **)(puVar30 + -0x10) = &local_9ac;
LAB_0040b215:
              local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
              *(undefined8 *)(puVar30 + -0x18) = 0x40b23d;
              iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00,(ulong)local_988[0]);
            }
            else {
              local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
              *(undefined8 *)(puVar30 + -8) = 0x40b6dd;
              iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00);
            }
          }
          goto joined_r0x0040b3c8;
        case 0xb:
          auVar49 = ZEXT816(*(ulong *)(local_930 + puVar16[10] * 0x20 + 0x10));
          if ((int)local_a18 == 1) {
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            *(undefined8 *)(puVar30 + -8) = 0x40d69e;
            iVar8 = thunk_FUN_00615028(auVar49,local_ad0,puVar29,local_a00);
          }
          else {
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            if ((int)local_a18 == 2) {
              *(undefined8 *)(puVar30 + -8) = 0x40b569;
              iVar8 = thunk_FUN_00615028(auVar49,local_ad0,puVar29,local_a00);
            }
            else {
              *(undefined8 *)(puVar30 + -8) = 0x40c6c7;
              iVar8 = thunk_FUN_00615028(auVar49,local_ad0,puVar29,local_a00);
            }
          }
          goto joined_r0x0040b3c8;
        case 0xc:
          Var1 = *(unkbyte10 *)(local_930 + puVar16[10] * 0x20 + 0x10);
          if ((int)local_a18 == 1) {
            *(unkbyte10 *)(puVar30 + -0x10) = Var1;
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            *(undefined8 *)(puVar30 + -0x18) = 0x40d658;
            iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00,(ulong)local_988[0],&local_9ac);
          }
          else {
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            if ((int)local_a18 == 2) {
              *(unkbyte10 *)(puVar30 + -0x10) = Var1;
              goto LAB_0040b215;
            }
            *(unkbyte10 *)(puVar30 + -0x10) = Var1;
            *(undefined8 *)(puVar30 + -0x18) = 0x40c6fc;
            iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00);
          }
          goto joined_r0x0040b3c8;
        }
        if ((int)local_a18 == 1) {
          local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
          *(undefined8 *)(puVar30 + -8) = 0x40b49e;
          iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00);
        }
        else {
          if ((int)local_a18 == 2) {
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            *(int **)(puVar30 + -0x10) = &local_9ac;
            *(undefined8 *)(puVar30 + -0x18) = 0x40b3b4;
            iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00);
          }
          else {
            local_ad0 = (byte *)((long)puVar21 + (long)local_9c8);
            *(undefined8 *)(puVar30 + -8) = 0x40b5dd;
            iVar8 = thunk_FUN_00615028(local_ad0,puVar29,local_a00);
          }
        }
joined_r0x0040b3c8:
        if (local_9ac < 0) {
          if (*(char *)((long)local_a38 + 1) == '\0') {
            if (-1 < iVar8) goto LAB_0040b278;
            uVar4 = local_9f8;
            uVar9 = uStack2548;
            switch(*(undefined *)(puVar16 + 9)) {
            case 0x41:
            case 0x61:
              puVar38 = local_a90;
              if (local_a98 <= local_a90) break;
LAB_0040c833:
              if ((uint)local_a08 == 0) {
                puVar18 = (undefined8 *)0xffffffffffffffff;
                puVar28 = (undefined8 *)0x0;
                goto LAB_0040c798;
              }
              goto LAB_0040c7a1;
            default:
              goto switchD_0040ab16_caseD_0;
            case 0x45:
            case 0x47:
            case 0x65:
            case 0x67:
              puVar38 = local_ac8;
              if (local_ac8 < (undefined8 *)0xc) goto LAB_0040c833;
              break;
            case 0x46:
            case 0x66:
              puVar38 = local_a88;
              if (local_a88 < local_ab8) goto LAB_0040c833;
              break;
            case 0x58:
            case 0x78:
              puVar38 = local_a80;
              if (local_a80 < local_ab0) goto LAB_0040c833;
              break;
            case 99:
              if (local_9f8 == 0xe) {
                *(undefined8 *)(puVar30 + -8) = 0x40f094;
                puVar38 = (undefined8 *)func_0x00401ab0();
                break;
              }
              puVar28 = (undefined8 *)0x2;
              if ((uint)local_a08 == 0) {
                puVar38 = (undefined8 *)0x1;
                goto LAB_0040c77d;
              }
              goto LAB_0040e22d;
            case 100:
            case 0x69:
            case 0x75:
              if ((local_a68 < local_aa8) || (puVar38 = local_a70, local_a70 < local_a68))
              goto LAB_0040c833;
              break;
            case 0x6f:
              puVar38 = local_a78;
              if (local_a78 < local_aa0) goto LAB_0040c833;
              break;
            case 0x70:
              puVar38 = (undefined8 *)0x13;
              break;
            case 0x73:
              uVar4 = local_9f8;
              uVar9 = uStack2548;
              if (local_9f8 == 0x10) goto switchD_0040ab16_caseD_0;
              uVar15 = *(undefined8 *)(local_930 + puVar16[10] * 0x20 + 0x10);
              if ((int)local_a60 == 0) {
                *(undefined8 *)(puVar30 + -8) = 0x40c766;
                puVar38 = (undefined8 *)func_0x00401d20();
              }
              else {
                *(undefined8 *)(puVar30 + -8) = 0x40ed31;
                puVar38 = (undefined8 *)func_0x00401a80(uVar15,local_9d0);
              }
            }
            if ((uint)local_a08 == 0) {
LAB_0040c77d:
              puVar18 = local_a30;
              puVar28 = local_ac0;
              if (local_a30 <= puVar38) goto LAB_0040dad5;
            }
            else {
LAB_0040dad5:
              puVar18 = puVar38;
              puVar28 = (undefined8 *)((long)puVar38 + 1);
            }
LAB_0040c798:
            if (puVar28 < puVar18) {
LAB_0040c7a1:
              puVar28 = (undefined8 *)0xffffffffffffffff;
            }
            else {
LAB_0040e22d:
              if (puVar28 <= puVar29) {
                puVar38 = (undefined8 *)(long)local_9ac;
                if (local_9ac < 0) {
                  *(undefined8 *)(puVar30 + -8) = 0x40e24a;
                  piVar14 = (int *)func_0x00401cb0();
                  iVar8 = *piVar14;
                  if ((iVar8 == 0) && (iVar8 = 0x54, (*(byte *)(puVar16 + 9) & 0xef) != 99)) {
                    iVar8 = 0x16;
                  }
                  if ((puVar21 != (undefined8 *)0x0) && (local_9d8 != puVar21)) {
                    *(undefined8 *)(puVar30 + -8) = 0x40e27d;
                    func_0x00401db0(puVar21);
                  }
                  if (local_a28 != (byte **)0x0) {
                    *(undefined8 *)(puVar30 + -8) = 0x40e291;
                    func_0x00401db0(local_a28);
                  }
                  if (local_840 != auStack2088) {
                    *(undefined8 *)(puVar30 + -8) = 0x40e2ad;
                    func_0x00401db0();
                  }
                  if (local_930 != auStack2344) {
                    *(undefined8 *)(puVar30 + -8) = 0x40e2c9;
                    func_0x00401db0();
                  }
                  *(undefined8 *)(puVar30 + -8) = 0x40e2ce;
                  piVar14 = (int *)func_0x00401cb0();
                  *piVar14 = iVar8;
                  return (undefined8 *)0x0;
                }
                goto LAB_0040b281;
              }
            }
            puVar29 = (undefined8 *)((long)puVar28 + (long)local_9c8);
            if (CARRY8((ulong)puVar28,(ulong)local_9c8)) {
              puVar29 = (undefined8 *)0xffffffffffffffff;
            }
            if ((long)puVar39 < 0) {
              puVar29 = (undefined8 *)0xffffffffffffffff;
LAB_0040b2c9:
              puVar28 = puVar21;
              if (puVar29 <= puVar39) goto LAB_0040aad7;
            }
            else {
              puVar17 = (undefined8 *)((long)(undefined8 *)((long)puVar39 * 2) + 0xc);
              if ((undefined8 *)((long)puVar39 * 2) <= puVar17) {
                if (puVar29 < puVar17) {
                  puVar29 = puVar17;
                }
                goto LAB_0040b2c9;
              }
              puVar29 = (undefined8 *)0xffffffffffffffff;
            }
            if (puVar39 == (undefined8 *)0x0) {
              puVar39 = (undefined8 *)0xc;
            }
            else {
              puVar17 = puVar21;
              if ((long)puVar39 < 0) goto LAB_0040a455;
              puVar39 = (undefined8 *)((long)puVar39 * 2);
            }
            if (puVar39 < puVar29) {
              puVar39 = puVar29;
            }
            puVar17 = puVar21;
            if (puVar39 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
            bVar41 = puVar21 == local_9d8;
            if ((puVar21 == (undefined8 *)0x0) || (bVar41)) {
              *(undefined8 *)(puVar30 + -8) = 0x40b608;
              puVar28 = (undefined8 *)FUN_00408510(puVar39);
            }
            else {
              *(undefined8 *)(puVar30 + -8) = 0x40b31d;
              puVar28 = (undefined8 *)FUN_004085a0(puVar21,puVar39);
            }
            puVar17 = puVar21;
            if (puVar28 == (undefined8 *)0x0) goto LAB_0040a455;
            if ((bVar41) && (local_9c8 != (undefined8 *)0x0)) {
              *(undefined8 *)(puVar30 + -8) = 0x40bdca;
              puVar28 = (undefined8 *)func_0x00401990(puVar28,puVar21,local_9c8);
            }
            goto LAB_0040aad7;
          }
          *(undefined *)((long)local_a38 + 1) = 0;
          puVar28 = puVar21;
          goto LAB_0040aad7;
        }
        puVar38 = (undefined8 *)(long)local_9ac;
        if (((undefined8 *)(long)local_9ac < puVar29) &&
           (uVar4 = local_9f8, uVar9 = uStack2548,
           local_ad0[(long)(undefined8 *)(long)local_9ac] != 0)) goto switchD_0040ab16_caseD_0;
        if (local_9ac < iVar8) {
LAB_0040b278:
          puVar38 = (undefined8 *)(long)iVar8;
          local_9ac = iVar8;
        }
LAB_0040b281:
        if (puVar29 <= (undefined8 *)(ulong)((int)puVar38 + 1)) {
          if (puVar29 == (undefined8 *)0x7fffffff) {
            if ((puVar21 != (undefined8 *)0x0) && (local_9d8 != puVar21)) {
              *(undefined8 *)(puVar30 + -8) = 0x40d9ce;
              func_0x00401db0(puVar21);
            }
            if (local_a28 != (byte **)0x0) {
              *(undefined8 *)(puVar30 + -8) = 0x40d9e2;
              func_0x00401db0(local_a28);
            }
            if (local_840 != auStack2088) {
              *(undefined8 *)(puVar30 + -8) = 0x40d9fe;
              func_0x00401db0();
            }
            if (local_930 != auStack2344) {
              *(undefined8 *)(puVar30 + -8) = 0x40da1a;
              func_0x00401db0();
            }
            *(undefined8 *)(puVar30 + -8) = 0x40da1f;
            puVar19 = (undefined4 *)func_0x00401cb0();
            *puVar19 = 0x4b;
            return (undefined8 *)0x0;
          }
          puVar17 = (undefined8 *)0xffffffffffffffff;
          if (-1 < (long)puVar39) {
            puVar17 = (undefined8 *)((long)puVar39 * 2);
          }
          uVar11 = (ulong)((int)puVar38 + 2);
          puVar38 = (undefined8 *)(uVar11 + (long)local_9c8);
          puVar29 = (undefined8 *)0xffffffffffffffff;
          if ((!CARRY8(uVar11,(ulong)local_9c8)) && (puVar29 = puVar38, puVar38 <= puVar17)) {
            puVar29 = puVar17;
          }
          goto LAB_0040b2c9;
        }
        pbVar22 = local_ad0;
        if ((((uint)local_a08 & (uint)local_a48) != 0) && (pbVar22 = local_ad0, puVar38 < local_a30)
           ) {
          puVar29 = (undefined8 *)((long)local_a30 - (long)puVar38);
          if (puVar17 < local_a30) {
            puVar17 = (undefined8 *)((long)puVar39 * 2);
            if ((long)puVar39 < 0) {
              puVar17 = (undefined8 *)0xffffffffffffffff;
            }
            uVar12 = (long)local_9c8 + (long)puVar38;
            uVar11 = 0xffffffffffffffff;
            if (!CARRY8((ulong)local_9c8,(ulong)puVar38)) {
              uVar11 = uVar12;
            }
            puVar38 = (undefined8 *)0xffffffffffffffff;
            if ((!CARRY8(uVar11,(ulong)puVar29)) &&
               (puVar38 = puVar17, puVar17 <= (undefined8 *)(uVar11 + (long)puVar29))) {
              puVar38 = (undefined8 *)(uVar11 + (long)puVar29);
            }
            if (puVar39 < puVar38) {
              if (puVar39 == (undefined8 *)0x0) {
                puVar39 = (undefined8 *)0xc;
              }
              else {
                puVar17 = puVar21;
                if ((long)puVar39 < 0) goto LAB_0040a455;
                puVar39 = (undefined8 *)((long)puVar39 * 2);
              }
              if (puVar39 < puVar38) {
                puVar39 = puVar38;
              }
              puVar17 = puVar21;
              if (puVar39 == (undefined8 *)0xffffffffffffffff) goto LAB_0040a455;
              bVar41 = local_9d8 == puVar21;
              if ((puVar21 == (undefined8 *)0x0) || (bVar41)) {
                *(undefined8 *)(puVar30 + -8) = 0x40ed4c;
                local_9c8 = puVar21;
                puVar21 = (undefined8 *)FUN_00408510(puVar39);
              }
              else {
                *(undefined8 *)(puVar30 + -8) = 0x40d8d6;
                local_9c8 = puVar21;
                puVar21 = (undefined8 *)FUN_004085a0(puVar21);
              }
              puVar17 = local_9c8;
              if (puVar21 == (undefined8 *)0x0) goto LAB_0040a455;
              if ((uVar12 != 0) && (bVar41)) {
                *(undefined8 *)(puVar30 + -8) = 0x40f16c;
                puVar21 = (undefined8 *)func_0x00401990(puVar21,local_9c8,uVar12);
              }
            }
            lVar33 = (long)local_9ac;
            local_9c8 = (undefined8 *)(uVar12 - lVar33);
            pbVar22 = (byte *)((long)puVar21 + (long)local_9c8);
          }
          else {
            lVar33 = (long)local_9ac;
            pbVar22 = local_ad0;
          }
          bVar6 = *pbVar22;
          pbVar20 = pbVar22 + lVar33;
          pbVar24 = pbVar20 + (long)puVar29;
          pbVar32 = pbVar22;
          if (bVar6 == 0x2d) {
            pbVar32 = pbVar22 + 1;
            bVar6 = pbVar22[1];
          }
          local_9ac = (int)lVar33 + (int)puVar29;
          if ((byte)((bVar6 & 0xdf) + 0xbf) < 0x1a) {
            if (local_a50._0_4_ == 0.00000000) {
LAB_0040d950:
              pbVar32 = pbVar20;
              if (pbVar22 < pbVar20) {
                do {
                  pbVar20 = pbVar20 + -1;
                  pbVar24 = pbVar24 + -1;
                  *pbVar24 = *pbVar20;
                  pbVar32 = pbVar22;
                } while (pbVar20 != pbVar22);
              }
              if (puVar29 != (undefined8 *)0x0) {
                pbVar24 = pbVar32;
                do {
                  pbVar20 = pbVar24 + 1;
                  *pbVar24 = 0x20;
                  pbVar24 = pbVar20;
                } while (pbVar20 != pbVar32 + (long)puVar29);
              }
            }
            else {
LAB_0040e50c:
              if (puVar29 != (undefined8 *)0x0) {
                do {
                  pbVar32 = pbVar20 + 1;
                  *pbVar20 = 0x20;
                  pbVar20 = pbVar32;
                } while (pbVar24 != pbVar32);
              }
            }
          }
          else {
            if (local_a50._0_4_ != 0.00000000) goto LAB_0040e50c;
            if ((pbVar32 == (byte *)0x0) || (((ulong)local_9e8 & 0x20) == 0)) goto LAB_0040d950;
            if (pbVar32 < pbVar20) {
              do {
                pbVar20 = pbVar20 + -1;
                pbVar24 = pbVar24 + -1;
                *pbVar24 = *pbVar20;
              } while (pbVar20 != pbVar32);
            }
            if (puVar29 != (undefined8 *)0x0) {
              pbVar32 = pbVar20;
              do {
                pbVar24 = pbVar32 + 1;
                *pbVar32 = 0x30;
                pbVar32 = pbVar24;
              } while (pbVar24 != pbVar20 + (long)puVar29);
            }
          }
        }
        if ((*(char *)(puVar16 + 9) == 'F') && ((long)local_9ac != 0)) {
          pbVar32 = pbVar22 + (long)local_9ac;
          do {
            if ((byte)(*pbVar22 + 0x9f) < 0x1a) {
              *pbVar22 = *pbVar22 - 0x20;
            }
            pbVar22 = pbVar22 + 1;
          } while (pbVar22 != pbVar32);
        }
        puVar38 = (undefined8 *)((long)local_9c8 + (long)local_9ac);
        *(undefined8 *)(puVar30 + -8) = 0x40d9ab;
        local_9d0 = puVar21;
        puVar19 = (undefined4 *)func_0x00401cb0();
        *puVar19 = local_a58._0_4_;
        puVar17 = local_9d0;
        goto LAB_0040a5e7;
      }
      local_9f8 = (uint)puVar16;
      uStack2548 = (uint)((ulong)puVar16 >> 0x20);
      if ((char *)puVar16[3] == (char *)puVar16[4]) {
        pcVar36 = (char *)puVar16[6];
        pcVar31 = (char *)puVar16[7];
        local_a08 = (undefined8 *)0x0;
        local_a30 = (undefined8 *)((ulong)local_a30._4_4_ << 0x20);
        if (pcVar36 != pcVar31) goto LAB_0040be59;
        puVar29 = *(undefined8 **)(puVar13 + 4);
LAB_0040cecc:
        uVar10 = 0;
        *(undefined8 *)(puVar30 + -8) = 0x40ced7;
        local_9f8 = uVar4;
        uStack2548 = uVar9;
        lVar33 = func_0x00401960(puVar29);
        local_9e8 = (undefined8 *)((long)puVar29 + lVar33 * 4);
        local_9f8 = 0;
        uStack2548 = 0;
LAB_0040ceed:
        local_a30._0_4_ = 0;
        local_a18 = (float10)((unkuint10)local_a18._8_2_ << 0x40);
LAB_0040bf56:
        if ((undefined8 *)local_a18 < local_a08) goto LAB_0040bf6a;
      }
      else {
        if (puVar16[5] == -1) {
          local_a08 = (undefined8 *)0x0;
          pcVar36 = (char *)puVar16[3];
          do {
            pcVar31 = pcVar36 + 1;
            uVar11 = 0xffffffffffffffff;
            if (local_a08 < (undefined8 *)0x199999999999999a) {
              uVar11 = (long)local_a08 * 10;
            }
            local_a08 = (undefined8 *)(uVar11 + (long)((int)*pcVar36 + -0x30));
            if (CARRY8(uVar11,(long)((int)*pcVar36 + -0x30))) {
              local_a08 = (undefined8 *)0xffffffffffffffff;
            }
            pcVar36 = pcVar31;
          } while (pcVar31 != (char *)puVar16[4]);
        }
        else {
          if (*(int *)(local_930 + puVar16[5] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
          uVar11 = SEXT48(*(int *)(local_930 + puVar16[5] * 0x20 + 4));
          uVar12 = uVar11 >> 0x3f;
          local_a08 = (undefined8 *)((uVar11 ^ -uVar12) + uVar12);
        }
        pcVar36 = (char *)puVar16[6];
        pcVar31 = (char *)puVar16[7];
        local_a30 = (undefined8 *)CONCAT44(local_a30._4_4_,1);
        if (pcVar36 == pcVar31) {
          puVar29 = *(undefined8 **)(puVar13 + 4);
        }
        else {
LAB_0040be59:
          if (puVar16[8] == -1) {
            if (pcVar31 == pcVar36 + 1) {
              puVar29 = *(undefined8 **)(puVar13 + 4);
              local_a18 = (float10)((unkuint10)local_a18._8_2_ << 0x40);
              local_5b8 = 0;
              local_9e8 = puVar29;
            }
            else {
              uVar11 = 0;
              pcVar36 = pcVar36 + 1;
              do {
                pcVar34 = pcVar36 + 1;
                uVar12 = 0xffffffffffffffff;
                if (uVar11 < 0x199999999999999a) {
                  uVar12 = uVar11 * 10;
                }
                uVar11 = uVar12 + (long)((int)*pcVar36 + -0x30);
                if (CARRY8(uVar12,(long)((int)*pcVar36 + -0x30))) {
                  uVar11 = 0xffffffffffffffff;
                }
                pcVar36 = pcVar34;
              } while (pcVar34 != pcVar31);
LAB_0040be85:
              puVar29 = *(undefined8 **)(puVar13 + 4);
              local_5b8 = 0;
              if ((uVar11 == 0) || (uVar9 = *(uint *)puVar29, uVar9 == 0)) {
                local_a18 = (float10)((unkuint10)local_a18._8_2_ << 0x40);
                local_9e8 = puVar29;
              }
              else {
                lVar33 = 0;
                puVar38 = puVar29;
                local_a38 = puVar29;
                local_9e8 = puVar39;
                while( true ) {
                  *(undefined8 *)(puVar30 + -8) = 0x40bee9;
                  iVar8 = func_0x00401d80(&local_2f8,(ulong)uVar9,&local_5b8);
                  puVar17 = local_9d0;
                  if (iVar8 < 0) goto joined_r0x0040d2ec;
                  uVar12 = SEXT48(iVar8);
                  if (uVar11 < uVar12) break;
                  puVar38 = (undefined8 *)((long)puVar38 + 4);
                  lVar33 = lVar33 + uVar12;
                  uVar11 = uVar11 - uVar12;
                  if ((uVar11 == 0) || (uVar9 = *(uint *)puVar38, uVar9 == 0)) break;
                }
                puVar16 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
                local_a18 = (float10)CONCAT28(local_a18._8_2_,lVar33);
                puVar29 = local_a38;
                puVar39 = local_9e8;
                local_9e8 = puVar38;
              }
            }
            uVar10 = 1;
            bVar41 = (uint)local_a30 == 0;
            local_9f8 = SUB104(local_a18,0);
            uStack2548 = (uint)((unkuint10)local_a18 >> 0x20);
            local_a30._0_4_ = 1;
            if (bVar41) goto LAB_0040ceed;
            goto LAB_0040bf56;
          }
          if (*(int *)(local_930 + puVar16[8] * 0x20) != 5) goto switchD_0040ab16_caseD_0;
          iVar8 = *(int *)(local_930 + puVar16[8] * 0x20 + 4);
          uVar11 = SEXT48(iVar8);
          if (-1 < iVar8) goto LAB_0040be85;
          puVar29 = *(undefined8 **)(puVar13 + 4);
          if ((uint)local_a30 == 0) goto LAB_0040cecc;
        }
        local_5b8 = 0;
        uVar9 = *(uint *)puVar29;
        if (uVar9 == 0) {
          local_a18 = (float10)((unkuint10)local_a18._8_2_ << 0x40);
          local_9e8 = puVar29;
        }
        else {
          lVar33 = 0;
          puVar38 = puVar29;
          local_a30 = puVar29;
          do {
            *(undefined8 *)(puVar30 + -8) = 0x40d2de;
            iVar8 = func_0x00401d80(&local_2f8,(ulong)uVar9,&local_5b8);
            puVar17 = local_9d0;
            if (iVar8 < 0) goto joined_r0x0040d2ec;
            puVar38 = (undefined8 *)((long)puVar38 + 4);
            uVar9 = *(uint *)puVar38;
            lVar33 = lVar33 + (long)iVar8;
          } while (uVar9 != 0);
          local_a18 = (float10)CONCAT28(local_a18._8_2_,lVar33);
          puVar16 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
          puVar29 = local_a30;
          local_9e8 = puVar38;
        }
        local_9f8 = SUB104(local_a18,0);
        uStack2548 = (uint)((unkuint10)local_a18 >> 0x20);
        if (local_a08 <= (undefined8 *)local_a18) {
LAB_0040c0b4:
          puVar38 = (undefined8 *)(CONCAT44(uStack2548,local_9f8) + (long)local_9c8);
          local_9a8[0] = 0;
          puVar17 = (undefined8 *)0xffffffffffffffff;
          if (!CARRY8(CONCAT44(uStack2548,local_9f8),(ulong)local_9c8)) {
            puVar17 = puVar38;
          }
          puVar28 = local_9d0;
          if (puVar39 < puVar17) {
            if (puVar39 == (undefined8 *)0x0) {
              puVar17 = local_9d0;
              if (puVar38 < local_9c8) goto LAB_0040a455;
              puVar39 = (undefined8 *)0xc;
            }
            else {
              puVar17 = local_9d0;
              if (((long)puVar39 < 0) ||
                 (puVar39 = (undefined8 *)((long)puVar39 * 2), puVar17 = local_9d0,
                 puVar38 < local_9c8)) goto LAB_0040a455;
            }
            if ((puVar39 < puVar38) &&
               (puVar39 = puVar38, puVar17 = local_9d0, puVar38 == (undefined8 *)0xffffffffffffffff)
               ) goto LAB_0040a455;
            bVar41 = local_9d0 == local_9d8;
            if ((local_9d0 == (undefined8 *)0x0) || (bVar41)) {
              *(undefined8 *)(puVar30 + -8) = 0x40dbcc;
              puVar28 = (undefined8 *)FUN_00408510();
            }
            else {
              *(undefined8 *)(puVar30 + -8) = 0x40c148;
              puVar28 = (undefined8 *)FUN_004085a0();
            }
            puVar17 = local_9d0;
            if (puVar28 == (undefined8 *)0x0) goto LAB_0040a455;
            if ((local_9c8 != (undefined8 *)0x0) && (bVar41)) {
              *(undefined8 *)(puVar30 + -8) = 0x40e221;
              puVar28 = (undefined8 *)func_0x00401990(puVar28,local_9d0,local_9c8);
            }
          }
          local_9d0 = puVar28;
          puVar38 = local_9c8;
          if (CONCAT44(uStack2548,local_9f8) != 0) {
            uVar10 = *(uint *)puVar29;
            uVar4 = local_9f8;
            uVar9 = uStack2548;
            if (uVar10 == 0) goto switchD_0040ab16_caseD_0;
            lVar33 = CONCAT44(uStack2548,local_9f8);
            puVar38 = local_9c8;
            local_a30 = puVar39;
            local_9c8 = puVar16;
            while( true ) {
              *(undefined8 *)(puVar30 + -8) = 0x40c1c9;
              iVar8 = func_0x00401d80(local_978,(ulong)uVar10,local_9a8);
              uVar4 = local_9f8;
              uVar9 = uStack2548;
              if (iVar8 < 1) goto switchD_0040ab16_caseD_0;
              lVar40 = (long)iVar8;
              puVar29 = (undefined8 *)((long)puVar29 + 4);
              lVar26 = (long)local_9d0 + (long)puVar38;
              puVar38 = (undefined8 *)((long)puVar38 + lVar40);
              *(undefined8 *)(puVar30 + -8) = 0x40c1f1;
              func_0x00401990(lVar26,local_978,lVar40);
              lVar33 = lVar33 - lVar40;
              puVar16 = local_9c8;
              puVar39 = local_a30;
              if (lVar33 == 0) break;
              uVar10 = *(uint *)puVar29;
              uVar4 = local_9f8;
              uVar9 = uStack2548;
              if (uVar10 == 0) goto switchD_0040ab16_caseD_0;
            }
          }
          uVar4 = local_9f8;
          uVar9 = uStack2548;
          puVar28 = local_9d0;
          if (puVar29 != local_9e8) {
switchD_0040ab16_caseD_0:
            uStack2548 = uVar9;
            local_9f8 = uVar4;
            *(undefined8 *)(puVar30 + -8) = 0x40b5ad;
            uVar15 = func_0x00401a50();
            return (undefined8 *)uVar15;
          }
LAB_0040c211:
          local_9d0 = puVar28;
          puVar17 = local_9d0;
          if (((undefined8 *)local_a18 < local_a08) &&
             (puVar17 = local_9d0, (*(byte *)(puVar16 + 2) & 2) != 0)) {
            local_a18._0_8_ = (undefined8 *)((long)local_a08 - (long)(undefined8 *)local_a18);
            puVar28 = (undefined8 *)((long)(undefined8 *)local_a18 + (long)puVar38);
            puVar29 = (undefined8 *)0xffffffffffffffff;
            if (!CARRY8((ulong)(undefined8 *)local_a18,(ulong)puVar38)) {
              puVar29 = puVar28;
            }
            puVar21 = local_9d0;
            if (puVar39 < puVar29) {
              if (puVar39 == (undefined8 *)0x0) {
                puVar17 = local_9d0;
                if (puVar28 < puVar38) goto LAB_0040a455;
                puVar39 = (undefined8 *)0xc;
              }
              else {
                puVar17 = local_9d0;
                if (((long)puVar39 < 0) ||
                   (puVar39 = (undefined8 *)((long)puVar39 * 2), puVar17 = local_9d0,
                   puVar28 < puVar38)) goto LAB_0040a455;
              }
              if ((puVar39 < puVar28) &&
                 (puVar39 = puVar28, puVar17 = local_9d0,
                 puVar28 == (undefined8 *)0xffffffffffffffff)) goto LAB_0040a455;
              local_9f8 = local_9f8 & 0xffffff00 | (uint)(local_9d0 == local_9d8);
              if ((local_9d0 == (undefined8 *)0x0) || (local_9d0 == local_9d8)) {
                *(undefined8 *)(puVar30 + -8) = 0x40e32a;
                local_9c8 = (undefined8 *)local_a18;
                puVar21 = (undefined8 *)FUN_00408510(puVar39);
              }
              else {
                *(undefined8 *)(puVar30 + -8) = 0x40c2c9;
                local_9c8 = (undefined8 *)local_a18;
                puVar21 = (undefined8 *)FUN_004085a0(local_9d0,puVar39);
              }
              puVar17 = local_9d0;
              if (puVar21 == (undefined8 *)0x0) goto LAB_0040a455;
              local_a18._0_8_ = local_9c8;
              if ((puVar38 != (undefined8 *)0x0) &&
                 (local_a18._0_8_ = local_9c8, (char)local_9f8 != '\0')) {
                *(undefined8 *)(puVar30 + -8) = 0x40c305;
                func_0x00401990(puVar21,local_9d0,puVar38);
                local_a18._0_8_ = local_9c8;
              }
            }
            *(undefined8 *)(puVar30 + -8) = 0x40c320;
            FUN_00401bc0((long)puVar21 + (long)puVar38,0x20,(undefined8 *)local_a18);
            puVar17 = puVar21;
            puVar38 = puVar28;
          }
LAB_0040a5e7:
          puVar29 = (undefined8 *)puVar16[1];
          puVar16 = puVar16 + 0xb;
          puVar28 = (undefined8 *)*puVar16;
          local_9e0 = local_9e0 + 1;
          local_9c8 = puVar38;
          if (puVar28 != puVar29) goto LAB_0040a24c;
          goto LAB_0040a324;
        }
        uVar10 = 0;
        local_a30._0_4_ = 1;
LAB_0040bf6a:
        if ((*(byte *)(puVar16 + 2) & 2) == 0) {
          local_a18._0_8_ = (undefined8 *)((long)local_a08 - (long)(undefined8 *)local_a18);
          puVar38 = (undefined8 *)((long)local_9c8 + (long)(undefined8 *)local_a18);
          puVar17 = (undefined8 *)0xffffffffffffffff;
          if (!CARRY8((ulong)local_9c8,(ulong)(undefined8 *)local_a18)) {
            puVar17 = puVar38;
          }
          puVar28 = local_9d0;
          local_a48._0_4_ = (uint)local_a30;
          if (puVar39 < puVar17) {
            if (puVar39 == (undefined8 *)0x0) {
              puVar17 = local_9d0;
              if (puVar38 < local_9c8) goto LAB_0040a455;
              puVar39 = (undefined8 *)0xc;
            }
            else {
              puVar17 = local_9d0;
              if (((long)puVar39 < 0) ||
                 (puVar39 = (undefined8 *)((long)puVar39 * 2), puVar17 = local_9d0,
                 puVar38 < local_9c8)) goto LAB_0040a455;
            }
            if ((puVar39 < puVar38) &&
               (puVar39 = puVar38, puVar17 = local_9d0, puVar38 == (undefined8 *)0xffffffffffffffff)
               ) goto LAB_0040a455;
            local_a48 = (float10)CONCAT64(local_a48._4_6_,(uint)local_a30);
            local_a38 = (undefined8 *)
                        ((ulong)local_a38 & 0xffffffffffffff00 | (ulong)(local_9d0 == local_9d8));
            if ((local_9d0 == (undefined8 *)0x0) || (local_9d0 == local_9d8)) {
              *(undefined8 *)(puVar30 + -8) = 0x40dd5c;
              local_a30 = (undefined8 *)local_a18;
              puVar28 = (undefined8 *)FUN_00408510(puVar39);
            }
            else {
              *(undefined8 *)(puVar30 + -8) = 0x40c019;
              local_a30 = (undefined8 *)local_a18;
              puVar28 = (undefined8 *)FUN_004085a0(local_9d0,puVar39);
            }
            puVar17 = local_9d0;
            if (puVar28 == (undefined8 *)0x0) goto LAB_0040a455;
            local_a18._0_8_ = local_a30;
            if ((local_9c8 != (undefined8 *)0x0) &&
               (local_a18._0_8_ = local_a30, (char)local_a38 != '\0')) {
              local_a38 = (undefined8 *)
                          ((ulong)local_a38 & 0xffffffff00000000 | (ulong)(uint)local_a48);
              *(undefined8 *)(puVar30 + -8) = 0x40c06a;
              puVar28 = (undefined8 *)func_0x00401990(puVar28,local_9d0,local_9c8);
              local_a18._0_8_ = local_a30;
              local_a48._0_4_ = (uint)local_a38;
            }
          }
          local_a30 = (undefined8 *)CONCAT44(local_a30._4_4_,(uint)local_a48);
          *(undefined8 *)(puVar30 + -8) = 0x40c09e;
          local_9d0 = puVar28;
          FUN_00401bc0((long)local_9c8 + (long)puVar28,0x20,(undefined8 *)local_a18);
          local_9c8 = puVar38;
        }
      }
      if (((uint)local_a30 | uVar10) != 0) goto LAB_0040c0b4;
      local_998[0] = 0;
      puVar38 = local_9c8;
      puVar28 = local_9d0;
      if (puVar29 < local_9e8) {
        uVar10 = *(uint *)puVar29;
        uVar4 = local_9f8;
        uVar9 = uStack2548;
        if (uVar10 != 0) {
          local_9f8 = (uint)&local_2f8;
          uStack2548 = (uint)((ulong)&local_2f8 >> 0x20);
          puVar21 = local_9c8;
          puVar17 = local_9d0;
          local_a30 = puVar16;
          do {
            *(undefined8 *)(puVar30 + -8) = 0x40cf5b;
            iVar8 = func_0x00401d80(CONCAT44(uStack2548,local_9f8),(ulong)uVar10,local_998);
            if (iVar8 < 1) {
joined_r0x0040d2ec:
              local_9d0 = puVar17;
              if ((local_9d0 != (undefined8 *)0x0) && (local_9d8 != local_9d0)) {
                *(undefined8 *)(puVar30 + -8) = 0x40d2ff;
                func_0x00401db0(local_9d0);
              }
              if (local_a28 != (byte **)0x0) {
                *(undefined8 *)(puVar30 + -8) = 0x40d313;
                func_0x00401db0(local_a28);
              }
              if (local_840 != auStack2088) {
                *(undefined8 *)(puVar30 + -8) = 0x40d32f;
                func_0x00401db0();
              }
              if (local_930 != auStack2344) {
                *(undefined8 *)(puVar30 + -8) = 0x40d34b;
                func_0x00401db0();
              }
              *(undefined8 *)(puVar30 + -8) = 0x40d350;
              puVar19 = (undefined4 *)func_0x00401cb0();
              *puVar19 = 0x54;
              return (undefined8 *)0x0;
            }
            puVar18 = (undefined8 *)(long)iVar8;
            puVar38 = (undefined8 *)((long)puVar18 + (long)puVar21);
            puVar16 = (undefined8 *)0xffffffffffffffff;
            if (!CARRY8((ulong)puVar18,(ulong)puVar21)) {
              puVar16 = puVar38;
            }
            puVar28 = puVar17;
            if (puVar39 < puVar16) {
              if (puVar39 == (undefined8 *)0x0) {
                if (puVar38 < puVar21) goto LAB_0040a455;
                puVar39 = (undefined8 *)0xc;
              }
              else {
                if (((long)puVar39 < 0) ||
                   (puVar39 = (undefined8 *)((long)puVar39 * 2), puVar38 < puVar21))
                goto LAB_0040a455;
              }
              if ((puVar39 < puVar38) &&
                 (puVar39 = puVar38, puVar38 == (undefined8 *)0xffffffffffffffff))
              goto LAB_0040a455;
              local_9d0 = (undefined8 *)
                          ((ulong)local_9d0 & 0xffffffffffffff00 | (ulong)(puVar17 == local_9d8));
              if ((puVar17 == (undefined8 *)0x0) || (puVar17 == local_9d8)) {
                *(undefined8 *)(puVar30 + -8) = 0x40d050;
                local_9c8 = puVar18;
                puVar28 = (undefined8 *)FUN_00408510(puVar39);
              }
              else {
                *(undefined8 *)(puVar30 + -8) = 0x40cfeb;
                local_9c8 = puVar18;
                puVar28 = (undefined8 *)FUN_004085a0(puVar17,puVar39);
              }
              if (puVar28 == (undefined8 *)0x0) goto LAB_0040a455;
              puVar18 = local_9c8;
              if ((puVar21 != (undefined8 *)0x0) && (puVar18 = local_9c8, (char)local_9d0 != '\0'))
              {
                *(undefined8 *)(puVar30 + -8) = 0x40d07d;
                puVar28 = (undefined8 *)func_0x00401990(puVar28,puVar17,puVar21);
                puVar18 = local_9c8;
              }
            }
            puVar29 = (undefined8 *)((long)puVar29 + 4);
            *(undefined8 *)(puVar30 + -8) = 0x40d024;
            func_0x00401990((long)puVar28 + (long)puVar21,CONCAT44(uStack2548,local_9f8),puVar18);
            puVar16 = local_a30;
            if (local_9e8 <= puVar29) goto LAB_0040c211;
            uVar10 = *(uint *)puVar29;
            puVar21 = puVar38;
            puVar17 = puVar28;
            uVar4 = local_9f8;
            uVar9 = uStack2548;
            if (uVar10 == 0) break;
          } while( true );
        }
        goto switchD_0040ab16_caseD_0;
      }
      goto LAB_0040c211;
    }
    if (puVar39 == (undefined8 *)0x0) {
      puVar39 = (undefined8 *)0xc;
LAB_0040a285:
      if (puVar39 < local_9c8) {
        puVar39 = local_9c8;
      }
      if (puVar39 != (undefined8 *)0xffffffffffffffff) {
        bVar41 = puVar17 == local_9d8;
        if ((puVar17 == (undefined8 *)0x0) || (bVar41)) {
          local_9f8 = local_9f8 & 0xffffff00 | (uint)bVar41;
          *(undefined8 *)(puVar30 + -8) = 0x40a50d;
          local_9e8 = puVar17;
          local_9d0 = (undefined8 *)FUN_00408510(puVar39);
          puVar21 = local_9e8;
          local_9e8._0_1_ = (char)local_9f8;
        }
        else {
          local_9e8 = (undefined8 *)((ulong)local_9e8 & 0xffffffffffffff00 | (ulong)bVar41);
          local_9f8 = (uint)puVar17;
          uStack2548 = (uint)((ulong)puVar17 >> 0x20);
          *(undefined8 *)(puVar30 + -8) = 0x40a2d0;
          local_9d0 = (undefined8 *)FUN_004085a0(puVar17,puVar39);
          puVar21 = (undefined8 *)CONCAT44(uStack2548,local_9f8);
        }
        puVar17 = puVar21;
        if (local_9d0 != (undefined8 *)0x0) {
          puVar17 = local_9d0;
          if ((puVar38 != (undefined8 *)0x0) && (puVar17 = local_9d0, (char)local_9e8 != '\0')) {
            *(undefined8 *)(puVar30 + -8) = 0x40a30e;
            func_0x00401990(local_9d0,puVar21,puVar38);
            puVar17 = local_9d0;
          }
          goto LAB_0040a30e;
        }
      }
    }
    else {
      if (-1 < (long)puVar39) {
        puVar39 = (undefined8 *)((long)puVar39 * 2);
        goto LAB_0040a285;
      }
    }
LAB_0040a455:
    local_9d0 = puVar17;
    if ((local_9d0 != (undefined8 *)0x0) && (local_9d0 != local_9d8)) {
      *(undefined8 *)(puVar30 + -8) = 0x40a475;
      func_0x00401db0(local_9d0);
    }
    puVar27 = puVar30;
    if (local_a28 != (byte **)0x0) {
      *(undefined8 *)(puVar30 + -8) = 0x40a489;
      func_0x00401db0(local_a28);
      puVar27 = puVar30;
    }
  }
LAB_0040a490:
  if (local_840 != auStack2088) {
    *(undefined8 *)(puVar27 + -8) = 0x40a4ac;
    func_0x00401db0();
  }
  if (local_930 != auStack2344) {
    *(undefined8 *)(puVar27 + -8) = 0x40a4c8;
    func_0x00401db0();
  }
  *(undefined8 *)(puVar27 + -8) = 0x40a4cd;
  puVar19 = (undefined4 *)func_0x00401cb0();
  *puVar19 = 0xc;
  return (undefined8 *)0x0;
}



double FUN_0040f4d0(int *piParm1)

{
  int iVar1;
  uint uVar2;
  double dVar3;
  int local_c;
  
  dVar3 = (double)func_0x00401d90(&local_c);
  dVar3 = dVar3 + dVar3;
  iVar1 = local_c + -1;
  if (iVar1 < -0x3fe) {
    uVar2 = local_c + 0x3fd;
    local_c = iVar1;
    dVar3 = (double)func_0x00401a00((ulong)uVar2);
    iVar1 = -0x3fe;
  }
  *piParm1 = iVar1;
  return dVar3;
}



void FUN_0040f510(uint *param_1)

{
  uint uVar1;
  ulong uVar2;
  uint uVar3;
  undefined8 param_5;
  undefined8 param_6;
  float10 in_ST0;
  ushort in_FPUControlWord;
  undefined8 param_7;
  ulong param_8;
  ushort uVar4;
  uint local_1c [3];
  
  local_1c[0] = local_1c[0] & 0xffff0000 | (uint)in_FPUControlWord;
  local_1c[0] = local_1c[0] | 0x300;
  uVar4 = (ushort)param_8;
  func_0x00401cd0(local_1c);
  uVar2 = (ulong)local_1c[0];
  uVar1 = local_1c[0] - 1;
  if ((int)uVar1 < -0x3ffe) {
    uVar3 = local_1c[0] + 0x3ffd;
    local_1c[0] = uVar1;
    func_0x00401d60((ulong)uVar3,param_8 & 0xffffffffffff0000 | (ulong)uVar4,uVar2,param_7,param_5,
                    param_6,SUB108(in_ST0 + in_ST0,0),(short)((unkuint10)(in_ST0 + in_ST0) >> 0x40))
    ;
    uVar1 = 0xffffc002;
  }
  *param_1 = uVar1;
  return;
}



undefined8 FUN_0040f590(uint *puParm1,ulong *puParm2)

{
  uint uVar1;
  undefined4 *puVar2;
  unkbyte10 *pVar3;
  undefined1 *puVar4;
  char *pcVar5;
  ulong uVar6;
  undefined4 *puVar7;
  undefined8 *puVar8;
  long *plVar9;
  char **ppcVar10;
  
  uVar6 = 0;
  puVar2 = (undefined4 *)puParm2[1];
  if (*puParm2 != 0) {
    do {
      switch(*puVar2) {
      default:
        return 0xffffffff;
      case 1:
      case 2:
        uVar1 = *puParm1;
        if (uVar1 < 0x30) {
          puVar7 = (undefined4 *)((ulong)uVar1 + *(long *)(puParm1 + 4));
          *puParm1 = uVar1 + 8;
        }
        else {
          puVar7 = *(undefined4 **)(puParm1 + 2);
          *(undefined4 **)(puParm1 + 2) = puVar7 + 2;
        }
        *(char *)(puVar2 + 4) = (char)*puVar7;
        break;
      case 3:
      case 4:
        uVar1 = *puParm1;
        if (uVar1 < 0x30) {
          puVar7 = (undefined4 *)((ulong)uVar1 + *(long *)(puParm1 + 4));
          *puParm1 = uVar1 + 8;
        }
        else {
          puVar7 = *(undefined4 **)(puParm1 + 2);
          *(undefined4 **)(puParm1 + 2) = puVar7 + 2;
        }
        *(short *)(puVar2 + 4) = (short)*puVar7;
        break;
      case 5:
      case 6:
      case 0xd:
      case 0xe:
        uVar1 = *puParm1;
        if (uVar1 < 0x30) {
          puVar7 = (undefined4 *)((ulong)uVar1 + *(long *)(puParm1 + 4));
          *puParm1 = uVar1 + 8;
        }
        else {
          puVar7 = *(undefined4 **)(puParm1 + 2);
          *(undefined4 **)(puParm1 + 2) = puVar7 + 2;
        }
        puVar2[4] = *puVar7;
        break;
      case 7:
      case 8:
      case 9:
      case 10:
      case 0x11:
      case 0x12:
      case 0x13:
      case 0x14:
      case 0x15:
      case 0x16:
        uVar1 = *puParm1;
        if (uVar1 < 0x30) {
          puVar8 = (undefined8 *)((ulong)uVar1 + *(long *)(puParm1 + 4));
          *puParm1 = uVar1 + 8;
        }
        else {
          puVar8 = *(undefined8 **)(puParm1 + 2);
          *(undefined8 **)(puParm1 + 2) = puVar8 + 1;
        }
        *(undefined8 *)(puVar2 + 4) = *puVar8;
        break;
      case 0xb:
        uVar1 = puParm1[1];
        if (uVar1 < 0xb0) {
          puVar8 = (undefined8 *)((ulong)uVar1 + *(long *)(puParm1 + 4));
          puParm1[1] = uVar1 + 0x10;
        }
        else {
          puVar8 = *(undefined8 **)(puParm1 + 2);
          *(undefined8 **)(puParm1 + 2) = puVar8 + 1;
        }
        *(undefined8 *)(puVar2 + 4) = *puVar8;
        break;
      case 0xc:
        pVar3 = (unkbyte10 *)(*(long *)(puParm1 + 2) + 0xfU & 0xfffffffffffffff0);
        *(long *)(puParm1 + 2) = (long)pVar3 + 0x10;
        *(unkbyte10 *)(puVar2 + 4) = *pVar3;
        break;
      case 0xf:
        uVar1 = *puParm1;
        if (uVar1 < 0x30) {
          ppcVar10 = (char **)((ulong)uVar1 + *(long *)(puParm1 + 4));
          *puParm1 = uVar1 + 8;
        }
        else {
          ppcVar10 = *(char ***)(puParm1 + 2);
          *(char ***)(puParm1 + 2) = ppcVar10 + 1;
        }
        pcVar5 = *ppcVar10;
        if (pcVar5 == (char *)0x0) {
          pcVar5 = "(NULL)";
        }
        *(char **)(puVar2 + 4) = pcVar5;
        break;
      case 0x10:
        uVar1 = *puParm1;
        if (uVar1 < 0x30) {
          plVar9 = (long *)((ulong)uVar1 + *(long *)(puParm1 + 4));
          *puParm1 = uVar1 + 8;
        }
        else {
          plVar9 = *(long **)(puParm1 + 2);
          *(long **)(puParm1 + 2) = plVar9 + 1;
        }
        puVar4 = (undefined1 *)*plVar9;
        if (puVar4 == (undefined1 *)0x0) {
          puVar4 = null_ARRAY_00412690;
        }
        *(undefined1 **)(puVar2 + 4) = puVar4;
      }
      uVar6 = uVar6 + 1;
      puVar2 = puVar2 + 8;
    } while (uVar6 <= *puParm2 && *puParm2 != uVar6);
  }
  return 0;
}



undefined8 FUN_0040f7b0(byte *pbParm1,ulong *puParm2,byte **ppbParm3)

{
  byte **ppbVar1;
  ulong *puVar2;
  byte **ppbVar3;
  byte bVar4;
  int iVar5;
  uint uVar6;
  int iVar7;
  byte *pbVar8;
  byte *pbVar9;
  undefined4 *puVar10;
  byte *pbVar11;
  ulong *puVar12;
  byte **ppbVar13;
  byte **ppbVar14;
  ulong uVar15;
  ulong uVar16;
  byte bVar17;
  byte *pbVar18;
  bool bVar19;
  byte *local_88;
  byte *local_80;
  byte *local_78;
  ulong local_70;
  byte *local_60;
  
  ppbVar1 = ppbParm3 + 2;
  puVar2 = puParm2 + 4;
  *puParm2 = 0;
  *(ulong **)(puParm2 + 1) = puVar2;
  uVar16 = 0;
  *ppbParm3 = (byte *)0x0;
  *(byte ***)(ppbParm3 + 1) = ppbVar1;
  local_78 = (byte *)0x0;
  local_88 = (byte *)0x0;
  local_80 = (byte *)0x7;
  local_70 = 7;
  local_60 = (byte *)0x0;
  puVar12 = puVar2;
LAB_0040f835:
  do {
    pbVar18 = pbParm1;
    if (*pbVar18 == 0) {
      *(byte **)(puVar12 + uVar16 * 0xb) = pbVar18;
      *(byte **)(puParm2 + 2) = local_88;
      *(byte **)(puParm2 + 3) = local_78;
      return 0;
    }
    pbParm1 = pbVar18 + 1;
  } while (*pbVar18 != 0x25);
  ppbVar3 = (byte **)(puVar12 + uVar16 * 0xb);
  *ppbVar3 = pbVar18;
  *(undefined4 *)(ppbVar3 + 2) = 0;
  ppbVar3[3] = (byte *)0x0;
  ppbVar3[4] = (byte *)0x0;
  ppbVar3[5] = (byte *)0xffffffffffffffff;
  ppbVar3[6] = (byte *)0x0;
  ppbVar3[7] = (byte *)0x0;
  ppbVar3[8] = (byte *)0xffffffffffffffff;
  ppbVar3[10] = (byte *)0xffffffffffffffff;
  bVar17 = pbVar18[1];
  pbVar18 = pbParm1;
  if ((byte)(bVar17 - 0x30) < 10) {
    do {
      bVar4 = pbVar18[1];
      pbVar18 = pbVar18 + 1;
    } while ((byte)(bVar4 - 0x30) < 10);
    if (bVar4 == 0x24) {
      uVar16 = 0;
      do {
        uVar15 = 0xffffffffffffffff;
        if (uVar16 < 0x199999999999999a) {
          uVar15 = uVar16 * 10;
        }
        uVar16 = uVar15 + (long)((int)(char)bVar17 + -0x30);
        if (CARRY8(uVar15,(long)((int)(char)bVar17 + -0x30))) {
          bVar17 = pbParm1[1];
          uVar16 = 0xffffffffffffffff;
          if (9 < (byte)(bVar17 - 0x30)) goto switchD_0040f9ea_caseD_26;
        }
        else {
          bVar17 = pbParm1[1];
          if (9 < (byte)(bVar17 - 0x30)) goto code_r0x0040fb79;
        }
        pbParm1 = pbParm1 + 1;
      } while( true );
    }
  }
  pbVar18 = (byte *)0xffffffffffffffff;
  pbVar11 = pbParm1;
  goto LAB_0040f907;
LAB_0040f960:
  if (bVar17 == 0x2a) {
    ppbVar3[3] = pbVar11;
    ppbVar3[4] = pbVar8;
    bVar17 = pbVar11[1];
    if (local_88 == (byte *)0x0) {
      local_88 = (byte *)0x1;
    }
    pbVar11 = pbVar8;
    if ((byte)(bVar17 - 0x30) < 10) {
      do {
        pbVar11 = pbVar11 + 1;
      } while ((byte)(*pbVar11 - 0x30) < 10);
      if (*pbVar11 != 0x24) goto LAB_0040fcd2;
      uVar16 = 0;
      do {
        while( true ) {
          pbVar11 = pbVar8;
          uVar15 = 0xffffffffffffffff;
          if (uVar16 < 0x199999999999999a) {
            uVar15 = uVar16 * 10;
          }
          uVar16 = uVar15 + (long)((int)(char)bVar17 + -0x30);
          if (!CARRY8(uVar15,(long)((int)(char)bVar17 + -0x30))) break;
          bVar17 = pbVar11[1];
          uVar16 = 0xffffffffffffffff;
          pbVar8 = pbVar11 + 1;
          if (9 < (byte)(bVar17 - 0x30)) goto switchD_0040f9ea_caseD_26;
        }
        bVar17 = pbVar11[1];
        pbVar8 = pbVar11 + 1;
      } while ((byte)(bVar17 - 0x30) < 10);
      pbVar9 = (byte *)(uVar16 - 1);
      if ((byte *)0xfffffffffffffffd < pbVar9) goto switchD_0040f9ea_caseD_26;
      ppbVar3[5] = pbVar9;
      pbVar8 = pbVar11 + 2;
    }
    else {
LAB_0040fcd2:
      ppbVar3[5] = local_60;
      bVar19 = local_60 == (byte *)0xffffffffffffffff;
      pbVar9 = local_60;
      local_60 = local_60 + 1;
      if (bVar19) goto switchD_0040f9ea_caseD_26;
    }
    if (pbVar9 < local_80) {
      ppbVar13 = (byte **)ppbParm3[1];
    }
    else {
      if (((long)local_80 < 0) ||
         (((local_80 = (byte *)((long)local_80 * 2), local_80 <= pbVar9 &&
           (local_80 = pbVar9 + 1, local_80 < pbVar9)) || ((byte *)0x7ffffffffffffff < local_80))))
      goto LAB_0040fc71;
      if (ppbVar1 == (byte **)ppbParm3[1]) {
        ppbVar13 = (byte **)FUN_00408510((long)local_80 << 5);
      }
      else {
        ppbVar13 = (byte **)FUN_004085a0();
      }
      if (ppbVar13 == (byte **)0x0) goto LAB_0040fc71;
      if (ppbVar1 == (byte **)ppbParm3[1]) {
        ppbVar13 = (byte **)func_0x00401990(ppbVar13,ppbVar1,(long)*ppbParm3 << 5);
      }
      *(byte ***)(ppbParm3 + 1) = ppbVar13;
    }
    pbVar11 = *ppbParm3;
    ppbVar14 = ppbVar13 + (long)pbVar11 * 4;
    if (pbVar11 <= pbVar9) {
      do {
        pbVar11 = pbVar11 + 1;
        *(undefined4 *)ppbVar14 = 0;
        ppbVar14 = ppbVar14 + 4;
      } while (pbVar11 <= pbVar9);
      *ppbParm3 = pbVar11;
    }
    iVar7 = *(int *)(ppbVar13 + (long)pbVar9 * 4);
    if (iVar7 != 0) {
      pbVar9 = local_88;
      if (iVar7 != 5) goto LAB_0040fdb7;
      goto LAB_0040fbe1;
    }
    *(undefined4 *)(ppbVar13 + (long)pbVar9 * 4) = 5;
    bVar17 = *pbVar8;
    pbVar9 = local_60;
  }
  else {
    pbVar8 = pbVar11;
    pbVar9 = local_60;
    if ((byte)(bVar17 - 0x30) < 10) {
      ppbVar3[3] = pbVar11;
      pbVar8 = pbVar11;
      if ((byte)(*pbVar11 - 0x30) < 10) {
        do {
          pbVar8 = pbVar8 + 1;
        } while ((byte)(*pbVar8 - 0x30) < 10);
        pbVar9 = pbVar8 + -(long)pbVar11;
      }
      else {
        pbVar9 = (byte *)0x0;
        pbVar8 = pbVar11;
      }
      ppbVar3[4] = pbVar8;
      if (pbVar9 <= local_88) {
        pbVar9 = local_88;
      }
LAB_0040fbe1:
      local_88 = pbVar9;
      bVar17 = *pbVar8;
      pbVar9 = local_60;
    }
  }
  pbParm1 = pbVar8;
  pbVar11 = local_78;
  local_60 = pbVar9;
  if (bVar17 != 0x2e) goto LAB_0040f980;
  if (pbVar8[1] != 0x2a) {
    ppbVar3[6] = pbVar8;
    bVar17 = pbVar8[1];
    pbParm1 = pbVar8 + 1;
    while ((byte)(bVar17 - 0x30) < 10) {
      pbParm1 = pbParm1 + 1;
      bVar17 = *pbParm1;
    }
    ppbVar3[7] = pbParm1;
    bVar17 = *pbParm1;
    pbVar11 = pbParm1 + -(long)pbVar8;
    local_60 = pbVar9;
    if (pbParm1 + -(long)pbVar8 <= local_78) {
      pbVar11 = local_78;
      local_60 = pbVar9;
    }
    goto LAB_0040f980;
  }
  pbParm1 = pbVar8 + 2;
  ppbVar3[6] = pbVar8;
  ppbVar3[7] = pbParm1;
  iVar7 = (int)(char)pbVar8[2];
  bVar17 = pbVar8[2] - 0x30;
  if (local_78 < (byte *)0x2) {
    local_78 = (byte *)0x2;
    pbVar11 = pbParm1;
    if (bVar17 < 10) goto LAB_0040ff32;
LAB_0040ffa1:
    pbVar11 = ppbVar3[8];
    local_60 = pbVar9;
    if (ppbVar3[8] == (byte *)0xffffffffffffffff) {
      ppbVar3[8] = pbVar9;
      local_60 = pbVar9 + 1;
      pbVar11 = pbVar9;
      if (pbVar9 == (byte *)0xffffffffffffffff) goto switchD_0040f9ea_caseD_26;
    }
  }
  else {
    pbVar11 = pbParm1;
    if (9 < bVar17) goto LAB_0040ffa1;
LAB_0040ff32:
    do {
      bVar17 = pbVar11[1];
      pbVar11 = pbVar11 + 1;
    } while ((byte)(bVar17 - 0x30) < 10);
    if (bVar17 != 0x24) goto LAB_0040ffa1;
    uVar16 = 0;
    do {
      while( true ) {
        pbVar8 = pbParm1;
        uVar15 = 0xffffffffffffffff;
        if (uVar16 < 0x199999999999999a) {
          uVar15 = uVar16 * 10;
        }
        uVar16 = uVar15 + (long)(iVar7 + -0x30);
        if (!CARRY8(uVar15,(long)(iVar7 + -0x30))) break;
        bVar17 = pbVar8[1];
        iVar7 = (int)(char)bVar17;
        uVar16 = 0xffffffffffffffff;
        pbParm1 = pbVar8 + 1;
        if (9 < (byte)(bVar17 - 0x30)) goto switchD_0040f9ea_caseD_26;
      }
      bVar17 = pbVar8[1];
      iVar7 = (int)(char)bVar17;
      pbParm1 = pbVar8 + 1;
    } while ((byte)(bVar17 - 0x30) < 10);
    pbVar11 = (byte *)(uVar16 - 1);
    if ((byte *)0xfffffffffffffffd < pbVar11) goto switchD_0040f9ea_caseD_26;
    ppbVar3[8] = pbVar11;
    pbParm1 = pbVar8 + 2;
    local_60 = pbVar9;
  }
  if (pbVar11 < local_80) {
    ppbVar13 = (byte **)ppbParm3[1];
  }
  else {
    if ((((long)local_80 < 0) ||
        ((local_80 = (byte *)((long)local_80 * 2), local_80 <= pbVar11 &&
         (local_80 = pbVar11 + 1, local_80 < pbVar11)))) || ((byte *)0x7ffffffffffffff < local_80))
    goto LAB_0040fc71;
    if (ppbVar1 == (byte **)ppbParm3[1]) {
      ppbVar13 = (byte **)FUN_00408510((long)local_80 << 5,(long)local_80 << 5);
    }
    else {
      ppbVar13 = (byte **)FUN_004085a0();
    }
    if (ppbVar13 == (byte **)0x0) goto LAB_0040fc71;
    if (ppbVar1 == (byte **)ppbParm3[1]) {
      ppbVar13 = (byte **)func_0x00401990(ppbVar13,ppbVar1,(long)*ppbParm3 << 5);
    }
    *(byte ***)(ppbParm3 + 1) = ppbVar13;
  }
  pbVar8 = *ppbParm3;
  ppbVar14 = ppbVar13 + (long)pbVar8 * 4;
  if (pbVar8 <= pbVar11) {
    do {
      pbVar8 = pbVar8 + 1;
      *(undefined4 *)ppbVar14 = 0;
      ppbVar14 = ppbVar14 + 4;
    } while (pbVar8 <= pbVar11);
    *ppbParm3 = pbVar8;
  }
  iVar7 = *(int *)(ppbVar13 + (long)pbVar11 * 4);
  if (iVar7 == 0) {
    *(undefined4 *)(ppbVar13 + (long)pbVar11 * 4) = 5;
    bVar17 = *pbParm1;
    pbVar11 = local_78;
LAB_0040f980:
    local_78 = pbVar11;
    uVar16 = 0;
    do {
      pbParm1 = pbParm1 + 1;
      uVar6 = (uint)uVar16;
      if (bVar17 == 0x68) {
        uVar16 = (ulong)(uVar6 | 1 << ((byte)uVar16 & 1));
      }
      else {
        if (bVar17 == 0x4c) {
          uVar16 = (ulong)(uVar6 | 4);
        }
        else {
          if (((bVar17 != 0x6c) && (bVar17 != 0x6a)) &&
             (((bVar17 & 0xdf) != 0x5a && (bVar17 != 0x74)))) goto code_r0x0040f9e7;
          uVar16 = (ulong)(uVar6 + 8);
        }
      }
      bVar17 = *pbParm1;
    } while( true );
  }
  if (iVar7 == 5) {
    bVar17 = *pbParm1;
    pbVar11 = local_78;
    goto LAB_0040f980;
  }
  goto LAB_0040fdb7;
code_r0x0040fb79:
  pbVar18 = (byte *)(uVar16 - 1);
  if (pbVar18 < (byte *)0xfffffffffffffffe) {
    pbVar11 = pbParm1 + 2;
    bVar17 = pbParm1[2];
LAB_0040f907:
    do {
      pbVar8 = pbVar11 + 1;
      if (bVar17 == 0x27) {
        *(uint *)(ppbVar3 + 2) = *(uint *)(ppbVar3 + 2) | 1;
      }
      else {
        if (bVar17 == 0x2d) {
          *(uint *)(ppbVar3 + 2) = *(uint *)(ppbVar3 + 2) | 2;
        }
        else {
          if (bVar17 == 0x2b) {
            *(uint *)(ppbVar3 + 2) = *(uint *)(ppbVar3 + 2) | 4;
          }
          else {
            if (bVar17 == 0x20) {
              *(uint *)(ppbVar3 + 2) = *(uint *)(ppbVar3 + 2) | 8;
            }
            else {
              if (bVar17 == 0x23) {
                *(uint *)(ppbVar3 + 2) = *(uint *)(ppbVar3 + 2) | 0x10;
              }
              else {
                if (bVar17 != 0x30) goto LAB_0040f960;
                *(uint *)(ppbVar3 + 2) = *(uint *)(ppbVar3 + 2) | 0x20;
              }
            }
          }
        }
      }
      bVar17 = *pbVar8;
      pbVar11 = pbVar8;
    } while( true );
  }
switchD_0040f9ea_caseD_26:
  ppbVar13 = (byte **)ppbParm3[1];
LAB_0040fdb7:
  if (ppbVar1 != ppbVar13) {
    func_0x00401db0();
  }
  if (puVar2 != (ulong *)puParm2[1]) {
    func_0x00401db0();
  }
  puVar10 = (undefined4 *)func_0x00401cb0();
  *puVar10 = 0x16;
  return 0xffffffff;
code_r0x0040f9e7:
  switch(bVar17) {
  case 0x25:
    goto switchD_0040f9ea_caseD_25;
  default:
    goto switchD_0040f9ea_caseD_26;
  case 0x41:
  case 0x45:
  case 0x46:
  case 0x47:
  case 0x61:
  case 0x65:
  case 0x66:
  case 0x67:
    iVar7 = 0xc;
    if ((int)uVar6 < 0x10) {
      iVar7 = 0xc - (uint)((uVar16 & 4) == 0);
    }
    break;
  case 0x43:
    iVar7 = 0xe;
    bVar17 = 99;
    break;
  case 0x53:
    iVar7 = 0x10;
    bVar17 = 0x73;
    break;
  case 0x58:
  case 0x6f:
  case 0x75:
  case 0x78:
    iVar7 = 10;
    if ((((int)uVar6 < 0x10) && ((uVar16 & 4) == 0)) &&
       ((iVar7 = 8, (int)uVar6 < 8 && (iVar7 = 2, (uVar16 & 2) == 0)))) {
      iVar7 = (-(uint)((uVar16 & 1) == 0) & 2) + 4;
    }
    break;
  case 99:
    iVar7 = (uint)(7 < (int)uVar6) + 0xd;
    break;
  case 100:
  case 0x69:
    iVar7 = 9;
    if ((((int)uVar6 < 0x10) && ((uVar16 & 4) == 0)) &&
       ((iVar7 = 7, (int)uVar6 < 8 && (iVar7 = 1, (uVar16 & 2) == 0)))) {
      iVar7 = (-(uint)((uVar16 & 1) == 0) & 2) + 3;
    }
    break;
  case 0x6e:
    iVar7 = 0x16;
    if (((((int)uVar6 < 0x10) && ((uVar16 & 4) == 0)) && (iVar7 = 0x15, (int)uVar6 < 8)) &&
       (iVar7 = 0x12, (uVar16 & 2) == 0)) {
      iVar7 = 0x14 - (uVar6 & 1);
    }
    break;
  case 0x70:
    iVar7 = 0x11;
    break;
  case 0x73:
    iVar7 = (uint)(7 < (int)uVar6) + 0xf;
  }
  if (pbVar18 == (byte *)0xffffffffffffffff) {
    ppbVar3[10] = local_60;
    bVar19 = local_60 == (byte *)0xffffffffffffffff;
    pbVar18 = local_60;
    local_60 = local_60 + 1;
    if (bVar19) goto switchD_0040f9ea_caseD_26;
  }
  else {
    ppbVar3[10] = pbVar18;
  }
  if (pbVar18 < local_80) {
    ppbVar13 = (byte **)ppbParm3[1];
  }
  else {
    if (((long)local_80 < 0) ||
       (((local_80 = (byte *)((long)local_80 * 2), local_80 <= pbVar18 &&
         (local_80 = pbVar18 + 1, local_80 < pbVar18)) || ((byte *)0x7ffffffffffffff < local_80))))
    goto LAB_0040fc71;
    if (ppbVar1 == (byte **)ppbParm3[1]) {
      ppbVar13 = (byte **)FUN_00408510((long)local_80 << 5,(long)local_80 << 5);
    }
    else {
      ppbVar13 = (byte **)FUN_004085a0();
    }
    if (ppbVar13 == (byte **)0x0) goto LAB_0040fc71;
    if (ppbVar1 == (byte **)ppbParm3[1]) {
      ppbVar13 = (byte **)func_0x00401990(ppbVar13,ppbVar1,(long)*ppbParm3 << 5);
    }
    *(byte ***)(ppbParm3 + 1) = ppbVar13;
  }
  pbVar11 = *ppbParm3;
  ppbVar14 = ppbVar13 + (long)pbVar11 * 4;
  if (pbVar11 <= pbVar18) {
    do {
      pbVar11 = pbVar11 + 1;
      *(undefined4 *)ppbVar14 = 0;
      ppbVar14 = ppbVar14 + 4;
    } while (pbVar11 <= pbVar18);
    *ppbParm3 = pbVar11;
  }
  iVar5 = *(int *)(ppbVar13 + (long)pbVar18 * 4);
  if (iVar5 == 0) {
    *(int *)(ppbVar13 + (long)pbVar18 * 4) = iVar7;
  }
  else {
    if (iVar7 != iVar5) goto LAB_0040fdb7;
  }
switchD_0040f9ea_caseD_25:
  *(byte *)(ppbVar3 + 9) = bVar17;
  uVar16 = *puParm2;
  ppbVar3[1] = pbParm1;
  uVar16 = uVar16 + 1;
  *puParm2 = uVar16;
  if (uVar16 < local_70) {
    puVar12 = (ulong *)puParm2[1];
    goto LAB_0040f835;
  }
  if ((-1 < (long)local_70) && (local_70 = local_70 * 2, local_70 < 0x2e8ba2e8ba2e8bb)) {
    if (puVar2 == (ulong *)puParm2[1]) {
      puVar12 = (ulong *)FUN_00408510();
    }
    else {
      puVar12 = (ulong *)FUN_004085a0();
    }
    if (puVar12 != (ulong *)0x0) {
      if (puVar2 == (ulong *)puParm2[1]) {
        puVar12 = (ulong *)func_0x00401990(puVar12,puVar2,*puParm2 * 0x58);
      }
      *(ulong **)(puParm2 + 1) = puVar12;
      uVar16 = *puParm2;
      goto LAB_0040f835;
    }
  }
LAB_0040fc71:
  if (ppbVar1 != (byte **)ppbParm3[1]) {
    func_0x00401db0();
  }
  if (puVar2 != (ulong *)puParm2[1]) {
    func_0x00401db0();
  }
  puVar10 = (undefined4 *)func_0x00401cb0();
  *puVar10 = 0xc;
  return 0xffffffff;
}



ulong FUN_00410420(undefined auParm1 [16])

{
  undefined auVar1 [16];
  
  auVar1 = orpd((auParm1 & (undefined  [16])0xffffffffffffffff | ZEXT816(0) << 0x40) &
                (undefined  [16])0xffffffffffffffff,(undefined  [16])0x3ff0000000000000);
  return (ulong)(SUB168(auVar1,0) < 0.00000000);
}



ulong FUN_00410440(void)

{
  float10 fVar1;
  float10 param_7;
  
  fVar1 = (float10)1;
  if (param_7 < (float10)0) {
    fVar1 = -(float10)1;
  }
  return (ulong)(fVar1 < (float10)0);
}



void FUN_00410470(void)

{
  code *pcVar1;
  code **ppcVar2;
  
  ppcVar2 = (code **)&DAT_00614000;
  pcVar1 = DAT_00614000;
  if (DAT_00614000 != (code *)0xffffffffffffffff) {
    do {
      ppcVar2 = ppcVar2 + -1;
      (*pcVar1)();
      pcVar1 = *ppcVar2;
    } while (pcVar1 != (code *)0xffffffffffffffff);
  }
  return;
}



undefined8 _DT_FINI(void)

{
  undefined8 in_RAX;
  
  FUN_00402e30();
  return in_RAX;
}


